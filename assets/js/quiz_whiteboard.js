// JavaScript Document

var
selectedFont,
selectedFontName,
selectedCustomSymbol,
eq_details = new Array(),
selectedEquation,
selectedFontSize = 15,
selectedColor = '#000',
equid = 1,
latexctrl,
row_added_equ = 1,
vremove_row = 0,
vmainkey_dragged = false,
vkeyboard_dragged = false,
vpages = 1,
vcurrentpage = 1,
vtool, vactivetool = '',
vtextobjectCreated = '',
vtextobjectVal = '',
vtextobjectop = '',
vtextobjectleft = '',
vdefaultcanvaswidth,u_ans_items = '', u_ans_item = '',
vdefaultcanvasposwidth;
vgrid = false,
vgridtype = '';
vSelectedEditor = '';
vSoundPlayed = 0;
v_workout_draw = '';

var imageObj = [];
var canvas, ctx, flag = false,
prevX = 0,
currX = 0,
prevY = 0,
currY = 0,
dot_flag = false;

var x = "black",
y = 2;

function methodToFixLayout(e) {
    //$("#footer").html('height: ' + $(window).height() + ' width: ' + $(window).width() + ' qstudy : ' + $("#footer_holder_qstudy").css('width'));
  }

  $(function () {
    /* new js */

    $(document).on('click', '.rsclse_scores', function () {
      $('#showscores').hide();
    })
    /* new js */

    $(document).on('click', '.scroring_note', function () {
      var qid = $(this).attr('data_qid');
      var qdsc = $(this).attr('data_scrore_desc');
      if (qid) {
        $.ajax({
          type: 'POST',
          url: 'WhiteBoard/get_qdesc',
          async: false,
          data: {qid: qid},
          success: function (result) {
            CKEDITOR.instances['scores_descrition_rssss_demos'].setData(result);
            $('.score_desc').show();
          },
          dataType: "html"
        });
      } else {
        CKEDITOR.instances['scores_descrition_rssss_demos'].setData(qdsc);
        $('.score_desc').show();
      }
    });

    $('.rsqdata').click(function () {
      var qid = $(this).attr('rsq_id');
      var mids = $(this).attr('rsmid');
      $.ajax({
        type: 'POST',
        url: 'WhiteBoard/showsubQuestion_item',
        async: false,
        data: {qid: qid, mids: mids},
        success: function (result) {
          $('#showscores').html(result);
          $('#showscores').show();
        },
        dataType: "html"
      });
    });

    $('.rsclse_desc').click(function () {
      $('.score_desc').hide();
    });


    //$( "#accordion" ).accordion();
    $canvas = $("#can_" + vcurrentpage + "");
//    alert(screen.width + ' ' + screen.height);
$(document).tooltip({
  show: {
    effect: "slideDown",
    delay: 250
  }
});

    /*
     dragging
     /*$( "#main_keyboard" ).draggable({ cursor: "move", cursorAt: { top: 0, left: 0 },
     stop: function( event, ui ) {vmainkey_dragged = true}});
     
     */

     $(window).on("resize", methodToFixLayout);
     var vleft = $("#leftside").width();
     var vright = $("#rightside").width();
     var vscore = $("#score").width();
    /*vfooter = $("#footer_holder_qstudy").width();
     if ($(window).width() >= 1280){
     $("#footer_holder_qstudy").css('bottom','1px');
     $("#footer_holder_qstudy").width(vfooter - (vright + vleft + vscore) + 'px');
     $("#top_holder").width(vfooter - (vright + vleft + vscore) + 'px');
     }else{
     $("#footer_holder_qstudy").width(vfooter - (vright + vleft + vscore) + 'px');
     $("#top_holder").width(vfooter - (vright + vleft + vscore) + 'px');
   }*/
   $("#li_date").hover(function () {
    $("li_date").css("background-color", "#fff");
  });

   Date.prototype.monthNames = [
   "January", "February", "March",
   "April", "May", "June",
   "July", "August", "September",
   "October", "November", "December"
   ];

   Date.prototype.getMonthName = function () {
    return this.monthNames[this.getMonth()];
  };
  Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
  };


  $("input:button, button").button();
  var date = new Date();
  var day = date.getDate();
  var month = date.getShortMonthName();
  var yy = date.getFullYear();
  $("#li_date").html(day + '-' + month + '-' + yy);
    //$("#datebox").html(date.toDateString('dd-mmm-yyyy'));
    $("#sliderbox").html('');
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    /*
     $('#spinner').bind('mouseenter', function() {
     $('#spinval').removeClass('spinner_edit').addClass('spinner_edit_hover');
     });
     $('#spinner').bind('mouseleave', function() {
     $('#spinval').removeClass('spinner_edit_hover').addClass('spinner_edit');
   });*/
   $("#spinval").val('0');
   $("#font_name").val('');
    //colorbox draggable
    //$( "#color_box" ).draggable({ cursor: "move", cursorAt: { top: 25, left: 25 } });
    /*$( "#main_keyboard" ).draggable({ cursor: "move", cursorAt: { top: 0, left: 0 },
     stop: function( event, ui ) {vmainkey_dragged = true}});
     $( "#keyboard" ).draggable({ cursor: "move", cursorAt: { top: 25, left: 25 },
     stop: function( event, ui ) {vkeyboard_dragged = true}});*/
    //$("#use").hide();
    //for calculator	
    $('#scalculator').calculator({useThemeRoller: true,
      prompt: 'Click on Calculator Toolbar to close it.', layout: $.calculator.scientificLayout, onClose: function (value, inst) {
            // alert('Closed with value ' + value); 
            $('#scalculator').val('');
            CKEDITOR.instances['answer_editor'].setData(value);
          }});
    $("#" + latexctrl + "").bind('keydown keypress', function () {
      setTimeout(function () {
        var latex = $("#" + latexctrl + "").mathquill('latex');
        $('#latex-source').val(latex);
      });
    }).keydown().focus();

    $('#slider').strackbar({callback: onTick1, defaultValue: 1, sliderHeight: 4, sliderWidth: 58, style: 'style1', animate: true, ticks: false, labels: true, trackerHeight: 20, trackerWidth: 19, maxValue: 102});
    function onTick1(value) {
      $('#linewidth').html(value);
      $("#colorselect").val(selectedColor);
      y = value;
    }
    //resize canvas width and height
    var vcan_width = parseInt($("#canvas_area").css('width'));
    vdefaultcanvaswidth = vcan_width;
    var vcan_height = parseInt($("#canvas_area").css('height'));
    vcan_width = vcan_width - 20;
    //$("#can_" + vcurrentpage + "").width(vcan_width + 'px').height(vcan_height);
    //$("#can_" + vcurrentpage + "").width($(document).width() - 110).height($(document).height() - 126);
    //$("#can_" + vcurrentpage + "").width($(document).width() - 110).height($(document).height() - 130);	
    //$("#can_temp_" + vcurrentpage + "").width($(document).width() - 110).height($(document).height() - 130);	
//	alert($("#can_" + vcurrentpage + "").width() + ' ' + $("#can_" + vcurrentpage + "").height());
    //alert($("#canvas_area").height() + ' ' + $("#canvas_pos").height());
    $("#tool_panel").height($("#main_canvas").height());
    if ($(document).height() >= 864) {
        //$("#tool_panel").css('height', '91.5%');
        $("#mathmall").css('height', '91.5%');
        $("#english").css('height', '91.5%');
      } else {
        //$("#tool_panel").css('height', '89.1%');
        $("#mathmall").css('height', '89.1%');
        $("#english").css('height', '89.1%');
      }
    //activate html editor for english

    /*tinymce.init({
     selector:'textarea',
     plugins: "table,print,preview,spellchecker,image",
     toolbar1: "undo redo bold italic underline strikethrough alignleft aligncenter alignright alignjustify",
     toolbar2: "cut copy paste bullist numlist outdent indent blockquote  removeformat subscript superscript",
     toolbar3: "styleselect formatselect fontselect fontsizeselect spellchecker",
     menubar: "edit format table insert spellchecker",
     resize: false,
     width:parseInt($("#eng_table").css('width') - 2),
     height:parseInt($("#eng_table").css('height'))
     });	
     //$("#english_text").remove();
     initTinymce('');*/
    /*    tinyMCE.execCommand('mceFocus', false, 'english_text');                    
    tinyMCE.execCommand('mceRemoveControl', false, 'english_text');*/
    //making input text as spinner
    //var spinner = $( "#year_grade" ).spinner({ max: 12, min:1 });
    var spinner = $("#score_value").spinner({max: 10, min: 1});
    var spinner1 = $("#time_elapse").spinner({max: 200, min: 1});

    //$( "#year_grade" ).css('height', '30px');
    //$( "#year_grade" ).css('text-align', 'center');
    $("#score_value").css('height', '25px');
    $("#score_value").css('text-align', 'center');
    $("#time_elapse").css('height', '25px');
    $("#time_elapse").css('text-align', 'center');

    //draggable of the equation dialog
    /*$( "#equation_title" ).draggable(
     { cursor: "move", cursorAt: { top: 0, left: 0 }
   });*/

 });

function initTinymce(aselector) {
    /*	tinymce.init({
     selector:'textarea',
     plugins: "table,print,preview,spellchecker,image",
     toolbar1: "undo redo bold italic underline strikethrough alignleft aligncenter alignright alignjustify",
     toolbar2: "cut copy paste bullist numlist outdent indent blockquote  removeformat subscript superscript",
     toolbar3: "styleselect formatselect fontselect fontsizeselect spellchecker",
     menubar: "edit format table insert spellchecker",
     resize: false,
     width:parseInt($("#eng_table").css('width') - 2),
     height:parseInt($("#eng_table").css('height'))
   });	*/
    //editor_id = $("#eng_table textarea").attr('id');*/
    if (aselector) {
        //$("#eng_table").append('<textarea name="english_text" id="english_text" style="top:0px;"></textarea>');
        tinyMCE.execCommand('mceAddControl', false, 'english_text');
      } else {
        //$("#eng_table").remove('english_text');
        tinyMCE.execCommand('mceFocus', false, 'english_text');
        tinyMCE.execCommand('mceRemoveControl', false, 'english_text');
        $('#ul_equation div[id^="drawing_"]').each(function (index) {
          tinyMCE.execCommand('mceFocus', false, 'drawing_' + index);
          tinyMCE.execCommand('mceRemoveControl', false, 'drawing_' + index);
        });
      }
    }

    function resizeInput() {
      $(this).attr('size', $(this).val().length);
    }

    function fn_color_change() {
      alert($("#colorselect").val());
    }

    window.onresize = function () {
      var vcan_width = $("#canvas_area").css('width');
      var vcan_height = $("#canvas_area").css('height');

    //$("#can_" + vcurrentpage + "").width(vcan_width).height(vcan_height);
    //alert($("#can_" + vcurrentpage + "").width());
  }

  function showhorizontalHand(visible, atool) {
    /*if (visible){
     var pos = $("#" + atool + "").offset();
     //alert(pos.top + ' left:' + pos.left + ' tool:' + atool );
     var atop = Math.round(pos.top);
     var aleft = Math.round(pos.left);
     $("#horizontal_hand").css({top: atop - 20, left: aleft + 40 });
     $("#horizontal_hand_part").css({top: atop, left: aleft + 90 });
     $("#horizontal_hand").show();
     $("#horizontal_hand_part").show();	
     }else{
     $("#horizontal_hand").hide();
     $("#horizontal_hand_part").hide();	
   }*/
 }

 function showhandfortext() {
  var pos = $("#tool_text").offset();
  $("#horizontal_hand1").css({top: pos.top - 20, left: pos.left + 40});
  $("#horizontal_hand_part1").css({top: pos.top, left: pos.left + 90});
  $("#horizontal_hand1").show();
  $("#horizontal_hand_part1").show();
}

function showverticalHand(visible, atool) {
  if (visible) {
    var pos = $("#" + atool + "").offset();
    var aheight = $("#" + atool + "").height();
    $("#vertical_hand").css({top: pos.top + aheight + 10, left: pos.left - 10});
    $("#vertical_hand_part").css({top: pos.top + aheight + 60, left: pos.left});
    $("#vertical_hand").show();
    $("#vertical_hand_part").show();
  } else {
    $("#vertical_hand").hide();
    $("#vertical_hand_part").hide();
  }
}


function drawRemoveTextValonCanvas() {
  canvas = document.getElementById('can_' + vcurrentpage);
  context = canvas.getContext('2d');
  context.fillText(vtextobjectVal, vtextobjectleft, vtextobjectop);
  $("#" + vtextobjectCreated + "").remove();
}

function getcanvas() {
  canvas = document.getElementById('can_' + vcurrentpage);
  context = canvas.getContext('2d');
  return context;
}

function hideEquationsWindow() {
  $("#main_keyboard").hide();
  $(".color_box").hide();
  $("#keyboard").hide();
    //$("#customize_symbols").hide();
    $('#scalculator').hide();
    $("#horizontal_hand").hide();
    $("#horizontal_hand_part").hide();
    $("#horizontal_hand1").hide();
    $("#horizontal_hand_part1").hide();
    $(".text_box").hide();
    removeText();
    //drawtext(vtextobjectVal, vtextobjectop, vtextobjectleft);
    //$("#" + vtextobjectCreated + "").remove();
    mclick = 0;
    spx = 0;
    spy = 0;
    cp1x = 0;
    cp1y = 0;
    cp2x = 0;
    cp2y = 0;
    epx = 0;
    epy = 0;

    zigclick = 0;
    zigx = new Array();
    zigy = Array();
    stopzigzag = false;

    //drawRemoveTextValonCanvas();
    //$("#" + vtextobjectCreated + "").remove();
  }

  function hideEquations() {
    var key_board = $("#main_keyboard").is(':visible');
    if (key_board) {
      $("#main_keyboard").show();
    } else {
      $("#main_keyboard").hide();
    }
    $(".color_box").hide();
    $("#keyboard").hide();
    //$("#customize_symbols").hide();
    $('#scalculator').hide();
    $("#horizontal_hand").hide();
    $("#horizontal_hand_part").hide();
    $("#horizontal_hand1").hide();
    $("#horizontal_hand_part1").hide();
    $(".text_box").hide();
    removeText();
    //drawtext(vtextobjectVal, vtextobjectop, vtextobjectleft);
    //$("#" + vtextobjectCreated + "").remove();
    mclick = 0;
    spx = 0;
    spy = 0;
    cp1x = 0;
    cp1y = 0;
    cp2x = 0;
    cp2y = 0;
    epx = 0;
    epy = 0

    zigclick = 0;
    zigx = new Array();
    zigy = Array();
    stopzigzag = false;
    //drawRemoveTextValonCanvas();
    //$("#" + vtextobjectCreated + "").remove();
  }

  function hideWindows() {
    $("#main_keyboard").hide('slow');
    $(".color_box").hide();
    $("#keyboard").hide();
    //$("#customize_symbols").hide();
    $('#scalculator').hide('slow');
    $("#horizontal_hand").hide();
    $("#horizontal_hand_part").hide();
    $(".text_box").hide();
  }

  function fn_normalize_canvas() {
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    hideEquationsWindow();
  }

  function fn_tool_arrow() {
    tool_default = 'empty';
    drawline = false;
    toolzigzag = false;
    toolbeizer = false;
    tooltext = false;
    toolrect = false;
    toolpencil = false;
    drag = false;
    width = 0;
    height = 0;
    startX = 0;
    startY = 0;
    endX = 0;
    endY = 0;
    fn_drag();
    //init();	
    //canvaso.onmousedown = myDown;
    //canvaso.onmouseup = myUp;
    //document.getElementById('canvas_area').className = "default_cur";
    vactivetool = '';
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    $(".spinnerbox").hide();
    //$canvas.getLayer('question').draggable = false;
    hideEquationsWindow();
  }

  function fn_tool_text() {
    vactivetool = 'tool_text';
    tool_default = 'text';
    drawline = false;
    toolzigzag = false;
    toolbeizer = false;
    draggable = false;
    erasing = false;
    toolrect = false;
    tooltext = true;
    toolpencil = false;
    draw_text_modal();
    //init();	
    //document.getElementById('canvas_area').className = "text_font";
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text').addClass('tool_text_active');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    $(".spinnerbox").hide();
    //$canvas.getLayer('question').draggable = false;
    hideEquations();
    showhorizontalHand(true, 'tool_text');
    $(".text_box").show('slow');
    var pos = $("#tool_text").offset();
    //$("#horizontal_hand").css({top: pos.top - 20, left: pos.left + 40 });
    $(".text_box").css({left: 130, top: pos.top - 120});
  }

  function ev_canvas(ev) {
    if (ev.layerX || ev.layerX == 0) { // Firefox
      ev._x = ev.layerX;
      ev._y = ev.layerY;
    } else if (ev.offsetX || ev.offsetX == 0) { // Opera
      ev._x = ev.offsetX;
      ev._y = ev.offsetY;
    }
  }

  function fn_tool_pencil() {
    tool_default = 'pencil';
    toolzigzag = false;
    toolbeizer = false;
    drawline = false;
    draggable = false;
    erasing = false;
    tooltext = false;
    toolrect = false;
    toolpencil = true;
    draw_pencil();
    //init();
    //pencil_tool();
    //document.getElementById('canvas_area').className = "pencil";
    vactivetool = 'tool_pencil';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil').addClass('tool_pencil_active');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    //$canvas.getLayer('question').draggable = false;
    hideEquations();
  }

  function fn_tool_line() {
    tool_default = 'line';
    toolzigzag = false;
    toolbeizer = false;
    drawline = true;
    linepress = false;
    draggable = false;
    erasing = false;
    tooltext = false;
    toolrect = false;
    toolpencil = false;
    draw_line();
    //init();
    //document.getElementById('canvas_area').className = "default_cur";
    vactivetool = 'tool_line';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line').addClass('tool_line_active');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    $(".spinnerbox").hide();
    //$canvas.getLayer('question').draggable = false;
    hideEquations();
  }

  function fn_tool_zigzag() {
    tool_default = 'zigzag';
    drawline = false;
    toolzigzag = true;
    toolbeizer = false;
    tooltext = false;
    zigclick = 0;
    zigx = new Array();
    zigy = new Array();
    stopzigzag = false;
    toolrect = false;
    toolpencil = false;
    draw_zigzag();
    //init();	
    vactivetool = 'tool_zigzag';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag').addClass('tool_zigzag_active');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    $(".spinnerbox").hide();
    //$canvas.getLayer('question').draggable = false;
    //hideEquationsWindow();
    hideEquations();
  }

  function fn_tool_curved() {
    tool_default = 'bezeir';
    toolzigzag = false;
    toolbeizer = true;
    tooltext = false;
    toolrect = false;
    mclick = 0;
    spx = 0;
    spy = 0;
    cp1x = 0;
    cp1y = 0;
    cp2x = 0;
    cp2y = 0;
    epx = 0;
    epy = 0;
    drawline = false;
    draggable = false;
    erasing = false;
    linepress = false;
    toolpencil = false;
    draw_bezeir();
    //init();	
    //document.getElementById('canvas_area').className = "default_cur";
    vactivetool = 'tool_curved';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved').addClass('tool_curved_active');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    $(".spinnerbox").hide();
    //$canvas.getLayer('question').draggable = false;
    hideEquations();
  }

  function fn_tool_eraser() {
    tool_default = 'eraser';
    draggable = false;
    erasing = true;
    toolzigzag = false;
    toolbeizer = false;
    tooltext = false;
    toolrect = false;
    toolpencil = false;
    eraser();
    //init();	
    //document.getElementById('canvas_area').className = "eraser";
    vactivetool = 'tool_eraser';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser').addClass('tool_eraser_active');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    $(".spinnerbox").hide();
    //$canvas.getLayer('question').draggable = false;
    hideEquations();
    //drawGrid('#CCC');
  }

  function fn_tool_color() {
    //document.getElementById('canvas_area').className = "default_cur";
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    //$("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    //$("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    //$("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    //$("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    //$("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    //$("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color').addClass('tool_color_active');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    //$("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    //$("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    //$("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    //$("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    //$("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    $(".spinnerbox").hide();
    //$canvas.getLayer('question').draggable = false;
    hideWindows();
    var tooltxt = $("#tool_text").attr('class');
    if (tooltxt == 'tool_text_active') {
      $(".text_box").show();
      showhandfortext();
    }
    //$("#color_box_more").hide();
    //$("#color_box").hide();
    //$("#color_box").show();
    var pos = $("#tool_color").offset();
    //$("#horizontal_hand").css({top: pos.top - 20, left: pos.left + 40 });
    $(".color_box").css({left: 120, top: pos.top - 155});
    /*
     $("#horizontal_hand_part").css({top: pos.top, left: pos.left + 90 });
     $("#horizontal_hand").show();
     $("#horizontal_hand_part").show();	*/
     showhorizontalHand(true, 'tool_color');
     show_color_buttons('less');
     $(".color_box").css('height', 135);
     $("#more_color").html('more color');
    //$("#color_box").css('height', 122);
    $(".color_box").show('slow');
    var cb = $(".color_box").is(':visible');
    //alert(cb);
    vactivetool = '';
    //show only the less colors at default
    //alert(pos.top + ' left ' + pos.left  + ' css ' + $("#horizontal_hand").css('top'));
  }

  function fn_tool_fullscreen() {
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    hideEquationsWindow();
  }

  function fn_tool_compass() {
    vactivetool = 'tool_compass';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass').addClass('tool_compass_active');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    hideEquationsWindow();
  }

  function fn_tool_protactor() {
    vactivetool = 'tool_protactor';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor').addClass('tool_protactor_active');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    hideEquationsWindow();
  }

  function fn_tool_segma() {
    vtool = '';
    //document.getElementById("canvas_area").className = "default_cur";
    vactivetool = '';
    var segma_on = $("#tool_segma").attr('class');
    //var calcClass = $("#tool_calculator").attr('class');
    if (segma_on == 'tool_segma') {
      $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
      $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
      $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
      $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
      $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
      $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
      $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
      $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
      $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
      $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
      $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
      $("#tool_segma").removeClass('tool_segma').addClass('tool_segma_active');
      $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
      $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
      if (vgrid) {
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }
      $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
      $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
      $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
      $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
      $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
      $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
      $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
      $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
      $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
      $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
      $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
      $("#main_keyboard").show('slow');
      $("#key_current_symbols").removeClass('keys_li_active').addClass('keys_li');
        //$("#color_key").removeClass('key_color_active').addClass('key_color');
        $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
        $("#equation_key").removeClass('keys_li_active').addClass('keys_li');
        $('#scalculator').hide('slow');
        $("#main_keyboard").css({top: '160px', left: '80px'});
        $("#enter_key").hide();
        $("#bkspace_key").hide();
        $("#space_key").hide();
        $("#close_mainkey").css('left', '178px');

        $("#dlg_equation").dialog({
          resizable: true,
          modal: false,
          closeOnEscape: false,
          title: 'Equation',
          open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
          },
          height: 400,
          width: 470,
          buttons: {
            Close: function () {
              $(".ui-dialog-titlebar").show();
              fn_tool_segma_inactive();
              $(this).dialog("close");
            }
          }
        });

      } else {
        $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
        $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
        $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
        $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
        $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
        $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
        $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
        $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
        $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
        $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
        $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
        $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
        $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
        $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
        if (vgrid) {
          $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
        } else {
          $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
        }
        $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
        $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
        $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
        $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
        $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
        $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
        $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
        $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
        $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
        $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
        $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
        $("#main_keyboard").show('slow');
        $("#key_current_symbols").removeClass('keys_li_active').addClass('keys_li');
        //$("#color_key").removeClass('key_color_active').addClass('key_color');
        $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
        $("#equation_key").removeClass('keys_li_active').addClass('keys_li');
        $('#scalculator').hide('slow');
        $("#main_keyboard").css({top: '160px', left: '80px'});
        $("#enter_key").hide();
        $("#bkspace_key").hide();
        $("#space_key").hide();
        $("#close_mainkey").css('left', '178px');
      }
    //fn_show_mathmall();
  }

  function fn_tool_segma_inactive() {
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#main_keyboard").show('slow');
    $("#key_current_symbols").removeClass('keys_li_active').addClass('keys_li');
    //$("#color_key").removeClass('key_color_active').addClass('key_color');
    $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
    $("#equation_key").removeClass('keys_li_active').addClass('keys_li');
    $('#scalculator').hide('slow');
    $("#main_keyboard").css({top: '160px', left: '80px'});
    $("#enter_key").hide();
    $("#bkspace_key").hide();
    $("#space_key").hide();
    $("#close_mainkey").css('left', '178px');
  }

  function fn_tool_rough() {
    vactivetool = '';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough').addClass('tool_rough_active');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    hideEquationsWindow();
  }

  function fn_tool_ruler() {
    vactivetool = 'tool_ruler';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler').addClass('tool_ruler_active');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    hideEquationsWindow();
  }

  function drawGrid(acolor) {
    var cnv = document.getElementById('can_' + vcurrentpage);

    var gridOptions = {
      minorLines: {
        separation: 10,
            color: acolor//'#00FF00'
          },
          majorLines: {
            separation: 70,
            color: '#999'//'#FF0000'
          }
        };

        drawGridLines(cnv, gridOptions.minorLines);
        drawGridLines(cnv, gridOptions.majorLines);

        return;
      }

      function drawGridLines(cnv, lineOptions) {


        var iWidth = cnv.width;
        var iHeight = cnv.height;

        var ctx = cnv.getContext('2d');

        ctx.strokeStyle = lineOptions.color;
        ctx.strokeWidth = 1;

        ctx.beginPath();

        var iCount = null;
        var i = null;
        var x = null;
        var y = null;

        iCount = Math.floor(iWidth / lineOptions.separation);

        for (i = 1; i <= iCount; i++) {
          x = (i * lineOptions.separation);
          ctx.moveTo(x, 0);
          ctx.lineTo(x, iHeight);
          ctx.stroke();
        }


        iCount = Math.floor(iHeight / lineOptions.separation);

        for (i = 1; i <= iCount; i++) {
          y = (i * lineOptions.separation);
          ctx.moveTo(0, y);
          ctx.lineTo(iWidth, y);
          ctx.stroke();
        }

        ctx.closePath();

        return;
      }

      function fn_tool_grid() {
        if (!vgrid) {
        //drawGrid('#CCC');
        //$("#can_" + vcurrentpage + "").addClass('cnv');

        showverticalHand(true, 'tool_grid');
        $("#grid_box").show('slow');
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        //drawGrid('#FFF');
        $("#can_" + vcurrentpage + "").removeClass(vgridtype);
        vgrid = false;
        showverticalHand(false, 'tool_grid');
        $("#grid_box").hide('slow');
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }

    /*vactivetool = '';
     $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
     $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
     $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
     $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
     $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
     $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
     $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
     $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
     $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
     $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
     $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
     $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
     $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
     $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
     $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
     $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
     $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
     $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
     $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
     $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
     $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
     $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
     $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');	
     $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
     $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
     $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
     hideEquationsWindow();*/
   }

   function fn_tool_shape() {
    vactivetool = 'tool_shape';
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape').addClass('tool_shape_active');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    //$canvas.getLayer('question').draggable = false;
    //hideEquationsWindow();
    showverticalHand(true, 'tool_shape');
    $("#shape_box").show('slow');
  }

  function restore_undo_redo() {
    drawline = false;
    toolzigzag = false;
    toolbeizer = false;
    tooltext = false;
    toolrect = false;
    toolpencil = false;
    drag = false;
    width = 0;
    height = 0;
    startX = 0;
    startY = 0;
    endX = 0;
    endY = 0;
    vactivetool = '';
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
    $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = false;
    }
    //$canvas.getLayer('question').draggable = false;
    hideEquationsWindow();
  }

  function fn_tool_undo() {
    if (cStep > 0) {
      restore_undo_redo();
      cStep--;
      last.src = hist[cStep];
        //console.log('undo:' + last.src);
        $canvas.clearCanvas();
        $canvas.removeLayers();
        $canvas.drawImage({
          source: last.src,
          x: 0, y: 0,
          fromCenter: false
        });
        //$canvas.drawLayers();
      }


    //cUndo();
    //$canvas = $("#can_" + vcurrentpage + "");
    /*
     console.log('hist:' + hist.toSource());
     console.log('hist count:' + hist.length);
     var index = hist.length - 1;
     cStep--;
     console.log('index:' + index);
     //$canvas.mouseup();
     clicks = 0;
     if (index < 0) {
     index = 0;
     return;
     }
     last.src = hist[index];
     console.log('undo:' + last.src);
     $canvas.removeLayers();
     $canvas.clearCanvas();
     $canvas.drawImage({
     source: last.src,
     x: 0, y: 0,
     fromCenter: false
     });
     hist.splice(hist.length-1, hist.length-1);*/
   }

   function fn_tool_redo() {
    if (cStep < hist.length - 1) {
      restore_undo_redo();
        //alert(cStep);
        cStep++;
        last.src = hist[cStep];
        //console.log('undo:' + last.src);
        $canvas.clearCanvas();
        //$canvas.removeLayers();
        //layers = $canvas.getLayers();
        //$canvas.removeLayer(layers);
        $canvas.drawImage({
          source: last.src,
          x: 0, y: 0,
          fromCenter: false
        });
        //$canvas.drawLayers();
      }

    //cRedo();
    /*console.log('redo hist:' + hist.toSource());
     console.log('redo hist count:' + hist.length);
     console.log('cStep:' + cStep);
     cStep++;
     var index = cStep;
     console.log('redo index:' + index + ' cStep:' + cStep);
     //$canvas.mouseup();
     clicks = 0;
     if (index < 0) {
     index = 0;
     return;
     }
     last.src = hist[index];
     console.log('redo:' + last.src);
     $canvas.removeLayers();
     $canvas.clearCanvas();
     $canvas.drawImage({
     source: last.src,
     x: 0, y: 0,
     fromCenter: false
     });
     hist.splice(hist.length+1, hist.length+1);*/
   }

   function fn_tool_clearall() {
    vtextobjectVal = '';
    vtextobjectop = '';
    vtextobjectleft = '';
    clearall();
    //$("#can_" + vcurrentpage + "").clearCanvas();
    //init();
    //clearCurrent();

    /*mycanvas = document.getElementById('can_' + vcurrentpage);
     ctx = mycanvas.getContext("2d");
     w = mycanvas.width;
     h = mycanvas.height;
     ctx.clearRect(0, 0, w, h);*/

    /*tempcanvas = document.getElementById('can_temp_' + vcurrentpage);
     tempctx = tempcanvas.getContext("2d");
     var tempw = tempcanvas.width;
     var temph = tempcanvas.height;
     tempctx.clearRect(0, 0, tempw, temph);*/

    //fn_tool_arrow();
    vgrid = false;
    mclick = 0;
    spx = 0;
    spy = 0;
    cp1x = 0;
    cp1y = 0;
    cp2x = 0;
    cp2y = 0;
    epx = 0;
    epy = 0;

    zigclick = 0;
    zigx = new Array();
    zigy = Array();
    stopzigzag = false;
    //init();
    drawline = false;
    toolzigzag = false;
    toolbeizer = false;
    tooltext = false;
    toolrect = false;
    toolpencil = false;
    drag = false;
  }

  function fn_tool_calculator() {
    var segma_on = $("#tool_segma").attr('class');
    var calcClass = $("#tool_calculator").attr('class');
    if (calcClass == 'tool_calculator') {
      $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
      $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
      $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
      $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
      $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
      $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
      $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
      $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
      $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
      $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
      $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
      if (segma_on != 'tool_segma_active') {
        $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
      } else {
        $("#tool_segma").removeClass('tool_segma').addClass('tool_segma_active');
      }

      $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
      $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
      if (vgrid) {
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }
      $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
      $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
      $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
      $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
      $("#tool_calculator").removeClass('tool_calculator').addClass('tool_calculator_active');
      $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
      $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
      $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
      $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
      $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
      $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
      $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
      var layers = $canvas.getLayer('question');
      if (layers) {
        layers.draggable = false;
      }
        //$canvas.getLayer('question').draggable = false;
        //hideEquationsWindow();
        var pos = $("#tool_calculator").offset();
        $('#scalculator').css('left', pos.left);
        $('#scalculator').calculator({value: 0});
        $('#scalculator').show('slow');
      } else {
        $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
        $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
        $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
        $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
        $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
        $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
        $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
        $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
        $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
        $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
        $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
        if (segma_on != 'tool_segma_active') {
          $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
        } else {
          $("#tool_segma").removeClass('tool_segma').addClass('tool_segma_active');
        }
        $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
        $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
        if (vgrid) {
          $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
        } else {
          $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
        }
        $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
        $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
        $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
        $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
        $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
        $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
        $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
        $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
        $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
        $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
        $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
        $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
        var layers = $canvas.getLayer('question');
        if (layers) {
          layers.draggable = false;
        }
        //$canvas.getLayer('question').draggable = false;
        //hideEquationsWindow();
        //var pos = $("#tool_calculator").offset();
        //alert($("#calculator").value);
        $('#scalculator').hide('slow');
      }
    }

    function fn_tool_screen_share() {
      $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
      $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
      $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
      $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
      $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
      $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
      $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
      $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
      $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
      $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
      $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
      $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
      $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
      $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
      if (vgrid) {
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }
      $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
      $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
      $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
      $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
      $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
      $("#tool_screen_share").removeClass('tool_screen_share').addClass('tool_screen_share_active');
      $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
      $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
      $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
      $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
      $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
      hideEquationsWindow();
    }

    function fn_tool_library() {
      $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
      $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
      $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
      $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
      $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
      $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
      $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
      $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
      $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
      $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
      $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
      $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
      $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
      $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
      if (vgrid) {
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }
      $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
      $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
      $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
      $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
      $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
      $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
      $("#tool_library").removeClass('tool_library').addClass('tool_library_active');
      $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
      $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
      $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
      $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
      hideEquationsWindow();
    }

    function fn_tool_hand() {
      $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
      $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
      $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
      $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
      $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
      $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
      $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
      $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
      $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
      $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
      $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
      $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
      $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
      $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
      if (vgrid) {
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }
      $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
      $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
      $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
      $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
      $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
      $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
      $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
      $("#tool_hand").removeClass('tool_hand').addClass('tool_hand_active');
      $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
      $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
      $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
      hideEquationsWindow();
    }

    function fn_tool_inspirational_tool() {
      $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
      $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
      $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
      $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
      $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
      $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
      $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
      $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
      $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
      $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
      $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
      $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
      $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
      $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
      if (vgrid) {
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }
      $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
      $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
      $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
      $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
      $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
      $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
      $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
      $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
      $("#tool_inspirational_tool").removeClass('tool_inspirational_tool').addClass('tool_inspirational_tool_active');
      $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
      $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
      hideEquationsWindow();
    }

    function fn_tool_volume_control() {
      $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
      $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
      $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
      $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
      $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
      $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
      $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
      $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
      $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
      $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
      $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
      $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
      $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
      $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
      if (vgrid) {
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }
      $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
      $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
      $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
      $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
      $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
      $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
      $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
      $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
      $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
      $("#tool_volume_control").removeClass('tool_volume_control').addClass('tool_volume_control_active');
      $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
      hideEquationsWindow();
    }

    function fn_tool_help() {
      $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
      $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
      $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
      $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
      $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
      $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
      $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
      $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
      $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
      $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
      $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
      $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
      $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
      $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
      if (vgrid) {
        $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
      } else {
        $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
      }
      $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
      $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
      $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
      $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
      $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
      $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
      $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
      $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
      $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
      $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
      $("#tool_help").removeClass('tool_help').addClass('tool_help_active');
      hideEquationsWindow();
    }

    function fn_save() {
      canvas = document.getElementById('can_' + vcurrentpage);
      context = canvas.getContext('2d');
      context.fillStyle = '#fff';
      window.open(canvas.toDataURL(), '_blank');
    }

    function fn_print() {
      canvas = document.getElementById('can_' + vcurrentpage);
      window.open(canvas.toDataURL(), '_blank');
      window.print();
    }

    function fn_hidekeyboard() {
      $("#keyboard").hide('slow');
      $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
    }

    function fn_showkeyboard() {
      var equation = $("#equations").is(':visible');
      var cust_symbol = $("#customize_symbols").is(':visible');
      var vtable_math = $("#mathmall").is(':visible');

      if (!vkeyboard_dragged) {
        $("#keyboard").css('left', '33px');
      }

      var keyclass = $("#key_keyboard").attr('class');
      if (keyclass == 'keys_li') {
        $("#keyboard").show('slow');
        $("#key_keyboard").removeClass('keys_li').addClass('keys_li_active');

        if (cust_symbol) {
          if (equation) {
            $("#keyboard").animate({
              top: '240px'
            });
          } else {
            $("#keyboard").animate({
              top: '170px'
            });
          }
        } else {
          if (equation) {
            $("#keyboard").animate({
              top: '110px'
            });
          } else {
            $("#keyboard").animate({
              top: '30px'
            });
          }
        }
      } else {
        $("#keyboard").hide('slow');
        $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
      }
    }

    function fn_hide_mainkeyboard() {
      $("#main_keyboard").hide('slow');
      $("#keyboard").hide();
    //$("#customize_symbols").hide();
    $(".color_box").hide();
    $("#equations").hide('slow');
    $("#equation_key").removeClass('keys_li_active').addClass('keys_li');
    fn_remove_symbols_details();
    //set main keyboard as false
    vmainkey_dragged = false;
    vkeyboard_dragged = false;

    $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
    $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
    $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
    $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
    $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
    $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
    $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
    $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
    $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
    $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
    if (vgrid) {
      $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
    } else {
      $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    }
    $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
    $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
    $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
    $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
    $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
    $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
    $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
    $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
    $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
    $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
    $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
  }

  function fn_keyboard_click(aname, atitle, avalue) {
    var vvalue;
    switch (aname) {
      case 'back_slash':
      if (avalue == 'bs') {
        vvalue = '\\';
      } else {
        vvalue = avalue;
      }
      break;

      case 'single_quote':
      if (avalue == 'sq') {
        vvalue = "'";
      } else {
        vvalue = avalue;
      }
      break;

      default:
      vvalue = avalue;
      break;
    }
    //$("#mathmall").show();
    fn_activate_mathmall();
    switch (vvalue) {
      case 'enter':
      fn_enter();
      $("#keyboard").hide('slow');
      $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
      break;

      case 'ctrl':
      $("#keyboard").hide('slow');
      $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
      break;

      case 'alt':
      $("#keyboard").hide('slow');
      $("#key_keyboard").removeClass('keys_li_active').addClass('keys_li');
      break;

      default:
      fn_insert_math(vvalue);
      var vkey_enter = $("#enter_key").is(':visible');
      if (!vkey_enter) {
        $("#enter_key").show();
        $("#bkspace_key").show();
        $("#space_key").show();
      }

      $("#close_mainkey").css('left', '362px');
            //$("#keyboard").hide('slow');
            //$("#key_keyboard").removeClass('key_k_active').addClass('key_k');
            break;
          }
    //alert(vvalue);
    //&#39;single quote = sq
    //&#92; back slash = bs
  }

  function fn_enter() {
    var table_math = $("#mathmall").is(':visible');
    if (table_math) {
        /*	    var latex = $('#latex-source').val();
        $('#latex-source').val(latex + "\r\n");*/

        var rowCount = $('#ul_equation li').length;
        equid++;
        var vsl = '<div class="table_math_sl" id="sl_' + equid + '">' + equid + '</div>';
        var vdelete_row = '<div class="table_math_td_close" title="Delete" id="delete_row_' + equid +
        '" onClick="fn_remove_mathrow(' + equid + ')">Close</div>';
        var vimg = '<img id="img_' + equid + '" />';
        var vmathquill = '<div id="drawing_' + equid + '" class="mathquill-editable" style="position:absolute;margin-top:-31px;margin-left:-40px;margin-bottom:2px;width:398px;height:30px; background:#fff; border:0px solid #D0D0D0;float:left;text-align:left;vertical-align:middle;" onmousedown="javascript:fn_currentdiv(' + equid + ')"></div>';

        var vtd = '<li class="table_math_li" style="display:block" id="trequ_' + equid + '">' + vsl + vdelete_row + vmathquill + vimg + '</li>';
        $("#ul_equation").append(vtd);
        $("#trequ_" + equid).find('.mathquill-editable').mathquill('editable');


        //$('#ul_equation li').find('.mathquill-editable').css('font-size', selectedFontSize);
        //$('#ul_equation li').find('.mathquill-editable').css('font-family', selectedFont);
        //$('#ul_equation li').find('.mathquill-editable').css('color', selectedColor);

        $("#drawing_" + equid + "").focus();
        var equ_table_height = $("#tb_equation").css('height');

        //update serial number
        //update_equ_sl();
        $("#math_equations").scrollTop($("#math_equations").height());
        //on scroll update ul width
        var isscroll_math = $('#math_equations').hasScrollBar();
        if (isscroll_math) {
          $('#ul_equation li').css('width', '352px');
          $('#ul_equation div[id^="drawing_"]').each(function (index) {
            $(this).css({
              'margin-left': '-40px',
              'width': '381px'
            });
          });
          $('#ul_equation div[id^="delete_row_"]').each(function (index) {
            $(this).css({
              'left': '390px'
            });
          });
        } else {
          $('#ul_equation li').css('width', '369px');
          $('#ul_equation div[id^="drawing_"]').each(function (index) {
            $(this).css({
              'margin-left': '-40px',
              'width': '398px'
            });
          });
          $('#ul_equation div[id^="delete_row_"]').each(function (index) {
            $(this).css({
              'left': '407px'
            });
          });
        }

        /*var vmain_keyboard_top = $("#main_keyboard").css('top');
         //var colorbox = $("#color_key_select").css('top');
         if (!vmainkey_dragged){
         if (equid >= 3){
         vmain_keyboard_top = parseInt(vmain_keyboard_top) + rowCount + 30;
         $("#main_keyboard").animate({
         top: vmain_keyboard_top + 'px'
         });
         }
       }*/
     }
   }

   function fn_remove_mathrow(arow) {
    vremove_row = arow;
    $("#trequ_" + arow).remove();
    //on scroll update ul width
    var isscroll_math = $('#math_equations').hasScrollBar();
    if (isscroll_math) {
      $('#ul_equation li').css('width', '352px');
      $('#ul_equation div[id^="drawing_"]').each(function (index) {
        $(this).css({
          'margin-left': '-40px',
          'width': '381px'
        });
      });
      $('#ul_equation div[id^="delete_row_"]').each(function (index) {
        $(this).css({
          'left': '390px'
        });
      });
    } else {
      $('#ul_equation li').css('width', '369px');
      $('#ul_equation div[id^="drawing_"]').each(function (index) {
        $(this).css({
          'margin-left': '-40px',
          'width': '398px'
        });
      });
      $('#ul_equation div[id^="delete_row_"]').each(function (index) {
        $(this).css({
          'left': '407px'
        });
      });
    }

    //$("#trequ_" + arow).html('');
    //update serial number
    //update_equ_sl();
    $("#math_equations").scrollTop($("#math_equations").height());
    var equ_table_height = $("#tb_equation").css('height');
    var vmain_keyboard_top = $("#main_keyboard").css('top');
    var rowCount = $('#ul_equation li').length;
    /*if (!vmainkey_dragged){
     if (equid >= 3){
     vmain_keyboard_top = parseInt(vmain_keyboard_top) + rowCount - 30;
     $("#main_keyboard").animate({
     top: vmain_keyboard_top + 'px'
     });
     }
   }*/
   equid = arow - 1;
   $("#drawing_" + equid + "").focus();
   row_added_equ = row_added_equ - 1;
 }

 function update_equ_sl() {
  $('#ul_equation div[id^="sl_"]').each(function (index) {
    this.id = "sl_" + (index + 1);
    $(this).html(index + 1);
  });
}

function fn_insert_math(alatex) {
    /*var latex = $('#latex-source').val();
     $('#latex-source').val(latex + alatex);
     var maxlen = $('#latex-source').length;
     */
    //alert($('#latex-source').val() + ' len:' + maxlen);
    //if (maxlen <= 40){
      $("#drawing_" + equid + "").mathquill('write', alatex);
    //alert($("#drawing_" + equid + "").html());
    $("#drawing_" + equid + "").focus();
    /*}else{
     alert('Limit of 40 character exceed.');
   }*/
 }

 function fn_currentdiv(aid) {
  equid = aid;
  latexctrl = 'drawing_' + aid;
    /*
     $('#ul_equation div[id^="drawing_"]').each(function(index) {
     $(this).css('background','#FFFF');
     var dr_id = this.id;
     if (dr_id.contains(aid)){
     $(this).css('background','#FFFFDD');
     }else{
     $(this).css('background','#FFFF');
     }
   });*/

    /*var rowCount = $('#ul_equation li').length;
     for (var i=1;i < rowCount;i++){
     if (id != i){
     $("#drawing_" + i).css('background','#FFFF');
     }else{
     $("#drawing_" + i).css('background','#FFFFDD');
     }
   }*/
 }

 function fn_caps_lock_click(aname, atitle, avalue) {
  var aclass = $('#caps_lock').attr('class');
  if (aclass == 'key_caps_lock') {
    $("#caps_lock").removeClass('key_caps_lock').addClass('key_caps_lock_active');
    fn_caps_shift('caps', 'Caps Lock On');
  } else {
    $("#caps_lock").removeClass('key_caps_lock_active').addClass('key_caps_lock');
    fn_caps_shift('', 'Caps Lock Off');
  }
}

function fn_shift_click(aname, atitle, avalue) {
  var aclass = $('#shift').attr('class');
  if (aclass == 'key_shift') {
    $("#shift").removeClass('key_shift').addClass('key_shift_active');
    $("#shift_n").removeClass('key_shift_n').addClass('key_shift_n_active');
    fn_caps_shift('shift', 'Shift On');
  } else {
    $("#shift").removeClass('key_shift_active').addClass('key_shift');
    $("#shift_n").removeClass('key_shift_n_active').addClass('key_shift_n');
    fn_caps_shift('', 'Shift Off');
  }
}

function fn_caps_shift(acaps_shift, atitle) {
  $.ajax({
    type: 'POST',
    url: '/welcome/setCapsShift',
    data: {caps_shift: acaps_shift},
    beforeSend: function () {
            //showBusy(atitle, 'Please wait...');
          },
          success: function (result) {
            if (result.result == 't') {
              $('#keyboard').html(result.response);
              $("#keyboard").show();
            } else {
              errormsg_dlg(result.error, atitle);
            }
          },

          error: function (xhr, textStatus, error) {
            errormsg_dlg(getAjaxErrorMsg(xhr.responseText), atitle);
          },
          dataType: "json"
        });
}

function fn_SetSpinnerVal(ainc_dec) {
  var vSpinnerVal = $("#spinval").html();
  if (ainc_dec == 1) {
    vSpinnerVal++;
  } else {
    vSpinnerVal--;
  }
  if (vSpinnerVal < 1) {
    vSpinnerVal = 1;
  }
  if (vSpinnerVal > 999) {
    vSpinnerVal = 999;
  }
  $("#spinval").html(vSpinnerVal);
  selectedFontSize = vSpinnerVal;
    /*$('#ul_equation li').find('.mathquill-editable').css('font-size', selectedFontSize);
     $('#ul_equation li').find('.mathquill-editable').css('font-family', selectedFont);
     $('#ul_equation li').find('.mathquill-editable').css('color', selectedColor);*/
   }

   function fn_SetSpinnerVertVal(ainc_dec) {
    var vSpinnerVal = $("#spinval_cs").html();
    if (ainc_dec == 1) {
      vSpinnerVal++;
    } else {
      vSpinnerVal--;
    }
    if (vSpinnerVal < 1) {
      vSpinnerVal = 1;
    }
    if (vSpinnerVal > 5) {
      vSpinnerVal = 5;
    }
    $("#spinval_cs").html(vSpinnerVal);
    switch (vSpinnerVal) {
      case 1:
      for (var i = 1; i < 21; i++) {
        $("#cs_" + i).show();
      }
      for (var i = 21; i < 101; i++) {
        $("#cs_" + i).hide();
      }
      break;

      case 2:
      for (var i = 21; i < 41; i++) {
        $("#cs_" + i).show();
      }
      for (var i = 1; i < 21; i++) {
        $("#cs_" + i).hide();
      }
      for (var i = 41; i < 101; i++) {
        $("#cs_" + i).hide();
      }
      break;

      case 3:
      for (var i = 41; i < 61; i++) {
        $("#cs_" + i).show();
      }
      for (var i = 1; i < 41; i++) {
        $("#cs_" + i).hide();
      }
      for (var i = 61; i < 101; i++) {
        $("#cs_" + i).hide();
      }
      break;

      case 4:
      for (var i = 61; i < 81; i++) {
        $("#cs_" + i).show();
      }
      for (var i = 1; i < 61; i++) {
        $("#cs_" + i).hide();
      }
      for (var i = 81; i < 101; i++) {
        $("#cs_" + i).hide();
      }
      break;

      case 5:
      for (var i = 81; i < 101; i++) {
        $("#cs_" + i).show();
      }
      for (var i = 1; i < 81; i++) {
        $("#cs_" + i).hide();
      }
      break;
    }
  }


  function fn_getFontNames() {
    $.ajax({
      type: 'POST',
      url: '/welcome/getFontNames',
      data: {afont: $("#font_name").val()},
      beforeSend: function () {

      },
      success: function (result) {
        if (result.result == 't') {
          $('#list_fonts').html(result.response);
        } else {
          errormsg_dlg(result.error, 'Font Name');
        }
      },

      error: function (xhr, textStatus, error) {
        errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Font Name');
      },
      dataType: "json"
    });
  }

  function fn_fontnameChange() {
    var selLst = $("#lstfontname option:selected").text();
    $("#font_name").html(selLst);
    selectedFont = $("#lstfontname").val();
    selectedFontName = selLst;
    $("#lstfontname").hide();
    /*$('#ul_equation li').find('.mathquill-editable').css('font-size', selectedFontSize);
     $('#ul_equation li').find('.mathquill-editable').css('font-family', selectedFont);
     $('#ul_equation li').find('.mathquill-editable').css('color', selectedColor);*/
   }

   function fn_close_colorbox() {
    $(".color_box").hide('slow');
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
    $("#horizontal_hand").hide();
    $("#horizontal_hand_part").hide();
    show_color_buttons('less');
    $(".color_box").css('height', 135);
    $("#more_color").html('more color');
    $(".color_box").css('height', 135);
  }

  function fn_close_fontbox() {
    $(".text_box").hide('slow');
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
    $("#horizontal_hand").hide();
    $("#horizontal_hand_part").hide();
    $("#horizontal_hand1").hide();
    $("#horizontal_hand_part1").hide();
    drawline = false;
    toolzigzag = false;
    toolbeizer = false;
    tooltext = false;
    toolrect = false;
    toolpencil = false;
    drag = false;
    removeText();
    //drawtext(vtextobjectVal, vtextobjectop, vtextobjectleft);
    //$("#" + vtextobjectCreated + "").remove();
    //document.getElementById('canvas_area').className = "default_cur";
  }

  function fn_show_colorbox() {
    var color = $("#color_key").attr('class');
    if (color == 'key_color') {
      $("#color_key").removeClass('key_color').addClass('key_color_active');
      if ($("#more_color").html() == 'more color') {
        $(".color_box").show('slow');
      } else {
        $("#color_box_more").show('slow');
      }
    } else {
      $("#color_key").removeClass('key_color_active').addClass('key_color');
      if ($("#more_color").html() == 'more color') {
        $(".color_box").hide('slow');
      } else {
        $("#color_box_more").hide('slow');
      }
    }
    //var color_pos = $("#color_key").offset();
    //alert(color_pos.top + ' ' + color_pos.left);
  }

  function fn_show_more_color() {
    if ($("#more_color").html() == 'more color') {
      $(".color_box").css('height', 260);
      $("#more_color").html('less color');
      show_color_buttons('more');
        //fn_colorbox('m');
        //$( "#color_box_more" ).draggable({ cursor: "move", cursorAt: { top: 25, left: 25 } });
      } else {
        $(".color_box").css('height', 135);
        $("#more_color").html('more color');
        show_color_buttons('less');
        //fn_colorbox('l');
        //$( "#color_box" ).draggable({ cursor: "move", cursorAt: { top: 25, left: 25 } });
      }
    }

    function show_color_buttons(amore_less) {
      var vbuttons = new Array();
      var nvbuttons = new Array();
      $.ajax({
        type: 'POST',
        url: '/welcome/color_more_less',
        data: {more_less: amore_less},
        beforeSend: function () {

        },
        success: function (result) {
          if (result.result == 't') {
            var v_buttons = result.response;
                vbuttons = v_buttons;//.split(',');
                //alert(vbuttons.toString());
                var nv_buttons = result.not_visible;
                nvbuttons = nv_buttons;//.split(',')
                //for (var i in nvbuttons) {
                  for (i = 0; i < nvbuttons.length; i++) {
                    $("#" + nvbuttons[i] + "").hide();
                  }
                //for (var i in vbuttons) {
                  for (i = 0; i < vbuttons.length; i++) {
                    $("#" + vbuttons[i] + "").show();
                  }
                //alert(result.response + ' ' + result.not_visible);
                //$('#colorbox').html(result.response);
              } else {
                errormsg_dlg(result.error, 'Color Box');
              }
            },

            error: function (xhr, textStatus, error) {
              errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Color Box');
            },
            dataType: "json"
          });
    }

    function fn_colorbox(amore_less) {
      $.ajax({
        type: 'POST',
        url: '/welcome/color_more_less',
        data: {more_less: amore_less},
        beforeSend: function () {

        },
        success: function (result) {
          if (result.result == 't') {
            $('#colorbox').html(result.response);
          } else {
            errormsg_dlg(result.error, 'Color Box');
          }
        },

        error: function (xhr, textStatus, error) {
          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Color Box');
        },
        dataType: "json"
      });
    }

//color selection
function fn_select_color(acolor) {
  selectedColor = acolor;
  $("#colorselect").val(acolor);

  $(".color_box").hide('slow');
  $("#color_box_more").hide('slow');
  $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
  showhorizontalHand(false, '');
    //set the cursor of the canvas
    /*switch (tool_default){
     case 'empty':
     //document.getElementById('canvas_area').className = "default_cur";
     break;
     case 'pencil':
     //document.getElementById('canvas_area').className = "pencil";
     break;
     case 'text':
     //document.getElementById('canvas_area').className = "text_font";
     break;
     case 'line':
     //document.getElementById('canvas_area').className = "default_cur";
     break;
     case 'eraser':
     //document.getElementById('canvas_area').className = "eraser";
     break;
   }*/
 }


 function fn_current_symbols() {
  var equation = $("#equations").is(':visible');
  var vkeyboard = $("#keyboard").is(':visible');
  var keyclass = $("#key_current_symbols").attr('class');
  if (keyclass == 'keys_li') {
    $("#customize_symbols").show('slow');
    $("#key_current_symbols").removeClass('keys_li').addClass('keys_li_active');
    if (equation) {
      $("#equations").animate({
        top: '175px'
      });
      if (vkeyboard) {
        $("#keyboard").animate({
          top: '250px'
        });
      } else {
        $("#keyboard").animate({
          top: '30px'
        });
      }
    } else {
      $("#equations").animate({
        top: '40px'
      });
      if (vkeyboard) {
        $("#keyboard").animate({
          top: '170px'
        });
      } else {
        $("#keyboard").animate({
          top: '30px'
        });
      }
    }

  } else {
    var equation = $("#equations").is(':visible');
    if (equation) {
      $("#equations").animate({
        top: '40px'
      });
      if (vkeyboard) {
        $("#keyboard").animate({
          top: '120px'
        });
      }
    } else {
      if (vkeyboard) {
        $("#keyboard").animate({
          top: '30px'
        });
      }
    }

        //$("#customize_symbols").hide('slow');		
        $("#key_current_symbols").removeClass('keys_li_active').addClass('keys_li');
        var eq_class = $("#equation_key").attr('class');
      }
    }

    function fn_hide_customize_symbols() {
      var equation = $("#equations").is(':visible');
      var vkeyboard = $("#keyboard").is(':visible');
      if (equation) {
        $("#equations").animate({
          top: '40px'
        });
        if (vkeyboard) {
          $("#keyboard").animate({
            top: '120px'
          });
        }
      } else {
        if (vkeyboard) {
          $("#keyboard").animate({
            top: '30px'
          });
        }
      }

    //$("#customize_symbols").hide('slow');
    $("#key_current_symbols").removeClass('keys_li_active').addClass('keys_li');
  }

  function fn_show_equations() {
    var cust_symbol = $("#customize_symbols").is(':visible');
    var vkeyboard = $("#keyboard").is(':visible');

    var eq_class = $("#equation_key").attr('class');
    if (eq_class == 'keys_li') {
      if (cust_symbol) {
        $("#equations").animate({
          top: '175px'
        });

        if (vkeyboard) {
          $("#keyboard").animate({
            top: '240px'
          });
        } else {
          $("#keyboard").animate({
            top: '110px'
          });
        }
      } else {
        $("#equations").animate({
          top: '40px'
        });
        if (vkeyboard) {
          $("#keyboard").animate({
            top: '110px'
          });
        } else {
          $("#keyboard").animate({
            top: '30px'
          });
        }
      }

        /*if (vkeyboard){
         $("#keyboard").animate({
         top:'110px'
         });
         }else{
         $("#keyboard").animate({
         top:'30px'
         });
       }*/

       $("#equations").show('slow');
       $("#equation_key").removeClass('keys_li').addClass('keys_li_active');
     } else {
      if (cust_symbol) {
        $("#equations").animate({
          top: '175px'
        });

        if (vkeyboard) {
          $("#keyboard").animate({
            top: '170px'
          });
        }
      } else {
        $("#equations").animate({
          top: '40px'
        });
        if (vkeyboard) {
          $("#keyboard").animate({
            top: '30px'
          });
        }
      }
      $("#equations").hide('slow');
      $("#equation_key").removeClass('keys_li_active').addClass('keys_li');
      fn_remove_symbols_details();
    }
  }

  function fn_hide_equations() {
    $("#equations").hide('slow');
    $("#equation_key").removeClass('keys_li_active').addClass('keys_li');
    fn_remove_symbols_details();
    var vkeyboard = $("#keyboard").is(':visible');
    var cust_symbol = $("#customize_symbols").is(':visible');
    if (cust_symbol) {
      $("#equations").animate({
        top: '175px'
      });

      if (vkeyboard) {
        $("#keyboard").animate({
          top: '170px'
        });
      }
    } else {
      $("#equations").animate({
        top: '40px'
      });
      if (vkeyboard) {
        $("#keyboard").animate({
          top: '30px'
        });
      }
    }

    //$("#equations_details_1").hide();
  }

  function fn_remove_symbols_details() {
    //for (var i in eq_details) {
      for (var i = 0; i < eq_details.length; i++) {
        var vequations = eq_details[i].toString();
        var vequation_details = vequations.substr(0, vequations.indexOf(';'));
        var vequation = vequations.substr(vequations.indexOf(';') + 1);
        $("#" + vequation_details + "").hide('slow');
        $("#" + vequation + "").removeClass('symbol_li_' + vequation + '_active').addClass('symbol_li_' + vequation);
      }
      eq_details = new Array();
    }

    function fn_remove_other_symbols(aid) {
//console.log(eq_details.toString() + ' count:' + eq_details.length);
    //for (var i in eq_details) {
      for (var i = 0; i < eq_details.length; i++) {
        var vequations = eq_details[i].toString();
        var vequation_details = vequations.substr(0, vequations.indexOf(';'));
        //console.log('1 equation_details:' + vequation_details + ' equations:' + vequations);
        var vequation = vequations.substr(vequations.indexOf(';') + 1);
        //console.log('2 equation:' + vequation + ' aid-equations_details_' + aid);
        if (vequation_details != 'equations_details_' + aid) {
          $("#" + vequation_details + "").hide('slow');
          $("#" + vequation + "").removeClass('symbol_li_' + vequation + '_active').addClass('symbol_li_' + vequation);
        }
      }
    }

    function fn_equations_click(aid, aname) {
      selectedEquation = aid;
      var eq_class = $("#" + aname + "").attr('class');
      if (eq_class == 'symbol_li_' + aname) {
        //fn_hidekeyboard();
        eq_details.push('equations_details_' + aid + ';' + aname);
        $("#" + aname + "").removeClass('symbol_li_' + aname).addClass('symbol_li_' + aname + '_active');
        fn_remove_other_symbols(aid);
        $("#equations_details_" + aid + "").show('slow');
      } else {
        $("#" + aname + "").removeClass('symbol_li_' + aname + '_active').addClass('symbol_li_' + aname);
        $("#equations_details_" + aid + "").hide('slow');
        var aIndex = eq_details.indexOf('equations_details_' + aid + ';' + aname);
        eq_details.splice(aIndex, 1);
      }
    }

    function fn_hide_equations_details(aid, aname) {
      $("#" + aname + "").removeClass('symbol_li_' + aname + '_active').addClass('symbol_li_' + aname);
      $("#equations_details_" + aid + "").hide('slow');
    }


//set font bold
function fn_bold() {
  var bold_key = $("#key_bold").attr('class');
  if (bold_key == 'keys_li') {
    isBold = true;
    $("#key_bold").removeClass('keys_li').addClass('keys_li_active');
  } else {
    isBold = false;
    $("#key_bold").removeClass('keys_li_active').addClass('keys_li');
  }
}

//set font underline
function fn_underline() {
  var underline_key = $("#key_underline").attr('class');
  if (underline_key == 'keys_li') {
    isUnderline = true;
    $("#key_underline").removeClass('keys_li').addClass('keys_li_active');
  } else {
    isUnderline = false;
    $("#key_underline").removeClass('keys_li_active').addClass('keys_li');
  }
}

//set font italic
function fn_italic() {
  var italic_key = $("#key_italic").attr('class');
  if (italic_key == 'keys_li') {
    isItalic = true;
    $("#key_italic").removeClass('keys_li').addClass('keys_li_active');
  } else {
    isItalic = false;
    $("#key_italic").removeClass('keys_li_active').addClass('keys_li');
  }
}

//set font strikeout
function fn_strikeout() {
  var strikeout_key = $("#key_strikeout").attr('class');
  if (strikeout_key == 'keys_li') {
    isStrikeout = true;
    $("#key_strikeout").removeClass('keys_li').addClass('keys_li_active');
  } else {
    isStrikeout = false;
    $("#key_strikeout").removeClass('keys_li_active').addClass('keys_li');
  }
}

//write text in canvas
function fn_write_to_canvas(atext) {
  var canvas = document.getElementById("can");
  var context = canvas.getContext("2d");
  context.fillStyle = selectedColor; /*"blue";*/
  if (selectedFontSize > 0) {
    if (isBold) {
      context.font = 'bold' + selectedFontSize * 5 + 'px ' + selectedFontName;/* "bold 16px Arial";*/
    } else {
      context.font = selectedFontSize * 5 + 'px ' + selectedFontName;/* "bold 16px Arial";*/
    }
  } else {
    context.font = "bold 10px Arial";
  }

  context.fillText(atext, 100, 100);
    /*  
     selectedFont,
     selectedCustomSymbol,
     eq_details = new Array(),
     selectedEquation,
     selectedFontSize,
     selectedColor;
     */
   }

   var fselect = '';
   function fn_equations_detail_click(aid, avalue, aimg) {
    var cust_symbol = $("#customize_symbols").is(':visible');
    if (!vkeyboard_dragged) {
      $("#keyboard").hide('slow');
    }
    if (!cust_symbol) {
      var tb_math = $("#mathmall").is(':visible');
      if (!tb_math) {
            //$("#mathmall").show();
            fn_activate_mathmall();
          }
          $("#close_mainkey").css('left', '362px');
        //$("#enter_key").show();
        var vkey_enter = $("#enter_key").is(':visible');
        if (!vkey_enter) {
          $("#enter_key").show();
          $("#bkspace_key").show();
          $("#space_key").show();
        }
        fn_insert_math(avalue);
      } else {
        if (selectedCustomSymbol > 0) {
          if (selectedCustomSymbol <= 100) {
            if (fselect == '') {
              fselect = aid;
            } else {
              fselect = fselect + ',' + aid;
            }
            $("#selected_equation").val(fselect);
            $("#cs_" + selectedCustomSymbol + "").css('background', 'url(' + aimg + ') no-repeat');
            $("#cs_" + selectedCustomSymbol + "").css('background-position', 'center');

            $("#cs_" + selectedCustomSymbol + "").hover(function () {
              $(this).css('background', 'url(' + aimg + ') no-repeat #F68D23');
              $(this).css('background-position', 'center');
            }, function () {
              $(this).css('background', 'url(' + aimg + ') no-repeat #FCFAF2');
              $(this).css('background-position', 'center');
            });
            $("#close_mainkey").css('left', '362px');
                //$("#enter_key").show();		  
                var vkey_enter = $("#enter_key").is(':visible');
                if (!vkey_enter) {
                  $("#enter_key").show();
                  $("#bkspace_key").show();
                  $("#space_key").show();
                }
                $("#cs_" + selectedCustomSymbol + "").text(avalue);
                //set the next list available
                switch (selectedCustomSymbol) {
                  case 21:
                  fn_SetSpinnerVertVal(1);
                  break;

                  case 41:
                  fn_SetSpinnerVertVal(1);
                  break;

                  case 61:
                  fn_SetSpinnerVertVal(1);
                  break;

                  case 81:
                  fn_SetSpinnerVertVal(1);
                  break;
                }
                selectedCustomSymbol = selectedCustomSymbol + 1;
              } else {
                errormsg_dlg('Limit of 100 Customized symbols done.', 'Adding Customized symbols');
              }
            } else {
              warning_msg_dlg('Please select the symbol area first.', 'Symbol Insert');
            }
          }
        }

        function fn_select_custom_symbol(aid) {
          selectedCustomSymbol = aid;

          var cust_sym = '<p><span class="math-tex">' + $("#cs_" + aid + "").text() + '</span></p>';

          if (vSelectedEditor != '') {
            CKEDITOR.instances[vSelectedEditor].insertHtml(cust_sym);
          } else {
            warning_msg_dlg('Please select at least one editor to enter the equation', 'Inserting Equation');
          }

    /*var NewDialog = $('<div id="msgdialog">\
     <div class="mathquill-editable" id="math_edit" style="width:398px;height:30px;"></div>\
     <div id="normal"></div>\
     </div>');
     
     //<span class="mathquill-editable" id="math_edit" style="width:398px;height:30px;background:#fff; border:0px solid #D0D0D0">\\frac{d}{dx}</span>\
     //<span class="mathquill-editable">f(x)=?</span>\
     var atext = $("#cs_" + aid + "").text();
     //alert(atext);
     
     //$("#math_edit").focus();
     if (atext != ''){		
     NewDialog.dialog({
     resizable: false,
     modal: true,
     closeOnEscape: false,
     title : 'Please edit Equation',
     open: function(event, ui) { 
     $(".ui-dialog-titlebar-close").hide();
     //$("#math_edit").html($("#cs_" + aid + "").text());
     //$("#math_edit").mathquill('write','\\frac{d}{dx}');
     //$('<div>\sqrt{2}</div>').mathquill().appendTo('body').mathquill('redraw')
     //$("#math_edit").find('.mathquill-editable').mathquill('editable');
     //$('<div>\frac{d}{dx}</div>').appendTo('body').mathquill();
     //$("#math_edit").mathquill().appendTo('body').mathquill('latex','a_n x^n');
     $("#normal").html(atext);
     $("#math_edit").mathquill('editable').focus();
     $("#math_edit").mathquill('write','\\frac{d}{dx}');
     },
     height:'auto',
     width:'auto',
     buttons: {
     Ok: function() {
     if (vSelectedEditor != ''){
     CKEDITOR.instances[vSelectedEditor].insertHtml(cust_sym);
     }else{
     warning_msg_dlg('Please select at least one editor to enter the equation', 'Inserting Equation');
     }			
     $( this ).dialog( "close" );
     },
     Cancel: function(){
     $( this ).dialog( "close" );
     }
     }
     });		
   }*/
 }

 function fn_SetSpinnerEqDetailVertVal(ainc_dec, aid, amax) {
  var vSpinnerVal = $("#spinval_eq_det_" + aid + "").html();
  if (ainc_dec == 1) {
    vSpinnerVal++;
  } else {
    vSpinnerVal--;
  }
  if (vSpinnerVal <= 0) {
    vSpinnerVal = 1;
  }
  if (vSpinnerVal > amax) {
    vSpinnerVal = amax;
  }
    //alert(selectedEquation);
    $("#spinval_eq_det_" + aid + "").html(vSpinnerVal);
    //send ajax call to update details view
    $.ajax({
      type: 'POST',
      url: '/welcome/show_eq_next_row_details',
      data: {equid: selectedEquation, next_value: vSpinnerVal},
      beforeSend: function () {

      },
      success: function (result) {
        if (result.result == 't') {
          $("#details_" + aid + "").html(result.response);
        } else {
          errormsg_dlg(result.error, 'Detail View');
        }
      },

      error: function (xhr, textStatus, error) {
        errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Detail View');
      },
      dataType: "json"
    });
  }

  function showcurrent_canvas() {
    for (var i = 1; i <= vpages; i++) {
      $("#can_" + i + "").hide();
    }
    $("#can_" + vcurrentpage + "").show();
  }

//add new page
function fn_addnewpage() {
  vpages++;
  vcurrentpage++;
  $("#curr_page").html(vpages);
  $("#page_num").html(vpages);
  var vwidth = $("#can_1").width();
  var vheight = $("#can_1").height();
  var new_canvas = '<canvas id="can_' + vpages + '" width="' + vwidth + 'px" height="' + vheight + 'px" style="position:absolute;border:0px solid;">html5 canvas not supported</canvas>';
  var new_temp_canvas = '<canvas id="can_temp_' + vpages + '" width="' + vwidth + 'px" height="' + vheight + 'px" style="position:absolute;border:0px solid;">html5 canvas not supported</canvas>';

  $("#canvas_area").append(new_canvas);

    /*var c=document.getElementById("can_" + vpages);
     var ctx=c.getContext("2d");
     ctx.font="30px Arial";
     ctx.fillText(vpages,10,50);*/

     $("#prev_page").show();
     $("#remove_page").show();
     $("#prev_page").css('left', '150px');
     showcurrent_canvas();
     var vcan_width = $("#canvas_area").css('width');
     var vcan_height = $("#canvas_area").css('height');
    //add grid if grid is on
    if (vgrid) {
      $("#can_" + vcurrentpage + "").addClass(vgridtype);
    } else {
      $("#can_" + vcurrentpage + "").removeClass(vgridtype);
    }
    //$("#can_" + vcurrentpage + "").width(vcan_width).height(vcan_height);	
    /*if (tool_default != 'empty'){
     init();
     }
     if (vtextobjectVal){
     drawtext(vtextobjectVal, vtextobjectop, vtextobjectleft);
     $("#" + vtextobjectCreated + "").remove();
   }*/
   removeText();
 }

//go to next page
function fn_nextpage() {
  if (vcurrentpage < vpages) {
    vcurrentpage = vcurrentpage + 1;
    $("#curr_page").html(vcurrentpage);
    $("#page_num").html(vpages);
    $("#prev_page").show();
  } else {
    vcurrentpage = vpages;
    $("#next_page").hide();
    $("#prev_page").show();
  }
  if (vcurrentpage == vpages) {
    $("#prev_page").show();
    $("#next_page").hide();
    $("#next_page").css('left', '150px');
    $("#prev_page").css('left', '150px');
  } else {
    $("#next_page").css('left', '150px');
    $("#prev_page").css('left', '170px');
  }
  if (vpages > 1) {
    $("#remove_page").show();
  }
  showcurrent_canvas();
    /*if (tool_default != 'empty'){
     init();
     }
     if (vtextobjectVal){
     drawtext(vtextobjectVal, vtextobjectop, vtextobjectleft);
     $("#" + vtextobjectCreated + "").remove();
   }*/
   removeText();
 }

//go to previous page
function fn_prevpage() {
  vcurrentpage = vcurrentpage - 1;
  $("#curr_page").html(vcurrentpage);
  $("#page_num").html(vpages);
  if (vcurrentpage == 1) {
    $("#prev_page").hide();
    $("#next_page").show();
    $("#next_page").css('left', '150px');
    $("#prev_page").css('left', '170px');
    $("#remove_page").hide();
  } else {
    $("#next_page").show();
    $("#next_page").css('left', '150px');
    $("#prev_page").css('left', '170px');
    $("#remove_page").show();
  }
  showcurrent_canvas();
    /*if (tool_default != 'empty'){
     init();
     }
     if (vtextobjectVal){
     drawtext(vtextobjectVal, vtextobjectop, vtextobjectleft);
     $("#" + vtextobjectCreated + "").remove();
   }*/
   removeText();
 }

 function fn_removepage() {
  $("#can_" + vcurrentpage + "").remove();
  for (var i = vcurrentpage + 1; i <= vpages; i++) {
    var vnewid = i - 1;
    $("#can_" + i).attr('id', 'can_' + vnewid);
  }
  vpages--;
  vcurrentpage--;
  $("#curr_page").html(vcurrentpage);
  $("#page_num").html(vpages);

  if (vpages == 1) {
    $("#remove_page").hide();
  } else {
    $("#remove_page").show();
  }
  if (vcurrentpage == 1) {
    $("#remove_page").hide();
  } else {
    $("#remove_page").show();
  }

  if (vcurrentpage == vpages) {
    $("#prev_page").hide();
    $("#next_page").hide();
  } else {
    $("#prev_page").show();
    $("#next_page").show();
    $("#next_page").css('left', '150px');
    $("#prev_page").css('left', '170px');
  }
  showcurrent_canvas();
    /*if (tool_default != 'empty'){
     init();
     }
     if (vtextobjectVal){
     drawtext(vtextobjectVal, vtextobjectop, vtextobjectleft);
     $("#" + vtextobjectCreated + "").remove();
   }*/
   removeText();
 }

 function fn_pin_tool() {
  var pintool = $("#pin_tool").attr('class');
  var mtoolwidth = 442;
  var mtoolvisible = $("#mathmall").is(':visible');
  var etoolvisible = $("#english").is(':visible');
  var canvaswidth = $("#canvas_area").css('width');
  var canvas_footer_width = $("#canvas_pos").css('width');
  var toolwidth = 33;
  var qstudylogo = $("#qstudylogo").css('width');
  var vcan_width = 0;

  if (pintool == 'pin') {
    $("#pin_tool").removeClass('pin').addClass('unpin');
    $("#pin_tool").tooltip({content: "Show Tool Panel"});
    $("#tool_panel").hide('slow');
    $("#score").hide('slow');
        /*var vscorewidth = $("#score").css('width');
         var vfooterwidth = $("#footer_holder_qstudy").css('width');
         var vwholefooterwidth = vfooterwidth + vscorewidth;
         $("#footer_holder_qstudy").css('width', vwholefooterwidth);*/

         if (mtoolvisible) {
          var vcanwidth = parseInt(canvaswidth) + parseInt(mtoolwidth) - parseInt(toolwidth);
          var vcan_footer_width = parseInt(canvas_footer_width) + parseInt(mtoolwidth) - parseInt(toolwidth);
          var vqstudylogo = parseInt(qstudylogo) - parseInt(toolwidth);
          $("#mathmall").animate({right: '18px'});
            //$("#pages").css('right', '381px');
            $("#pages").css('right', '5px');
          } else {
            var vcanwidth = parseInt(canvaswidth) + parseInt(toolwidth);
            var vcan_footer_width = parseInt(canvas_footer_width) + parseInt(toolwidth);
            var vqstudylogo = qstudylogo;
          }

          if (etoolvisible) {
            var vcanwidth = parseInt(canvaswidth) + parseInt(mtoolwidth) - parseInt(toolwidth);
            var vcan_footer_width = parseInt(canvas_footer_width) + parseInt(mtoolwidth) - parseInt(toolwidth);
            var vqstudylogo = parseInt(qstudylogo) - parseInt(toolwidth);
            $("#english").animate({right: '18px'});
            $("#pages").css('right', '381px');
          } else {
            var vcanwidth = parseInt(canvaswidth) + parseInt(toolwidth);
            var vcan_footer_width = parseInt(canvas_footer_width) + parseInt(toolwidth);
            var vqstudylogo = qstudylogo;
          }

          if ((mtoolvisible) && (!etoolvisible)) {
            vcan_width = $(document).width() - 524;
          } else if ((!mtoolvisible) && (etoolvisible)) {
            vcan_width = $(document).width() - 524;
          } else {
            vcan_width = $(document).width() - 77;
          }

          $("#canvas_area").animate({width: vcanwidth + 'px'});
          $("#canvas_pos").animate({width: vcan_footer_width + 'px'});
          $("#qstudylogo").css('width', vqstudylogo + 'px');

          var vcan_height = $("#canvas_area").css('height');
        //$("#can_" + vcurrentpage + "").width(vcanwidth).height(vcan_height);

        //$("#can_" + vcurrentpage + "").width(vcan_width).height($(document).height() - 126);
        $("#can_" + vcurrentpage + "").width(1033).height(700);
        $("#can_temp_" + vcurrentpage + "").width(1033).height(700);
      } else {
        $("#pin_tool").removeClass('unpin').addClass('pin');
        $("#pin_tool").tooltip({content: "Hide Tool Panel"});
        $("#tool_panel").show('slow');
        $("#score").show('slow');

        /*var vscorewidth = $("#score").css('width');
         var vfooterwidth = $("#footer_holder_qstudy").css('width');
         var vwholefooterwidth = vfooterwidth - vscorewidth;
         $("#footer_holder_qstudy").css('width', vwholefooterwidth);*/


         if (mtoolvisible) {
          var vcanwidth = parseInt(canvaswidth) - parseInt(mtoolwidth) + parseInt(toolwidth);
          var vcan_footer_width = parseInt(canvas_footer_width) - parseInt(mtoolwidth) + parseInt(toolwidth);
          $("#mathmall").animate({right: '46px'});
          $("#pages").css('right', '0px');
          var vqstudylogo = parseInt(qstudylogo) + parseInt(toolwidth);
        } else {
          var vcanwidth = parseInt(canvaswidth) - parseInt(toolwidth);
          var vcan_footer_width = parseInt(canvas_footer_width) - parseInt(toolwidth);
          var vqstudylogo = qstudylogo;
        }

        if (etoolvisible) {
          var vcanwidth = parseInt(canvaswidth) - parseInt(mtoolwidth) + parseInt(toolwidth);
          var vcan_footer_width = parseInt(canvas_footer_width) - parseInt(mtoolwidth) + parseInt(toolwidth);
          $("#english").animate({right: '46px'});
          $("#pages").css('right', '0px');
          var vqstudylogo = parseInt(qstudylogo) + parseInt(toolwidth);
        } else {
          var vcanwidth = parseInt(canvaswidth) - parseInt(toolwidth);
          var vcan_footer_width = parseInt(canvas_footer_width) - parseInt(toolwidth);
          var vqstudylogo = qstudylogo;
        }

        $("#canvas_area").animate({width: vcanwidth + 'px'});
        $("#canvas_pos").animate({width: vcan_footer_width + 'px'});
        $("#qstudylogo").css('width', vqstudylogo + 'px');
        //$("#mathmall").animate({right: '46px'});
        var vcan_height = $("#canvas_area").css('height');
        //$("#can_" + vcurrentpage + "").width(vcanwidth).height(vcan_height);	
        if ((mtoolvisible) && (!etoolvisible)) {
          vcan_width = $(document).width() - 552;
        } else if ((!mtoolvisible) && (etoolvisible)) {
          vcan_width = $(document).width() - 552;
        } else {
          vcan_width = $(document).width() - 110;
        }
        //$("#can_" + vcurrentpage + "").width(vcan_width).height($(document).height() - 126);	
        $("#can_" + vcurrentpage + "").width(1000).height(700);
        $("#can_temp_" + vcurrentpage + "").width(1000).height(700);
      }
    }

    function fn_show_mathmall() {
    //fn_tool_arrow();
    fn_normalize_canvas();
    var mtool = $("#math_button").attr('class');
    var etoolvisible = $("#english").is(':visible');
    var qstudylogo = $("#qstudylogo").css('width', '100%');
    if (mtool == 'math_tool') {
      if (etoolvisible) {
        hide_english();
      }
      $("#mathmall").show('slow');
      $("#math_button").removeClass('math_tool').addClass('math_tool_active');
      var mathwidth = 442;
      var canvaswidth = $("#canvas_area").css('width');
      var vcanwidth = parseInt(canvaswidth) - parseInt(mathwidth);
      var canvas_footer_width = $("#canvas_pos").css('width');
      var vcan_footer_width = parseInt(canvas_footer_width) - parseInt(mathwidth);
      $("#canvas_area").animate({width: vcanwidth + 'px'});
      $("#canvas_pos").animate({width: vcan_footer_width + 'px'});
        //$("#can_" + vcurrentpage + "").width(vcanwidth - 2);
        var vcan_height = $("#canvas_area").css('height');
        //$("#can_" + vcurrentpage + "").height($(document).height() - 126);
        //$("#can_" + vcurrentpage + "").width($(document).width() - 552).height($(document).height() - 126);
        $("#can_" + vcurrentpage + "").width(728).height(700);
        $("#can_temp_" + vcurrentpage + "").width(728).height(700);
        var vqstudylogo = parseInt(qstudylogo) - parseInt(mathwidth);
        $("#qstudylogo").css('width', vqstudylogo + 'px');
        $("#main_keyboard").css({top: '160px', left: '80px'});
        $("#main_keyboard").show('slow');
        $("#enter_key").hide();
        $("#bkspace_key").hide();
        $("#space_key").hide();
        $("#close_mainkey").css('left', '178px');
        $("#math_equations").css('height', $("#main_canvas").height() - 3);
      } else {
        if (etoolvisible) {
          hide_english();
        }
        $("#mathmall").hide('slow');
        $("#math_button").removeClass('math_tool_active').addClass('math_tool');
        var mathwidth = 442;
        var canvaswidth = $("#canvas_area").css('width');
        var vcanwidth = parseInt(canvaswidth) + parseInt(mathwidth);
        var canvas_footer_width = $("#canvas_pos").css('width');
        var vcan_footer_width = parseInt(canvas_footer_width) + parseInt(mathwidth);
        $("#canvas_area").animate({width: vcanwidth + 'px'});
        $("#canvas_pos").animate({width: vcan_footer_width + 'px'});
        var vcan_height = $("#canvas_area").css('height');
        //$("#can_" + vcurrentpage + "").width(vcanwidth - 2).height($(document).height() - 126);
        //$("#can_" + vcurrentpage + "").width($(document).width() - 110).height($(document).height() - 126);
        $("#can_" + vcurrentpage + "").width(1000).height(700);
        $("#can_temp_" + vcurrentpage + "").width(1000).height(700);
        var vqstudylogo = parseInt(qstudylogo) - parseInt(mathwidth);
        $("#qstudylogo").css('width', vqstudylogo + 'px');
        $("#main_keyboard").hide('slow');
      }
    }

    function hide_mathmall() {
      var qstudylogo = $("#qstudylogo").css('width', '100%');
      $("#mathmall").hide('slow');
      $("#math_button").removeClass('math_tool_active').addClass('math_tool');
      var mathwidth = 442;
      var canvaswidth = $("#canvas_area").css('width');
      var vcanwidth = parseInt(canvaswidth) + parseInt(mathwidth);
      var canvas_footer_width = $("#canvas_pos").css('width');
      var vcan_footer_width = parseInt(canvas_footer_width) + parseInt(mathwidth);
      $("#canvas_area").css('width', vcanwidth + 'px');
      $("#canvas_pos").css('width', vcan_footer_width + 'px');
      var vcan_height = $("#canvas_area").css('height');
      $("#can_" + vcurrentpage + "").width(vcanwidth).height(vcan_height);
      var vqstudylogo = parseInt(qstudylogo) - parseInt(mathwidth);
      $("#qstudylogo").css('width', vqstudylogo + 'px');
      $("#main_keyboard").hide('slow');
    }

    function hide_english() {
    //$("#english_text").remove();
    var qstudylogo = $("#qstudylogo").css('width', '100%');
    $("#english").hide('slow');
    $("#english_button").removeClass('english_tool_active').addClass('english_tool');
    var mathwidth = 442;
    var canvaswidth = $("#canvas_area").css('width');
    var vcanwidth = parseInt(canvaswidth) + parseInt(mathwidth);
    var canvas_footer_width = $("#canvas_pos").css('width');
    var vcan_footer_width = parseInt(canvas_footer_width) + parseInt(mathwidth);
    $("#canvas_area").css('width', vcanwidth + 'px');
    $("#canvas_pos").css('width', vcan_footer_width + 'px');
    var vcan_height = $("#canvas_area").css('height');
    $("#can_" + vcurrentpage + "").width(vcanwidth).height(vcan_height);
    var vqstudylogo = parseInt(qstudylogo) - parseInt(mathwidth);
    $("#qstudylogo").css('width', vqstudylogo + 'px');
  }

  function fn_activate_mathmall() {
    $("#mathmall").show('slow');
    $("#math_button").removeClass('math_tool').addClass('math_tool_active');
  }

  function fn_show_english() {
    //fn_tool_arrow();
    fn_normalize_canvas();
    var mtoolvisible = $("#mathmall").is(':visible');
    var etool = $("#english_button").attr('class');
    var qstudylogo = $("#qstudylogo").css('width', '100%');


    var percents = parseInt($("#eng_table").css('height'));
    var parentWidth = parseInt($("#eng_table").css('width'));
    var pixels = $("#tool_panel").height() - 142;//parentWidth*(percents/100) + 39;

    if (etool == 'english_tool') {
      if (mtoolvisible) {
        hide_mathmall();
      }
      $("#english").show('slow');
      $("#english_button").removeClass('english_tool').addClass('english_tool_active');

      var editor = CKEDITOR.instances['english_text'];
      if (editor) {
        editor.destroy(true);
      }

      CKEDITOR.replace('english_text', {
        uiColor: '#256D7A',
        toolbar: [
        ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Link', '-', 'Unlink', 'Undo', 'Redo'],
        ['Font', 'FontSize', 'TextColor', 'BGColor', 'Styles', 'Colors', '/', 'Cut', 'Copy', 'Paste']
        ],
        height: pixels + 'px',
        resize_enabled: false
      });


      var mathwidth = 442;
      var canvaswidth = $("#canvas_area").css('width');
      var vcanwidth = parseInt(canvaswidth) - parseInt(mathwidth);
      var canvas_footer_width = $("#canvas_pos").css('width');
      var vcan_footer_width = parseInt(canvas_footer_width) - parseInt(mathwidth);
      $("#canvas_area").animate({width: vcanwidth + 'px'});
      $("#canvas_pos").animate({width: vcan_footer_width + 'px'});
      var vcan_height = $("#canvas_area").css('height');
        //$("#can_" + vcurrentpage + "").width($(document).width() - 552).height($(document).height() - 126);
        $("#can_" + vcurrentpage + "").width(728).height(700);
        $("#can_temp_" + vcurrentpage + "").width(728).height(700);
        var vqstudylogo = parseInt(qstudylogo) - parseInt(mathwidth);
        $("#qstudylogo").css('width', vqstudylogo + 'px');
        $("#eng_table").css('height', $("#main_canvas").height() - 3);
        init();
      } else {
        if (mtoolvisible) {
          hide_mathmall();
        }
        $("#english").hide('slow');
        $("#english_button").removeClass('english_tool_active').addClass('english_tool');
        var mathwidth = 442;
        var canvaswidth = $("#canvas_area").css('width');
        var vcanwidth = parseInt(canvaswidth) + parseInt(mathwidth);
        var canvas_footer_width = $("#canvas_pos").css('width');
        var vcan_footer_width = parseInt(canvas_footer_width) + parseInt(mathwidth);
        $("#canvas_area").animate({width: vcanwidth + 'px'});
        $("#canvas_pos").animate({width: vcan_footer_width + 'px'});
        var vcan_height = $("#canvas_area").css('height');
        //$("#can_" + vcurrentpage + "").width($(document).width() - 110).height($(document).height() - 126);
        $("#can_" + vcurrentpage + "").width(1000).height(700);
        $("#can_temp_" + vcurrentpage + "").width(1000).height(700);
        var vqstudylogo = parseInt(qstudylogo) - parseInt(mathwidth);
        $("#qstudylogo").css('width', vqstudylogo + 'px');
      }
    }


    function fn_toimg() {
//  $("#rough").show('slow');  
//  $("#drawing_area").show();

//  $("#draw_line").show();
//  $("#eraser").show();
//  $("#clear").show();
//  erase_();
var canvas = document.getElementById('can_' + vcurrentpage);
var context = canvas.getContext('2d');
var vcanHide = false;
$('#ul_equation div[id^="drawing_"]').each(function (index) {
  id = $(this).attr('id');
  last_ = id.indexOf('_') + 1;
  theid = id.substr(last_, id.length);

  var latex = $("#drawing_" + theid + "").mathquill('latex');
        //console.log(latex);	

        if (typeof latex != 'undefined') {
          vcanHide = true;
          var img1 = $("#img_" + theid + "");
          img1.attr('src', 'http://latex.codecogs.com/gif.latex?' + latex);
          console.log('http://latex.codecogs.com/gif.latex?' + latex);
          imageObj[theid] = new Image();
          var img = document.getElementById("img_" + theid + "");

          imageObj[theid].onload = function () {
            if (theid == '1') {
              context.drawImage(img, 50, 1);
            } else {
              context.drawImage(img, 50, 30 * parseInt(theid) + 5);
            }
          }();
          imageObj[theid].src = img;
        } else {
          vcanHide = false;
        }
      });
    /*alert(vcanHide);
     if (vcanHide){					
     hide_mathmall();
   }*/

    //var imgNo = 0;
    /*$('#tb_equation tr:visible').each(function() {	
     id = $(this).attr('id');
     last_ = id.indexOf('_') + 1;
     theid = id.substr(last_, id.length);
     var latex = $("#drawing_" + theid + "").mathquill('latex');
     //if (latex != ''){
     var img1 = $("#img_" + theid + "");
     img1.attr('src','http://latex.codecogs.com/gif.latex?'+latex);
     
     imageObj[theid] = new Image();
     var img=document.getElementById("img_" + theid + "");
     
     imageObj[theid].onload = function() {
     if (theid == '1'){
     context.drawImage(img, 50, 1);
     }else{
     context.drawImage(img, 50, 30 * parseInt(theid) + 5);
     }
     }();
     imageObj[theid].src = img;//'http://latex.codecogs.com/gif.latex?'+latex;
   });*/
 }

 function fn_rough() {

 }

 function fn_show_more_shape() {
  if ($("#more_shape").html() == 'more shape') {
    $("#shape_box").css('height', 180);
    $("#more_shape").html('less shape');
    show_shape_buttons('more');
  } else {
    $("#shape_box").css('height', 100);
    $("#more_shape").html('more shape');
    show_shape_buttons('less');
  }
}

function show_shape_buttons(amore_less) {
  var vbuttons = new Array();
  var nvbuttons = new Array();
  $.ajax({
    type: 'POST',
    url: '/welcome/shape_more_less',
    data: {more_less: amore_less},
    beforeSend: function () {

    },
    success: function (result) {
      if (result.result == 't') {
        var v_buttons = result.response;
        vbuttons = v_buttons;
        var nv_buttons = result.not_visible;
        nvbuttons = nv_buttons;
        for (var i in nvbuttons) {
          $("#" + nvbuttons[i] + "").hide();
        }
        for (var i in vbuttons) {
          $("#" + vbuttons[i] + "").show();
        }
      } else {
        errormsg_dlg(result.error, 'Shape Box');
      }
    },

    error: function (xhr, textStatus, error) {
      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Shape Box');
    },
    dataType: "json"
  });
}

function fn_close_shapebox() {
  $("#shape_box").hide('slow');
  $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
  $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
  $("#vertical_hand").hide();
  $("#vertical_hand_part").hide();
  show_shape_buttons('less');
  $("#shape_box").css('height', 100);
  $("#more_shape").html('more shape');
}


function fn_draw_shape(ashape) {
  tool_default = ashape;
  switch (ashape) {
    case 'square':
    draggable = false;
    erasing = false;
    drawline = false;
    toolzigzag = false;
    toolbeizer = false;
    tooltext = false;
    toolrect = true;
    toolpencil = false;
    draw_rect();
    break;
  }
    //init();
    fn_close_shapebox();
  }

  function fn_close_gridbox() {
    $("#grid_box").hide('slow');
    $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
    $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
    $("#vertical_hand").hide();
    $("#vertical_hand_part").hide();
  }


  function fn_show_grid(atype) {
    vgridtype = atype;
    $("#can_" + vcurrentpage + "").addClass(atype);
    $("#grid_box").hide('slow');
    showverticalHand(false, 'tool_grid');
    vgrid = true;
    /*if (!vgrid){
     //drawGrid('#CCC');
     $("#can_" + vcurrentpage + "").addClass(atype);
     vgrid = true;
     $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
     }else{
     //drawGrid('#FFF');
     $("#can_" + vcurrentpage + "").removeClass(atype);
     vgrid = false;
     $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
   }*/
 }

//for increase/ decrease rows of option
function fn_rows_inc(inc_dec) {
  var vstop_increase;
  var vstop_decrease;
  var vSpinnerVal = $("#rows_count").html();
  if (inc_dec == 1) {
    vSpinnerVal++;
  } else {
    vSpinnerVal--;
  }
  if (vSpinnerVal < 3) {
    vstop_decrease = true;
    vSpinnerVal = 3;
  } else {
    vstop_decrease = false;
  }
  if (vSpinnerVal > 5) {
    vSpinnerVal = 5;
    vstop_increase = true;
  } else {
    vstop_increase = false;
  }
  $("#rows_count").html(vSpinnerVal);
  var aindex, awidth;
  switch (vSpinnerVal) {
    case 1:
    aindex = 'A';
    awidth = '350px';
    break;

    case 2:
    aindex = 'B';
    awidth = '350px';
    break;

    case 3:
    aindex = 'C';
    awidth = '350px';
    break;

    case 4:
    aindex = 'D';
    awidth = '380px';
    break;

    case 5:
    aindex = 'E';
    awidth = '410px';
    break;
  }
  if (inc_dec == 1) {
    if (!vstop_increase) {
      html = '<tr>';
      html = html + '<td class="option_tb1">' + aindex + '</td>';
      html = html + '<td class="option_tb2"></td>';
      html = html + '<td><div class="options_checks"></div></td>';
      html = html + '</tr>';
      var i = vSpinnerVal - 2;
      $('#tb_options > tbody > tr:eq(' + i + ')').after(html);
      $("#option").css({height: awidth});
    }
  } else {
    if (!vstop_decrease) {
      $('#tb_options tr:eq(' + vSpinnerVal + ')').remove();
      $("#option").css({height: awidth});
    }
  }
}

function fn_save_question_opensource(aController, aForm, aQuiz, aQuizOption, questionsId, m_id) {
  m_id = m_id || 0;
  var cansave;
  var mchoice_rows;
  var mchoice = '';
  var mchoices = new Array();
  var mmatchings = new Array();
  var mmatching_answer = new Array();
  var answer_matching = new Array();
  var mmatching = '';
  var assign_score = '';
  var assign_scores = new Array();
  var assign_score_des = '';
  var assign_scores_des = new Array();
  var assign_slno = '';
  var assign_slnos = new Array();
  var question_creative = '';
  if (CKEDITOR.instances['solution_editor']) {
    vsolution = CKEDITOR.instances['solution_editor'].getData();
  } else {
    vsolution = $('#solution_editor').val();
  }
  var word_to_search = '';
  var definition = '';
  var parts_of_speech = '';
  var synonym = '';
  var antonym = '';
  var near_antonym = '';
  var orig_sentence = '';
  var audio_file = '';
  var video_file = '';
  var vquestion = '';
  var qtimes = '';
  var q_calculators = '';
  var q_desciption = '';
  var q_values = '';

  /*rs new */
  qtimes = $('.q_timesorginalvalue').val();
  if ($('.q_times').prop("checked") === true) {
    qtimes = qtimes;
  } else {
    qtimes = '';
  }
  if ($('.q_calculators').prop("checked") == true) {
    q_calculators = 1;
  } else {
    q_calculators = 0;
  }
  if (aQuizOption != 9)
  {
    if (CKEDITOR.instances['org_qeditor_1']) {
      q_desciption = CKEDITOR.instances['org_qeditor_1'].getData();
    } else {
      q_desciption = $('#org_qeditor_1').val();
    }
    q_values = $('.q_marks_1').val();
    if (q_values != '') {
      cansave = true;
    } else {
      cansave = false;
      $('.error').show();
      $('.error').html('Please add questions mark');
    }

  }

  /*rs new */

  switch (aQuizOption) {
        case 4: //multiple choice
        if ($("#answer").val() == '') {
                //errormsg_dlg('You didn\'t select any choice', 'Multiple Choice');
                $('.error').show();
                $('.error').html('You didn\'t select any choice');
                cansave = false;
              } else {
                cansave = true;
                var trows = $("#trows_number").val();
                mchoice_rows = trows;
                for (var i = 1; i <= trows; i++) {
                  var hasText = CKEDITOR.instances['choice_' + i].getData();
                  if (hasText) {
                    mchoices.push(hasText);
                    if (mchoice == '') {
                      mchoice = hasText;
                    } else {
                      mchoice = mchoice + '|' + hasText;
                    }
                  }
                }
                question_creative = '';
                if (mchoices.length != trows) {
                    //errormsg_dlg('You didn\'t select all choices', 'Multiple Choice');
                    $('.error').show();
                    $('.error').html('You didn\'t select all choices');
                    cansave = false;
                  } else {
                    cansave = true;
                  }
                }
                word_to_search = '';
                definition = '';
                parts_of_speech = '';
                synonym = '';
                antonym = '';
                near_antonym = '';
                orig_sentence = '';
                audio_file = '';
                video_file = '';
                vquestion = CKEDITOR.instances['question_editor'].getData();
                break;

        case 5: //multiple response
        if ($("#answer").val() == '') {
                //errormsg_dlg('You didn\'t select any response', 'Multiple Response');
                $('.error').show();
                $('.error').html('You didn\'t select any response');
                cansave = false;
              } else {
                cansave = true;
                var trows = $("#trows_number").val();
                mchoice_rows = trows;
                for (var i = 1; i <= trows; i++) {
                  var hasText = CKEDITOR.instances['choice_' + i].getData();
                  if (hasText) {
                    mchoices.push(hasText);
                    if (mchoice == '') {
                      mchoice = hasText;
                    } else {
                      mchoice = mchoice + '|' + hasText;
                    }
                  }
                }
                question_creative = '';
                if (mchoices.length != trows) {
                    //errormsg_dlg('You didn\'t select all response', 'Multiple Response');
                    $('.error').show();
                    $('.error').html('You didn\'t select all response');
                    cansave = false;
                  } else {
                    cansave = true;
                  }
                }
                word_to_search = '';
                definition = '';
                parts_of_speech = '';
                synonym = '';
                antonym = '';
                near_antonym = '';
                orig_sentence = '';
                audio_file = '';
                video_file = '';
                vquestion = CKEDITOR.instances['question_editor'].getData();
                break;

        case 7: //matching
        vquestion = $("#question_editor").val();
        if (vquestion == '') {
                //errormsg_dlg('Please enter Question', 'Matching');
                $('.error').show();
                $('.error').html('Please enter Question');
                cansave = false;
              } else {
                if ($("#answer").val() == '') {
                    //errormsg_dlg('You didn\'t select answer for Matching', 'Matching');
                    $('.error').show();
                    $('.error').html('You didn\'t select answer for Matching');
                    cansave = false;
                  } else {
                    cansave = true;
                    var trows = $("#trows_number").val();
                    mchoice_rows = trows;
                    answer_matching = $("#answer").val().split(',');
                    for (var i = 1; i <= trows; i++) {
                      var hasText = CKEDITOR.instances['left_matching_' + i].getData();
                      if (hasText) {
                        mchoices.push(hasText);
                        if (mchoice == '') {
                          mchoice = hasText;
                        } else {
                          mchoice = mchoice + '|' + hasText;
                        }
                      }
                        //for right side matching				
                        var hasTextr = CKEDITOR.instances['right_matching_' + i].getData();
                        if (hasTextr) {
                          mmatchings.push(hasTextr);
                          if (mmatching == '') {
                            mmatching = hasTextr;
                          } else {
                            mmatching = mmatching + '|' + hasTextr;
                          }
                        }
                        //check for actual answer
                        if ($("#chk_" + i + "").val() != '') {
                          mmatching_answer.push('c');
                        }
                      }
                      question_creative = '';
                      if (mchoices.length == trows) {
                        if (mmatchings.length == trows) {
                          if (mmatching_answer.length == trows) {
                            if (answer_matching.length == trows) {
                              cansave = true;
                            } else {
                                    //errormsg_dlg('You didn\'t generate all answers', 'Matching');
                                    $('.error').show();
                                    $('.error').html('You didn\'t generate all answers');
                                    cansave = false;
                                  }
                                } else {
                                //errormsg_dlg('You didn\'t select all answers', 'Matching');
                                $('.error').show();
                                $('.error').html('You didn\'t generate all answers');
                                cansave = false;
                              }
                            } else {
                            //errormsg_dlg('You didn\'t select all right side Matching', 'Matching');
                            $('.error').show();
                            $('.error').html('You didn\'t select all right side Matching');
                            cansave = false;
                          }
                        } else {
                        //errormsg_dlg('You didn\'t select all Left side Matching', 'Matching');
                        $('.error').show();
                        $('.error').html('You didn\'t select all Left side Matching');
                        cansave = false;
                      }
                    }
                  }
                  word_to_search = '';
                  definition = '';
                  parts_of_speech = '';
                  synonym = '';
                  antonym = '';
                  near_antonym = '';
                  orig_sentence = '';
                  audio_file = '';
                  video_file = '';
                  break;

        case 9: //assignment
//		   if ($("#answer").val() == ''){
//			   //messagebox('You didn\'t input answer', 'Assignment Score');
//			   $('.error').show();
//			   $('.error').html('You didn\'t input answer');
//			   cansave = false;
//		   }else{
  cansave = true;
  var totalsscores = 0;
            //  var trows = $("#trows_number").val();
            var trows = $("#mark_rows").val();
            mchoice_rows = trows;
            for (var i = 1; i <= trows; i++) {
                // var hasText = $("#score_description_" + i + "").val();
                if (CKEDITOR.instances['org_qeditor_' + i]) {
                  var hasText = CKEDITOR.instances['org_qeditor_' + i].getData();
                } else {
                  var hasText = $('#org_qeditor_' + i).val();
                }
                if (hasText) {
                  assign_scores_des.push(hasText);
                  if (assign_score_des == '') {
                    assign_score_des = hasText;
                  } else {
                    assign_score_des = assign_score_des + '|' + hasText;
                  }
                } else {
                  assign_score_des = assign_score_des + '|' + '';
                }
                //for score				
                var hasTextr = Number($(".q_marks_" + i + "").val());
                totalsscores += hasTextr;
                if (hasTextr) {
                  assign_scores.push(hasTextr);
                  if (assign_score == '') {
                    assign_score = hasTextr;
                  } else {
                    assign_score = assign_score + '|' + hasTextr;
                  }
                }

                var hasSlNo = $(".rsSL_no_" + i + "").val();

                if (hasSlNo) {
                  assign_slnos.push(hasSlNo);
                  if (assign_slno == '') {
                    assign_slno = hasSlNo;
                  } else {
                    assign_slno = assign_slno + '|' + hasSlNo;
                  }
                }


              }
              q_values = totalsscores;
              question_creative = '';
            //if (assign_scores_des.length == trows){
              if (assign_scores.length == trows) {
                cansave = true;
              } else {
                //errormsg_dlg('You didn\'t select all Score', 'Assignment Score');
                $('.error').show();
                $('.error').html('You didn\'t select all Score');
                cansave = false;
              }
            /*}else{
             errormsg_dlg('You didn\'t select all Score Description', 'Assignment Score');
             cansave = false;
           }*/
            //}		   
            word_to_search = '';
            definition = '';
            parts_of_speech = '';
            synonym = '';
            antonym = '';
            near_antonym = '';
            orig_sentence = '';
            audio_file = '';
            video_file = '';
            vquestion = CKEDITOR.instances['question_editor'].getData();
            break;

        case 10: //skip
        if ($("#answer").val() == '') {
                //errormsg_dlg('You didn\'t select any choice', 'Multiple Choice');
                $('.error').show();
                $('.error').html('You didn\'t select any choice');
                cansave = false;
              } else {
                cansave = true;
                var trows = $("#trows_number").val();
                mchoice_rows = trows;
                for (var i = 1; i <= trows; i++) {
                  var hasText = CKEDITOR.instances['choice_' + i].getData();
                  if (hasText) {
                    mchoices.push(hasText);
                    if (mchoice == '') {
                      mchoice = hasText;
                    } else {
                      mchoice = mchoice + '|' + hasText;
                    }
                  }
                }
                question_creative = '';
                if (mchoices.length != trows) {
                    //errormsg_dlg('You didn\'t select all choices', 'Multiple Choice');
                    $('.error').show();
                    $('.error').html('You didn\'t select all choices');
                    cansave = false;
                  } else {
                    cansave = true;
                  }
                }
                word_to_search = '';
                definition = '';
                parts_of_speech = '';
                synonym = '';
                antonym = '';
                near_antonym = '';
                orig_sentence = '';
                audio_file = '';
                video_file = '';
                vquestion = CKEDITOR.instances['question_editor'].getData();
                break;

        case 11: //creative
        if ($("#answer").val() == '') {
                //errormsg_dlg('You didn\'t input answer', 'Assignment Score');
                $('.error').show();
                $('.error').html('You didn\'t input answer');
                cansave = false;
              } else {
                cansave = true;
                var trows = $("#trows_number").val();
                mchoice_rows = trows;
                for (var i = 1; i <= trows; i++) {
                  var hasText = CKEDITOR.instances['choice_' + i].getData();
                  if (hasText) {
                    mchoices.push(hasText);
                    if (mchoice == '') {
                      mchoice = hasText;
                    } else {
                      mchoice = mchoice + '|' + hasText;
                    }
                  }
                }
                question_creative = $("#question_creative").val();

                if (mchoices.length == trows) {
                  if (question_creative != '') {
                    cansave = true;
                  } else {
                        //errormsg_dlg('You didn\'t enter question on Creative', 'Creative');
                        $('.error').show();
                        $('.error').html('You didn\'t enter question on Creative');
                        cansave = false;
                        $("#question_creative").focus();
                      }
                    } else {
                    //errormsg_dlg('You didn\'t select all choices', 'Creative');
                    $('.error').show();
                    $('.error').html('You didn\'t select all choices');
                    cansave = false;
                  }
                }
                word_to_search = '';
                definition = '';
                parts_of_speech = '';
                synonym = '';
                antonym = '';
                near_antonym = '';
                orig_sentence = '';
                audio_file = '';
                video_file = '';
                vquestion = CKEDITOR.instances['question_editor'].getData();
                break;

        case 8: //vocabulary
        var trows = $("#trows_number").val();
        mchoice_rows = trows;
        for (var i = 1; i <= trows; i++) {
          var hasText = CKEDITOR.instances['choice_' + i].getData();
          if (hasText) {
            mchoices.push(hasText);
            if (mchoice == '') {
              mchoice = hasText;
            } else {
              mchoice = mchoice + '|' + hasText;
            }
          }
        }
        question_creative = '';
        word_to_search = $("#word_to_search").val();
        definition = $("#definition").val();
        parts_of_speech = $("#parts_of_speech").val();
        synonym = $("#synonym").val();
        antonym = $("#antonym").val();
        near_antonym = $("#near_antonym").val();
        orig_sentence = CKEDITOR.instances['orig_sentence_editor'].getData();
        audio_file = CKEDITOR.instances['audio_file_editor'].getData();
        video_file = CKEDITOR.instances['video_file_editor'].getData();
        var theQuestion = 'Word: ' + word_to_search + '<br>';
        theQuestion = theQuestion + 'Definition: ' + definition + '<br>';
        theQuestion = theQuestion + 'Parts of speech: ' + parts_of_speech + '<br>';
        theQuestion = theQuestion + 'Synonym: ' + synonym + '<br>';
        theQuestion = theQuestion + 'Antonym: ' + antonym + '<br>';
        theQuestion = theQuestion + 'Your Sentence: ' + orig_sentence;
        vquestion = theQuestion;
        if (word_to_search != '') {
          if (definition != '') {
            if (parts_of_speech != '') {
              if (synonym != '') {
                if (antonym != '') {
                  if (orig_sentence != '') {
                    if (mchoices.length == trows) {
                      if ($("#answer").val() != '') {
                        cansave = true;
                      } else {
                        cansave = false;
                                            //messageboxWithFocus('Please enter answer', 'Vocabulary', 'answer');
                                            $('.error').show();
                                            $('.error').html('Please enter answer');
                                            $("#answer").focus();
                                          }
                                        } else {
                                          cansave = false;
                                        //errormsg_dlg('You didn\'t select all image', 'Vocabulary');
                                        $('.error').show();
                                        $('.error').html('You didn\'t select all image');
                                        //$("#answer").focus();									  
                                      }
                                    } else {
                                      cansave = false;
                                    //errormsg_dlg('Please enter your sentence', 'Vocabulary');
                                    $('.error').show();
                                    $('.error').html('Please enter sentence');
                                    //$("#answer").focus();								   
                                  }
                                } else {
                                  cansave = false;
                                //messageboxWithFocus('Please enter Antonym', 'Vocabulary', 'antonym');
                                $('.error').show();
                                $('.error').html('Please enter Antonym');
                                $("#antonym").focus();
                              }
                            } else {
                              cansave = false;
                            //messageboxWithFocus('Please enter Synonym', 'Vocabulary', 'synonym');
                            $('.error').show();
                            $('.error').html('Please enter Synonym');
                            $("#synonym").focus();
                          }
                        } else {
                          cansave = false;
                        //messageboxWithFocus('Please enter Parts of Speech', 'Vocabulary', 'parts_of_speech');
                        $('.error').show();
                        $('.error').html('Please enter Parts of Speech');
                        $("#parts_of_speech").focus();
                      }
                    } else {
                      cansave = false;
                    //messageboxWithFocus('Please enter Definition', 'Vocabulary', 'definition');
                    $('.error').show();
                    $('.error').html('Please enter Definition');
                    $("#definition").focus();
                  }
                } else {
                  cansave = false;
                //messageboxWithFocus('Please enter Word', 'Vocabulary', 'word_to_search');
                $('.error').show();
                $('.error').html('Please enter Word');
                $("#word_to_search").focus();
              }
              break;


              default:
              cansave = true;
              mchoice_rows = '';
              mchoice = '';
              mmatching = '';
              assign_score = '';
              assign_score_des = '';
              assign_slno = '';
              question_creative = '';
              word_to_search = '';
              definition = '';
              parts_of_speech = '';
              synonym = '';
              antonym = '';
              near_antonym = '';
              orig_sentence = '';
              audio_file = '';
              video_file = '';
              vquestion = CKEDITOR.instances['question_editor'].getData();
              break;
            }

            if (cansave) {
              if (sessionAvail() == 'f') {
                if (isOnline() == 't') {
                  $.ajax({
                    type: 'POST',
                    url: aController,
                    async: false,
                    data: {year_grade: $("#year_grade").val(),
                    subject_name: $("#subject_name").val(),
                    chapter_name: $("#chapter_name").val(),
                    modules: $("#modules").val(),
                    score_value: q_values,
                    time_elapse: qtimes,
                    question: vquestion,
                    solution: vsolution,
                    answer: $("#answer").val(),
                    solution_by_student: $("#solution_by_student").val(),
                    answer_by_student: $("#answer_by_student").val(),
                    selected_equation: $("#selected_equation").val(),
                    choice_rows: mchoice_rows,
                    choices: mchoice,
                    quizoption: aQuizOption,
                    right_matching: mmatching,
                    assign_score_des: assign_score_des,
                    assign_score: assign_score,
                    assign_slno: assign_slno,
                    question_creative: question_creative,
                    word_to_search: word_to_search,
                    definition: definition,
                    parts_of_speech: parts_of_speech,
                    synonym: synonym,
                    antonym: antonym,
                    near_antonym: near_antonym,
                    orig_sentence: orig_sentence,
                    audio_file: audio_file,
                    video_file: video_file,
                    rsModulesId: m_id,
                    q_calculators: q_calculators,
                    q_desciption: q_desciption},
                    beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (m_id != 0) {
                            window.location.href = '/module-edit/' + m_id;
                          } else {
                            window.location.href = result.response;
                          }
                        } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            /*$("#td_error").show();
                            $("#td_error").html(result.error);*/
                            $('.error').show();
                            $('.error').html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Payment Option.');
                        },
                        dataType: "json"
                      });
                } else {
                  window.location.href = '/';
                }
              } else {
                MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
              }
            }
          }

          function fn_save_question(aController, aForm, aQuiz, aQuizOption, questionsId, m_id) {


            m_id = m_id || 0;
            var cansave;
            var mchoice_rows;
            var mchoice = '';
            var mchoices = new Array();
            var mmatchings = new Array();
            var mmatching_answer = new Array();
            var answer_matching = new Array();
            var mmatching = '';
            var assign_score = '';
            var assign_scores = new Array();
            var assign_score_des = '';
            var assign_scores_des = new Array();
            var assign_slno = '';
            var assign_slnos = new Array();
            var question_creative = '';
            if (CKEDITOR.instances['solution_editor']) {
              vsolution = CKEDITOR.instances['solution_editor'].getData();
            } else {
              vsolution = $('#solution_editor').val();
            }
            var word_to_search = '';
            var definition = '';
            var parts_of_speech = '';
            var synonym = '';
            var antonym = '';
            var near_antonym = '';
            var orig_sentence = '';
            var audio_file = '';
            var video_file = '';
            var vquestion = '';
            var qtimes = '';
            var q_calculators = '';
            var q_desciption = '';
            var q_values = '';
	//skip item
	var skip_all_items = '';
	var skip_answer_items = '';
	var skip_question_items = '';
	var skip_col = '';
	var skip_row = '';
	var skip_col_row = '';
	var ans_itm = new Array();
  /*rs new */
  qtimes = $('.q_timesorginalvalue').val();
  if ($('.q_times').prop("checked") === true) {
    qtimes = qtimes;
  } else {
    qtimes = '';
  }
  if ($('.q_calculators').prop("checked") == true) {
    q_calculators = 1;
  } else {
    q_calculators = 0;
  }
  if (aQuizOption != 9)
  {
    if (CKEDITOR.instances['org_qeditor_1']) {
      q_desciption = CKEDITOR.instances['org_qeditor_1'].getData();
    } else {
      q_desciption = $('#org_qeditor_1').val();
    }
    q_values = $('.q_marks_1').val();
    if (q_values != '') {
      cansave = true;
    } else {
      cansave = false;
      $('.error').show();
      $('.error').html('Please add questions mark');
    }

  }

  /*rs new */

  switch (aQuizOption) {
        case 4: //multiple choice
        if ($("#answer").val() == '') {
                //errormsg_dlg('You didn\'t select any choice', 'Multiple Choice');
                $('.error').show();
                $('.error').html('You didn\'t select any choice');
                cansave = false;
              } else {
                cansave = true;
                var trows = $("#trows_number").val();
                mchoice_rows = trows;
                for (var i = 1; i <= trows; i++) {
                  var hasText = CKEDITOR.instances['choice_' + i].getData();
                  if (hasText) {
                    mchoices.push(hasText);
                    if (mchoice == '') {
                      mchoice = hasText;
                    } else {
                      mchoice = mchoice + '|' + hasText;
                    }
                  }
                }
                question_creative = '';
                if (mchoices.length != trows) {
                    //errormsg_dlg('You didn\'t select all choices', 'Multiple Choice');
                    $('.error').show();
                    $('.error').html('You didn\'t select all choices');
                    cansave = false;
                  } else {
                    cansave = true;
                  }
                }
                word_to_search = '';
                definition = '';
                parts_of_speech = '';
                synonym = '';
                antonym = '';
                near_antonym = '';
                orig_sentence = '';
                audio_file = '';
                video_file = '';
                vquestion = CKEDITOR.instances['question_editor'].getData();
                break;

        case 5: //multiple response
        if ($("#answer").val() == '') {
                //errormsg_dlg('You didn\'t select any response', 'Multiple Response');
                $('.error').show();
                $('.error').html('You didn\'t select any response');
                cansave = false;
              } else {
                cansave = true;
                var trows = $("#trows_number").val();
                mchoice_rows = trows;
                for (var i = 1; i <= trows; i++) {
                  var hasText = CKEDITOR.instances['choice_' + i].getData();
                  if (hasText) {
                    mchoices.push(hasText);
                    if (mchoice == '') {
                      mchoice = hasText;
                    } else {
                      mchoice = mchoice + '|' + hasText;
                    }
                  }
                }
                question_creative = '';
                if (mchoices.length != trows) {
                    //errormsg_dlg('You didn\'t select all response', 'Multiple Response');
                    $('.error').show();
                    $('.error').html('You didn\'t select all response');
                    cansave = false;
                  } else {
                    cansave = true;
                  }
                }
                word_to_search = '';
                definition = '';
                parts_of_speech = '';
                synonym = '';
                antonym = '';
                near_antonym = '';
                orig_sentence = '';
                audio_file = '';
                video_file = '';
                vquestion = CKEDITOR.instances['question_editor'].getData();
                break;

        case 7: //matching
        vquestion = $("#question_editor").val();
        if (vquestion == '') {
                //errormsg_dlg('Please enter Question', 'Matching');
                $('.error').show();
                $('.error').html('Please enter Question');
                cansave = false;
              } else {
                if ($("#answer").val() == '') {
                    //errormsg_dlg('You didn\'t select answer for Matching', 'Matching');
                    $('.error').show();
                    $('.error').html('You didn\'t select answer for Matching');
                    cansave = false;
                  } else {
                    cansave = true;
                    var trows = $("#trows_number").val();
                    mchoice_rows = trows;
                    answer_matching = $("#answer").val().split(',');
                    for (var i = 1; i <= trows; i++) {
                      var hasText = CKEDITOR.instances['left_matching_' + i].getData();
                      if (hasText) {
                        mchoices.push(hasText);
                        if (mchoice == '') {
                          mchoice = hasText;
                        } else {
                          mchoice = mchoice + '|' + hasText;
                        }
                      }
                        //for right side matching				
                        var hasTextr = CKEDITOR.instances['right_matching_' + i].getData();
                        if (hasTextr) {
                          mmatchings.push(hasTextr);
                          if (mmatching == '') {
                            mmatching = hasTextr;
                          } else {
                            mmatching = mmatching + '|' + hasTextr;
                          }
                        }
                        //check for actual answer
                        if ($("#chk_" + i + "").val() != '') {
                          mmatching_answer.push('c');
                        }
                      }
                      question_creative = '';
                      if (mchoices.length == trows) {
                        if (mmatchings.length == trows) {
                          if (mmatching_answer.length == trows) {
                            if (answer_matching.length == trows) {
                              cansave = true;
                            } else {
                                    //errormsg_dlg('You didn\'t generate all answers', 'Matching');
                                    $('.error').show();
                                    $('.error').html('You didn\'t generate all answers');
                                    cansave = false;
                                  }
                                } else {
                                //errormsg_dlg('You didn\'t select all answers', 'Matching');
                                $('.error').show();
                                $('.error').html('You didn\'t generate all answers');
                                cansave = false;
                              }
                            } else {
                            //errormsg_dlg('You didn\'t select all right side Matching', 'Matching');
                            $('.error').show();
                            $('.error').html('You didn\'t select all right side Matching');
                            cansave = false;
                          }
                        } else {
                        //errormsg_dlg('You didn\'t select all Left side Matching', 'Matching');
                        $('.error').show();
                        $('.error').html('You didn\'t select all Left side Matching');
                        cansave = false;
                      }
                    }
                  }
                  word_to_search = '';
                  definition = '';
                  parts_of_speech = '';
                  synonym = '';
                  antonym = '';
                  near_antonym = '';
                  orig_sentence = '';
                  audio_file = '';
                  video_file = '';
                  break;

        case 9: //assignment
//		   if ($("#answer").val() == ''){
//			   //messagebox('You didn\'t input answer', 'Assignment Score');
//			   $('.error').show();
//			   $('.error').html('You didn\'t input answer');
//			   cansave = false;
//		   }else{
  cansave = true;
  var totalsscores = 0;
            //  var trows = $("#trows_number").val();
            var trows = $("#mark_rows").val();
            mchoice_rows = trows;
            for (var i = 1; i <= trows; i++) {
                // var hasText = $("#score_description_" + i + "").val();
                if (CKEDITOR.instances['org_qeditor_' + i]) {
                  var hasText = CKEDITOR.instances['org_qeditor_' + i].getData();
                } else {
                  var hasText = $('#org_qeditor_' + i).val();
                }
                if (hasText) {
                  assign_scores_des.push(hasText);
                  if (assign_score_des == '') {
                    assign_score_des = hasText;
                  } else {
                    assign_score_des = assign_score_des + '|' + hasText;
                  }
                } else {
                  assign_score_des = assign_score_des + '|' + '';
                }
                //for score				
                var hasTextr = Number($(".q_marks_" + i + "").val());
                totalsscores += hasTextr;
                if (hasTextr) {
                  assign_scores.push(hasTextr);
                  if (assign_score == '') {
                    assign_score = hasTextr;
                  } else {
                    assign_score = assign_score + '|' + hasTextr;
                  }
                }

                var hasSlNo = $(".rsSL_no_" + i + "").val();

                if (hasSlNo) {
                  assign_slnos.push(hasSlNo);
                  if (assign_slno == '') {
                    assign_slno = hasSlNo;
                  } else {
                    assign_slno = assign_slno + '|' + hasSlNo;
                  }
                }


              }
              q_values = totalsscores;
              question_creative = '';
            //if (assign_scores_des.length == trows){
              if (assign_scores.length == trows) {
                cansave = true;
              } else {
                //errormsg_dlg('You didn\'t select all Score', 'Assignment Score');
                $('.error').show();
                $('.error').html('You didn\'t select all Score');
                cansave = false;
              }
            /*}else{
             errormsg_dlg('You didn\'t select all Score Description', 'Assignment Score');
             cansave = false;
           }*/
            //}		   
            word_to_search = '';
            definition = '';
            parts_of_speech = '';
            synonym = '';
            antonym = '';
            near_antonym = '';
            orig_sentence = '';
            audio_file = '';
            video_file = '';
            vquestion = CKEDITOR.instances['question_editor'].getData();
            break;

        case 10: //skip

        skip_all_items = '';
        skip_answer_items = '';
        skip_question_items = '';
        skip_col = $('#trows_number').val();
        skip_row = $('#tcolumns_number').val();
        skip_col_row = skip_col+'_'+skip_row;
        ans_itm = [];
        var skip_all_item = '';
        var skip_a_item = '';
        var skip_q_item = '';
        $('.rsskpin').each(function(){
          if(skip_col!='' && skip_row!='' && $(this).val()!=''){
           skip_all_item +=$(this).val()+',';
         }
       });
        $('.skp_a_type').each(function(){
          skip_a_item +=$(this).val()+',';
        });
        $('.skp_q_type').each(function(){
          skip_q_item +=$(this).val()+',';
        });
        if(skip_all_item==''){
          $('.error').show();
          $('.error').html('You didn\'t select any choice');
          cansave = false;
        }else{
          cansave = true;
        }
        skip_all_items = skip_all_item.substring(0,skip_all_item.length - 1);
        skip_answer_items = skip_a_item.substring(0,skip_a_item.length - 1);
        skip_question_items = skip_q_item.substring(0,skip_q_item.length - 1);
        $('#answer').val(skip_answer_items);
        word_to_search = '';
        definition = '';
        parts_of_speech = '';
        synonym = '';
        antonym = '';
        near_antonym = '';
        orig_sentence = '';
        audio_file = '';
        video_file = '';
        vquestion = CKEDITOR.instances['question_editor'].getData();
        break;

        case 11: //creative
        if ($("#answer").val() == '') {
                //errormsg_dlg('You didn\'t input answer', 'Assignment Score');
                $('.error').show();
                $('.error').html('You didn\'t input answer');
                cansave = false;
              } else {
                cansave = true;
                var trows = $("#trows_number").val();
                mchoice_rows = trows;
                for (var i = 1; i <= trows; i++) {
                  var hasText = CKEDITOR.instances['choice_' + i].getData();
                  if (hasText) {
                    mchoices.push(hasText);
                    if (mchoice == '') {
                      mchoice = hasText;
                    } else {
                      mchoice = mchoice + '|' + hasText;
                    }
                  }
                }
                question_creative = $("#question_creative").val();

                if (mchoices.length == trows) {
                  if (question_creative != '') {
                    cansave = true;
                  } else {
                        //errormsg_dlg('You didn\'t enter question on Creative', 'Creative');
                        $('.error').show();
                        $('.error').html('You didn\'t enter question on Creative');
                        cansave = false;
                        $("#question_creative").focus();
                      }
                    } else {
                    //errormsg_dlg('You didn\'t select all choices', 'Creative');
                    $('.error').show();
                    $('.error').html('You didn\'t select all choices');
                    cansave = false;
                  }
                }
                word_to_search = '';
                definition = '';
                parts_of_speech = '';
                synonym = '';
                antonym = '';
                near_antonym = '';
                orig_sentence = '';
                audio_file = '';
                video_file = '';
                vquestion = CKEDITOR.instances['question_editor'].getData();
                break;

        case 8: //vocabulary
        var trows = $("#trows_number").val();
        mchoice_rows = trows;
        for (var i = 1; i <= trows; i++) {
          var hasText = CKEDITOR.instances['choice_' + i].getData();
          if (hasText) {
            mchoices.push(hasText);
            if (mchoice == '') {
              mchoice = hasText;
            } else {
              mchoice = mchoice + '|' + hasText;
            }
          }
        }
        question_creative = '';
        word_to_search = $("#word_to_search").val();
        definition = $("#definition").val();
        parts_of_speech = $("#parts_of_speech").val();
        synonym = $("#synonym").val();
        antonym = $("#antonym").val();
        near_antonym = $("#near_antonym").val();
        orig_sentence = CKEDITOR.instances['orig_sentence_editor'].getData();
        audio_file = CKEDITOR.instances['audio_file_editor'].getData();
        video_file = CKEDITOR.instances['video_file_editor'].getData();
        var theQuestion = 'Word: ' + word_to_search + '<br>';
        theQuestion = theQuestion + 'Definition: ' + definition + '<br>';
        theQuestion = theQuestion + 'Parts of speech: ' + parts_of_speech + '<br>';
        theQuestion = theQuestion + 'Synonym: ' + synonym + '<br>';
        theQuestion = theQuestion + 'Antonym: ' + antonym + '<br>';
        theQuestion = theQuestion + 'Your Sentence: ' + orig_sentence;
        vquestion = theQuestion;
        if (word_to_search != '') {
          if (definition != '') {
            if (parts_of_speech != '') {
              if (synonym != '') {
                if (antonym != '') {
                  if (orig_sentence != '') {
                    if (mchoices.length == trows) {
                      if ($("#answer").val() != '') {
                        cansave = true;
                      } else {
                        cansave = false;
                                            //messageboxWithFocus('Please enter answer', 'Vocabulary', 'answer');
                                            $('.error').show();
                                            $('.error').html('Please enter answer');
                                            $("#answer").focus();
                                          }
                                        } else {
                                          cansave = false;
                                        //errormsg_dlg('You didn\'t select all image', 'Vocabulary');
                                        $('.error').show();
                                        $('.error').html('You didn\'t select all image');
                                        //$("#answer").focus();									  
                                      }
                                    } else {
                                      cansave = false;
                                    //errormsg_dlg('Please enter your sentence', 'Vocabulary');
                                    $('.error').show();
                                    $('.error').html('Please enter sentence');
                                    //$("#answer").focus();								   
                                  }
                                } else {
                                  cansave = false;
                                //messageboxWithFocus('Please enter Antonym', 'Vocabulary', 'antonym');
                                $('.error').show();
                                $('.error').html('Please enter Antonym');
                                $("#antonym").focus();
                              }
                            } else {
                              cansave = false;
                            //messageboxWithFocus('Please enter Synonym', 'Vocabulary', 'synonym');
                            $('.error').show();
                            $('.error').html('Please enter Synonym');
                            $("#synonym").focus();
                          }
                        } else {
                          cansave = false;
                        //messageboxWithFocus('Please enter Parts of Speech', 'Vocabulary', 'parts_of_speech');
                        $('.error').show();
                        $('.error').html('Please enter Parts of Speech');
                        $("#parts_of_speech").focus();
                      }
                    } else {
                      cansave = false;
                    //messageboxWithFocus('Please enter Definition', 'Vocabulary', 'definition');
                    $('.error').show();
                    $('.error').html('Please enter Definition');
                    $("#definition").focus();
                  }
                } else {
                  cansave = false;
                //messageboxWithFocus('Please enter Word', 'Vocabulary', 'word_to_search');
                $('.error').show();
                $('.error').html('Please enter Word');
                $("#word_to_search").focus();
              }
              break;


              default:
              cansave = true;
              mchoice_rows = '';
              mchoice = '';
              mmatching = '';
              assign_score = '';
              assign_score_des = '';
              assign_slno = '';
              question_creative = '';
              word_to_search = '';
              definition = '';
              parts_of_speech = '';
              synonym = '';
              antonym = '';
              near_antonym = '';
              orig_sentence = '';
              audio_file = '';
              video_file = '';
              vquestion = CKEDITOR.instances['question_editor'].getData();

              skip_all_items='';
              skip_answer_items='';
              skip_question_items='';
              skip_col_row='';

              break;
            }

            if (cansave) {
              if (sessionAvail() == 'f') {
                if (isOnline() == 't') {
                  $.ajax({
                    type: 'POST',
                    url: aController,
                    async: false,
                    data: {year_grade: $("#year_grade").val(),
                    subject_name: $("#subject_name").val(),
                    chapter_name: $("#chapter_name").val(),
                    modules: $("#modules").val(),
                    score_value: q_values,
                    time_elapse: qtimes,
                    question: vquestion,
                    solution: vsolution,
                    answer: $("#answer").val(),
                    solution_by_student: $("#solution_by_student").val(),
                    answer_by_student: $("#answer_by_student").val(),
                    selected_equation: $("#selected_equation").val(),
                    choice_rows: mchoice_rows,
                    choices: mchoice,
                    quizoption: aQuizOption,
                    quiz_cat_id: aQuiz,
                    right_matching: mmatching,
                    assign_score_des: assign_score_des,
                    assign_score: assign_score,
                    assign_slno: assign_slno,
                    question_creative: question_creative,
                    word_to_search: word_to_search,
                    definition: definition,
                    parts_of_speech: parts_of_speech,
                    synonym: synonym,
                    antonym: antonym,
                    near_antonym: near_antonym,
                    orig_sentence: orig_sentence,
                    audio_file: audio_file,
                    video_file: video_file,
                    rsModulesId: m_id,
                    q_calculators: q_calculators,
                    q_desciption: q_desciption,
                    skip_all_items:skip_all_items,
                    skip_answer_items:skip_answer_items,
                    skip_question_items:skip_question_items,
                    skip_col_row:skip_col_row},
                    beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (m_id != 0) {
                            window.location.href = '/module-edit/' + m_id;
                          } else {

                            messageboxcorrect('Save successfuly', 'Question', result.response);
                                //window.location.href = result.response;

                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            /*$("#td_error").show();
                            $("#td_error").html(result.error);*/
                            $('.error').show();
                            $('.error').html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Payment Option.');
                        },
                        dataType: "json"
                      });
                } else {
                  window.location.href = '/';
                }
              } else {
                MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
              }
            }
          }



          function fn_show_solutions() {
            if (sessionAvail() == 'f') {
              if (isOnline() == 't') {
                $("#dlg_solution").dialog({
                  resizable: false,
                  modal: false,
                  closeOnEscape: false,
                  title: 'Solution',
                //open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                height: 480,
                width: 480,
                position: 'center',
                zIndex: -1,
                open: function () {
                  $(".ui-dialog-titlebar-close").hide();
                  CKEDITOR.replace( 'solution_editor', {
                    extraPlugins: 'onchange,simpleuploads,ckeditor_wiris,colorbutton,font',
                    height: 200,
                    filebrowserBrowseUrl: '/browser/browse.php?type=Images',
                    filebrowserUploadUrl: '/welcome/uploadImage?type=Files',
                    allowedContent: true
                  });
                },
                Save: function () {
                  $('#solution_editor').each(function () {
                    $(this).ckeditorGet().destroy();
                  });
                },
                buttons: {
                  Save: function () {
                    $(".ui-dialog-titlebar").show();
                    $(this).dialog("close");
                  }
                }
              });
              } else {
                window.location.href = '/';
              }
            } else {
              MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
            }
          }

          function fn_show_answer() {
            if (sessionAvail() == 'f') {
              if (isOnline() == 't') {
                $("#dlg_answer").dialog({
                  resizable: false,
                  modal: false,
                  closeOnEscape: false,
                  title: 'Answer',
                //open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                height: 480,
                width: 480,
                position: 'center',
                zIndex: -1,
                open: function () {
                  $(".ui-dialog-titlebar-close").hide();
                  CKEDITOR.replace( 'answer_editor', {
                    extraPlugins: 'onchange,simpleuploads,ckeditor_wiris,colorbutton,font',
                    height: 200,
                    filebrowserBrowseUrl : '/browser/browse.php?type=Images',
                    filebrowserUploadUrl : '/welcome/uploadFiles?type=Files',
                    allowedContent: true
                  });
                },
                close: function () {
                  $('#answer_editor').each(function () {
                    $(this).ckeditorGet().destroy();
                  });
                },
                buttons: {
                  Close: function () {
                    $(".ui-dialog-titlebar").show();
                    $(this).dialog("close");
                  }
                }
              });
              } else {
                window.location.href = '/';
              }
            } else {
              MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
            }
          }

          function fn_gen_matching_answer() {
            var totalRows = $("#trows_number").val();
            var answers = new Array();
            for (var i = 1; i <= totalRows; i++) {
              if ($("#chk_" + i + "").val() != '') {
                answers.push($("#chk_" + i + "").val());
              }
            }
            $("#answer").val(answers.toString());
          }


          function getAnswerQuestionCategory() {
            if (sessionAvail() == 'f') {
              if (isOnline() == 't') {
                var uri = window.location.pathname.substring(9);
                var questionid = uri.substring(2);
                questionid = questionid.substr(0, questionid.indexOf('/'));
                var categoryid = '';
                $.ajax({
                  type: 'POST',
                  url: 'WhiteBoard/getQuestionCategory',
                  data: {question: questionid},
                  async: false,
                  beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      categoryid = result.response;
                    } else {
                      categoryid = '';
                      errormsg_dlg(result.error, 'Error in gettting Question Category.');
                    }
                  },

                  error: function (xhr, textStatus, error) {
                    errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in gettting Question Category.');
                  },
                  dataType: "json"
                });
                return categoryid;
              } else {
                window.location.href = '/';
              }
            } else {
              MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
            }
          }

          var vworkout_text = '';
          var fanswerControl = '';

          function getStudentAnswer() {
            aQuizOption = $("#quiz_category").val();
            var vanswer = '';
            switch (aQuizOption) {
              case '1':
              vanswer = $("#answer").val();
            //fanswerControl = $("#cke_answer_editor");
            fanswerControl = $('.editor_holder');
            vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();//$("#workout").val();
            break;

            case '2':
            vanswer = $("#answer").val();
            //fanswerControl = $("#answer");
            fanswerControl = $(".answer");
            vworkout_text = '';
            break;

            case '3':
            vanswer = $("#answer").val();
            //fanswerControl = $("#cke_answer_editor");
            fanswerControl = $('.editor_holder');
            vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();//$("#workout").val();
            break;

        case '4': //multiple choice
        vanswer = $("#answer").val();
        fanswerControl = $(".editor_holder_choices");
        vworkout_text = '';
        break;

        case '5': //multiple response
        vanswer = $("#answer").val();
        fanswerControl = $(".editor_holder_choices");
        vworkout_text = '';
            /*var trows = $("#rows_number").val();
             mchoice_rows = trows;
             for (var i = 1; i <= trows; i++){
             if ($("#chk_" + i).prop('checked')){
             hasText = i;
             }
             if (hasText){
             if (vanswer == ''){
             vanswer = hasText;
             }else{
             vanswer = vanswer + ',' + hasText;
             }
             }
           }*/
           break;

        case '7': //matching
        var totalRows = $("#rows_number").val();
        var answers = new Array();
        for (var i = 1; i <= totalRows; i++) {
          if ($("#chk_" + i + "").val() != '') {
            answers.push($("#chk_" + i + "").val());
          }
        }
        $("#answer").val(answers.toString());
        vanswer = $("#answer").val();
        fanswerControl = $(".editor_holder_choices");
        vworkout_text = '';
        break;

        case '9': //assignment
        vanswer = $("#answer").val();
            //fanswerControl = $("#answer");
            fanswerControl = $(".answer");
            vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();//$("#workout").val();
            break;

        case '10': //skip
        $('.skp_a_type').each(function(){
          u_ans_items += $(this).val()+',';
        });
        u_ans_item = u_ans_items.substring(0,u_ans_items.length - 1);

        $("#answer").val(u_ans_item);
        vanswer = $("#answer").val();
        fanswerControl = $(".editor_holder_choices");
        vworkout_text = '';
        break;

        case '11': //creative
        vanswer = $("#answer").val();
        fanswerControl = $(".editor_holder_choices");
        vworkout_text = '';
        break;

        case '8': //vocabulary
        vanswer = $("#answer").val();
            //fanswerControl = $("#answer");
            fanswerControl = $(".answers");
            vworkout_text = '';
            break;


            default:
            vanswer = CKEDITOR.instances['answer_editor'].getData();
            fanswerControl = $("#cke_answer_editor");//CKEDITOR.instances['answer_editor'];//$("#answer_editor");
            vworkout_text = '';
            break;
          }
    //CKEDITOR.instances['answer_editor'].getData();
    return vanswer;
  }


  var vworkout_text_sp = '';
  var fanswerControl_sp = '';

  function getStudentAnswer_check_sp(is_need_ans) {
    aQuizOption = $("#quiz_category").val();
    var vanswer = '';
    switch (aQuizOption) {
      case '1':
      vanswer = $("#answer").val();
            //fanswerControl = $("#cke_answer_editor");
            fanswerControl_sp = $('.editor_holder');
            vworkout_text_sp = CKEDITOR.instances['workout_editor_draw'].getData();//$("#workout").val();
            break;

            case '2':
            vanswer = $("#answer").val();
            //fanswerControl = $("#answer");
            fanswerControl_sp = $(".answer");
            vworkout_text_sp = '';
            break;

            case '3':
            vanswer = $("#answer").val();
            //fanswerControl = $("#cke_answer_editor");
            fanswerControl_sp = $('.editor_holder');
            vworkout_text_sp = CKEDITOR.instances['workout_editor_draw'].getData();//$("#workout").val();
            break;

        case '4': //multiple choice
        vanswer = $("#answer").val();
        fanswerControl_sp = $(".editor_holder_choices");
        vworkout_text_sp = '';
        break;

        case '5': //multiple response
        vanswer = $("#answer").val();
        fanswerControl_sp = $(".editor_holder_choices");
        vworkout_text_sp = '';
            /*var trows = $("#rows_number").val();
             mchoice_rows = trows;
             for (var i = 1; i <= trows; i++){
             if ($("#chk_" + i).prop('checked')){
             hasText = i;
             }
             if (hasText){
             if (vanswer == ''){
             vanswer = hasText;
             }else{
             vanswer = vanswer + ',' + hasText;
             }
             }
           }*/
           break;

        case '7': //matching
        var totalRows = $("#rows_number").val();
        var answers = new Array();
        for (var i = 1; i <= totalRows; i++) {
          if ($("#chk_" + i + "").val() != '') {
            answers.push($("#chk_" + i + "").val());
          }
        }
        $("#answer").val(answers.toString());
        vanswer = $("#answer").val();
        fanswerControl_sp = $(".editor_holder_choices");
        vworkout_text_sp = '';
        break;

        case '9': //assignment
        if (is_need_ans == 0) {
          vanswer = '<p>&nbsp;</p>';
        } else {
          vanswer = $("#answer").val();
        }
            //fanswerControl = $("#answer");
            fanswerControl_sp = $(".answer");
            vworkout_text_sp = CKEDITOR.instances['workout_editor_draw'].getData();//$("#workout").val();
            break;

        case '10': //skip

        $('.skp_a_type').each(function(){
          u_ans_items += $(this).val()+',';
        });
        u_ans_item = u_ans_items.substring(0,u_ans_items.length - 1);

        $("#answer").val(u_ans_item);
        vanswer = $("#answer").val();
        fanswerControl_sp = $(".editor_holder_choices");
        vworkout_text_sp = '';
        break;

        case '11': //creative
        vanswer = $("#answer").val();
        fanswerControl_sp = $(".editor_holder_choices");
        vworkout_text_sp = '';
        break;

        case '8': //vocabulary
        vanswer = $("#answer").val();
            //fanswerControl = $("#answer");
            fanswerControl_sp = $(".answers");
            vworkout_text_sp = '';
            break;


            default:
            vanswer = CKEDITOR.instances['answer_editor'].getData();
            fanswerControl_sp = $("#cke_answer_editor");//CKEDITOR.instances['answer_editor'];//$("#answer_editor");
            vworkout_text_sp = '';
            break;
          }
    //CKEDITOR.instances['answer_editor'].getData();
    return vanswer;
  }



  function fn_play_sound_answer(afile, atitle) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
        var src = '/files/audio/' + afile;
        var acontent = fn_sound_content(atitle, src);
        var NewDialog = $('<div id="play_dialog">\
          <p>' + acontent + '</p>\
          </div>');
            //$("#audio_file").val(src);
            NewDialog.dialog({
              open: function (event, ui) {
                vSoundPlayed = 1;
                $("#audio_player").jPlayer({
                  ready: function (event) {
                    $(this).jPlayer("setMedia", {
                      mp3: src
                    }).jPlayer("play");
                  },
                  swfPath: "/js",
                  supplied: "mp3",
                  wmode: "window",
                  smoothPlayBar: true,
                  keyEnabled: true
                });
              },
              beforeClose: function (event, ui) {
                $("#audio_player").jPlayer("destroy");
              },
              close: function (event, ui) {
                vSoundPlayed = 1;
              },
              position: ['left', 277],
              modal: false,
              title: 'Play Audio',
                //open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                height: 'auto',
                width: 'auto'
              });
          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }

      function fn_textToSpeech(atext) {


        meSpeak.speak(atext, {amplitude: 100, wordgap: 0, pitch: 50, speed: 175, variant: ''});
        vSoundPlayed = 1;


      }


      function fn_next_question(atutor, atask, amodule, aquestion, aprevquestion, atype) {
        if (sessionAvail() == 'f') {
          if (isOnline() == 't') {
            var vcategory = $("#quiz_category").val();
            $.ajax({
              type: 'POST',
              url: 'WhiteBoard/rsneednt_answer_next_url',
              async: false,
              data: {
                tutor: atutor,
                task: atask,
                module: amodule,
                question: aquestion,
                prevquestion: aprevquestion,
                category: vcategory,
              },
              beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.results == 't') {
                        //messageboxcorrect('You need not input to answer', 'Answer', result.redirect_url);
                        provide_everyday_question_answer(atutor, atask, amodule, aquestion, aprevquestion, atype, 0);
                      } else {
                        provide_everyday_question_answer(atutor, atask, amodule, aquestion, aprevquestion, atype, 1);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                    },
                    dataType: "json"
                  });
          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }

      function provide_everyday_question_answer(atutor, atask, amodule, aquestion, aprevquestion, atype, ans_need_or_not) {

    //var vanswer = getStudentAnswer();//CKEDITOR.instances['answer_editor'].getData();
    var vanswer = getStudentAnswer_check_sp(ans_need_or_not);
    var vcategory = $("#quiz_category").val();
    var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();

    if (vanswer != '') {
      $.ajax({
        type: 'POST',
        url: 'WhiteBoard/showNextQuestion',
        async: false,
        data: {tutor: atutor,
          task: atask,
          module: amodule,
          question: aquestion,
          prevquestion: aprevquestion,
          answer: vanswer,
          soundplayed: vSoundPlayed,
          category: vcategory,
          workout: vworkout_text,
        workout_draw: vworkout_text/* $("#workout_draw").val()*/},
        beforeSend: function () {
                //showBusy(atitle, 'Please wait...');
                $("#time_elapse_timer").TimeCircles().stop();
                $('.timeelapsed').hide();
                $('.time_circles').hide();
              },
              success: function (result) {
                if (result.result == 't') {
                  if (result.answer == '1') {
                        //$.notify("Your answer is correct.", "correct", "showDuration: 600");
                        $('.timeelapsed').hide();
                        $('.time_circles').hide();
                        //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                        messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                        //window.location.href = result.redirect_url;
                      } else {
                        fn_show_answer_solution(result.redirect_url, result.solution, atype,vanswer);
                        
                      }
                    } else {
                    //$("#td_error").removeClass('select-file').addClass('v_error');  
                    $("#td_error").show();
                    $("#td_error").html(result.error);
                  }
                },

                error: function (xhr, textStatus, error) {
                  errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                },
                dataType: "json"
              });
    } else {
      errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
        /*if (atype == 1){
         $("#time_elapse_timer").TimeCircles().restart();
       }*/
     }

   }

   function fn_complete_question(atutor, atask, amodule, aquestion, aprevquestion, atype) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
       var vcategory = $("#quiz_category").val();

       $.ajax({
        type: 'POST',
        url: 'WhiteBoard/rsneednt_answer_next_url',
        async: false,
        data: {
          tutor: atutor,
          task: atask,
          module: amodule,
          question: aquestion,
          prevquestion: aprevquestion,
          category: vcategory,
        },
        beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.results == 't') {
                        //messageboxcorrect('You need not input to answer', 'Answer', result.redirect_url);
                        fn_everyday_completed_question_check(atutor, atask, amodule, aquestion, aprevquestion, atype, 0);
                      } else {
                        fn_everyday_completed_question_check(atutor, atask, amodule, aquestion, aprevquestion, atype, 1);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                    },
                    dataType: "json"
                  });
     } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}
function fn_everyday_completed_question_check(atutor, atask, amodule, aquestion, aprevquestion, atype, ans_need_or_not){
  // var vanswer = getStudentAnswer();//CKEDITOR.instances['answer_editor'].getData();
  var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
  var vanswer = getStudentAnswer_check_sp(ans_need_or_not);     
  var vcategory = $("#quiz_category").val();
            var d = new Date(); // for now
            var vhours = d.getHours();
            var vmins = d.getMinutes();
            var vsecs = d.getSeconds();
            var vend_time = vhours + ':' + vmins + ':' + vsecs;
            if (vanswer != '') {
              $.ajax({
                type: 'POST',
                url: 'WhiteBoard/showCompleteQuestion',
                async: false,
                data: {tutor: atutor,
                  task: atask,
                  module: amodule,
                  question: aquestion,
                  prevquestion: aprevquestion,
                  answer: vanswer,
                  soundplayed: vSoundPlayed,
                  category: vcategory,
                  end_time: vend_time,
                  workout: vworkout_text,
                workout_draw: vworkout_text/*$("#workout_draw").val()*/},
                beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                                //$.notify("Your answer is correct.", "correct", "showDuration: 600");	
                                //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                                //window.location.href = result.redirect_url;
                                messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype,vanswer);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                        },
                        dataType: "json"
                      });
            } else {
              errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
            } 
          }

          function fn_error_revision(ahost, atask, amodule, aquestion, arevision, atype) {
            if (sessionAvail() == 'f') {
              if (isOnline() == 't') {
            var vanswer = getStudentAnswer();//CKEDITOR.instances['answer_editor'].getData();
            var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
            var vcategory = $("#quiz_category").val();
            if (vanswer != '') {
              $.ajax({
                type: 'POST',
                url: 'WhiteBoard/ErrorRevision',
                async: false,
                data: {host: ahost,
                  task: atask,
                  module: amodule,
                  question: aquestion,
                  revision: arevision,
                  answer: vanswer,
                  soundplayed: vSoundPlayed,
                  category: vcategory,
                  workout: vworkout_text,
                workout_draw: vworkout_text/*$("#workout_draw").val()*/},
                beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                                //$.notify("Your answer is correct.", "correct", "showDuration: 600");
                                $('.timeelapsed').hide();
                                $('.time_circles').hide();
                                //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                                //window.location.href = result.redirect_url;
                                messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                        },
                        dataType: "json"
                      });
            } else {
              errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
            }
          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }

      function fn_select_question(ahost, atask, amodule, aquestion, aprevquestion) {
        if (sessionAvail() == 'f') {
          if (isOnline() == 't') {
            $.ajax({
              type: 'POST',
              url: 'WhiteBoard/showSelectedQuestion',
              async: false,
              data: {host: ahost,
                task: atask,
                module: amodule,
                question: aquestion,
                prevquestion: aprevquestion},
                beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      window.location.href = result.redirect_url;
                    } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Question.');
                    },
                    dataType: "json"
                  });
          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }

      function fn_select_question_special(ahost, atask, amodule, aquestion, aprevquestion) {
        if (sessionAvail() == 'f') {
          if (isOnline() == 't') {
            $.ajax({
              type: 'POST',
              url: '/my_special_exam/showSelectedQuestion',
              async: false,
              data: {host: ahost,
                task: atask,
                module: amodule,
                question: aquestion,
                prevquestion: aprevquestion},
                beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      window.location.href = result.redirect_url;
                    } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Question.');
                    },
                    dataType: "json"
                  });
          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }

      function fn_skip_video(ahost, atask, amodule, aquestion, aprevquestion) {
        if (sessionAvail() == 'f') {
          if (isOnline() == 't') {
            $.ajax({
              type: 'POST',
              url: 'WhiteBoard/showSelectedQuestion',
              async: false,
              data: {host: ahost,
                task: atask,
                module: amodule,
                question: aquestion,
                prevquestion: aprevquestion,
                skip: 't'},
                beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      window.location.href = result.redirect_url;
                    } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Skip Video.');
                    },
                    dataType: "json"
                  });
          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }

      function fn_show_images() {
        if (sessionAvail() == 'f') {
          if (isOnline() == 't') {
            var uri = window.location.pathname;//.substring(9);
            uri = uri.substr(1);
            uri = uri.substr(uri.indexOf('/') + 1);
            uri = uri.substr(uri.indexOf('/') + 1);
            uri = uri.substring(2);
            var amodule = uri.substr(0, uri.indexOf('/'));//.substring(2);	
            if (amodule == '') {
              amodule = uri.substr(uri.indexOf('/') + 1);
            }
            //console.log(uri);
            //alert(amodule);
            //amodule = amodule.substr(0, amodule.indexOf('/'));
            $.ajax({
              type: 'POST',
              url: 'WhiteBoard/getModuleImages',
              async: false,
              data: {module: amodule},
              beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      $("#dlg_solution").dialog({
                        resizable: false,
                        modal: false,
                        closeOnEscape: false,
                        title: 'List of Images',
                            //open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                            height: 480,
                            width: 480,
                            position: 'center',
                            zIndex: -1,
                            open: function () {
                              $("#ck_editor").html(result.response);
                              $('#nivo_slider').nivoSlider();
                            },
                            buttons: {
                              Close: function () {
                                $(".ui-dialog-titlebar").show();
                                $(this).dialog("close");
                              }
                            }
                          });
                    } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Images.');
                    },
                    dataType: "json"
                  });
          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }

      function fn_sendsms() {
        if (sessionAvail() == 'f') {
          $.ajax({
            type: 'POST',
            url: '/sms/send_sms',
            data: {to: $("#smsto").val(), from: $("#smsfrom").val(), smstext: $("#sms_text").val()},
            beforeSend: function () {
              $.blockUI({
                message: '<img src="/images/sms_sending.gif">',
                title: 'Offer read confirmation',
                css: {
                  border: 'none',
                  width: 'auto',
                  padding: '15px',
                  backgroundColor: '#000',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .95,
                  color: '#FFF',
                  'font-size': '9px'
                }
              });
            },
            success: function (result) {
              if (result.result == 't') {
                $.unblockUI();
                infomsg_dlg(result.status + ' SMS to ' + result.to, 'Sending SMS');
                return true;
              } else {
                $.unblockUI();
                errormsg_dlg(result.error + '<br>' + result.status, 'Sending SMS');
              }
            },

            error: function (xhr, textStatus, error) {
              $.unblockUI();
              errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Sending SMS');
            },
            dataType: "json"
          });
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }
/* 
function setStartTime() {
	alert('time start now');
    var d = new Date(); 
    var vhours = d.getHours();
    var vmins = d.getMinutes();
    var vsecs = d.getSeconds();
    $.ajax({
        type: 'POST',
        url: 'WhiteBoard/setStartTime',
        data: {hours: vhours,
            mins: vmins,
            secs: vsecs},
        beforeSend: function () {
        },
        success: function (result) {
            if (result.result == 't') {
                return false;
            } else {
                return false;
            }
        },

        error: function (xhr, textStatus, error) {
            errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Set Start Time');
        },
        dataType: "json"
    });
  } */

  function setStartTime() {
    var d = new Date(); 
    var vhours = d.getHours();
    var vmins = d.getMinutes();
    var vsecs = d.getSeconds();
    $.ajax({
      type: 'POST',
      url: '/preview_all_users/setStartTime',
      data: {hours: vhours,
        mins: vmins,
        secs: vsecs},
        beforeSend: function () {
        },
        success: function (result) {
          if (result.result == 't') {
            return false;
          } else {
            return false;
          }
        },

        error: function (xhr, textStatus, error) {
          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Set Start Time');
        },
        dataType: "json"
      });
  }

  function fn_next_question_answer(atask, amodule, aquestion, aprevquestion, auserid, aquizids) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
        var quiz_id = aquizids.split(',');
        quiz_id.shift();
        var vnext_id = quiz_id[0];
        $.ajax({
          type: 'POST',
          url: '/show_answer/showNextQuestion',
          async: false,
          data: {task: atask,
            module: amodule,
            question: aquestion,
            prevquestion: aprevquestion,
            userid: auserid,
            next_id: vnext_id,
            quiz_ids: quiz_id},
            beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      window.location.href = result.redirect_url;
                    } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                    },
                    dataType: "json"
                  });
      } else {
        window.location.href = '/';
      }
    } else {
      MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
    }
  }

  function fn_next_question_answer_student_view(atask, amodule, aquestion, aprevquestion, auserid, aquizids) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
        var quiz_id = aquizids.split(',');
        quiz_id.shift();
        var vnext_id = quiz_id[0];
        $.ajax({
          type: 'POST',
          url: '/show_answer/showNextQuestion_student_view',
          async: false,
          data: {task: atask,
            module: amodule,
            question: aquestion,
            prevquestion: aprevquestion,
            userid: auserid,
            next_id: vnext_id,
            quiz_ids: quiz_id},
            beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      window.location.href = result.redirect_url;
                    } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                    },
                    dataType: "json"
                  });
      } else {
        window.location.href = '/';
      }
    } else {
      MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
    }
  }

  function fn_select_question_progress(atask, amodule, aquestion, aprevquestion, auserid) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
        $.ajax({
          type: 'POST',
          url: '/show_answer/showSelectedQuestion',
          async: false,
          data: {task: atask,
            module: amodule,
            question: aquestion,
            prevquestion: aprevquestion,
            userid: auserid},
            beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      window.location.href = result.redirect_url;
                    } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Question.');
                    },
                    dataType: "json"
                  });
      } else {
        window.location.href = '/';
      }
    } else {
      MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
    }
  }

  var
  vreturn = '';
  verror = '';

  function fn_set_number(ataskfor, amodule_id, aquestion_id, auserid) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
        $.ajax({
          type: 'POST',
          url: '/show_answer/showPendingDlg',
          async: false,
          data: {task: ataskfor,
            module: amodule_id,
            question: aquestion_id,
            userid: auserid},
            beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      if (result.count > 0) {
                        $("#dlg_pending").dialog({
                          resizable: false,
                          modal: false,
                          async: false,
                          closeOnEscape: false,
                          title: result.title,
                                //open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                                height: 480,
                                width: 300,
                                position: 'center',
                                zIndex: -1,
                                open: function () {
                                  $("#pending").html(result.response);
                                },
                                buttons: {
                                  Cancel: function () {
                                    $(".ui-dialog-titlebar").show();
                                    $(this).dialog("close");
                                  },
                                  Update: function () {
                                    $(".ui-dialog-titlebar").show();
                                    var canclose = fn_update_answer(result.spinner_id, aquestion_id, result.count, result.score, result.id);
                                    if (canclose) {
                                      $(this).dialog("close");
                                    }
                                  }
                                }
                              });
                      } else {
                        warning_msg_dlg('There is no Pending List to set mark', result.title);
                      }
                    } else {
                      errormsg_dlg(result.error, result.title);
                    }
                  },

                  error: function (xhr, textStatus, error) {
                    errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Pending List.');
                  },
                  dataType: "json"
                });
      } else {
        window.location.href = '/';
      }
    } else {
      MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
    }
  }

  function fn_update_answer(aids, aquestion_id, acount, ascores, arids) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
        var vids = aids.split(',');
        var vValues = new Array();
        var vid_ValueIds = new Array();
        var vid_Values = new Array();
        if (acount > 1) {
          var vscores = ascores.split(',');
          var vrids = arids.split(',');
        } else {
          var vscores = new Array();
          vscores.push(ascores);
          var vrids = new Array();
          vrids.push(arids);
        }

        for (i = 0; i < vids.length; i++) {
          aElement = $("#" + vids[i] + "");
          if (aElement.val() != '') {
            vValues.push(aElement.val());
          }
        }

        if (vValues.length == acount) {
          for (i = 0; i < vids.length; i++) {
            aid = vids[i].substr(vids[i].lastIndexOf('_') + 1);
            vid_ValueIds.push(aid);
            vid_Values.push(vValues[i]);
          }
          fn_update_answer_db(vid_ValueIds.toString(), vid_Values.toString());
          if (vreturn == 't') {
            for (i = 0; i < vscores.length; i++) {
              aid = vids[i].substr(vids[i].lastIndexOf('_') + 1);
                        //var vmarktd = 'td_mark_' + aquestion_id + '_' + vscores[i];
                        var vmarktd = 'td_mark_' + vrids[i];
                        $("#" + vmarktd + "").html(vValues[i]);
                      }
                      var num_btn = 'number_' + aquestion_id;
                      $("#" + num_btn + "").hide();
                      for (i = 0; i < vrids.length; i++) {
                        $("#td_okwrong_" + vrids[i] + "").html('');
                      }
                      $("#score").removeClass('fluid rightside_score_progress').addClass('fluid rightside_score');
                      $("#math_button").removeClass('fluid math_tool_progress').addClass('fluid math_tool');
                      $("#score_table").removeClass('table_progress').addClass('table');
                      return true;
                    } else {
                      errormsg_dlg(verror, 'Setting Mark');
                      return false;
                    }
                  } else {
                    warning_msg_dlg('Please enter all Marks', 'Setting Mark');
                    return false;
                  }
                } else {
                  window.location.href = '/';
                }
              } else {
                MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
              }
            }

            function fn_update_answer_db(aids, avalues) {
              $.ajax({
                type: 'POST',
                url: '/show_answer/UpdatePendingMark',
                async: false,
                data: {ids: aids, values: avalues},
                beforeSend: function () {
            //showBusy(atitle, 'Please wait...');
          },
          success: function (result) {
            if (result.result == 't') {
              vreturn = 't';
              verror = '';
            } else {
              vreturn = 'f';
              verror = result.error;
            }
          },

          error: function (xhr, textStatus, error) {
            errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Updating Pending Mark.');
          },
          dataType: "json"
        });
            }

            function fn_repeat(atask, amodule, aquestion, aquestionfor, auserid, arepeateddate, atype) {

              if (sessionAvail() == 'f') {
                if (isOnline() == 't') {
                  var vanswer = getStudentAnswer();
                  var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
			//CKEDITOR.instances['answer_editor'].getData();
      var vcategory = $("#quiz_category").val();
      $.ajax({
        type: 'POST',
        url: '/repeat/SetRepeat',
        async: false,
        data: {task: atask,
          userid: auserid,
          module: amodule,
          question: aquestion,
          questionfor: aquestionfor,
          repeateddate: arepeateddate,
          answer: vanswer,
          soundplayed: vSoundPlayed,
          category: vcategory,
          workout: vworkout_text},
          beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      if (result.answer == '1') {
                            //window.location.href = result.redirect_url;
                            MessageboxSuccessGoto('Your answer is right', 'Repeat Task', result.redirect_url);
                          } else {
                            fn_show_answer_solution(result.redirect_url, result.solution, atype);
                          }
                        } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Repeatation.');
                    },
                    dataType: "json"
                  });
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}


function fn_preview_question(acatagory, aquestion, atype) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vanswer = getStudentAnswer();
      $.ajax({
        type: 'POST',
        url: '/preview/CheckAnswerPreview',
        async: false,
        data: {question: aquestion,
          answer: vanswer,
          category: acatagory},
          beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      if (result.answer == '1') {
                        MessageboxSuccessGoto('Your answer is right', 'Preview', result.redirect_url);
                            //window.location.href = result.redirect_url;
                          } else {
                            fn_show_answer_solution(result.redirect_url, result.solution, atype);
                          }
                        } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                    },
                    dataType: "json"
                  });
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}

//rs new changes
function fn_preview_question_previews(acatagory, aquestion, RsModuleId, atype) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vanswer = getStudentAnswer();
      if (vanswer) {
        $.ajax({
          type: 'POST',
          url: '/preview_all/CheckAnswerPreview',
          async: false,
          data: {question: aquestion,
            answer: vanswer,
            category: acatagory,
            rsmodules_id: RsModuleId},
            beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                            MessageboxSuccessGoto('Your answer is right', 'Preview', result.redirect_url);
                                //window.location.href = result.redirect_url;
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                        },
                        dataType: "json"
                      });
      }
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}
//rs new changes
function fn_preview_question_previews_opnse_adminview(acatagory, aquestion, RsModuleId, atype) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vanswer = getStudentAnswer();
      if (vanswer) {
        $.ajax({
          type: 'POST',
          url: '/preview_all_admin/CheckAnswerPreview',
          async: false,
          data: {question: aquestion,
            answer: vanswer,
            category: acatagory,
            rsmodules_id: RsModuleId},
            beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                            MessageboxSuccessGoto('Your answer is right', 'Preview', result.redirect_url);
                                //window.location.href = result.redirect_url;
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                        },
                        dataType: "json"
                      });
      }
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}
//rs new changes
function fn_preview_question_previews_opnse_tutorsview(acatagory, aquestion, RsModuleId, atype) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vanswer = getStudentAnswer();
      if (vanswer) {
        $.ajax({
          type: 'POST',
          url: '/preview_all_tutors/CheckAnswerPreview',
          async: false,
          data: {question: aquestion,
            answer: vanswer,
            category: acatagory,
            rsmodules_id: RsModuleId},
            beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                            MessageboxSuccessGoto('Your answer is right', 'Preview', result.redirect_url);
                                //window.location.href = result.redirect_url;
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                        },
                        dataType: "json"
                      });
      }
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}

//rs new changes
function fn_preview_question_previews_opnse_qstudysview(acatagory, aquestion, RsModuleId,job_id, atype) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vanswer = getStudentAnswer();
      if (vanswer) {
        $.ajax({
          type: 'POST',
          url: '/preview_all_qstudys/CheckAnswerPreview',
          async: false,
          data: {question: aquestion,
            answer: vanswer,
            category: acatagory,
            rsmodules_id: RsModuleId,job_id:job_id},
            beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                            MessageboxSuccessGoto('Your answer is right', 'Preview', result.redirect_url);
                                //window.location.href = result.redirect_url;
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                        },
                        dataType: "json"
                      });
      }
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}
//rs new changes
function fn_preview_question_previews_opnse_usersview(acatagory, aquestion, RsModuleId, atype) {

  var vanswer = getStudentAnswer();
  if (vanswer) {
    $.ajax({
      type: 'POST',
      url: '/preview_all_users/CheckAnswerPreview',
      async: false,
      data: {question: aquestion,
        answer: vanswer,
        category: acatagory,
        rsmodules_id: RsModuleId},
        beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                            MessageboxSuccessGoto('Your answer is right', 'Preview', result.redirect_url);
                                //window.location.href = result.redirect_url;
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                        },
                        dataType: "json"
                      });
  }

}
//rs new changes
function fn_preview_from_question(acatagory, aquestion, atype) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vanswer = getStudentAnswer();
      $.ajax({
        type: 'POST',
        url: '/preview_from/CheckAnswerPreview',
        async: false,
        data: {question: aquestion,
          answer: vanswer,
          category: acatagory},
          beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      if (result.answer == '1') {
                        MessageboxSuccessGoto('Your answer is right', 'Preview', result.redirect_url);
                            //window.location.href = result.redirect_url;
                          } else {
                            fn_show_answer_solution(result.redirect_url, result.solution, atype);
                          }
                        } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                    },
                    dataType: "json"
                  });
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}

function fn_show_combo(elem, avisible) {
  if (avisible == 't') {
    $("#" + elem + "").show('slow');
  } else {
    $("#" + elem + "").hide('slow');
  }
}

function fn_select_combo_value(elem, acombo, avalue) {
  $("#" + elem + "").val(avalue);
  $("#" + acombo + "").hide('slow');
}

function fn_show_workout() {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var usre_type = $("#user_type").val();
      if (usre_type == 1) {
        dialogButtons = [{
          text: "Close",
          click: function () {
            $(".ui-dialog-titlebar").show();
            $(this).dialog("close");
          }
        }]
      } else {
        dialogButtons = {
                    //text: "Close",
                    'Close': function () {
                      $(".ui-dialog-titlebar").show();
                      $(this).dialog("close");
                    },
                    'Get Question': function () {
                      $("#workout_editor").val($("#question_div").html());
                    },
                    'Set Answer': function () {
                      CKEDITOR.instances['answer_editor'].setData($("#workout_editor").val());
                      $("#workout").val($("#workout_editor").val());
                      $(this).dialog("close");
                    }
                  }
                }
                $("#dlg_workout_text").dialog({
                  resizable: false,
                  modal: false,
                  closeOnEscape: false,
                  title: 'Workout Text',
                //open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                height: 580,
                width: 680,
                position: 'center',
                zIndex: -1,
                open: function () {
                  $(".ui-dialog-titlebar-close").hide();
                  CKEDITOR.replace( 'workout_editor', {
                    extraPlugins: 'onchange,simpleuploads,ckeditor_wiris,colorbutton,font',
                    height: 350,
                    width: 650,
                    filebrowserBrowseUrl: '/browser/browse.php?type=Images',
                    filebrowserUploadUrl: '/welcome/uploadFiles?type=Files',
                    simpleuploads_acceptedExtensions: 'jpe?g|gif|png|bmp|mp4|mp3|doc|docx|pdf|xls|xlsx|txt',
                    allowedContent: true
                  });
                },
                close: function () {
                  $('#workout_editor').each(function () {
                    $(this).ckeditorGet().destroy();
                  });
                },
                buttons: dialogButtons/*{
                 Close: function() {
                 $(".ui-dialog-titlebar").show();
                 $( this ).dialog( "close" );
                 },
                 'Get Answer': function() {
                 $("#workout_editor").val($("#question_div").html());
                 },
                 'Set Answer': function() {
                 CKEDITOR.instances['answer_editor'].setData($("#workout_editor").val());
                 $("#workout").val($("#workout_editor").val());
                 $( this ).dialog( "close" );
                 }
               }*/
             });
              } else {
                window.location.href = '/';
              }
            } else {
              MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
            }
          }

          function fn_show_workout_draw() {
            if (1 /*sessionAvail() == 'f'*/) {
              if (1 /*isOnline() == 't'*/) {
                $.ajax({
                  type: 'POST',
                  url: 'WhiteBoard/draw_whiteboard',
                  async: false,
                  data: {},
                  beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      var usre_type = $("#user_type").val();

                      if (usre_type == 1) {

                        dialogButtons = [{
                          text: "Close",
                          click: function () {
                            $(".ui-dialog-titlebar").show();
                            $(this).dialog("close");
                          }
                        }]
                      } else {
                        dialogButtons = {
                                //text: "Close",
                                'Close': function () {
                                  $(".ui-dialog-titlebar").show();
                                  $(this).dialog("close");
                                },
                                'Get Question': function () {

                                  var canvas = document.getElementById("can_1");
                                  var ctx = canvas.getContext("2d");

                                  var ctx = canvas.getContext('2d');
                                  var question = $("#question_div").html();
                                  $("#question_div").removeClass('question_holder');
                                    /*html2canvas(document.querySelector("#question_div"), {canvas: canvas}).then(function(canvas) {
                                     //console.log('Drew on the existing canvas');
                                     $("#question_div").addClass('question_holder');
                                     getanswered = true;
                                   });*/

                                   html2canvas($("#question_div"), {
                                    onrendered: function (canvas) {
                                      $("#question_div").addClass('question_holder');
                                      getanswered = true;
                                            // canvas is the final rendered <canvas> element
                                            var myImage = canvas.toDataURL("image/png");

                                            var apage = window.location.href;
                                            apage = apage.substr(apage.indexOf('q-study.com/'), apage.length);
                                            apage = apage.substr(apage.indexOf('my-exam/'), apage.length);
                                            apage = apage.substr(apage.indexOf('/') + 1, apage.length);
                                            apage = replaceAll('/', '_', apage);

                                            $.ajax({
                                              type: 'POST',
                                              url:'/WhiteBoard/savetoimg',
                                              async: false,
                                              data: {image: myImage, id: apage},
                                              beforeSend: function () {
                                                    //showBusy(atitle, 'Please wait...');
                                                  },
                                                  success: function (result) {
                                                    if (result.result == 't') {
                                                      $canvas.drawImage({
                                                        layer: true,
                                                        name: 'question',
                                                            //draggable: true,
                                                            source: result.response,
                                                            x: 250, y: 110,
                                                            cursors: {
                                                                // Show pointer on hover
                                                                mouseover: "pointer",
                                                                // Show 'move' cursor on mousedown
                                                                mousedown: "move",
                                                                // Revert cursor on mouseup
                                                                mouseup: "pointer"
                                                              }
                                                            });
                                                        //layer.data.joints += 1;										
                                                      } else {
                                                      }
                                                    },

                                                    error: function (xhr, textStatus, error) {
                                                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                                                    },
                                                    dataType: "json"
                                                  });
                                          }
                                        });
                                   $("#drag_question").show();
                                    /*var data = '<svg xmlns="http://www.w3.org/2000/svg" width="765" height="600">' +
                                     '<foreignObject width="100%" height="100%">' +
                                     '<div xmlns="http://www.w3.org/1999/xhtml">' +
                                     question + '</div>' +
                                     '</foreignObject>' +
                                     '</svg>';	
                                     
                                     var DOMURL = window.URL || window.webkitURL || window;
                                     
                                     var img = new Image();
                                     var svg = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
                                     var url = DOMURL.createObjectURL(svg);
                                     
                                     
                                     img.onload = function () {
                                     ctx.drawImage(img, 0, 0);
                                     DOMURL.revokeObjectURL(url);
                                     }
                                     img.src = url;
                                     $('can_1').drawImage({
                                     source: url,
                                     x: 0, y: 0
                                   });	*/
                                 },
                                 'Set Answer': function () {
                                  var canvas = document.getElementById("can_1");
                                  var dataUrl = canvas.toDataURL();

                                  var apage = window.location.href;
                                  apage = apage.substr(apage.indexOf('q-study.com/'), apage.length);
                                  apage = apage.substr(apage.indexOf('my-exam/'), apage.length);
                                  apage = apage.substr(apage.indexOf('/') + 1, apage.length);
                                  apage = replaceAll('/', '_', apage);

                                    //document.getElementById("textArea").value = dataUrl;									
                                    //CKEDITOR.instances['answer_editor'].setData('<img src="' + dataUrl + '"/>');
                                    //set canvas to file and get its name
                                    $.ajax({
                                      type: 'POST',
                                      url: 'WhiteBoard/getImageName',
                                      async: false,
                                      data: {image: dataUrl, id: apage},
                                      beforeSend: function () {
                                            //showBusy(atitle, 'Please wait...');
                                          },
                                          success: function (result) {
                                            if (result.result == 't') {
                                              $("#workout_draw").val('<img src="' + result.response + '"/>');
                                              $('.rsworkouts').show();
                                                //CKEDITOR.instances['answer_editor'].setData('<img src="' + result.response + '"/>');
                                                CKEDITOR.instances['workout_editor_draw'].setData('<img src="' + result.response + '"/>');
                                                //CKEDITOR.instances['answer_editor'].setData('<img src="' + result.response + '"/>');
                                              } else {
                                              }
                                            },

                                            error: function (xhr, textStatus, error) {
                                              errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                                            },
                                            dataType: "json"
                                          });

                                    //$("#workout_draw").val('<img src="' + dataUrl + '"/>');
                                    //$("#workout").val($("#workout_editor").val());
                                    $(this).dialog("close");
                                  }
                                }
                              }
                              $("#dlg_workout_draw").dialog({
                                autoOpen: true,
                                resizable: true,
                                modal: true,
                                closeOnEscape: false,
                                title: 'Workout Draw',
                            //open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                            //height:780,
                            //width:880,
                            /*width:'auto',
                             height:'auto',
                             maxWidth: 880,
                             maxHeight:750,*/
                             width: '80%',
                             height: 'auto',
                             fluid: true,
                             zIndex: -1,
                            //position:'center',
                            open: function () {
                                //$(".ui-dialog-titlebar-close").hide();
                                $("#drag_question").hide();
                                fluidDialog();
                                //$( "#line_width" ).spinner({ max: 100, min:1 });
                                $("#colorselect").val('#000');
                                $("#work_draw").html(result.response);
                                /*if (usre_type != 1){
                                 $("#work_draw").html(result.response);
                                 }else{
                                 $("#work_draw").html($("#workout_draw").val());	
                               }*/
                               $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
                                //$("#can_1").height = $('.canvas_div').css('height');
                                //$("#can_1").width = $('.canvas_div').css('width');
                                //alert($('.canvas_div').css('width') + ' height: ' + $('.canvas_div').css('height'));
                                var canvas = document.getElementById('can_1'),
                                context = canvas.getContext('2d');

                                // resize the canvas to fill browser window dynamically
                                window.addEventListener('resize', resizeCanvas, false);

                                function resizeCanvas() {
                                    canvas.width = $(this).outerWidth();//window.innerWidth;
                                    canvas.height = $(this).outerHeight();//window.innerHeight;
                                  }
                                  resizeCanvas();

                                  $canvas = $("#can_1");
                                //var c = $("#can_1"), 
                                //	ctx = c[0].getContext('2d');
                                //$canvas.height = $('.canvas_div').css('height');
                                //$canvas.width  = $('.canvas_div').css('width');
                                if ($("#user_type").val() == 1) {//only parent, tutor, school or corporate
                                  var img = v_workout_draw;
                                  $("#work_draw").html(img);
                                    /*img = img.substr(img.indexOf('"') + 1);
                                     img = img.substr(0, img.indexOf('"'));
                                     $("#can_1").drawImage({
                                     source: img,
                                     draggable:true,
                                     x: 340, y: 300,
                                     cursors: {
                                     // Show pointer on hover
                                     mouseover: "pointer",
                                     // Show 'move' cursor on mousedown
                                     mousedown: "move",
                                     // Revert cursor on mouseup
                                     mouseup: "pointer"
                                     }
                                   });*/

                                    /*var canvas = document.getElementById("can_1");
                                     var ctx = canvas.getContext("2d");
                                     
                                     var data = '<svg xmlns="http://www.w3.org/2000/svg" width="765" height="600">' +
                                     '<foreignObject width="100%" height="100%">' +
                                     '<div xmlns="http://www.w3.org/1999/xhtml">' +
                                     $("#workout_draw").val() + '</div>' +
                                     '</foreignObject>' +
                                     '</svg>';	
                                     
                                     var DOMURL = window.URL || window.webkitURL || window;
                                     
                                     var img = new Image();
                                     var svg = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
                                     var url = DOMURL.createObjectURL(svg);
                                     
                                     img.onload = function () {
                                     ctx.drawImage(img, 0, 0);
                                     DOMURL.revokeObjectURL(url);
                                     }
                                     
                                     img.src = url;*/

                                    /*var canvas = document.getElementById("can_1");
                                     var ctx = canvas.getContext("2d");
                                     
                                     var image = new Image();
                                     image.src = '"' + $("#workout_draw").val() + '"';
                                     image.onload = function() {
                                     ctx.drawImage(image, 0, 0);
                                   };*/
                                 }
                               },
                               close: function () {
                               },
                               buttons: dialogButtons
                             });
$("#dlg_workout_draw").dialog({position: 'center'});
} else {
  //errormsg_dlg(result.error, 'Error in Showing Workout Draw.');
  console.log('error');
}
},

error: function (xhr, textStatus, error) {
  //errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Showing Workout Draw.');
  console.log('error');
},
dataType: "json"
});

} else {
  window.location.href = '/';
}
} else {
  MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
}
}

function fn_show_workout_draw_progress() {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      $.ajax({
        type: 'POST',
        url: '/show_answerdraw_whiteboard_prgress',
        async: false,
        data: {},
        beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {

                      dialogButtons = {
                                //text: "Close",
                                'Close': function () {
                                  $(".ui-dialog-titlebar").show();
                                  $(this).dialog("close");
                                },
                                'Get Question': function () {

                                  var canvas = document.getElementById("can_1");
                                  var ctx = canvas.getContext("2d");
                                  var ctx = canvas.getContext('2d');
                                  var question = $("#question_div").html();

                                  $("#question_div").removeClass('question_holder');
                                    /*html2canvas(document.querySelector("#question_div"), {canvas: canvas}).then(function(canvas) {
                                     //console.log('Drew on the existing canvas');
                                     $("#question_div").addClass('question_holder');
                                     getanswered = true;
                                   });*/

                                   html2canvas($("#question_div"), {
                                    onrendered: function (canvas) {
                                      $("#question_div").addClass('question_holder');
                                      getanswered = true;
                                            // canvas is the final rendered <canvas> element
                                            var myImage = canvas.toDataURL("image/png");

                                            var apage = window.location.href;
                                            apage = apage.substr(apage.indexOf('q-study.com/'), apage.length);
                                            apage = apage.substr(apage.indexOf('show-answer/'), apage.length);
                                            apage = apage.substr(apage.indexOf('/') + 1, apage.length);
                                            apage = replaceAll('/', '_', apage);
                                            alert(apage);
                                            $.ajax({
                                              type: 'POST',
                                              url: '/show_answer/savetoimg',
                                              async: false,
                                              data: {image: myImage, id: apage},
                                              beforeSend: function () {
                                                    //showBusy(atitle, 'Please wait...');
                                                  },
                                                  success: function (result) {
                                                    if (result.result == 't') {
                                                      $canvas.drawImage({
                                                        layer: true,
                                                        name: 'question',
                                                            //draggable: true,
                                                            source: result.response,
                                                            x: 250, y: 110,
                                                            cursors: {
                                                                // Show pointer on hover
                                                                mouseover: "pointer",
                                                                // Show 'move' cursor on mousedown
                                                                mousedown: "move",
                                                                // Revert cursor on mouseup
                                                                mouseup: "pointer"
                                                              }
                                                            });
                                                        //layer.data.joints += 1;										
                                                      } else {
                                                      }
                                                    },

                                                    error: function (xhr, textStatus, error) {
                                                      //errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                                                      console.log('error');
                                                    },
                                                    dataType: "json"
                                                  });
                                          }
                                        });
                                   $("#drag_question").show();

                                 },
                                 'Set Answer': function () {
                                  var canvas = document.getElementById("can_1");
                                  var dataUrl = canvas.toDataURL();

                                  var apage = window.location.href;
                                  apage = apage.substr(apage.indexOf('q-study.com/'), apage.length);
                                  apage = apage.substr(apage.indexOf('show-answer/'), apage.length);

                                  apage = apage.substr(apage.indexOf('/') + 1, apage.length);
                                  apage = replaceAll('/', '_', apage);

                                    //document.getElementById("textArea").value = dataUrl;									
                                    //CKEDITOR.instances['answer_editor'].setData('<img src="' + dataUrl + '"/>');
                                    //set canvas to file and get its name
                                    $.ajax({
                                      type: 'POST',
                                      url: '/show_answer/getImageName',
                                      async: false,
                                      data: {image: dataUrl, id: apage},
                                      beforeSend: function () {
                                            //showBusy(atitle, 'Please wait...');
                                          },
                                          success: function (result) {
                                            if (result.result == 't') {
                                              $("#workout_draw").val('<img src="' + result.response + '"/>');
                                              $('.rsworkouts').show();
                                                //CKEDITOR.instances['answer_editor'].setData('<img src="' + result.response + '"/>');
                                                CKEDITOR.instances['workout_editor_draw'].setData('<img src="' + result.response + '"/>');
                                                //CKEDITOR.instances['answer_editor'].setData('<img src="' + result.response + '"/>');
                                              } else {
                                              }
                                            },

                                            error: function (xhr, textStatus, error) {
                                              errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Preview.');
                                            },
                                            dataType: "json"
                                          });

                                    //$("#workout_draw").val('<img src="' + dataUrl + '"/>');
                                    //$("#workout").val($("#workout_editor").val());
                                    $(this).dialog("close");
                                  }
                                }

                                $("#dlg_workout_draw").dialog({
                                  autoOpen: true,
                                  resizable: true,
                                  modal: true,
                                  closeOnEscape: false,
                                  title: 'Workout Draw',
                            //open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                            //height:780,
                            //width:880,
                            /*width:'auto',
                             height:'auto',
                             maxWidth: 880,
                             maxHeight:750,*/
                             width: '80%',
                             height: 'auto',
                             fluid: true,
                             zIndex: -1,
                            //position:'center',
                            open: function () {
                                //$(".ui-dialog-titlebar-close").hide();
                                $("#drag_question").hide();
                                fluidDialog();
                                //$( "#line_width" ).spinner({ max: 100, min:1 });
                                $("#colorselect").val('#000');
                                $("#work_draw").html(result.response);
                                /*if (usre_type != 1){
                                 $("#work_draw").html(result.response);
                                 }else{
                                 $("#work_draw").html($("#workout_draw").val());	
                               }*/
                               $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
                                //$("#can_1").height = $('.canvas_div').css('height');
                                //$("#can_1").width = $('.canvas_div').css('width');
                                //alert($('.canvas_div').css('width') + ' height: ' + $('.canvas_div').css('height'));
                                var canvas = document.getElementById('can_1'),
                                context = canvas.getContext('2d');

                                // resize the canvas to fill browser window dynamically
                                window.addEventListener('resize', resizeCanvas, false);

                                function resizeCanvas() {
                                    canvas.width = $(this).outerWidth();//window.innerWidth;
                                    canvas.height = $(this).outerHeight();//window.innerHeight;
                                  }
                                  resizeCanvas();

                                  $canvas = $("#can_1");
                                //var c = $("#can_1"), 
                                //	ctx = c[0].getContext('2d');
                                //$canvas.height = $('.canvas_div').css('height');
                                //$canvas.width  = $('.canvas_div').css('width');

                              },
                              close: function () {
                              },
                              buttons: dialogButtons
                            });
                                $("#dlg_workout_draw").dialog({position: 'center'});
                              } else {
                                errormsg_dlg(result.error, 'Error in Showing Workout Draw.');
                              }
                            },

                            error: function (xhr, textStatus, error) {
                              errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in Showing Workout Draw.');
                            },
                            dataType: "json"
                          });

} else {
  window.location.href = '/';
}
} else {
  MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
}
}

function fn_drag_question() {
  $("#tool_text").removeClass('tool_text_active').addClass('tool_text');
  $("#tool_pencil").removeClass('tool_pencil_active').addClass('tool_pencil');
  $("#tool_line").removeClass('tool_line_active').addClass('tool_line');
  $("#tool_zigzag").removeClass('tool_zigzag_active').addClass('tool_zigzag');
  $("#tool_curved").removeClass('tool_curved_active').addClass('tool_curved');
  $("#tool_eraser").removeClass('tool_eraser_active').addClass('tool_eraser');
  $("#tool_color").removeClass('tool_color_active').addClass('tool_color');
  $("#tool_fullscreen").removeClass('tool_fullscreen_active').addClass('tool_fullscreen');
  $("#tool_compass").removeClass('tool_compass_active').addClass('tool_compass');
  $("#tool_protactor").removeClass('tool_protactor_active').addClass('tool_protactor');
  $("#tool_segma").removeClass('tool_segma_active').addClass('tool_segma');
  $("#tool_rough").removeClass('tool_rough_active').addClass('tool_rough');
  $("#tool_ruler").removeClass('tool_ruler_active').addClass('tool_ruler');
  if (vgrid) {
    $("#tool_grid").removeClass('tool_grid').addClass('tool_grid_active');
  } else {
    $("#tool_grid").removeClass('tool_grid_active').addClass('tool_grid');
  }
  $("#tool_shape").removeClass('tool_shape_active').addClass('tool_shape');
  $("#tool_undo").removeClass('tool_undo_active').addClass('tool_undo');
  $("#tool_redo").removeClass('tool_redo_active').addClass('tool_redo');
  $("#tool_clearall").removeClass('tool_clearall_active').addClass('tool_clearall');
  $("#tool_calculator").removeClass('tool_calculator_active').addClass('tool_calculator');
  $("#tool_screen_share").removeClass('tool_screen_share_active').addClass('tool_screen_share');
  $("#tool_library").removeClass('tool_library_active').addClass('tool_library');
  $("#tool_hand").removeClass('tool_hand_active').addClass('tool_hand');
  $("#tool_inspirational_tool").removeClass('tool_inspirational_tool_active').addClass('tool_inspirational_tool');
  $("#tool_volume_control").removeClass('tool_volume_control_active').addClass('tool_volume_control');
  $("#tool_help").removeClass('tool_help_active').addClass('tool_help');
  var className = $('#drag_question').attr('class');
  if (className == 'drag_question') {
    $("#tool_arrow").removeClass('tool_arrow_active').addClass('tool_arrow');
    $("#drag_question").removeClass('drag_question').addClass('drag_question_active');
    var layers = $canvas.getLayer('question');
    if (layers) {
      layers.draggable = true;
    }
        //$canvas.getLayer('question').draggable = false;
        //$canvas.getLayer('question').draggable = true;
      } else {
        $("#tool_arrow").removeClass('tool_arrow').addClass('tool_arrow_active');
        $("#drag_question").removeClass('drag_question_active').addClass('drag_question');
        //$canvas.getLayer('question').draggable = false;
        var layers = $canvas.getLayer('question');
        if (layers) {
          layers.draggable = false;
        }
        fn_drag();
        //$canvas.getLayer('question').draggable = false;
      }
    }


    function fn_show_slider() {
      var isspinner = $(".spinnerbox").is(':visible');
      if (!isspinner) {
        $(".spinnerbox").show();
        $(".sliderarea").slider({
          max: 100,
          min: 1,
          change: function (event, ui) {
            var value = $(".sliderarea").slider("option", "value");
            $(".line_width").html(value);
          }
        });
      } else {
        $(".spinnerbox").hide();
      }
    }

    function fn_hide_spin() {
      $(".spinnerbox").hide();
    }

    function fn_tool_segma_new() {
      $("#dlg_equation").dialog({
        resizable: true,
        modal: false,
        closeOnEscape: false,
        title: 'Equation',
        open: function (event, ui) {
          $(".ui-dialog-titlebar-close").hide();
        },
        height: 400,
        width: 470,
        buttons: {
          Close: function () {
            $(".ui-dialog-titlebar").show();
            fn_tool_segma_inactive_new();
            $(this).dialog("close");
          }
        }
      });
    }
/*

function fn_tool_segma_new_wiris() {
	
	$("#dlg_wiris_cntent").dialog({
        resizable: true,
        modal: false,
        closeOnEscape: false,
        title: 'Equation',
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        height: 400,
        width: 570,
        buttons: {
            Close: function () {
                $(this).dialog("close");
            },
			Export: function () {
				var editorParams;
				
					editorParams = "&backgroundColor=%23fff";
					openResource("http://www.wiris.net/demo/editor/render.png", com.wiris.jsEditor.JsEditor.getInstance(document.getElementById('wiris_editor_content')).getMathML(), editorParams);
                $(this).dialog("close");
            }
        }
    });			
       
}


			function openResource(url, mathml, editorParams) {
				editorParams = typeof editorParams !== 'undefined' ? editorParams : '';
				wnd = window.open(url + '?mml=' + wrs_urlencode(wrs_mathmlEntities(mathml)) + editorParams,"new_window","width=350,height=200,location=0,status=0,toolbar=0,top=100,left=500");
				wnd.focus();
				
			}
	function wrs_urlencode(clearString) {
				var output = '';
				var x = 0;
				clearString = clearString.toString();
				var regex = /(^[a-zA-Z0-9_.]*)/;
				
				var clearString_length = ((typeof clearString.length) == 'function') ? clearString.length() : clearString.length;

				while (x < clearString_length) {
					var match = regex.exec(clearString.substr(x));
					if (match != null && match.length > 1 && match[1] != '') {
						output += match[1];
						x += match[1].length;
					}
					else {
						var charCode = clearString.charCodeAt(x);
						var hexVal = charCode.toString(16);
						output += '%' + ( hexVal.length < 2 ? '0' : '' ) + hexVal.toUpperCase();
						++x;
					}
				}
				
				return output;
			}
			
			function wrs_mathmlEntities(mathml) {
				var toReturn = '';
				
				for (var i = 0; i < mathml.length; ++i) {
					//parsing > 128 characters
					if (mathml.charCodeAt(i) > 128) {
						toReturn += '&#' + mathml.charCodeAt(i) + ';';
					}
					else {
						toReturn += mathml.charAt(i);
					}
				}
				
				return toReturn;
			}
function fn_tool_segma_inactive_new() {
	
}
*/
function fn_skip_video_special(atask, amodule, aquestion, aprevquestion) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      $.ajax({
        type: 'POST',
        url: '/my_special_exam/showSelectedQuestion',
        async: false,
        data: {task: atask,
          module: amodule,
          question: aquestion,
          prevquestion: aprevquestion,
          skip: 't'},
          beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.result == 't') {
                      window.location.href = result.redirect_url;
                    } else {
                        //$("#td_error").removeClass('select-file').addClass('v_error');  
                        $("#td_error").show();
                        $("#td_error").html(result.error);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in getting Skip Video.');
                    },
                    dataType: "json"
                  });
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}

function fn_next_question_special(ahost, atask, amodule, aquestion, aprevquestion, atype) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vcategory = $("#quiz_category").val();

      $.ajax({
        type: 'POST',
        url: '/my_special_exam/rsneednt_answer_next_url',
        async: false,
        data: {
          host: ahost,
          task: atask,
          module: amodule,
          question: aquestion,
          prevquestion: aprevquestion,
          category: vcategory,
        },
        beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.results == 't') {
                        //messageboxcorrect('You need not input to answer', 'Answer', result.redirect_url);
                        rsneednt_answer_next_urls(ahost, atask, amodule, aquestion, aprevquestion, atype, 0);
                      } else {
                        rsneednt_answer_next_urls(ahost, atask, amodule, aquestion, aprevquestion, atype, 1);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                    },
                    dataType: "json"
                  });

    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}

function rsneednt_answer_next_urls(ahost, atask, amodule, aquestion, aprevquestion, atype, ans_need_or_not) {

    var vanswer = getStudentAnswer_check_sp(ans_need_or_not);//CKEDITOR.instances['answer_editor'].getData();
    var vcategory = $("#quiz_category").val();
    var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
    //var needdntq_ans = rsneednt_answer_next_urls(ahost, atask, amodule, aquestion, aprevquestion, atype,vcategory);


    if (vanswer != '') {
      $.ajax({
        type: 'POST',
        url: '/my_special_exam/showNextQuestion',
        async: false,
        data: {host: ahost,
          task: atask,
          module: amodule,
          question: aquestion,
          prevquestion: aprevquestion,
          answer: vanswer,
          soundplayed: vSoundPlayed,
          category: vcategory,
          workout: vworkout_text,
        workout_draw: vworkout_text/*$("#workout_draw").val()*/},
        beforeSend: function () {
                //showBusy(atitle, 'Please wait...');
                $("#time_elapse_timer").TimeCircles().stop();
                $('.timeelapsed').hide();
                $('.time_circles').hide();
              },
              success: function (result) {
                if (result.result == 't') {
                  if (result.answer == '1') {
                        //$.notify("Your answer is correct.", "correct", "showDuration: 600");
                        $('.timeelapsed').hide();
                        $('.time_circles').hide();
                        //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                        //window.location.href = result.redirect_url;
                        messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                      } else {
                        fn_show_answer_solution(result.redirect_url, result.solution, atype, vanswer);
                      }
                    } else {
                    //$("#td_error").removeClass('select-file').addClass('v_error');  
                    $("#td_error").show();
                    $("#td_error").html(result.error);
                  }
                },

                error: function (xhr, textStatus, error) {
                  errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                },
                dataType: "json"
              });
    } else {
      errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
        /*if (atype == 1){
         $("#time_elapse_timer").TimeCircles().restart();
       }*/
     }


   }

   function no_need_answer(aquestion) {
    $.ajax({
      type: 'POST',
      url: '/my_special_exam/rsneednt_answer',
      async: false,
      data: {
        question: aquestion
      },
      beforeSend: function () {
            //showBusy(atitle, 'Please wait...');
          },
          success: function (result) {
            if (result.results == 't') {
              return true;
            } else {
              return false;
            }
          },

          error: function (xhr, textStatus, error) {
            errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
          },
          dataType: "json"
        });
  }

  function fn_complete_question_special(ahost, atask, amodule, aquestion, aprevquestion, atype) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
        var vcategory = $("#quiz_category").val();

        $.ajax({
          type: 'POST',
          url: '/my_special_exam/rsneednt_answer_next_url',
          async: false,
          data: {
            host: ahost,
            task: atask,
            module: amodule,
            question: aquestion,
            prevquestion: aprevquestion,
            category: vcategory,
          },
          beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.results == 't') {
                        //messageboxcorrect('You need not input to answer', 'Answer', result.redirect_url);
                        rsmodule_last_qustion_is_completed(ahost, atask, amodule, aquestion, aprevquestion, atype, 0);
                      } else {
                        rsmodule_last_qustion_is_completed(ahost, atask, amodule, aquestion, aprevquestion, atype, 1);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                    },
                    dataType: "json"
                  });

      } else {
        window.location.href = '/';
      }
    } else {
      MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
    }
  }

  function rsmodule_last_qustion_is_completed(ahost, atask, amodule, aquestion, aprevquestion, atype, ans_need_or_not) {

    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
            //var vanswer = getStudentAnswer();//CKEDITOR.instances['answer_editor'].getData();
            var vanswer = getStudentAnswer_check_sp(ans_need_or_not);//CKEDITOR.instances['answer_editor'].getData();
            var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
            var vcategory = $("#quiz_category").val();
            var d = new Date(); // for now
            var vhours = d.getHours();
            var vmins = d.getMinutes();
            var vsecs = d.getSeconds();
            var vend_time = vhours + ':' + vmins + ':' + vsecs;
            if (vanswer != '') {
              $.ajax({
                type: 'POST',
                url: '/my_special_exam/showCompleteQuestion',
                async: false,
                data: {host: ahost,
                  task: atask,
                  module: amodule,
                  question: aquestion,
                  prevquestion: aprevquestion,
                  answer: vanswer,
                  soundplayed: vSoundPlayed,
                  category: vcategory,
                  end_time: vend_time,
                  workout: vworkout_text,
                workout_draw: vworkout_text/*$("#workout_draw").val()*/},
                beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                                //$.notify("Your answer is correct.", "correct", "showDuration: 600");	
                                //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                                //window.location.href = result.redirect_url;
                                messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype, vanswer);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                        },
                        dataType: "json"
                      });
            } else {
              errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
            }



          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }


      }

      function fn_error_revision_special(ahost, atask, amodule, aquestion, arevision, atype) {
        if (sessionAvail() == 'f') {
          if (isOnline() == 't') {
            var vanswer = getStudentAnswer();
            var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
			//CKEDITOR.instances['answer_editor'].getData();
      var vcategory = $("#quiz_category").val();
      if (vanswer != '') {
        $.ajax({
          type: 'POST',
          url: '/my_special_exam/ErrorRevision',
          async: false,
          data: {host: ahost,
            task: atask,
            module: amodule,
            question: aquestion,
            revision: arevision,
            answer: vanswer,
            soundplayed: vSoundPlayed,
            category: vcategory,
            workout: vworkout_text,
          workout_draw: vworkout_text/*$("#workout_draw").val()*/},
          beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                                //$.notify("Your answer is correct.", "correct", "showDuration: 600");
                                $('.timeelapsed').hide();
                                $('.time_circles').hide();
                                //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                                //window.location.href = result.redirect_url;
                                messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                        },
                        dataType: "json"
                      });
      } else {
        errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
      }
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}

//new progress update
function fn_save_progress_byteacher(atask, amodule, aquestion, aprevquestion, auserid, aquizids,isnextQ){
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vanswer = CKEDITOR.instances['answer_editor'].getData();
      var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();

      if(aquizids!=0){
        var quiz_id = aquizids.split(',');

        quiz_id.shift();
        var vnext_id = quiz_id[0];
      }else{
       var vnext_id = 0; 
     }
     $.ajax({
      type: 'POST',
      url: '/show_answer/progress_update',
      async: false,
      data: {task: atask,
        module: amodule,
        question: aquestion,
        prevquestion: aprevquestion,
        userid: auserid,
        next_id: vnext_id,
        quiz_ids: quiz_id,
        answer:vanswer,
        isnextquestion:isnextQ,
        workout: vworkout_text,
      workout_draw: vworkout_text/*$("#workout_draw").val()*/},
      beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          window.location.href = result.redirect_url;
                        } else { 
                          $("#td_error").show();
                          $("#td_error").html(result.error);
                        }
                      },

                      error: function (xhr, textStatus, error) {
                        errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                      },
                      dataType: "json"
                    });
   } else {
    window.location.href = '/';
  }
} else {
  MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
}
}
/* new open source module question answer */


function fn_next_question_op_modules(atutor, atask, amodule, aquestion, aprevquestion, atype) {
  if (sessionAvail() == 'f') {
    if (isOnline() == 't') {
      var vcategory = $("#quiz_category").val();

      $.ajax({
        type: 'POST',
        url: '/all_tasks_opensource/rsneednt_answer_next_url',
        async: false,
        data: {
          tutor: atutor,
          task: atask,
          module: amodule,
          question: aquestion,
          prevquestion: aprevquestion,
          category: vcategory,
        },
        beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.results == 't') {
                        //messageboxcorrect('You need not input to answer', 'Answer', result.redirect_url);
                        provide_everyday_question_answer_op_module(atutor, atask, amodule, aquestion, aprevquestion, atype, 0);
                      } else {
                        provide_everyday_question_answer_op_module(atutor, atask, amodule, aquestion, aprevquestion, atype, 1);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                    },
                    dataType: "json"
                  });
    } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}

function provide_everyday_question_answer_op_module(atutor, atask, amodule, aquestion, aprevquestion, atype, ans_need_or_not) {
  var vanswer = getStudentAnswer_check_sp(ans_need_or_not);
  var vcategory = $("#quiz_category").val();
  var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
  if (vanswer != '') {
    $.ajax({
      type: 'POST',
      url: '/all_tasks_opensource/showNextQuestion',
      async: false,
      data: {tutor: atutor,
        task: atask,
        module: amodule,
        question: aquestion,
        prevquestion: aprevquestion,
        answer: vanswer,
        soundplayed: vSoundPlayed,
        category: vcategory,
        workout: vworkout_text,
      workout_draw: vworkout_text/* $("#workout_draw").val()*/},
      beforeSend: function () {
                //showBusy(atitle, 'Please wait...');

                $("#time_elapse_timer").TimeCircles().stop();
                $('.timeelapsed').hide();
                $('.time_circles').hide();

              },
              success: function (result) {
                if (result.result == 't') {
                  if (result.answer == '1') {
                        //$.notify("Your answer is correct.", "correct", "showDuration: 600");
                        $('.timeelapsed').hide();
                        $('.time_circles').hide();
                        //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                        messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                        //window.location.href = result.redirect_url;
                      } else {
                        fn_show_answer_solution(result.redirect_url, result.solution, atype,vanswer);
                        
                      }
                    } else {
                    //$("#td_error").removeClass('select-file').addClass('v_error');  
                    $("#td_error").show();
                    $("#td_error").html(result.error);
                  }
                },

                error: function (xhr, textStatus, error) {
                  errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                },
                dataType: "json"
              });
  } else {
    errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
        /*if (atype == 1){
         $("#time_elapse_timer").TimeCircles().restart();
       }*/
     }

   }
   /* new open source module question answer */

   /*  complete question opensource  */


   function fn_complete_question_op_modules(atutor, atask, amodule, aquestion, aprevquestion, atype) {
    if (sessionAvail() == 'f') {
      if (isOnline() == 't') {
       var vcategory = $("#quiz_category").val();
       $.ajax({
        type: 'POST',
        url: '/all_tasks_opensource/rsneednt_answer_next_url',
        async: false,
        data: {
          tutor: atutor,
          task: atask,
          module: amodule,
          question: aquestion,
          prevquestion: aprevquestion,
          category: vcategory,
        },
        beforeSend: function () {
                    //showBusy(atitle, 'Please wait...');
                  },
                  success: function (result) {
                    if (result.results == 't') {
                        //messageboxcorrect('You need not input to answer', 'Answer', result.redirect_url);
                        fn_everyday_completed_question_check_op_mod(atutor, atask, amodule, aquestion, aprevquestion, atype, 0);
                      } else {
                        fn_everyday_completed_question_check_op_mod(atutor, atask, amodule, aquestion, aprevquestion, atype, 1);
                      }
                    },

                    error: function (xhr, textStatus, error) {
                      errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                    },
                    dataType: "json"
                  });
     } else {
      window.location.href = '/';
    }
  } else {
    MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
  }
}
function fn_everyday_completed_question_check_op_mod(atutor, atask, amodule, aquestion, aprevquestion, atype, ans_need_or_not){
  // var vanswer = getStudentAnswer();//CKEDITOR.instances['answer_editor'].getData();
  var vanswer = getStudentAnswer_check_sp(ans_need_or_not);     
  var vcategory = $("#quiz_category").val();
  var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
            var d = new Date(); // for now
            var vhours = d.getHours();
            var vmins = d.getMinutes();
            var vsecs = d.getSeconds();
            var vend_time = vhours + ':' + vmins + ':' + vsecs;
            if (vanswer != '') {
              $.ajax({
                type: 'POST',
                url: '/all_tasks_opensource/showCompleteQuestion',
                async: false,
                data: {tutor: atutor,
                  task: atask,
                  module: amodule,
                  question: aquestion,
                  prevquestion: aprevquestion,
                  answer: vanswer,
                  soundplayed: vSoundPlayed,
                  category: vcategory,
                  end_time: vend_time,
                  workout: vworkout_text,
                workout_draw: vworkout_text/*$("#workout_draw").val()*/},
                beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                                //$.notify("Your answer is correct.", "correct", "showDuration: 600");	
                                //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                                //window.location.href = result.redirect_url;
                                messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype,vanswer);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                        },
                        dataType: "json"
                      });
            } else {
              errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
            } 
          }


          /*  complete question opensource  */
          /*  error revesion question opensource  */

          function fn_error_revision_opensource_modules(ahost, atask, amodule, aquestion, arevision, atype) {
            if (sessionAvail() == 'f') {
              if (isOnline() == 't') {
            var vanswer = getStudentAnswer();//CKEDITOR.instances['answer_editor'].getData();
            var vcategory = $("#quiz_category").val();
            var vworkout_text = CKEDITOR.instances['workout_editor_draw'].getData();
            if (vanswer != '') {
              $.ajax({
                type: 'POST',
                url: '/all_tasks_opensource/ErrorRevision',
                async: false,
                data: {host: ahost,
                  task: atask,
                  module: amodule,
                  question: aquestion,
                  revision: arevision,
                  answer: vanswer,
                  soundplayed: vSoundPlayed,
                  category: vcategory,
                  workout: vworkout_text,
                workout_draw: vworkout_text/*$("#workout_draw").val()*/},
                beforeSend: function () {
                        //showBusy(atitle, 'Please wait...');
                      },
                      success: function (result) {
                        if (result.result == 't') {
                          if (result.answer == '1') {
                                //$.notify("Your answer is correct.", "correct", "showDuration: 600");
                                $('.timeelapsed').hide();
                                $('.time_circles').hide();
                                //$(fanswerControl).notify("Your answer is correct.", "correct", "showDuration: 600", { position:"bottom" });
                                //window.location.href = result.redirect_url;
                                messageboxcorrect('Your answer is correct', 'Answer', result.redirect_url);
                              } else {
                                fn_show_answer_solution(result.redirect_url, result.solution, atype);
                              }
                            } else {
                            //$("#td_error").removeClass('select-file').addClass('v_error');  
                            $("#td_error").show();
                            $("#td_error").html(result.error);
                          }
                        },

                        error: function (xhr, textStatus, error) {
                          errormsg_dlg(getAjaxErrorMsg(xhr.responseText), 'Error in going to next question.');
                        },
                        dataType: "json"
                      });
            } else {
              errormsg_dlg('You must provide an answer to continue', 'Go to next question.');
            }
          } else {
            window.location.href = '/';
          }
        } else {
          MessageboxGoto('Your session is expire, please login again.', 'Session Expire', '/home');
        }
      }
      /*  error revision question opensource  */
