<?php

class MessageModel extends CI_Model
{

    public $loggedUserId, $loggedUserType;
    function __construct()
    {
        parent::__construct();
        $this->loggedUserId   = $this->session->userdata('user_id');
        $this->loggedUserType = $this->session->userdata('userType');
    }

    /**
     * Single row insert.
     *
     * @param string $tableName    table to insert data
     * @param array  $dataToInsert data
     *
     * @return integer             inserted item id
     */
    public function insert($tableName, $dataToInsert)
    {
        $this->db
            ->insert($tableName, $dataToInsert);

        return $this->db->insert_id();
    }

    /**
     * Get all message topics
     *
     * @return array all topics
     */
    public function allTopics($conditions = [])
    {
        return $this->db
            ->where($conditions)
            ->get('message_topics')
            ->result_array();
    }

    /**
     * Get all messages
     *
     * @return array all messages
     */
    public function allMessages($conditions = [])
    {
        return $this->db
            ->where($conditions)
            ->get('messages')
            ->result_array();
    }

    /**
     * Get info of a entry
     *
     * @param  array $conditions condition for where clause
     * @return array          faq info
     */
    public function info($tableName, $conditions)
    {
        $res = $this->db
            ->where($conditions)
            ->get($tableName)
            ->result_array();
        
        return count($res) ? $res[0] : [];
    }

    public function studentsOfTutor($conditions)
    {
        $this->db
            ->where($conditions)
            ->join('tbl_enrollment.sct_id=tbl_useraccount.id')
            ->get('tbl_enrollment');
    }
}
