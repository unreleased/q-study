<?php

class Admin_model extends CI_Model
{
   
    public function insertInfo($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function insertId($table, $data)
    {
        $this->db->insert($table, $data);

        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function getAllInfo($table)
    {
        $this->db->select('*');
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_where($select, $table, $columnName, $columnValue)
    {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($columnName, $columnValue);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSelectItem($select, $table)
    {
        $this->db->select($select);
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateInfo($table, $colName, $colValue, $data)
    {
        $this->db->where($colName, $colValue);
        $this->db->update($table, $data);
    }

    public function deleteInfo($table, $colName, $colValue)
    {
        $this->db->where($colName, $colValue);
        $this->db->delete($table);
    }

    public function getInfo($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }

    public function getRow($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function total_registered()
    {
        $this->db->select('tbl_useraccount.*,tbl_usertype.userType,tbl_country.countryName');
        $this->db->from('tbl_useraccount');
        
        $this->db->join('tbl_usertype', 'tbl_useraccount.user_type = tbl_usertype.id', 'LEFT');
        $this->db->join('tbl_country', 'tbl_useraccount.country_id = tbl_country.id', 'LEFT');
        
        $this->db->where('user_type != ', 0);
        $this->db->where('user_type != ', 7);

        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function today_registered()
    {
        $this->db->select('count(*) AS total_registered');
        $this->db->from('tbl_useraccount');
        
        $this->db->where('user_type != ', 0);
        $this->db->where('user_type != ', 7);
        //        $this->db->where('user_type != ', 7);

        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function tutor_with_10_student()
    {
        $query = $this->db->query(
            'SELECT *
                                    FROM tbl_useraccount t 
                                    LEFT JOIN tbl_country ON t.country_id = tbl_country.id 
                                    LEFT JOIN tbl_usertype ON t.user_type = tbl_usertype.id 
                                    WHERE ((SELECT count(*) FROM tbl_enrollment c WHERE c.sct_id = t.id)) >= 10 AND 
                                    user_type = 3'
        );
        return $query->result_array();
    }
    
    public function tutor_with_50_vocabulary()
    {
        $query = $this->db->query(
            'SELECT * '
                                . 'FROM tbl_useraccount t '
                                . 'LEFT JOIN tbl_country ON t.country_id = tbl_country.id 
                                   LEFT JOIN tbl_usertype ON t.user_type = tbl_usertype.id '
                                . 'WHERE ((SELECT COUNT(*) FROM tbl_question q WHERE q.user_id = t.id AND q.questionType = 3 HAVING COUNT(*))) >= 50 AND '
            . 'user_type = 3 '
        );
        return $query->result_array();
    }
    
    public function get_todays_data($date)
    {
        $query = $this->db->query('SELECT COUNT(*) AS today_registered FROM `tbl_useraccount` WHERE DATE(FROM_UNIXTIME(created)) LIKE "%'.$date.'%" ');
        return $query->result_array();
    }
    
    
    //Course Schedule
    public function get_course($subscription_type, $user_type, $country_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_course');
        
        $this->db->where('subscription_type', $subscription_type);
        $this->db->where('user_type', $user_type);
        $this->db->where('country_id', $country_id);

        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Course name by course ID
     *
     * @param integer $courseId course ID
     *
     * @return string            course name
     */
    public function courseName($courseId)
    {
        $res = $this->db
            ->select('courseName')
            ->where('id', $courseId)
            ->get('tbl_course')
            ->result_array();

        return isset($res[0]) ? $res[0]['courseName'] : '';
    }

    /**
     * Where not in functionality
     *
     * @param string $table       table name to filter through
     * @param string $selectorCol selector column
     * @param array  $filter      get all columns without these items
     *
     * @return array               result array
     */
    public function whereNotIn($table, $selectorCol, $filter)
    {
        $res = $this->db
            ->where_not_in($selectorCol, $filter)
            ->get($table)
            ->result_array();

        return $res;
    }

    /**
     * Insert multiple data
     *
     * @param string $table table to insert
     * @param array  $data  data to insert
     *
     * @return id            last insert if
     */
    public function insertBatch($table, $data)
    {
        $this->db->insert_batch($table, $data);
        return $this->db->insert_id();
    }
}
