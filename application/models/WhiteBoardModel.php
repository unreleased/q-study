<?php

class WhiteBoardModel extends CI_Model
{

    public $loggedUserId, $loggedUserType;
    function __construct()
    {
        parent::__construct();
        $this->loggedUserId   = $this->session->userdata('user_id');
        $this->loggedUserType = $this->session->userdata('userType');
    }


    
    var $id = 0;
    var $user_id = 0;
    var $module_id = 0;
    var $question_id = 0;
    var $rs_host_id = 0;
    var $answer = '';
    var $answer_date = '';
    var $answer_time = '';
    var $answer_default_date = '';
    var $answer_default_time = '';
    var $answer_right = 0;
    var $answer_mark_obtained = 0;
    var $answer_orig_mark = 0;
    var $time_elapse = 0;
    var $quiz_ids = array();
    var $soundplayed = 0;
    var $answer_mark_obtaineds = array();
    var $answer_orig_marks = array();
    var $quiz_cate = 0;
    var $workout = '';
    var $error_rev = 0;
    var $assign_score_id = 0;
    var $repeated = 0;
    var $workout_draw = '';
    var $assign_score_ids = array();
    //for score
    var $assign_score = array();
    var $score = 0;
    //for back
    var $back_quiz_ids = array();
    //error message
    var $error_msg = '';
    //key field of the table
    var $key_id = '';
    var $temp = '';

    function Quest_ans_student() {
        parent::__construct();
    }
    function cke_view(){
		return '<h2>test ckeditor</h2>';
	}
    function load($a_id = 0) {
        $stmt = "SELECT * FROM SP_QUEST_ANS_STUDENT_SEL_PRC(" . myInteger($a_id, true) . ");";
        $the_record = $this->db->query($stmt);
        if ($the_record) {
            $row = $the_record->row();
            if ($row) {
                $this->id = $row->R_ID;
                $this->user_id = $row->R_USER_ID;
                $this->module_id = $row->R_MODULE_ID;
                $this->question_id = $row->R_QUESTION_ID;
                $this->answer = $row->R_ANSWER;
                $this->answer_date = $row->R_ANSWER_DATE;
                $this->answer_time = $row->R_ANSWER_TIME;
                $this->answer_default_date = $row->R_ANSWER_DEFAULT_DATE;
                $this->answer_default_time = $row->R_ANSWER_DEFAULT_TIME;
                $this->answer_right = $row->R_ANSWER_RIGHT;
                $this->answer_mark_obtained = $row->R_ANSWER_MARK_OBTAINED;
                $this->answer_orig_mark = $row->R_ANSWER_ORIG_MARK;
                $this->time_elapse = $row->R_TIME_ELAPSE;
                $this->quiz_cate = $row->R_QUIZ_CATE;
                $this->workout = $row->R_WORKOUT;
                $this->assign_score_id = $row->R_ASSIGN_SCORE_ID;

                //only for blob fields
                $dbh = ibase_connect($this->config->item('DB_DATABASE'), $this->config->item('DB_USER'), $this->config->item('DB_PASS'));

                // Get result
                $result = ibase_query($dbh, $stmt);

                // Loop through results
                while ($row = ibase_fetch_object($result, IBASE_TEXT)) {
                    $this->workout = $row->R_WORKOUT;
                    $this->workout_draw = $row->R_WORKOUT_DRAW;
                }
                ibase_free_result($result);
                ibase_close($dbh);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function validate() {
        return true;
    }

    function backbutton() {
        $usertype = getSessionVar('userType');
        switch ($usertype) {
            case 2:
                $back = '/everyday-study';
                break;

            case 3:
                $back = '/everyday-study';
                break;

            case 4:
                $back = '/everyday-study';
                break;

            case 5:
                $back = '/everyday-study';
                break;

            case 6:
                $back = '/everyday-study';
                break;

            case 7:
                $back = '/everyday-study';
                break;

            case 8:
                $back = '/everyday-study';
                break;

            case 9:
                $back = '/everyday-study';
                break;

            case 10:
                $back = '/everyday-study';
                break;
        }
        return $back;
    }

    function backbuttonspecial() {
        return '/special-exam';
    }

    function showbackbuttonalltask() {
        return '/all-task';
    }
	
	function creationbackbuttons() {
		if ($this->uri->total_segments() > 4) {
			return '/grade-task/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4);
        }else{
			return '/module-creation';
		}

    }

    function getScoreValueFromId() {
        $score = array();
        $quiz_type = $this->getQuestionQuizCategory($this->question_id);
        if ($quiz_type != 9) {
            $load_score = $this->quiz_assign_score->load($this->question_id);
        } else {
            $load_score = $this->quiz_assign_score->loadAll($this->question_id);
        }
        if ($load_score) {
            if ($quiz_type != 9) {
                array_push($score, $this->quiz_assign_score->assign_score);
            } else {
                $score = $this->quiz_assign_score->assign_scores;
                $this->answer_orig_marks = $this->quiz_assign_score->assign_scores;
                $this->assign_score_ids = $this->quiz_assign_score->ids;
            }
            $return = $score;
        } else {
            $return = array();
        }
        return $return;
    }

    function checkAnswer($aquestion = 0, $aanswer = '') {
        $question_load = $this->question_answer->load($aquestion);
        if ($question_load) {
            $answer = $this->question_answer->answer;
            $solution = $this->question_answer->solution;
            if ($aanswer != '') {
                $subject = remove_para($answer);
                $pattern = remove_para($aanswer);
                //$pattern = "/$pattern/i";
                //$right = preg_match($pattern, $subject);
                if (strcasecmp($pattern, $subject) == 0) {
                    $right = 1;
                } else {
                    $right = 0;
                }
            } else {
                $right = 0;
            }
        } else {
            $right = 0;
        }
        $array = array('right' => $right, 'solution' => $solution);
        return $array;
    }

    function IsAnswerRight() {
        $question_load = $this->question_answer->load($this->question_id);
        if ($question_load) {
            $answer = $this->question_answer->answer;
            $score_value = $this->question_answer->score_value;
            $org_score_value = $this->question_answer->score_value;

            if ($this->answer != '') {
                //$subject = "$answer";
                //$pattern = '/' . $this->answer . '/i';
                //$right = preg_match($pattern, $subject);
                $subject = remove_para($answer);
                $pattern = remove_para($this->answer);
                //$pattern = "/$pattern/i";
                //$right = preg_match($pattern, $subject);
                if (strcasecmp($pattern, $subject) == 0) {
                    $right = 1;
                } else {
                    $right = 0;
                }
            } else {
                $right = 0;
            }
        } else {
            $right = 0;
        }
        $this->answer_right = $right;
        if ($right) {
            if ($this->soundplayed) {
                $score_value = $score_value - 1;
            } else {
                $score_value = $score_value;
            }
            $this->answer_mark_obtained = $score_value;
        } else {
            $this->answer_mark_obtained = 0;
        }
        $this->answer_orig_mark = $org_score_value;
        $load_score = $this->quiz_assign_score->load($this->question_id);
        if ($load_score) {
            $this->assign_score_id = $this->quiz_assign_score->id;
        } else {
            $this->assign_score_id = 0;
        }
    }

    function IsAnswerRightAssignment($index) {
        $question_load = $this->question_answer->load($this->question_id);
        if ($question_load) {
            $answer = $this->question_answer->answer;
            $score_value = $this->answer_orig_marks[$index]; //question_answer->score_value;
            if ($this->answer != '') {
                //$subject = "$answer";
                //$pattern = '/' . $this->answer . '/i';
                $subject = remove_para($answer);
                $pattern = remove_para($this->answer);
                //$pattern = "/$pattern/i";
                //$right = preg_match($pattern, $subject);
                if (strcasecmp($pattern, $subject) == 0) {
                    $right = 1;
                } else {
                    $right = 0;
                }
            } else {
                $right = 0;
            }
        } else {
            $right = 0;
        }

       // $this->answer_right = 0;
        $this->answer_right = $right;
        if ($right) {
            $score_value = $score_value;
            $this->answer_mark_obtained = 0;
        } else {
            $this->answer_mark_obtained = 0;
        }
        $this->answer_orig_mark = $score_value;
        $this->assign_score_id = $this->assign_score_ids[$index];
    }

    function getQuestionSolution() {
        $question_load = $this->question_answer->load($this->question_id);
        if ($question_load) {
            $solution = $this->question_answer->solution;
        } else {
            $solution = '';
        }
        return $solution;
    }

	function isWeekend_for_repeat_m($start_date, $end_date, $expected_days) {
    $start_timestamp = strtotime($start_date);
    $end_timestamp   = strtotime($end_date);
    $dates = array();
    while ($start_timestamp <= $end_timestamp) {
        if (in_array(date('l', $start_timestamp), $expected_days)) {
           $dates[] = date('Y-m-d', $start_timestamp);
        }
        $start_timestamp = strtotime('+1 day', $start_timestamp);
    }
    return $dates;
	}
	
	function save_repeat_task_for_year($host,$module,$question){
		$cur_date = date('Y-m-d');
		$startDate = date('Y-m-d',strtotime('+1 day', strtotime($cur_date)));
		//$EndDate = date('Y-m-d',strtotime('+1 year', strtotime($cur_date)));
		$EndDate = date('Y').'-12-31';
		$nxtDay = date('l',strtotime('+1 day', strtotime($cur_date)));
		$nxtDay2 = date('l',strtotime('+2 day', strtotime($cur_date)));
		$expected_days= array($nxtDay,$nxtDay2);
		
		$repeated_dates = $this->isWeekend_for_repeat_m($startDate, $EndDate, $expected_days);
		$repeated_dateList = implode(',',$repeated_dates);
		$user_id = getSessionVar('userid');
			$this->db->query("insert into QUESTION_REPEATED (HOST_ID,USER_ID,MODULE_ID,QUESTION_ID,STATUS,ANSWER_DEFAULT_DATE,REPEATED_DATES) values (".$host.",".$user_id.",".$module.",".$question.",0,'".$cur_date."','".$repeated_dateList."')");
		return true;
	}
	function is_already_answered($user_id,$module_id,$question_id){
		$reslt = $this->db->query("select ERROR_REV from QUEST_ANS_STUDENT where USER_ID='".$user_id."' and MODULE_ID='".$module_id."' AND QUESTION_ID='".$question_id."'")->row();
		if($reslt){
			$rrv = -1;
		}else{
			$rrv = 0;
		}
		return $rrv;
	}
    function save() {
		$erv = $this->is_already_answered($this->user_id,$this->module_id,$this->question_id);
        $this->id = -1;
        $userid = getSessionVar('userid');
        $this->user_id = $userid;
        $this->IsAnswerRight();
        $isanwerRight = $this->answer_right;
		$this->answer_default_date = date('Y-m-d');
		$this->answer_default_time = date('H:i:s');
        $this->error_rev = $erv;
        $valid = $this->validate();
        if ($valid) {
            $stmt = "SELECT * FROM SP_QUEST_ANS_STUDENT_IU_PRC("
                    . myInteger($this->id, true) . ","
                    . myInteger($this->user_id, true) . ","
                    . myInteger($this->module_id, true) . ","
                    . myInteger($this->question_id, true) . ","
                    . myBlob($this->answer) . ","
                    //. myDate($this->answer_date, true) . ","
                    //. myDate($this->answer_time, true) . ","
                    . myDate($this->answer_default_date, true) . ","
                    . myDate($this->answer_default_time, true) . ","
                    . myInteger($this->answer_right) . ","
                    . myMoney_custom($this->answer_mark_obtained,2) . ","
                    . myMoney_custom($this->answer_orig_mark, 2) . ","
                    . myInteger($this->time_elapse, true) . ","
                    . myInteger($this->quiz_cate, true) . ","
                    . myBlob($this->workout) . ","
                    . myInteger($this->error_rev) . ","
                    . myInteger($this->assign_score_id, true) . ","
                    . myInteger($this->repeated, true) . ","
                    . myBlob($this->workout_draw) . ");";

            $save_result = $this->db->query($stmt);
            if ($save_result) {
                $row = $save_result->row();
                if ($row) {
                    $this->id = $row->R_ID;
                    $return = array('return' => $row->R_ID, 'error' => '', 'right' => $isanwerRight, 'solution' => $this->getQuestionSolution());
					
					if($isanwerRight==0){
						$save_rpt = $this->save_repeat_task_for_year($this->rs_host_id,$this->module_id,$this->question_id);
					}
					
					
                    return $return; // $row->R_ID;
                } else {
                    $this->error_msg = 'Data fetching problem, save return false.';
                    $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                    return $return; //false;
                }
            } else {
                $this->error_msg = 'Save return false, Error in executing insert/update stored procedure.<br>' . $stmt;
                $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                return $return; //false;
            }
        } else {
            $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
            return $return; //false;
        }
    }

    function saveRepeat() {
        $this->id = -1;
        $userid = getSessionVar('userid');
        $this->user_id = $userid;
        $this->IsAnswerRight();
        $isanwerRight = $this->answer_right;
		$this->answer_default_date = date('Y-m-d');
		$this->answer_default_time = date('H:i:s');
        $this->error_rev = 0;
        $valid = $this->validate();
        if ($valid) {
            $stmt = "SELECT * FROM SP_QUEST_ANS_STUDENT_IU_PRC("
                    . myInteger($this->id, true) . ","
                    . myInteger($this->user_id, true) . ","
                    . myInteger($this->module_id, true) . ","
                    . myInteger($this->question_id, true) . ","
                    . myBlob($this->answer) . ","
                    //. myDate($this->answer_date, true) . ","
                    //. myDate($this->answer_time, true) . ","
                    . myDate($this->answer_default_date, true) . ","
                    . myDate($this->answer_default_time, true) . ","
                    . myInteger($this->answer_right) . ","
                     . myMoney_custom($this->answer_mark_obtained,2) . ","
                    . myMoney_custom($this->answer_orig_mark, 2) . ","
                    . myInteger($this->time_elapse, true) . ","
                    . myInteger($this->quiz_cate, true) . ","
                    . myBlob($this->workout) . ","
                    . myInteger(-1) . ","
                    . myInteger($this->assign_score_id, true) . ","
                    . myInteger($this->repeated, true) . ","
                    . myBlob($this->workout_draw) . ");";

            $save_result = $this->db->query($stmt);
            if ($save_result) {
                $row = $save_result->row();
                if ($row) {
                    $this->id = $row->R_ID;
                    $module_repeats_save = $this->module_repeats->save();
					
					$this->module_repeats->update_module_repeat_task($this->answer_right,$this->module_id,$this->user_id,$this->question_id);
					
                    if ($module_repeats_save) {
                        $return = array('return' => $row->R_ID, 'error' => '', 'right' => $isanwerRight, 'solution' => $this->getQuestionSolution());
                        return $return; // $row->R_ID;
                    } else {
                        $return = array('return' => '', 'error' => $this->module_repeats->error_msg, 'right' => '0', 'solution' => '');
                        return $return;
                    }
                } else {
                    $this->error_msg = 'Data fetching problem, save return false.';
                    $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                    return $return; //false;
                }
            } else {
                $this->error_msg = 'Save return false, Error in executing insert/update stored procedure.';
                $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                return $return; //false;
            }
        } else {
            $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
            return $return; //false;
        }
    }

    function saveAssignment($index) {
        $this->id = -1;
        $userid = getSessionVar('userid');
        $this->user_id = $userid;
        $this->IsAnswerRightAssignment($index);
        $isanwerRight = $this->answer_right;
        $this->answer_default_date = date('Y-m-d');
		$this->answer_default_time = date('H:i:s');
        $this->error_rev = 0;
        $valid = $this->validate();
        if ($valid) {
            $stmt = "SELECT * FROM SP_QUEST_ANS_STUDENT_IU_PRC("
                    . myInteger($this->id, true) . ","
                    . myInteger($this->user_id, true) . ","
                    . myInteger($this->module_id, true) . ","
                    . myInteger($this->question_id, true) . ","
                    . myBlob($this->answer) . ","
                    //. myDate($this->answer_date, true) . ","
                    //. myDate($this->answer_time, true) . ","
                    . myDate($this->answer_default_date, true) . ","
                    . myDate($this->answer_default_time, true) . ","
                    . myInteger($this->answer_right) . ","
                    . myMoney_custom($this->answer_mark_obtained,2) . ","
                    . myMoney_custom($this->answer_orig_mark, 2) . ","
                    . myInteger($this->time_elapse, true) . ","
                    . myInteger($this->quiz_cate, true) . ","
                    . myBlob($this->workout) . ","
                    . myInteger($this->error_rev) . ","
                    . myInteger($this->assign_score_id, true) . ","
                    . myInteger($this->repeated, true) . ","
                    . myBlob($this->workout_draw) . ");";

            $save_result = $this->db->query($stmt);
            if ($save_result) {
                $row = $save_result->row();
                if ($row) {
                    $this->id = $row->R_ID;
                    $return = array('return' => $row->R_ID, 'error' => '', 'right' => $isanwerRight, 'solution' => $this->getQuestionSolution());
					
					
					if($isanwerRight==0){
						$save_rept = $this->save_repeat_task_for_year($this->rs_host_id,$this->module_id,$this->question_id);
					}
					
                    return $return; // $row->R_ID;
                } else {
                    $this->error_msg = 'Data fetching problem, save return false.';
                    $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                    return $return; //false;
                }
            } else {
                $this->error_msg = 'Save return false, Error in executing insert/update stored procedure.<br>' . $stmt;
                $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                return $return; //false;
            }
        } else {
            $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
            return $return; //false;
        }
    }

    function saveAssignmentRepeat($index) {
        $this->id = -1;
        $userid = getSessionVar('userid');
        $this->user_id = $userid;
        $this->IsAnswerRightAssignment($index);
        $isanwerRight = $this->answer_right;
		$this->answer_default_date = date('Y-m-d');
		$this->answer_default_time = date('H:i:s');
        $this->error_rev = 0;
        $valid = $this->validate();
        if ($valid) {
            $stmt = "SELECT * FROM SP_QUEST_ANS_STUDENT_IU_PRC("
                    . myInteger($this->id, true) . ","
                    . myInteger($this->user_id, true) . ","
                    . myInteger($this->module_id, true) . ","
                    . myInteger($this->question_id, true) . ","
                    . myBlob($this->answer) . ","
                    //. myDate($this->answer_date, true) . ","
                    //. myDate($this->answer_time, true) . ","
                    . myDate($this->answer_default_date, true) . ","
                    . myDate($this->answer_default_time, true) . ","
                    . myInteger($this->answer_right) . ","
                    . myMoney_custom($this->answer_mark_obtained,2) . ","
                    . myMoney_custom($this->answer_orig_mark, 2) . ","
                    . myInteger($this->time_elapse, true) . ","
                    . myInteger($this->quiz_cate, true) . ","
                    . myBlob($this->workout) . ","
                    . myInteger(-1) . ","
                    . myInteger($this->assign_score_id, true) . ","
                    . myInteger($this->repeated, true) . ","
                    . myBlob($this->workout_draw) . ");";

            $save_result = $this->db->query($stmt);
            if ($save_result) {
                $row = $save_result->row();
                if ($row) {
                    $this->id = $row->R_ID;
                    $module_repeats_save = $this->module_repeats->save();
					
					$this->module_repeats->update_module_repeat_task($this->answer_right,$this->module_id,$this->user_id,$this->question_id);
					
					
                    if ($module_repeats_save) {
                        $return = array('return' => $row->R_ID, 'error' => '', 'right' => $isanwerRight, 'solution' => $this->getQuestionSolution());
                        return $return; // $row->R_ID;
                    } else {
                        $return = array('return' => '', 'error' => $this->module_repeats->error_msg, 'right' => '0', 'solution' => '');
                        return $return;
                    }
                } else {
                    $this->error_msg = 'Data fetching problem, save return false.';
                    $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                    return $return; //false;
                }
            } else {
                $this->error_msg = 'Save return false, Error in executing insert/update stored procedure.<br>' . $stmt;
                $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                return $return; //false;
            }
        } else {
            $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
            return $return; //false;
        }
    }

    function saveError() {
        $this->id = -1;
        $userid = getSessionVar('userid');
        $this->user_id = $userid;
        $this->IsAnswerRight();
		$this->answer_default_date = date('Y-m-d');
		$this->answer_default_time = date('H:i:s');
        $isanwerRight = $this->answer_right;
        $valid = $this->validate();
        if ($valid) {
            $stmt = "SELECT * FROM SP_QUEST_ANS_STUDENT_IU_PRC("
                    . myInteger($this->id, true) . ","
                    . myInteger($this->user_id, true) . ","
                    . myInteger($this->module_id, true) . ","
                    . myInteger($this->question_id, true) . ","
                    . myBlob($this->answer) . ","
                    //. myDate($this->answer_date, true) . ","
                    //. myDate($this->answer_time, true) . ","
                    . myDate($this->answer_default_date, true) . ","
                    . myDate($this->answer_default_time, true) . ","
                    . myInteger($this->answer_right) . ","
                    . myMoney_custom($this->answer_mark_obtained,2) . ","
                    . myMoney_custom($this->answer_orig_mark, 2) . ","
                    . myInteger($this->time_elapse, true) . ","
                    . myInteger($this->quiz_cate, true) . ","
                    . myBlob($this->workout) . ","
                    . myInteger($this->error_rev) . ","
                    . myInteger($this->assign_score_id, true) . ","
                    . myInteger($this->repeated, true) . ","
                    . myBlob($this->workout_draw) . ");";
            $save_result = $this->db->query($stmt);
            if ($save_result) {
                $row = $save_result->row();
                if ($row) {
                    $this->id = $row->R_ID;
                    $return = array('return' => $row->R_ID, 'error' => '', 'right' => $isanwerRight, 'solution' => $this->getQuestionSolution());
                    return $return; // $row->R_ID;
                } else {
                    $this->error_msg = 'Data fetching problem, save return false.';
                    $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                    return $return; //false;
                }
            } else {
                $this->error_msg = 'Save return false, Error in executing insert/update stored procedure.';
                $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                return $return; //false;
            }
        } else {
            $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
            return $return; //false;
        }
    }

    function saveAssignmentError($index) {
        $this->id = -1;
        $userid = getSessionVar('userid');
        $this->user_id = $userid;
        $this->IsAnswerRightAssignment($index);
        $isanwerRight = $this->answer_right;
		$this->answer_default_date = date('Y-m-d');
		$this->answer_default_time = date('H:i:s');
        $this->error_rev = 0;
        $valid = $this->validate();
        if ($valid) {
            $stmt = "SELECT * FROM SP_QUEST_ANS_STUDENT_IU_PRC("
                    . myInteger($this->id, true) . ","
                    . myInteger($this->user_id, true) . ","
                    . myInteger($this->module_id, true) . ","
                    . myInteger($this->question_id, true) . ","
                    . myBlob($this->answer) . ","
                    //. myDate($this->answer_date, true) . ","
                    //. myDate($this->answer_time, true) . ","
                    . myDate($this->answer_default_date, true) . ","
                    . myDate($this->answer_default_time, true) . ","
                    . myInteger($this->answer_right) . ","
                    . myMoney_custom($this->answer_mark_obtained,2) . ","
                    . myMoney_custom($this->answer_orig_mark, 2) . ","
                    . myInteger($this->time_elapse, true) . ","
                    . myInteger($this->quiz_cate, true) . ","
                    . myBlob($this->workout) . ","
                    . myInteger($this->error_rev) . ","
                    . myInteger($this->assign_score_id, true) . ","
                    . myInteger($this->repeated, true) . ","
                    . myBlob($this->workout_draw) . ");";
            $save_result = $this->db->query($stmt);
            if ($save_result) {
                $row = $save_result->row();
                if ($row) {
                    $this->id = $row->R_ID;
                    $return = array('return' => $row->R_ID, 'error' => '', 'right' => $isanwerRight, 'solution' => $this->getQuestionSolution());
                    return $return; // $row->R_ID;
                } else {
                    $this->error_msg = 'Data fetching problem, save return false.';
                    $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                    return $return; //false;
                }
            } else {
                $this->error_msg = 'Save return false, Error in executing insert/update stored procedure.<br>' . $stmt;
                $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
                return $return; //false;
            }
        } else {
            $return = array('return' => '', 'error' => $this->error_msg, 'right' => '0', 'solution' => '');
            return $return; //false;
        }
    }

    function delete($a_id = 0) {
        // Deletes the nominated ID
        $stmt = "SELECT R_STATUS FROM SP_QUEST_ANS_STUDENT_DEL_PRC(" . myInteger($a_id, true) . ");";
        $del_result = $this->db->query($stmt);
        if ($del_result) {
            $row = $del_result->row();
            if ($row) {
                if ($row->R_STATUS == 'Y') {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function deleteByModule($a_id = 0) {
        // Deletes the nominated ID
        $stmt = "SELECT R_STATUS FROM SP_DELETE_ANS_FOR_ALLTASKS(" . myInteger($a_id, true) . ");";
        $del_result = $this->db->query($stmt);
        if ($del_result) {
            $row = $del_result->row();
            if ($row) {
                if ($row->R_STATUS == 'Y') {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function clear() {
        // Clears out the properties of the current object and resets to starting point
        $this->id = 0;
        $this->user_id = 0;
        $this->module_id = 0;
        $this->question_id = 0;
        $this->answer = 0;
        $this->answer_date = '';
        $this->answer_time = '';
        $this->answer_default_date = '';
        $this->answer_default_time = '';
        $this->answer_right = 0;
        $this->answer_mark_obtained = 0;
        $this->answer_orig_mark = 0;
        $this->time_elapse = 0;
    }

    function show_main_title() {
        if ($this->uri->total_segments() > 3) {
            $taskfor = $this->uri->segment(3);
        } else {
            $taskfor = $this->uri->segment(2);
        }
        $uri = $this->uri->segment(1);
        switch ($taskfor) {
            case 4:
                switch ($uri) {
                    case 'all-task':
                        $title = 'Q-Study | Tutor | Tutorial';
                        break;

                    case 'all-tasks':
                        $title = 'Q-Study | Tutor | Tutorial';
                        break;

                    case 'my-exam':
                        $title = 'Q-Study | Tutor | Today\'s Task';
                        break;

                    case 'my-task':
                        $title = 'Q-Study | Tutor | Today\'s Task';
                        break;

                    case 'special-task';
                    case 'special-exam':
                        $title = 'Q-Study | Tutor | Special Exam';
                        break;

                    default:
                        $title = '';
                        break;
                }
                break;

            case 6:
                switch ($uri) {
                    case 'all-task':
                        $title = 'Q-Study | School | Tutorial';
                        break;

                    case 'all-tasks':
                        $title = 'Q-Study | School | Tutorial';
                        break;

                    case 'my-exam':
                        $title = 'Q-Study | School | Today\'s Task';
                        break;

                    case 'my-task':
                        $title = 'Q-Study | School | Today\'s Task';
                        break;

                    case 'special-task';
                    case 'special-exam':
                        $title = 'Q-Study | School | Special Exam';
                        break;

                    default:
                        $title = '';
                        break;
                }
                break;

            case 8:
                switch ($uri) {
                    case 'all-task':
                        $title = 'Q-Study | Corporate | Tutorial';
                        break;

                    case 'all-tasks':
                        $title = 'Q-Study | Corporate | Tutorial';
                        break;

                    case 'my-exam':
                        $title = 'Q-Study | Corporate | Today\'s Task';
                        break;

                    case 'my-task':
                        $title = 'Q-Study | Corporate | Today\'s Task';
                        break;

                    case 'special-task';
                    case 'special-exam':
                        $title = 'Q-Study | Corporate | Special Exam';
                        break;

                    default:
                        $title = '';
                        break;
                }
                break;

            case 10:
                switch ($uri) {
                    case 'all-task':
                        $title = 'Q-Study | Tutorial';
                        break;

                    case 'all-tasks':
                        $title = 'Q-Study | Tutorial';
                        break;

                    case 'my-exam':
                        $title = 'Q-Study | Today\'s Task';
                        break;

                    case 'my-task':
                        $title = 'Q-Study | Today\'s Task';
                        break;

                    case 'special-task';
                    case 'special-exam':
                        $title = 'Q-Study | Special Exam';
                        break;

                    default:
                        $title = '';
                        break;
                }
                break;

            default:
                $title = '';
                break;
        }
        return $title;
    }

    function show_main_title_progress() {
        $taskfor = $this->uri->segment(2);
        switch ($taskfor) {
            case 2:
                $title = 'Q-Study - View Task by Parent';
                break;

            case 4:
                $title = 'Q-Study - Task by Tutor';
                break;

            case 6:
                $title = 'Q-Study - Task by School';
                break;

            case 8:
                $title = 'Q-Study - Task by Corporate';
                break;

            case 10:
                $title = 'Q-Study - Task by Q-Study';
                break;

            default:
                $title = '';
                break;
        }
        return $title;
    }

    function show_page_title_ans_complete() {
        $taskfor = $this->uri->segment(3);
        switch ($taskfor) {
            case 4:
                $title = 'Q-Study | Tutor | Answer Complete';
                break;

            case 6:
                $title = 'Q-Study | School | Answer Complete';
                break;

            case 8:
                $title = 'Q-Study | Corporate | Answer Complete';
                break;

            case 10:
                $title = 'Q-Study | Answer Complete';
                break;

            default:
                $title = '';
                break;
        }
        return $title;
    }

    function show_page_title_error_rev() {
        $taskfor = $this->uri->segment(3);
        switch ($taskfor) {
            case 4:
                $title = 'Q-Study | Tutor | Error Revision';
                break;

            case 6:
                $title = 'Q-Study | School | Error Revision';
                break;

            case 8:
                $title = 'Q-Study | Corporate | Error Revision';
                break;

            case 10:
                $title = 'Q-Study | Error Revision';
                break;

            default:
                $title = '';
                break;
        }
        return $title;
    }

    function show_page_title_repeat() {
        $taskfor = $this->uri->segment(2);
        switch ($taskfor) {
            case 4:
                $title = 'Q-Study | Tutor | Repeat';
                break;

            case 6:
                $title = 'Q-Study | School | Repeat';
                break;

            case 8:
                $title = 'Q-Study | Corporate | Repeat';
                break;

            case 10:
                $title = 'Q-Study | Repeat';
                break;

            default:
                $title = '';
                break;
        }
        return $title;
    }

    function show_page_title() {
        /* $taskfor = $this->uri->segment(2);
          switch ($taskfor) {
          case 4:
          $title = 'Task for Tutor';
          break;

          case 6:
          $title = 'Task for School';
          break;

          case 8:
          $title = 'Task for Corporate';
          break;

          case 10:
          $title = 'Task for Q-Study';
          break;

          default:
          $title = '';
          break;
          } */
        $taskfor = $this->uri->segment(2);
        $uri = $this->uri->segment(1);
        switch ($uri) {
            case 'my-task':
                $title = 'Today\'s Task';
                break;

            case 'all-task':
               // $title = 'Tutorial';
                $title = 'Index';
                break;

            case 'special-task';
            case 'special-exam':
                $title = 'Special Exam';
                break;

            case 'answer-complete':
                switch ($taskfor) {
                    case 4:
                        $title = 'Q-Study | Tutor | Answer Complete';
                        break;

                    case 6:
                        $title = 'Q-Study | School | Answer Complete';
                        break;

                    case 8:
                        $title = 'Q-Study | Corporate | Answer Complete';
                        break;

                    case 10:
                        $title = 'Q-Study | Answer Complete';
                        break;

                    default:
                        break;
                }
                $title = 'Tutorial';
                break;

            case 8:
                $title = 'Task for Corporate';
                break;

            case 10:
                $title = 'Task for Q-Study';
                break;

            default:
                $title = '';
                break;
        }
        return $title;
    }

    function show_trackername($stmt) {
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $tracker_name = $row->R_TRACKER_NAME;
                $indi_name = $row->R_INDIVIDUAL_NAME;
                if ($tracker_name) {
                    if ($indi_name) {
                        $return = 'Tracker Name: ' . $tracker_name;
                        $return .= '<br> Individual Name: ' . $indi_name;
                    } else {
                        if ($tracker_name) {
                            $return = 'Tracker Name: ' . $tracker_name;
                        } else {
                            $return = '';
                        }
                    }
                } else {
                    if ($indi_name) {
                        $return = 'Individual Name: ' . $indi_name;
                    } else {
                        $return = '';
                    }
                }
            } else {
                $return = '';
            }
        } else {
            $return = '';
        }
        return $return;
    }

    function isModuleHasRepeatation($aModule = 0, $special = '', $hostid = 0) {
        $userid = myInteger(getSessionVar('userid'));
        $module = myInteger($aModule);
        $current_date = getSessionVar('current_date');
		//echo '<pre>';
		//print_r($this->session->all_userdata());exit;
        $stmt = 'select * from SP_MODULE_HAS_REPEAT(' . $module . ',' . $userid . ',' . myDate($current_date) . ')';
		
        //$return = array();
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = $row->R_RESULT;
                //array_push($return, $row->R_RESULT);
            } else {
                $return = 0;
            }
        } else {
            $return = 0;
        }
        $module_complete = $this->isModuleCompleted($aModule, $special, $hostid);
        if (($return) || ($module_complete)) {
            return true;
        } else {
            return false;
        }
    }
	
    function isModuleCompleted($aModule = 0, $special = '', $hostid = 0) {
        $userid = getSessionVar('userid');
        $taskfor = $this->uri->segment(2);
        $current_date = getSessionVar('current_date');
        $country = getSessionVar('country_name');
        switch ($taskfor) {
            case 4:
                //$stmt = 'select * from SP_MODULE_TUTOR(' . $userid . ')';
                if (!$special) {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_TUTOR\',' . myDate($current_date) . ',' . myInteger($hostid) . ')';
                } else {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_TUTOR_SE\',' . myDate($current_date) . ',' . myInteger($hostid) . ')';
                }
                $hostname = 'Tutor';
                break;

            case 6:
                //$stmt = 'select * from SP_MODULE_SCHOOL(' . $userid . ')';
                if (!$special) {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_SCHOOL\',' . myDate($current_date) . ')';
                } else {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_SCHOOL_SE\',' . myDate($current_date) . ')';
                }
                $hostname = 'School';
                break;

            case 8:
                //$stmt = 'select * from SP_MODULE_CORP(' . $userid . ')';
                if (!$special) {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_CORP\',' . myDate($current_date) . ')';
                } else {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_CORP_SE\',' . myDate($current_date) . ')';
                }
                $hostname = 'School';
                break;

            case 10:
                //$stmt = 'select * from SP_MODULE_QSTUDY(' . $userid . ')';
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    if (!$special) {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    } else {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_SE\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    }
                }  else {                    
                    if (!$special) {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_TR\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    } else {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_SE_TR\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    }
                }
                $hostname = 'QStudy';
                break;

            default:
                //$stmt = 'select * from SP_MODULE_QSTUDY(' . $userid . ')';
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    if (!$special) {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    } else {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_SE\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    }
                }  else {                    
                    if (!$special) {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_TR\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    } else {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_SE_TR\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    }
                }
                $hostname = 'QStudy';
                break;
        }

        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = $row->R_RESULT;
            } else {
                $return = 0;
            }
            /* foreach ($query->result() as $row) {
              if ($row->R_ID != $aModule) {
              $return = true;
              break;
              } else {
              $return = false;
              }
              } */
        } else {
            $return = 0;
        }
        if ($return == 0) {
            return true;
        } else {
            return false;
        }
        //return $return;
    }
	
	
    function isModuleHasRepeatation_by_ajax($aModule = 0, $special = '', $hostid = 0,$rs_task_for=0) {
        $userid = myInteger(getSessionVar('userid'));
        $module = myInteger($aModule);
        $current_date = getSessionVar('current_date');
		//echo '<pre>';
		//print_r($this->session->all_userdata());exit;
        $stmt = 'select * from SP_MODULE_HAS_REPEAT(' . $module . ',' . $userid . ',' . myDate($current_date) . ')';
		
        //$return = array();
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = $row->R_RESULT;
                //array_push($return, $row->R_RESULT);
            } else {
                $return = 0;
            }
        } else {
            $return = 0;
        }
        $module_complete = $this->isModuleCompleted_by_ajax($aModule, $special, $hostid,$rs_task_for);
        if (($return) || ($module_complete)) {
            return true;
        } else {
            return false;
        }
    }

	
    function isModuleCompleted_by_ajax($aModule = 0, $special = '', $hostid = 0,$rs_task_for=0) {
        $userid = getSessionVar('userid');
        $taskfor = $rs_task_for; //$this->uri->segment(2);
        $current_date = getSessionVar('current_date');
        $country = getSessionVar('country_name');
        switch ($taskfor) {
            case 4:
                //$stmt = 'select * from SP_MODULE_TUTOR(' . $userid . ')';
                if (!$special) {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_TUTOR\',' . myDate($current_date) . ',' . myInteger($hostid) . ')';
                } else {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_TUTOR_SE\',' . myDate($current_date) . ',' . myInteger($hostid) . ')';
                }
                $hostname = 'Tutor';
                break;

            case 6:
                //$stmt = 'select * from SP_MODULE_SCHOOL(' . $userid . ')';
                if (!$special) {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_SCHOOL\',' . myDate($current_date) . ')';
                } else {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_SCHOOL_SE\',' . myDate($current_date) . ')';
                }
                $hostname = 'School';
                break;

            case 8:
                //$stmt = 'select * from SP_MODULE_CORP(' . $userid . ')';
                if (!$special) {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_CORP\',' . myDate($current_date) . ')';
                } else {
                    $stmt = 'select * from SP_IS_MODULE_COMPLETE(' . $userid . ',' . $aModule . ',\'SP_MODULE_CORP_SE\',' . myDate($current_date) . ')';
                }
                $hostname = 'School';
                break;

            case 10:
                //$stmt = 'select * from SP_MODULE_QSTUDY(' . $userid . ')';
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    if (!$special) {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    } else {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_SE\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    }
                }  else {                    
                    if (!$special) {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_TR\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    } else {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_SE_TR\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    }
                }
                $hostname = 'QStudy';
                break;

            default:
                //$stmt = 'select * from SP_MODULE_QSTUDY(' . $userid . ')';
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    if (!$special) {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    } else {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_SE\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    }
                }  else {                    
                    if (!$special) {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_TR\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    } else {
                        $stmt = 'select * from SP_IS_MODULE_COMPLETE_QSTUDY(' . $userid . ',' . $aModule . ',\'SP_MODULE_QSTUDY_SE_TR\',' . myDate($current_date) . ',' . myInteger($country) . ')';
                    }
                }
                $hostname = 'QStudy';
                break;
        }

        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = $row->R_RESULT;
            } else {
                $return = 0;
            }
            /* foreach ($query->result() as $row) {
              if ($row->R_ID != $aModule) {
              $return = true;
              break;
              } else {
              $return = false;
              }
              } */
        } else {
            $return = 0;
        }
        if ($return == 0) {
            return true;
        } else {
            return false;
        }
        //return $return;
    }

    function getRepeatedQuestions($aModule = 0, $aparent = 0) {
        $userid = myInteger(getSessionVar('userid'));
       $module = myInteger($aModule);
		$current_date = getSessionVar('current_date');
	    $current_date = new DateTime($current_date);
        $current_date = date_format($current_date,"Y-m-d");
		
        $stmt = 'select * from SP_GET_MODULE_REPEAT_LISTS(' . $module . ',' . $userid . ',' . myDate($current_date) . ')';
		
        $taskfor = $this->uri->segment(2);
        $query = $this->db->query($stmt);
        if ($query) {
            $j = 2;
            $k = 1;
            $html = '';
			
            foreach ($query->result() as $row) {
                $html .= "<tr class='rsrepeat_questions' data-tt-id='" . $aparent . '-' . $j . "' data-tt-parent-id='" . $aparent . "'>";
                $alink = '/repeat/' . $taskfor . '/' . $row->R_MODULE_ID . '/' . $row->R_SELECTED_QUESTION . '/' . $row->R_QUESTION_ID . '/' . $row->R_CURRENT_DATE;
                $heldon = $row->R_ANSWER_DATE;
                $html .= '<td colspan="5"><span class=\'file\'><a href="' . $alink . '" class="alink">Repetition for [Q-' . $k . '] held on ' . $heldon . '</a></span></td>';
                //$html .= '<td>' . $row->R_SEL_SUBJECT_NAME . '</td>';
                //$html .= '<td>' . $row->R_SEL_CHAPTER_NAME . '</td>';
                $html .= '</tr>';
                // class="task_table_col"
                $j++;
                $k++;
            }
        } else {
            $html = 'Error in getting repeat question';
        }
        return $html;
    }

    function myEverydayTask() {
        $taskfor = $this->uri->segment(2);
        $userid = getSessionVar('userid');
        if ($taskfor == 4) {
            //for treeview
            $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
            $script .= '$("#maintask_table").treetable({ expandable: true });' . $this->config->item('CRLF');
            //$script .= '        $(".treetable").collapseAll();' . $this->config->item('CRLF');
            // Highlight selected row
            /*$script .= '            $("#maintask_table tbody").on("mousedown", "tr", function() {' . $this->config->item('CRLF');
            $script .= '            $(".selected").not(this).removeClass("selected");' . $this->config->item('CRLF');
            $script .= '            $(this).toggleClass("selected");' . $this->config->item('CRLF');
            $script .= '        });' . $this->config->item('CRLF');*/
            $script .= '</script>' . $this->config->item('CRLF');

            $stmt = 'select * from SP_GET_STUDENT_TUTORS(' . myInteger($userid) . ')';
            $html = '<table id="maintask_table" class="treetable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #D6E4F6;color:#000">Tutor Name</th>'; // class="task_table_header"
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $row->R_USER_LASTNAME . ' ' . $row->R_USER_FIRSTNAME . '</td>';
                    $html .= '</tr>';
                    //$html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= "<tr data-tt-id='c" . $row->R_ID . "' data-tt-parent-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $this->show_my_task($row->R_ID) . '</td>';
                    $html .= '</tr>';
                }
                $html .= '</tbody>';
            }
            $html .= '</table>';
        } else {
            $script = '';
            $html = $this->show_my_task();
        }
        return $html . $script;
    }

    function show_my_task($hostid = 0) {
		
        setSessionVar('task', '');
        setSessionVar('module', '');
        setSessionVar('question', '');
        setSessionVar('question_answer', '');
        setSessionVar('prevquestion', '');
        setSessionVar('allprev', '');
        setSessionVar('skip', '');
        setSessionVar('start_time', '');
        setSessionVar('end_time', '');
        setSessionVar('alltasks', '');

        $userid = getSessionVar('userid');
        $taskfor = $this->uri->segment(2);
        $current_date = getSessionVar('current_date');
        $current_date = new DateTime($current_date);
        $current_date = date_format($current_date,"Y-m-d");

        //perpare sql statement as per task
        switch ($taskfor) {
            case 4:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_TUTOR(' . myInteger($hostid) . ',' . myInteger($userid) . ',' . myDate($current_date) . ')';
                $hostname = 'Tutor';
                break;

            case 6:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_SCHOOL(' . $userid . ',' . myDate($current_date) . ')';
                $hostname = 'School';
                break;

            case 8:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_CORP(' . myInteger($userid) . ',' . myDate($current_date) . ')';
                $hostname = 'School';
                break;

            case 10:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_TR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                $hostname = 'QStudy';
                break;

            default:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_TR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                $hostname = 'QStudy';
                break;
        }       
        //for treeview
        $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
        //$script .= '    $(function() {' . $this->config->item('CRLF');
        $script .= '        $("#task_table").treetable({ expandable: true });' . $this->config->item('CRLF');
        // Highlight selected row
        $script .= '        $("#task_table tbody").on("mousedown", "tr", function() {' . $this->config->item('CRLF');
        $script .= '        $(".selected").not(this).removeClass("selected");' . $this->config->item('CRLF');
        $script .= '        $(this).toggleClass("selected");' . $this->config->item('CRLF');
        $script .= '        });' . $this->config->item('CRLF');
        $script .= '</script>' . $this->config->item('CRLF');
        //load user
        $load = $this->user_account->load($userid);
        if ($load) {
            if ($hostid == 0) {
                $parent_id = $this->user_account->host_id;
            } else {
                $parent_id = $hostid;
            }
            $student_year = $this->user_account->student_year;
            $parent_load = $this->user_account->load($parent_id);
            $school_id = $this->user_account->school_id;
            $school_load = $this->user_account->load($school_id);
            $corpo_id = $this->user_account->corporate_id;
            $corpo_load = $this->user_account->load($corpo_id);
            switch ($taskfor) {
                case 4:
                    if ($parent_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname;
                                    //$username = $lastname . ' ' . $fistname . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Tutor :' . $username;
                        } else {
                            $username = 'Tutor';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = '';
                        $logo = '';
                    }
                    break;

                case 6:
                    if ($school_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname ;
                                    //$username = $lastname . ' ' . $fistname . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'School :' . $username;
                        } else {
                            $username = 'School';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'School';
                        $logo = '';
                    }
                    break;

                case 8:
                    if ($corpo_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname;
                                    //$username = $lastname . ' ' . $fistname . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Corporate :' . $username;
                        } else {
                            $username = 'Corporate';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'Corporate';
                        $logo = '';
                    }
                    break;

                /* case 6:
                  $username = $this->user_account->user_lastname;
                  break; */

                case 10:
                    $username = 'Q-Study';
                    $logo = '';
                    break;
            }

            //make the top header for showing tracker name and logo
            $html = '<div id="task">';
            $html .= '<div class="header">' . $this->user_account->getlogo($logo);

            /* if ($username) {
              $html .= '<div class="title">' . $hostname . '-' . $username . '</div>';
              } else {
              $html .= '<div class="title">&nbsp;</div>';
              } */
            //$html .= '<div class="title">' . $this->show_trackername($stmt) . '</div>';
            $html .= '<div class="title">' . $username . '</div>';
            $html .= '</div>';
            //make modules header
            $html .= '<div class="table_div">';
            $html .= '<table id="task_table" class="treetable" style="border: 1px solid #C7DAF4;">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Module Name</th>'; // class="task_table_header"
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Tracker Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Individual Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Subject</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Chapter</th>';
            //$html .= '<th>Module ID</th>';
            //$html .= '<th>Has Repeat</th>';
            //$html .= '<th>User ID</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            //now make the parent tree node
            /* $html .= '<div class="progress_tree_div">';
              $html .= '<ul id="task_lists">' . $this->config->item('CRLF');
              $html .= '<li>'; */
            $html .= '<tbody>';

            $query = $this->db->query($stmt);
			
			//print_r($query->result());
			//exit;
			
            $i = 1;
            $j = 1;
            $k = 1;
            if ($query) {
				
				$arrays_slice = array_slice($query->result(), 0, 10);
                foreach ($arrays_slice as $row) {
				
               // foreach ($query->result() as $row) {
					
                    $hasRepeat = $this->isModuleHasRepeatation($row->R_ID, '', $hostid);
                   //$hasRepeat =false;
                    /* if (!$hasRepeat) {
                      $hasRepeat = $this->isModuleCompleted($row->R_ID);
                      } */
                    $tracker_name = $row->R_TRACKER_NAME;
                    $indi_name = $row->R_INDIVIDUAL_NAME;

                    //first create when module hasn't complete
                    $first_quiz = $this->getFirstQuizId($taskfor, $row->R_ID, '', 1, $hostid);
                   
                    if (!$hasRepeat) {
                        //if ($current_date == $row->R_MODULE_DATE){
                            if (!$hostid) {
                                $alink = '/my-exam/0/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                            } else {
                                $alink = '/my-exam/' . $hostid . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                            }
                        /* }  else {
                            $alink = 'undone';
                        } */
                    } else {
                        $alink = 'complete';
                    }
                    if ($student_year == $row->R_GRADE_YEAR) {
                        $isall_student = $row->R_IS_ALL_STUDENT;
                        if (!$isall_student) {//not all student, just selectde student
                            $students = explode(',', $row->R_SELECTED_STUDENTS);
                            if (count($students) > 0) {
                                foreach ($students as $student) {
                                    if ($student == $userid) {
                                        $html .= "<tr data-tt-id='" . $i . "'>";
                                        switch ($alink){
                                            case 'complete':
                                                $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                                break;
                                            
                                            case 'undone':
                                                $html .= '<td style="width:30%"><span class=\'undone\'>' . $row->R_MODULES_NAME . '</span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                break;
                                            
                                            default :
                                                $html .= '<td style="width:30%"><span class=\'file\'><a href="javascript:fn_everydaystudyvalidity(\'' . $alink . '\','.$k.')" data_attr_rowid="'.$k.'" class="alink rseveopen rsalink_'.$k.'">' . $row->R_MODULES_NAME . '</a></span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                $k++;
                                                break;
                                        }
                                        /*if ($alink) {
                                            $html .= '<td style="width:30%"><span class=\'file\'><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></span></td>';
                                            $html .= '<td>' . $tracker_name . '</td>';
                                            $html .= '<td>' . $indi_name . '</td>';
                                            $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                            $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                            $html .= '</tr>';
                                        } else {
                                            $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                            $html .= '<td>' . $tracker_name . '</td>';
                                            $html .= '<td>' . $indi_name . '</td>';
                                            $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                            $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                            $html .= '</tr>';
                                            $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                        }*/
                                        //}
                                        //$html .= '<tr>';
                                    }
                                }
                            }
                        } else {//all student
                            $html .= "<tr data-tt-id='" . $i . "'>";
                            switch ($alink){
                                case 'complete':
                                    $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                    $html .= '</tr>';
                                    break;

                                case 'undone':
                                    $html .= '<td style="width:30%"><span class=\'undone\'>' . $row->R_MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= '</tr>';
                                    break;

                                default :
                                    $html .= '<td style="width:30%"><span class=\'file\'><a href="javascript:fn_everydaystudyvalidity(\'' . $alink . '\','.$k.')" data_attr_rowid="'.$k.'" class="alink rseveopen rsalink_'.$k.'">' . $row->R_MODULES_NAME . '</a></span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= '<tr>';
                                    $k++;
                                    break;
                            }
                            
                            /*
                            if ($alink) {
                                $html .= '<td style="width:30%"><span class=\'file\'><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></span></td>';
                                $html .= '<td>' . $tracker_name . '</td>';
                                $html .= '<td>' . $indi_name . '</td>';
                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                $html .= '<tr>';
                            } else {
                                $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                $html .= '<td>' . $tracker_name . '</td>';
                                $html .= '<td>' . $indi_name . '</td>';
                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                $html .= '</tr>';
                            }
                            */
                            //$html .= '<tr>';
                        }
                    }
                    $i++;
                    $j++;
                }//end of foreach loop
            }
            $html .= '</tbody>';
            //$html .= '</li>';
            //$html .= '</ul>';
            $html .= '</table>';
			$html .= '<a class="rsload_more" data_host_id="'.$hostid.'" data_load_timer="10" href="javascript:fn_load_next_some('.$taskfor.')">Load nore</a>';
            $html .= '</div>';
            $html .= '</div>';
        } else {
            $html = $this->user_account->error_msg;
        }
        return $html . $script;
    }

    function mySpecialTask_recent() {
        $taskfor = $this->uri->segment(2);
        $userid = getSessionVar('userid');
        if ($taskfor == 4) {
            //for treeview
            $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
            $script .= '        $("#maintask_table").treetable({ expandable: true });' . $this->config->item('CRLF');
            $script .= '</script>' . $this->config->item('CRLF');

            $stmt = 'select * from SP_GET_STUDENT_TUTORS(' . myInteger($userid) . ')';
            $html = '<table id="maintask_table" class="treetable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #D6E4F6;color:#000">Tutor Name</th>'; // class="task_table_header"
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $row->R_USER_LASTNAME . ' ' . $row->R_USER_FIRSTNAME . '</td>';
                    $html .= '</tr>';
                    //$html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= "<tr data-tt-id='c" . $row->R_ID . "' data-tt-parent-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $this->show_my_recent_special_task($row->R_ID) . '</td>';
                    $html .= '</tr>';
                }
                $html .= '</tbody>';
            }
            $html .= '</table>';
        } else {
            $html = $this->show_my_recent_special_task();
        }
        //return $html;// . $script;
        return $html . $script;
    }

	function show_my_recent_special_task($hostid = 0){
		setSessionVar('task', '');
        setSessionVar('module', '');
        setSessionVar('question', '');
        setSessionVar('question_answer', '');
        setSessionVar('prevquestion', '');
        setSessionVar('allprev', '');
        setSessionVar('skip', '');
        setSessionVar('start_time', '');
        setSessionVar('end_time', '');
        setSessionVar('alltasks', '');

        $userid = getSessionVar('userid');
        $taskfor = $this->uri->segment(2);
        $current_date = getSessionVar('current_date');
        $current_date = new DateTime($current_date);
        $current_date = date_format($current_date,"Y-m-d");
		$cdate = date('Y-m-d');
		if($hostid==0){
			$getHostId = $this->db->query("select R_ID,R_PARENT_ID from SP_GET_USER_HOST('".$userid."','".$taskfor."')")->row();
			if($getHostId){
				$hostid = $getHostId->R_PARENT_ID;
			}
		}
		
		
	/*prepare your procedure*/
        //perpare sql statement as per task
        switch ($taskfor) {
            case 4:
                $stmt = "select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=2 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') and (STUDENT_ANSWERED is null or STUDENT_ANSWERED not containing '".$userid."') and SPECIFIC_DATES_FROM <='".$cdate."' and SPECIFIC_DATES_TO >='".$cdate."' order by QUIZ_MODULE.ID ASC";
                $hostname = 'Tutor';
                break;

            case 6:
                $stmt = "select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=2 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') and (STUDENT_ANSWERED is null or STUDENT_ANSWERED not containing '".$userid."') and SPECIFIC_DATES_FROM <='".$cdate."' and SPECIFIC_DATES_TO >='".$cdate."' order by QUIZ_MODULE.ID ASC";
                $hostname = 'School';
                break;

            case 8:
                $stmt = "select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=2 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') and (STUDENT_ANSWERED is null or STUDENT_ANSWERED not containing '".$userid."') and SPECIFIC_DATES_FROM <='".$cdate."' and SPECIFIC_DATES_TO >='".$cdate."' order by QUIZ_MODULE.ID ASC";
                $hostname = 'School';
                break;

            case 10:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
				
				if($hostid==0){
					$gethost = $this->db->query("select USER_ID from USER_ACCOUNT where USER_TYPE=10")->row();
					if($gethost){
						$hostid = $gethost->USER_ID;
					}
				}
				
                if (!$account_type){
                    $stmt = "select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=2 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') and (STUDENT_ANSWERED is null or STUDENT_ANSWERED not containing '".$userid."') and COUNTRY_ID='".$country."' and FOR_TRIAL IS NULL and SPECIFIC_DATES_FROM <='".$cdate."' and SPECIFIC_DATES_TO >='".$cdate."' order by QUIZ_MODULE.ID ASC";
                }  else {
                    $stmt = "select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=2 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') and (STUDENT_ANSWERED is null or STUDENT_ANSWERED not containing '".$userid."') and COUNTRY_ID='".$country."' and FOR_TRIAL=1 and SPECIFIC_DATES_FROM <='".$cdate."' and SPECIFIC_DATES_TO >='".$cdate."' order by QUIZ_MODULE.ID ASC";
                }
                $hostname = 'QStudy';
                break;

            default:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_SE(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_SETR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                $hostname = 'QStudy';
                break;
        }
		//echo $stmt;exit;
		$this->db->query($stmt)->result();
		/* echo '<pre>';
		print_r($r);
		exit; */
        //for treeview
        $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
        //$script .= '    $(function() {' . $this->config->item('CRLF');
        $script .= '        $("#task_table").treetable({ expandable: true });' . $this->config->item('CRLF');
        // Highlight selected row
        $script .= '$("#task_table tbody").on("mousedown", "tr", function() {' . $this->config->item('CRLF');
        $script .= '        $(".selected").not(this).removeClass("selected");' . $this->config->item('CRLF');
        $script .= '        $(this).toggleClass("selected");' . $this->config->item('CRLF');
        $script .= '        });' . $this->config->item('CRLF');
        $script .= '</script>' . $this->config->item('CRLF');
        //load user
        $load = $this->user_account->load($userid);
        if ($load) {
            $parent_id = $this->user_account->host_id;
            $student_year = $this->user_account->student_year;
            if (!$hostid) {
                $parent_load = $this->user_account->load($parent_id);
            } else {
                $parent_load = $this->user_account->load($hostid);
            }
            $school_id = $this->user_account->school_id;
            $school_load = $this->user_account->load($school_id);
            $corpo_id = $this->user_account->corporate_id;
            $corpo_load = $this->user_account->load($corpo_id);
            switch ($taskfor) {
                case 4:
                    if ($parent_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Tutor :' . $username;
                        } else {
                            $username = 'Tutor';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = '';
                        $logo = '';
                    }
                    break;

                case 6:
                    if ($school_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'School :' . $username;
                        } else {
                            $username = 'School';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'School';
                        $logo = '';
                    }
                    break;

                case 8:
                    if ($corpo_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Corporate :' . $username;
                        } else {
                            $username = 'Corporate';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'Corporate';
                        $logo = '';
                    }
                    break;
                case 10:
                    $username = 'Q-Study';
                    $logo = '';
                    break;
            }

			/* new query for recent special_exam */
		
			/* new query for recent special_exam */
			
			
            //make the top header for showing tracker name and logo
            $html = '<div id="task">';
            $html .= '<div class="header">' . $this->user_account->getlogo($logo);
            $html .= '<div class="title">' . $username . '</div>';
            $html .= '</div>';
            //make modules header
            $html .= '<div class="table_div">';
            $html .= '<table id="task_table" class="treetable" style="border: 1px solid #C7DAF4;">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Module Name</th>'; // class="task_table_header"
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Tracker Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Individual Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Subject</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Chapter</th>';
            $html .= '</tr>';
            $html .= '</thead>';
         
            $html .= '<tbody>';

            $query = $this->db->query($stmt);
			
            $i = 1;
            $j = 1;
            if ($query) {
                //print_r($query->result());exit;
                foreach ($query->result() as $row) {
                    
                    $tracker_name = $row->TRACKER_NAME;
                    $indi_name = $row->INDIVIDUAL_NAME;

                    //first create when module hasn't complete
                    $first_quiz = $this->getFirstQuizIdse($taskfor, $row->ID, '', 1, $hostid);
                   
                      
                            if (!$hostid) {
                                $alink = '/special-exam/0/' . $taskfor . '/' . $row->ID . '/' . $first_quiz . '/0';
                            } else {
                                $alink = '/special-exam/' . $hostid . '/' . $taskfor . '/' . $row->ID . '/' . $first_quiz . '/0';
                            }
                    
                    if ($student_year == $row->GRADE_YEAR) {
                        $isall_student = $row->IS_ALL_STUDENT;
                            $html .= "<tr data-tt-id='" . $i . "'>";
							$html .= '<td style="width:30%"><span class=\'file\'><a data_attrftime="'.$row->FROM_TIME.'" href="javascript:special_examValidity(\'' . $alink . '\',\'' . $row->NON_SPECIFIC_TIME . '\',\'' . $row->FROM_TIME . '\',\'' . $row->TO_TIME . '\',\'' . $row->SPECIFIC_DATES_FROM . '\',\'' . $row->SPECIFIC_DATES_TO . '\',\'' . $row->NON_SPECIFIC_DATE . '\',\'' .  $hostid . '\',\'' . $row->ID . '\')" class="alink">' . $row->MODULES_NAME . '</a></span></td>';
							$html .= '<td>' . $tracker_name . '</td>';
							$html .= '<td>' . $indi_name . '</td>';
							$html .= '<td>' . $row->SUBJECT_NAME . '</td>';
							$html .= '<td>' . $row->CHAPTER_NAME . '</td>';
							$html .= '<tr>';
                    }
                    $i++;
                    $j++;
                }//end of foreach loop
            }
            $html .= '</tbody>';
            //$html .= '</li>';
            //$html .= '</ul>';
            $html .= '</table>';
            $html .= '</div>';
            $html .= '</div>';
        } else {
            $html = $this->user_account->error_msg;
        }
        return $html . $script;
	}
	
    function mySpecialTask() {
        $taskfor = $this->uri->segment(2);
        $userid = getSessionVar('userid');
        if ($taskfor == 4) {
            //for treeview
            $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
            $script .= '        $("#maintask_table").treetable({ expandable: true });' . $this->config->item('CRLF');
            //$script .= '        $(".treetable").collapseAll();' . $this->config->item('CRLF');
            // Highlight selected row
            /*$script .= '            $("#maintask_table tbody").on("mousedown", "tr", function() {' . $this->config->item('CRLF');
            $script .= '            $(".selected").not(this).removeClass("selected");' . $this->config->item('CRLF');
            $script .= '            $(this).toggleClass("selected");' . $this->config->item('CRLF');
            $script .= '        });' . $this->config->item('CRLF');*/
            $script .= '</script>' . $this->config->item('CRLF');

            $stmt = 'select * from SP_GET_STUDENT_TUTORS(' . myInteger($userid) . ')';
            $html = '<table id="maintask_table" class="treetable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #D6E4F6;color:#000">Tutor Name</th>'; // class="task_table_header"
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $row->R_USER_LASTNAME . ' ' . $row->R_USER_FIRSTNAME . '</td>';
                    $html .= '</tr>';
                    //$html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= "<tr data-tt-id='c" . $row->R_ID . "' data-tt-parent-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $this->show_my_special_task($row->R_ID) . '</td>';
                    $html .= '</tr>';
                }
                $html .= '</tbody>';
            }
            $html .= '</table>';
        } else {
            $html = $this->show_my_special_task();
        }
        //return $html;// . $script;
        return $html . $script;
    }
	
	
    function show_my_special_task($hostid = 0) {
        setSessionVar('task', '');
        setSessionVar('module', '');
        setSessionVar('question', '');
        setSessionVar('question_answer', '');
        setSessionVar('prevquestion', '');
        setSessionVar('allprev', '');
        setSessionVar('skip', '');
        setSessionVar('start_time', '');
        setSessionVar('end_time', '');
        setSessionVar('alltasks', '');

        $userid = getSessionVar('userid');
        $taskfor = $this->uri->segment(2);
        $current_date = getSessionVar('current_date');
        $current_date = new DateTime($current_date);
        $current_date = date_format($current_date,"Y-m-d");

	/*prepare your procedure*/
        //perpare sql statement as per task
        switch ($taskfor) {
            case 4:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_TUTOR_SE(' . myInteger($hostid) . ',' . myInteger($userid) . ',' . myDate($current_date) . ')';
                $hostname = 'Tutor';
                break;

            case 6:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_SCHOOL_SE(' . $userid . ',' . myDate($current_date) . ')';
                $hostname = 'School';
                break;

            case 8:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_CORP_SE(' . myInteger($userid) . ',' . myDate($current_date) . ')';
                $hostname = 'School';
                break;

            case 10:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_SE(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_SETR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                $hostname = 'QStudy';
                break;

            default:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_SE(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_SETR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                $hostname = 'QStudy';
                break;
        }
        //for treeview
        $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
        //$script .= '    $(function() {' . $this->config->item('CRLF');
        $script .= '        $("#task_table").treetable({ expandable: true });' . $this->config->item('CRLF');
        // Highlight selected row
        $script .= '        $("#task_table tbody").on("mousedown", "tr", function() {' . $this->config->item('CRLF');
        $script .= '        $(".selected").not(this).removeClass("selected");' . $this->config->item('CRLF');
        $script .= '        $(this).toggleClass("selected");' . $this->config->item('CRLF');
        $script .= '        });' . $this->config->item('CRLF');
        $script .= '</script>' . $this->config->item('CRLF');
        //load user
        $load = $this->user_account->load($userid);
        if ($load) {
            $parent_id = $this->user_account->host_id;
            $student_year = $this->user_account->student_year;
            if (!$hostid) {
                $parent_load = $this->user_account->load($parent_id);
            } else {
                $parent_load = $this->user_account->load($hostid);
            }
            $school_id = $this->user_account->school_id;
            $school_load = $this->user_account->load($school_id);
            $corpo_id = $this->user_account->corporate_id;
            $corpo_load = $this->user_account->load($corpo_id);
            switch ($taskfor) {
                case 4:
                    if ($parent_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Tutor :' . $username;
                        } else {
                            $username = 'Tutor';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = '';
                        $logo = '';
                    }
                    break;

                case 6:
                    if ($school_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'School :' . $username;
                        } else {
                            $username = 'School';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'School';
                        $logo = '';
                    }
                    break;

                case 8:
                    if ($corpo_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Corporate :' . $username;
                        } else {
                            $username = 'Corporate';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'Corporate';
                        $logo = '';
                    }
                    break;

                /* case 6:
                  $username = $this->user_account->user_lastname;
                  break; */

                case 10:
                    $username = 'Q-Study';
                    $logo = '';
                    break;
            }

            //make the top header for showing tracker name and logo
            $html = '<div id="task">';
            $html .= '<div class="header">' . $this->user_account->getlogo($logo);

            /* if ($username) {
              $html .= '<div class="title">' . $hostname . '-' . $username . '</div>';
              } else {
              $html .= '<div class="title">&nbsp;</div>';
              } */
            //$html .= '<div class="title">' . $this->show_trackername($stmt) . '</div>';
            $html .= '<div class="title">' . $username . '</div>';
            $html .= '</div>';
            //make modules header
            $html .= '<div class="table_div">';
            $html .= '<table id="task_table" class="treetable" style="border: 1px solid #C7DAF4;">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Module Name</th>'; // class="task_table_header"
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Tracker Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Individual Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Subject</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Chapter</th>';
            //$html .= '<th>Module ID</th>';
            //$html .= '<th>Has Repeat</th>';
            //$html .= '<th>User ID</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            //now make the parent tree node
            /* $html .= '<div class="progress_tree_div">';
              $html .= '<ul id="task_lists">' . $this->config->item('CRLF');
              $html .= '<li>'; */
            $html .= '<tbody>';

            $query = $this->db->query($stmt);
            $i = 1;
            $j = 1;
            if ($query) {
                //print_r($query->result());exit;
                foreach ($query->result() as $row) {
                    $hasRepeat = $this->isModuleHasRepeatation($row->R_ID, 't', $hostid);
                    /* if (!$hasRepeat) {
                      $hasRepeat = $this->isModuleCompleted($row->R_ID);
                      } */
                    $tracker_name = $row->R_TRACKER_NAME;
                    $indi_name = $row->R_INDIVIDUAL_NAME;

                    //first create when module hasn't complete
                    $first_quiz = $this->getFirstQuizIdse($taskfor, $row->R_ID, '', 1, $hostid);
                    if (!$hasRepeat) {
                       if (($current_date >= $row->R_SPECIFIC_DATES_FROM) && ($current_date <= $row->R_SPECIFIC_DATES_TO)){
                            if (!$hostid) {
                                $alink = '/special-exam/0/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                            } else {
                                $alink = '/special-exam/' . $hostid . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                            }
                        }  else {
                            $alink = 'undone';
                        }
                        /*if (!$hostid) {
                           $alink = '/special-exam/' . $hostid . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0'; 
                        }else
                           $alink = '/special-exam/' . $hostid . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                        }*/
                    } else {
                        $alink = 'complete';
                    }
                    if ($student_year == $row->R_GRADE_YEAR) {
                        $isall_student = $row->R_IS_ALL_STUDENT;
                        if (!$isall_student) {//not all student, just selectde student
                            $students = explode(',', $row->R_SELECTED_STUDENTS);
                            if (count($students) > 0) {
                                foreach ($students as $student) {
                                    if ($student == $userid) {
                                        $html .= "<tr data-tt-id='" . $i . "'>";
                                        switch ($alink){
                                            case 'complete':
                                                $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                                break;
                                            
                                            case 'undone':
                                                $html .= '<td style="width:30%"><span class=\'undone\'>' . $row->R_MODULES_NAME . '</span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                break;
                                            
                                            default :
                                                $html .= '<td style="width:30%"><span class=\'file\'><a  data_attrftime="'.$row->R_FROM_TIME.'" href="javascript:special_examValidity(\'' . $alink . '\',\'' . $row->R_NON_SPECIFIC_TIME . '\',\'' . $row->R_FROM_TIME . '\',\'' . $row->R_TO_TIME . '\',\'' . $row->R_SPECIFIC_DATES_FROM . '\',\'' . $row->R_SPECIFIC_DATES_TO . '\',\'' . $row->R_NON_SPECIFIC_DATE . '\',\'' .  $hostid . '\',\'' . $row->R_ID . '\')" class="alink timevalidity_module">' . $row->R_MODULES_NAME . '</a></span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                break;
                                        }
                                        
                                        /*if ($alink) {
                                            $html .= '<td style="width:30%"><span class=\'file\'><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></span></td>';
                                            $html .= '<td>' . $tracker_name . '</td>';
                                            $html .= '<td>' . $indi_name . '</td>';
                                            $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                            $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                            $html .= '</tr>';
                                        } else {
                                            $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                            $html .= '<td>' . $tracker_name . '</td>';
                                            $html .= '<td>' . $indi_name . '</td>';
                                            $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                            $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                            $html .= '</tr>';
                                            $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                        }*/
                                        //}
                                        //$html .= '<tr>';
                                    }
                                }
                            }
                        } else {//all student
                            $html .= "<tr data-tt-id='" . $i . "'>";
                            switch ($alink){
                                case 'complete':
                                    $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                    $html .= '</tr>';
                                    break;
                                
                                case 'undone':
                                    $html .= '<td style="width:30%"><span class=\'undone\'>' . $row->R_MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= '</tr>';
                                    break;
                                
                                default :
                                    $html .= '<td style="width:30%"><span class=\'file\'><a data_attrftime="'.$row->R_FROM_TIME.'" href="javascript:special_examValidity(\'' . $alink . '\',\'' . $row->R_NON_SPECIFIC_TIME . '\',\'' . $row->R_FROM_TIME . '\',\'' . $row->R_TO_TIME . '\',\'' . $row->R_SPECIFIC_DATES_FROM . '\',\'' . $row->R_SPECIFIC_DATES_TO . '\',\'' . $row->R_NON_SPECIFIC_DATE . '\',\'' .  $hostid . '\',\'' . $row->R_ID . '\')" class="alink">' . $row->R_MODULES_NAME . '</a></span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= '<tr>';
                                    break;
                            }
                            /*if ($alink) {
                                $html .= '<td style="width:30%"><span class=\'file\'><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></span></td>';
                                $html .= '<td>' . $tracker_name . '</td>';
                                $html .= '<td>' . $indi_name . '</td>';
                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                $html .= '<tr>';
                            } else {
                                $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                $html .= '<td>' . $tracker_name . '</td>';
                                $html .= '<td>' . $indi_name . '</td>';
                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                $html .= '</tr>';
                            }*/
                            //$html .= '<tr>';
                        }
                    }
                    $i++;
                    $j++;
                }//end of foreach loop
            }
            $html .= '</tbody>';
            //$html .= '</li>';
            //$html .= '</ul>';
            $html .= '</table>';
            $html .= '</div>';
            $html .= '</div>';
        } else {
            $html = $this->user_account->error_msg;
        }
        return $html . $script;
    }

    function show_my_task1() {
        setSessionVar('task', '');
        setSessionVar('module', '');
        setSessionVar('question', '');
        setSessionVar('question_answer', '');
        setSessionVar('prevquestion', '');
        setSessionVar('allprev', '');
        setSessionVar('skip', '');
        setSessionVar('start_time', '');
        setSessionVar('end_time', '');
        setSessionVar('alltasks', '');
        $userid = getSessionVar('userid');
        $taskfor = $this->uri->segment(2);
        $current_date = getSessionVar('current_date');
        switch ($taskfor) {
            case 4:
                $stmt = 'select * from SP_MODULE_TUTOR(' . $userid . ',' . myDate($current_date) . ')';
                $hostname = 'Tutor';
                break;

            case 6:
                $stmt = 'select * from SP_MODULE_SCHOOL(' . $userid . ',' . myDate($current_date) . ')';
                $hostname = 'School';
                break;

            case 8:
                $stmt = 'select * from SP_MODULE_CORP(' . $userid . ',' . myDate($current_date) . ')';
                $hostname = 'School';
                break;

            case 10:
                $stmt = 'select * from SP_MODULE_QSTUDY(' . $userid . ',' . myDate($current_date) . ')';
                $hostname = 'QStudy';
                break;

            default:
                $stmt = 'select * from SP_MODULE_QSTUDY(' . $userid . ',' . myDate($current_date) . ')';
                $hostname = 'Tutor';
                break;
        }

        $load = $this->user_account->load($userid);
        if ($load) {
            $parent_id = $this->user_account->parent_id;
            $student_year = $this->user_account->student_year;
            $parent_load = $this->user_account->load($parent_id);
            if ($parent_load) {
                switch ($taskfor) {
                    case 4:
                        $username = $this->user_account->host_name;
                        break;

                    case 6:
                        $username = $this->user_account->user_lastname;
                        break;

                    case 10:
                        $username = '';
                        break;
                }

                $logo = $this->user_account->logo;
                $html = '<div id="task">';
                $html .= '<div class="header">' . $this->user_account->getlogo($logo);

                /* if ($username) {
                  $html .= '<div class="title">' . $hostname . '-' . $username . '</div>';
                  } else {
                  $html .= '<div class="title">&nbsp;</div>';
                  } */
                $html .= '<div class="title">' . $this->show_trackername($stmt) . '</div>';
                $html .= '</div>';
                $query = $this->db->query($stmt);
                if ($query) {
                    $html .= '<div class="table_div">';
                    $html .= '<table class="task_table">';
                    $html .= '<tr>';
                    $html .= '<td class="task_table_header">Module Name</td>';
                    $html .= '<td class="task_table_header">Subject</td>';
                    $html .= '<td class="task_table_header">Chapter</td>';
                    $html .= '</tr>';
                    foreach ($query->result() as $row) {
                        $first_quiz = $this->getFirstQuizId($taskfor, $row->R_ID);
                        $alink = '/my-exam/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                        //setSessionVar('exam_link', $taskfor . ',' . $row->R_ID);
                        if ($student_year == $row->R_GRADE_YEAR) {
                            $isall_student = $row->R_IS_ALL_STUDENT;
                            if (!$isall_student) {//not all student, just selectde student
                                $students = explode(',', $row->R_SELECTED_STUDENTS);
                                if (count($students) > 0) {
                                    foreach ($students as $student) {
                                        if ($student == $userid) {
                                            $html .= '<tr>';
                                            $html .= '<td class="task_table_col"><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></td>';
                                            $html .= '<td class="task_table_col">' . $row->R_SUBJECT_NAME . '</td>';
                                            $html .= '<td class="task_table_col">' . $row->R_CHAPTER_NAME . '</td>';
                                            $html .= '<tr>';
                                        }
                                    }
                                }
                            } else {//all student
                                $html .= '<tr>';
                                $html .= '<td class="task_table_col"><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></td>';
                                $html .= '<td class="task_table_col">' . $row->R_SUBJECT_NAME . '</td>';
                                $html .= '<td class="task_table_col">' . $row->R_CHAPTER_NAME . '</td>';
                                $html .= '<tr>';
                            }
                        }
                    }
                    $html .= '</table>';
                    $html .= '</div>';
                }
                $html .= '</div>';
            } else {
                $html = $this->user_account->error_msg;
            }
        } else {
            $html = $this->user_account->error_msg;
        }
        $usertype = getSessionVar('userType');
        switch ($usertype) {
            case 2:
                $back = 'home-parent';
                break;

            case 3:
                $back = 'everyday-study';
                break;

            case 4:
                $back = 'home-tutor';
                break;

            case 5:
                $back = 'everyday-study';
                break;

            case 6:
                $back = 'home-school';
                break;

            case 7:
                $back = 'everyday-study';
                break;

            case 8:
                $back = 'home-corporate';
                break;

            case 9:
                $back = 'everyday-study';
                break;

            case 10:
                $back = 'home-qstudy';
                break;
        }

        $html .= '<a class="back_button" href="/' . $back . '" style="margin-left:306px;margin-top:5px">Back</a>';

        //for hightlight row
        $script = '<script>' . $this->config->item('CRLF');
        $script .= '    $(function() {' . $this->config->item('CRLF');
        $script .= '        $(\'tr\').hover(function(){' . $this->config->item('CRLF');
        $script .= '            $(this).addClass(\'hightlight_cell\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().addClass(\'highlight_tr\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().addClass(\'highlight_td\');' . $this->config->item('CRLF');
        $script .= '            },function(){' . $this->config->item('CRLF');
        $script .= '            $(this).removeClass(\'hightlight_cell\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().removeClass(\'highlight_tr\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().removeClass(\'highlight_td\');' . $this->config->item('CRLF');
        $script .= '        }' . $this->config->item('CRLF');
        $script .= '        );' . $this->config->item('CRLF');
        $script .= '    });' . $this->config->item('CRLF');
        $script .= '</script>' . $this->config->item('CRLF');
        return $script . $html;
    }

    function getSubjectCount($auserid = 0) {
        $stmt = 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($auserid) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            $i = 0;
            foreach ($query->result() as $row) {
                $i++;
            }
            $return = $i;
        } else {
            $return = 0;
        }
        return $return;
    }
    function getSubjectCount_forhost($auserid = 0) {
        $stmt = 'select EXAM_NAME as R_SUBJECT_NAME from EXAM_TYPE inner join PAYPAL_USER_PAYPENT on PAYPAL_USER_PAYPENT.SELECTED_COURSE_ID = EXAM_TYPE.ID where PAYPAL_USER_PAYPENT.USER_ID='.$auserid.' and CAST(PAYPAL_USER_PAYPENT.END_DATE AS DATE) > CURRENT_DATE and PAYPAL_USER_PAYPENT.PAYMENT_STATUS=1 AND PAYPAL_USER_PAYPENT.CANCEL_STATUS=0';
        $query = $this->db->query($stmt);
        if ($query) {
            $i = 0;
            foreach ($query->result() as $row) {
                $i++;
            }
            $return = $i;
        } else {
            $return = 0;
        }
        return $return;
    }

    function getSubjectNames($host = 0) {
        $user_type = $this->uri->segment(2);
        $userid = $this->user_account->getUserIdByUserType($user_type);
        $taskfor = $this->uri->segment(2);
        //if ($userid) {
        if ($userid != 'f') {
            $stmt = 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($userid) . ')';
        } else {
            $stmt = 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($host) . ')';
        }
        $query = $this->db->query($stmt);
        $subjectCount = $this->getSubjectCount($userid);
        $html = '<ul id="easyPaginate">';
        $alllink = "'', '', $taskfor, $host";
        //$html .= '<li><a href="javascript:fn_search_alltasks(\'\',' . $taskfor . ',' . $host . ')">All</a></li>';
        $html .= '<li><a href="javascript:fn_search_alltasks(' . $alllink . ')">All</a></li>';
        $i = 1;
        $spinneruonclick = '';
        $spinnerdonclick = '';
        if ($query) {
            foreach ($query->result() as $row) {
                $spinneruonclick = 'fn_show_nextSubject(1' . ',' . $subjectCount . ')';
                $spinnerdonclick = 'fn_show_nextSubject(0' . ',' . $subjectCount . ')';
                $alink = "'$row->R_SUBJECT_NAME', '', $taskfor, $host";
                //\'' . $row->R_SUBJECT_NAME . '\',' . $taskfor . '
                if (!$host) {
                    $html .= '<li id="li_' . $i . '"><a href="javascript:fn_search_alltasks(' . $alink . ')">' . $row->R_SUBJECT_NAME . '</a></li>';
                } else {
                    $html .= '<li id="li_' . $i . '_' . $host . '"><a href="javascript:fn_search_alltasks(' . $alink . ')">' . $row->R_SUBJECT_NAME . '</a></li>';
                }
                /* } else {
                  $html .= '<li id="li_' . $i . '" style="display:none"><a href="javascript:fn_search_alltasks(' . $alink . ')">' . $row->R_SUBJECT_NAME . '</a></li>';
                  }
                  $i++; */
                $i++;
            }
           
        }
         $html .= '</ul>';
        /* if ($subjectCount > 6) {
          $html .= '<div class="spinbox">';
          $html .= '<div class="leftbutton" onclick="' . $spinneruonclick . '"></div>';
          $html .= '<div class="rightbutton" onclick="' . $spinnerdonclick . '"></div>';
          $html .= hiddenElement('spinner_val', 0);
          $html .= '</div>';
          } */
        /* } else {
          $html = '';
          } */
        /* } else {
          $html = $this->user_account->error_msg;
          } */
        return $html;
    }
	
	function get_host_subject_for_student_eve($host = 0){
		$user_type = $this->uri->segment(2);
        $userid = $this->user_account->getUserIdByUserType($user_type);
        $taskfor = $this->uri->segment(2);
        //$stmt = 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($host) . ')';
		
        $stmt = 'select EXAM_NAME as R_SUBJECT_NAME from EXAM_TYPE inner join PAYPAL_USER_PAYPENT on PAYPAL_USER_PAYPENT.SELECTED_COURSE_ID = EXAM_TYPE.ID where PAYPAL_USER_PAYPENT.USER_ID='.$host.' and CAST(PAYPAL_USER_PAYPENT.END_DATE AS DATE) > CURRENT_DATE and PAYPAL_USER_PAYPENT.PAYMENT_STATUS=1 AND PAYPAL_USER_PAYPENT.CANCEL_STATUS=0';
		
        $query = $this->db->query($stmt);
        $subjectCount = $this->getSubjectCount_forhost($host);
        $html = '<ul id="easyPaginate">';
        $alllink = "'', '', $taskfor, $host";
        $html .= '<li><a href="javascript:fn_search_evetasks(' . $alllink . ')">All</a></li>';
        $i = 1;
        $spinneruonclick = '';
        $spinnerdonclick = '';
        if ($query) {
            foreach ($query->result() as $row) {
                $spinneruonclick = 'fn_show_nextSubject(1' . ',' . $subjectCount . ')';
                $spinnerdonclick = 'fn_show_nextSubject(0' . ',' . $subjectCount . ')';
                $alink = "'$row->R_SUBJECT_NAME', '', $taskfor, $host";
                if (!$host) {
                    $html .= '<li id="li_' . $i . '"><a href="javascript:fn_search_evetasks(' . $alink . ')">' . $row->R_SUBJECT_NAME . '</a></li>';
                } else {
                    $html .= '<li id="li_' . $i . '_' . $host . '"><a href="javascript:fn_search_evetasks(' . $alink . ')">' . $row->R_SUBJECT_NAME . '</a></li>';
                }
                $i++;
            }
        }
        $html .= '</ul>';
        return $html;
	}
	
    function getSubjectNames_users($subjct_lst = array(),$host = 0) {
        $user_type = $this->uri->segment(2);
        $userid = $this->user_account->getUserIdByUserType($user_type);
        $taskfor = $this->uri->segment(2);
		$rt = getSessionVar('userid');
		
		
        //if ($userid) {
        if ($userid != 'f') {
            $stmt = 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($userid) . ')';
        } else {
            $stmt = 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($host) . ')';
        }
        $query = $this->db->query($stmt);
        $subjectCount = count($subjct_lst); //$this->getSubjectCount($userid);
        $html = '<ul id="easyPaginate">';
        $alllink = "'', '', $taskfor, $host";
        //$html .= '<li><a href="javascript:fn_search_alltasks(\'\',' . $taskfor . ',' . $host . ')">All</a></li>';
        $html .= '<li><a href="javascript:fn_search_alltasks(' . $alllink . ')">All</a></li>';
        $i = 1;
        $spinneruonclick = '';
        $spinnerdonclick = '';
        if ($subjct_lst) {
            foreach ($subjct_lst as $row) {
                $spinneruonclick = 'fn_show_nextSubject(1' . ',' . $subjectCount . ')';
                $spinnerdonclick = 'fn_show_nextSubject(0' . ',' . $subjectCount . ')';
                $alink = "'$row', '', $taskfor, $host";
                if (!$host) {
                    $html .= '<li id="li_' . $i . '"><a href="javascript:fn_search_alltasks(' . $alink . ')">' . $row . '</a></li>';
                } else {
                    $html .= '<li id="li_' . $i . '_' . $host . '"><a href="javascript:fn_search_alltasks(' . $alink . ')">' . $row . '</a></li>';
                }
               
                $i++;
            } 
        }
         $html .= '</ul>';
        return $html;
    }
	
    function getSubjectNames_users_all($subjct_lst = array(),$host = 0) {
        $user_type = $this->uri->segment(2);
        $userid = $this->user_account->getUserIdByUserType($user_type);
        $taskfor = $this->uri->segment(2);
		$rt = getSessionVar('userid');
		
		
        //if ($userid) {
        if ($userid != 'f') {
            $stmt = 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($userid) . ')';
        } else {
            $stmt = 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($host) . ')';
        }
        $query = $this->db->query($stmt);
        $subjectCount = count($subjct_lst); //$this->getSubjectCount($userid);
        $html = '<ul id="easyPaginate">';
        $alllink = "'', '', $taskfor, $host";
        //$html .= '<li><a href="javascript:fn_search_alltasks(\'\',' . $taskfor . ',' . $host . ')">All</a></li>';
        $html .= '<li><a href="javascript:fn_search_evetasks(' . $alllink . ')">All</a></li>';
        $i = 1;
        $spinneruonclick = '';
        $spinnerdonclick = '';
        if ($subjct_lst) {
            foreach ($subjct_lst as $row) {
                $spinneruonclick = 'fn_show_nextSubject(1' . ',' . $subjectCount . ')';
                $spinnerdonclick = 'fn_show_nextSubject(0' . ',' . $subjectCount . ')';
                $alink = "'$row', '', $taskfor, $host";
                if (!$host) {
                    $html .= '<li id="li_' . $i . '"><a href="javascript:fn_search_evetasks(' . $alink . ')">' . $row . '</a></li>';
                } else {
                    $html .= '<li id="li_' . $i . '_' . $host . '"><a href="javascript:fn_search_evetasks(' . $alink . ')">' . $row . '</a></li>';
                }
               
                $i++;
            } 
        }
         $html .= '</ul>';
        return $html;
    }

    function search_area($hostid = 0) {
        $user_type = $this->uri->segment(2);
        $userid = $this->user_account->getUserIdByUserType($user_type);
        if (!$hostid) {
            $subject_name = LookupComboKeyStr('subject_name', 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($userid) . ')', 'R_SUBJECT_NAME', 'R_SUBJECT_NAME', '', '', '', 'active_select_border');
            $chapter_name = LookupComboKeyStr('chapter_name', 'select R_CHAPTER_NAME from SP_GET_CHAPTER(' . myInteger($userid) . ')', 'R_CHAPTER_NAME', 'R_CHAPTER_NAME', '', '', '', 'active_select_border');
        } else {
            $subject_name = LookupComboKeyStr('subject_name_' . $hostid, 'select R_SUBJECT_NAME from SP_GET_SUBJECT(' . myInteger($userid) . ')', 'R_SUBJECT_NAME', 'R_SUBJECT_NAME', '', '', '', 'active_select_border');
            $chapter_name = LookupComboKeyStr('chapter_name_' . $hostid, 'select R_CHAPTER_NAME from SP_GET_CHAPTER(' . myInteger($userid) . ')', 'R_CHAPTER_NAME', 'R_CHAPTER_NAME', '', '', '', 'active_select_border');
        }
        $html = '<div class="searcharea">';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td>Subject</td>';
        $html .= '<td>' . $subject_name . '</td>';
        $html .= '<td>Chapter</td>';
        $html .= '<td>' . $chapter_name . '</td>';
        if (!$hostid) {
            $html .= '<td><a class="search" href="javascript:fn_search_tasks(' . $user_type . ',' . $hostid . ')" id="search_tasks" style="float:right;">Search</a></td>';
            $html .= '<td ><a class="cancel" href="javascript:fn_search_clear_tasks(' . $user_type . ',' . $hostid . ')" id="clear_search" style="float:right;display:none">Clear</a></td>';
        } else {
            $html .= '<td><a class="search" href="javascript:fn_search_tasks(' . $user_type . ',' . $hostid . ')" id="search_tasks_' . $hostid . '" style="float:right;">Search</a></td>';
            $html .= '<td ><a class="cancel" href="javascript:fn_search_clear_tasks(' . $user_type . ',' . $hostid . ')" id="clear_search_' . $hostid . '" style="float:right;display:none">Clear</a></td>';
        }
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';
        return $html;
    }
	

    function myallTask() {
		
        $taskfor = $this->uri->segment(2);
        $userid = getSessionVar('userid');
        if ($taskfor == 4) {
            //for treeview
            $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
            $script .= '        $("#maintask_table").treetable({ expandable: true });' . $this->config->item('CRLF');
            //$script .= '        $(".treetable").collapseAll();' . $this->config->item('CRLF');
            // Highlight selected row
            //$script .= '            $("#maintask_table tbody").on("mousedown", "tr", function() {' . $this->config->item('CRLF');
            //$script .= '            $(".selected").not(this).removeClass("selected");' . $this->config->item('CRLF');
            //$script .= '            $(this).toggleClass("selected");' . $this->config->item('CRLF');
            //$script .= '        });' . $this->config->item('CRLF');
            $script .= '</script>' . $this->config->item('CRLF');

            $stmt = 'select * from SP_GET_STUDENT_TUTORS(' . myInteger($userid) . ')';
            $html = '<table id="maintask_table" class="treetable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #D6E4F6;color:#000">Tutor Name</th>'; // class="task_table_header"
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            $query = $this->db->query($stmt);
            $style = '<style>' . $this->config->item('CRLF');
            if ($query) {
                foreach ($query->result() as $row) {
                    $style .= '#task .table_div_' . $row->R_ID . '{' . $this->config->item('CRLF');
                    $style .= '	margin:5px 0 0 5px;' . $this->config->item('CRLF');
                    $style .= '	width:98%;' . $this->config->item('CRLF');
                    $style .= '	overflow-y:auto;' . $this->config->item('CRLF');
                    $style .= '	height:430px;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');

                    $style .= '#task .subjectnames_' . $row->R_ID . '{' . $this->config->item('CRLF');
                    $style .= '    position:relative;' . $this->config->item('CRLF');
                    $style .= '    padding-bottom:10px;' . $this->config->item('CRLF');
                    $style .= '    background:#1F3367;' . $this->config->item('CRLF');
                    $style .= '    color:#fff;' . $this->config->item('CRLF');
                    $style .= '    max-height:120px;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' ul{' . $this->config->item('CRLF');
                    $style .= '    background:#1f3367;' . $this->config->item('CRLF');
                    $style .= '    margin-top:10px;' . $this->config->item('CRLF');
                    $style .= '    margin-bottom:10px;' . $this->config->item('CRLF');
                    $style .= '    padding-bottom:10px !important;' . $this->config->item('CRLF');
                    $style .= '    width:98.6%;' . $this->config->item('CRLF');
                    $style .= '    float:left;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' .spinbox{' . $this->config->item('CRLF');
                    $style .= '   float:right;' . $this->config->item('CRLF');
                    $style .= '   margin-right:3px;' . $this->config->item('CRLF');
                    $style .= '   width:10px;' . $this->config->item('CRLF');
                    $style .= '   height:7px;' . $this->config->item('CRLF');
                    $style .= '   margin:-25px 0 0 33.24%;' . $this->config->item('CRLF');
                    $style .= '   position:absolute;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' .spinbox .leftbutton{' . $this->config->item('CRLF');
                    $style .= '    background:url(/images/back_button_w.png) no-repeat;' . $this->config->item('CRLF');
                    $style .= '    width:4px;' . $this->config->item('CRLF');
                    $style .= '    height:7px;' . $this->config->item('CRLF');
                    $style .= '    margin:0px 0 0 0px;' . $this->config->item('CRLF');
                    $style .= '    cursor:pointer;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' .spinbox .rightbutton{' . $this->config->item('CRLF');
                    $style .= '    background:url(/images/right_button_w.png) no-repeat;' . $this->config->item('CRLF');
                    $style .= '    width:4px;' . $this->config->item('CRLF');
                    $style .= '    height:7px;' . $this->config->item('CRLF');
                    $style .= '    margin:-7px 0 0 6px;' . $this->config->item('CRLF');
                    $style .= '    cursor:pointer;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' li{' . $this->config->item('CRLF');
                    $style .= '    display:inline-block;' . $this->config->item('CRLF');
                    $style .= '    margin-right:15px;' . $this->config->item('CRLF');
                    $style .= '    margin-left:0px;' . $this->config->item('CRLF');
                    $style .= '    padding-right:4px;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' li a{' . $this->config->item('CRLF');
                    $style .= '  text-decoration:none;' . $this->config->item('CRLF');
                    $style .= '  color:#fff;' . $this->config->item('CRLF');
                    $style .= '  font-size:12px;' . $this->config->item('CRLF');
                    $style .= '}';
                    $style .= '.li_active_' . $row->R_ID . '{' . $this->config->item('CRLF');
                    $style .= '    font-weight:bold;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' li a:hover{' . $this->config->item('CRLF');
                    $style .= '    color:#EF0D10;' . $this->config->item('CRLF');
                    $style .= '}';


                    $html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $row->R_USER_LASTNAME . ' ' . $row->R_USER_FIRSTNAME . '</td>';
                    $html .= '</tr>';
                    //$html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= "<tr data-tt-id='c" . $row->R_ID . "' data-tt-parent-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $this->show_all_task($row->R_ID) . '</td>';
                    $html .= '</tr>';
                }
                $html .= '</tbody>';
            }
            $html .= '</table>';
            $style .= '</style>' . $this->config->item('CRLF');
        } else {
            $script = '';
            $style = '';
            $html = $this->show_all_task();
        }
        setSessionVar('alltasks', 't');
        return $style . $html . $script;
    }

	function gt_userssubjects($query,$student_year){
		$userid = getSessionVar('userid');
		$my_subjct_lst = array();
		if ($query) {
			
			foreach ($query->result() as $row) {
			   
				if ($student_year == $row->R_GRADE_YEAR) {
					$isall_student = $row->R_IS_ALL_STUDENT;
				
					if (!$isall_student) {//not all student, just selectde student
						$students = explode(',', $row->R_SELECTED_STUDENTS);
						if (count($students) > 0) {
							foreach ($students as $student) {
								if ($student == $userid) {
									$my_subjct_lst[] = $row->R_SUBJECT_NAME;
								}
							}
						}
					} else {//all student
						$my_subjct_lst[]=$row->R_SUBJECT_NAME;
					}
				}
			}
		}
		$my_subjct_lst = array_unique($my_subjct_lst);
		return $my_subjct_lst;
	}
	function gt_userssubjects_rtmt($query,$taskfor,$hostid,$student_year){
		$userid = getSessionVar('userid');
		$my_subjct_lst = array();
		if ($query) {
			
			foreach ($query as $row) {
				$is_rept_complte = $this->check_is_completed_or_not_complete($row->ID,$hostid,$taskfor);
				if($is_rept_complte!=1){
					if($student_year == $row->GRADE_YEAR) {
						if($is_rept_complte==0 || $is_rept_complte==2){
							$my_subjct_lst[] = $row->SUBJECT_NAME;
						}
					}
				}
			}
		}
		
		$my_subjct_lst = array_unique($my_subjct_lst);
		return $my_subjct_lst;
	}
	
    function show_all_task($hostid = 0) {

        setSessionVar('task', '');
        setSessionVar('module', '');
        setSessionVar('question', '');
        setSessionVar('question_answer', '');
        setSessionVar('prevquestion', '');
        setSessionVar('allprev', '');
        setSessionVar('skip', '');
        setSessionVar('start_time', '');
        setSessionVar('end_time', '');
        //setSessionVar('alltasks', '');
        $userid = getSessionVar('userid');
        $taskfor = $this->uri->segment(2);
        $country = getSessionVar('country_name');

        switch ($taskfor) {
            case 4:
                $stmt = 'select * from SP_ALL_TASK_LIST_TUTOR(' . myInteger($hostid) . ',' . myInteger($userid) . ')';
                $hostname = 'Tutor';
                break;

            case 6:
                $stmt = 'select * from SP_ALL_TASK_LIST_SCHOOL(' . myInteger($userid) . ')';
                $hostname = 'School';
                break;

            case 8:
                $stmt = 'select * from SP_ALL_TASK_LIST_CORP(' . myInteger($userid) . ')';
                $hostname = 'School';
                break;

            case 10:
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_ALL_TASK_LIST_QSTUDY(' . myInteger($userid) . ',' . myInteger($country) . ')';
                }  else {
                    $stmt = 'select * from SP_ALL_TASK_LIST_QSTUDY_TR(' . myInteger($userid) . ',' . myInteger($country) . ')';
                }
                $hostname = 'QStudy';
                break;

            default:
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_ALL_TASK_LIST_QSTUDY(' . myInteger($userid) . ',' . myInteger($country) . ')';
                }  else {
                    $stmt = 'select * from SP_ALL_TASK_LIST_QSTUDY_TR(' . myInteger($userid) . ',' . myInteger($country) . ')';
                }
                $hostname = 'QStudy';
                break;
        }

        $load = $this->user_account->load($userid);
        if ($load) {
            $parent_id = $this->user_account->host_id;
            $student_year = $this->user_account->student_year;
            if (!$hostid) {
                $parent_load = $this->user_account->load($parent_id);
            } else {
                $parent_load = $this->user_account->load($hostid);
            }
            $school_id = $this->user_account->school_id;
            $school_load = $this->user_account->load($school_id);
            $corpo_id = $this->user_account->corporate_id;
            $corpo_load = $this->user_account->load($corpo_id);
            switch ($taskfor) {
                case 4:
                    if ($parent_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Tutor :' . $username;
                        } else {
                            $username = 'Tutor';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = '';
                        $logo = '';
                    }
                    break;

                case 6:
                    if ($school_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'School :' . $username;
                        } else {
                            $username = 'School';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'School';
                        $logo = '';
                    }
                    break;

                case 8:
                    if ($corpo_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Corporate :' . $username;
                        } else {
                            $username = 'Corporate';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'Corporate';
                        $logo = '';
                    }
                    break;

                case 10:
                    $username = 'Q-Study';
                    $logo = '';
                    break;
            }

            $html = '<div id="task">';
            $html .= $this->search_area($hostid);
            $html .= '<div class="header">' . $this->user_account->getlogo($logo);
            $html .= '<div class="title">' . $username . '</div>';
            $html .= '</div>';
			$query = $this->db->query($stmt);
			/* get subject lst */
			$subjct_lst = $this->gt_userssubjects($query,$student_year);
			
			/* get subject lst */
            if (!$hostid) {
                //$html .= '<div class="subjectnames">' . $this->getSubjectNames($hostid) . '</div>';
                $html .= '<div class="subjectnames">' . $this->getSubjectNames($hostid) . '</div>';
            } else {
              //  $html .= '<div class="subjectnames_' . $hostid . '">' . $this->getSubjectNames($hostid) . '</div>';
                $html .= '<div class="subjectnames_' . $hostid . '">' . $this->getSubjectNames_users($subjct_lst,$hostid) . '</div>';
            }
            
            if ($query) {
                if (!$hostid) {
                    $html .= '<div class="table_div">';
                } else {
                    $html .= '<div class="table_div_' . $hostid . '">';
                }
                $html .= '<table class="task_table">';
                $html .= '<thead>';
                $html .= '<tr>';
                $html .= '<th>Module Name</th>'; // class="task_table_header"
                $html .= '<th>Tracker Name</th>';
                $html .= '<th>Individual Name</th>';
                $html .= '<th>Subject</td>';
                $html .= '<th>Chapter</th>';
                $html .= '</tr>';
                $html .= '</thead>';
				$my_subjct_lst = array();
                foreach ($query->result() as $row) {
                    $tracker_name = $row->R_TRACKER_NAME;
                    $indi_name = $row->R_INDIVIDUAL_NAME;
                    $this->deleteByModule($row->R_ID);
                    $first_quiz = $this->getFirstQuizId($taskfor, $row->R_ID, 't', '', $hostid);
                    $alink = '/all-tasks/' . $hostid . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';

                    //setSessionVar('exam_link', $taskfor . ',' . $row->R_ID);
                    if ($student_year == $row->R_GRADE_YEAR) {
                        $isall_student = $row->R_IS_ALL_STUDENT;
					
                        if (!$isall_student) {//not all student, just selectde student
                            $students = explode(',', $row->R_SELECTED_STUDENTS);
                            if (count($students) > 0) {
                                foreach ($students as $student) {
                                    if ($student == $userid) {
                                        $html .= '<tr>';
                                        $html .= '<td><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></td>'; // class="task_table_col"
                                        $html .= '<td>' . $tracker_name . '</td>';
                                        $html .= '<td>' . $indi_name . '</td>';
                                        $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                        $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                        $html .= '<tr>';
										$my_subjct_lst[]=$row->R_SUBJECT_NAME;
                                    }
                                }
                            }
                        } else {//all student
                            $html .= '<tr>';
                            $html .= '<td><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></td>';
                            $html .= '<td>' . $tracker_name . '</td>';
                            $html .= '<td>' . $indi_name . '</td>';
                            $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                            $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                            $html .= '<tr>';
							$my_subjct_lst[]=$row->R_SUBJECT_NAME;
                        }
                    }
                }
                $html .= '</table>';
                $html .= '</div>';
            }
            $html .= '</div>';
            /* } else {
              $html = $this->user_account->error_msg;
              } */
        } else {
            $html = $this->user_account->error_msg;
        }
		
        //for hightlight row
        $script = '<script>' . $this->config->item('CRLF');
        $script .= '    $(function() {' . $this->config->item('CRLF');
        $script .= '        $(\'tr\').hover(function(){' . $this->config->item('CRLF');
        $script .= '            $(this).addClass(\'hightlight_cell\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().addClass(\'highlight_tr\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().addClass(\'highlight_td\');' . $this->config->item('CRLF');
        $script .= '            },function(){' . $this->config->item('CRLF');
        $script .= '            $(this).removeClass(\'hightlight_cell\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().removeClass(\'highlight_tr\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().removeClass(\'highlight_td\');' . $this->config->item('CRLF');
        $script .= '        }' . $this->config->item('CRLF');
        $script .= '        );' . $this->config->item('CRLF');
        $script .= '    });' . $this->config->item('CRLF');
        $script .= '</script>' . $this->config->item('CRLF');
        //return $script . $html;
        return $html;
    }
	
    function search_area_opensource($hostid = 0) {
        $srch_mdule = '<input type="text" id="search_opensource_module" name="search_module_opensource" class="form-control active-input-border">';
        $html = '<div class="searcharea_opn_srce">';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td class="input-box">' . $srch_mdule . '</td>';
		$html .= '<td><a class="search" href="javascript:fn_search_all_opensource_mdle()" id="search_tasks" style="float:right;">Search</a></td>';        
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';
        return $html;
    }
    function show_all_task_opensource($hostid = 0) {
		setSessionVar('task', '');
        setSessionVar('module', '');
        setSessionVar('question', '');
        setSessionVar('question_answer', '');
        setSessionVar('prevquestion', '');
        setSessionVar('allprev', '');
        setSessionVar('skip', '');
        setSessionVar('start_time', '');
        setSessionVar('end_time', '');
        //setSessionVar('alltasks', '');
        $userid = getSessionVar('userid');
        $country = getSessionVar('country_name');

        $load = $this->user_account->load($userid);
        if ($load) {
            $parent_id = $this->user_account->host_id;
            $student_year = $this->user_account->student_year;
            if (!$hostid) {
                $parent_load = $this->user_account->load($parent_id);
            } else {
                $parent_load = $this->user_account->load($hostid);
            }
            $school_id = $this->user_account->school_id;
            $school_load = $this->user_account->load($school_id);
            $corpo_id = $this->user_account->corporate_id;
            $corpo_load = $this->user_account->load($corpo_id);
           
            $html = '<div id="task">';
            $html .= $this->search_area_opensource($hostid);
            $html .= '<div class="header">';
            $html .= '<div class="title">Open source</div>';
            $html .= '</div>';
           
			$stmt = '';
            $query = $this->db->query($stmt);
           
               $html .= '<div class="table_div_opensource">';				
                $html .= '<table class="task_table">';
                $html .= '<thead>';
                $html .= '<tr>';
                $html .= '<th>Module Name</th>';
                $html .= '<th>Tracker Name</th>';
                $html .= '<th>Individual Name</th>';
                $html .= '<th>Subject</th>';
                $html .= '<th>Chapter</th>';
                $html .= '</tr>';
                $html .= '</thead>';
                $html .= '<tbody class="rss_modlue_list">';
                $html .= '</tbody>';
                
                $html .= '</table>';
                $html .= '</div>';
				$html .= '<div class="userDetailsrs"></div>';
            $html .= '</div>';
           
        } else {
            $html = $this->user_account->error_msg;
        }
		return $html;
	}
	
    function search_all_tasks($asubject = '', $achapter = '', $taskfor = 0, $host = 0) {
        setSessionVar('alltasks', 't');
        $userid = getSessionVar('userid');
        $country = getSessionVar('country_name');
        switch ($taskfor) {
            case 4:
                $stmt = 'select * from SP_ALL_TASK_LIST_TUTOR_SEARCH(' . myInteger($userid) . ',' . myString($asubject) . ',' . myString($achapter) . ',' . myInteger($host) . ')';
                $hostname = 'Tutor';
                break;

            case 6:
                $stmt = 'select * from SP_ALL_TASK_LIST_SCHOOL_SEARCH(' . myInteger($userid) . ',' . myString($asubject) . ',' . myString($achapter) . ')';
                $hostname = 'School';
                break;

            case 8:
                $stmt = 'select * from SP_ALL_TASK_LIST_CORP_SEARCH(' . myInteger($userid) . ',' . myString($asubject) . ',' . myString($achapter) . ')';
                $hostname = 'School';
                break;

            case 10:
                $stmt = 'select * from SP_ALL_TASK_LIST_QSTUDY_SEARCH(' . myInteger($userid) . ',' . myInteger($country) . ',' . myString($asubject) . ',' . myString($achapter) . ')';
                $hostname = 'QStudy';
                break;

            default:
                $stmt = 'select * from SP_ALL_TASK_LIST_QSTUDY_SEARCH(' . myInteger($userid) . ',' . myInteger($country) . ',' . myString($asubject) . ',' . myString($achapter) . ')';
                $hostname = 'QStudy';
                break;
        }
        $student_year = getSessionVar('student_year');
        $html = '';
        $query = $this->db->query($stmt);
        if ($query) {
            $html .= '<table class="task_table">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th>Module Name</th>'; // class="task_table_header"
            $html .= '<th>Tracker Name</th>';
            $html .= '<th>Individual Name</th>';
            $html .= '<th>Subject</td>';
            $html .= '<th>Chapter</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            foreach ($query->result() as $row) {
                $tracker_name = $row->R_TRACKER_NAME;
                $indi_name = $row->R_INDIVIDUAL_NAME;
                $this->deleteByModule($row->R_ID);
                $first_quiz = $this->getFirstQuizId($taskfor, $row->R_ID, 't', 0, $host);
                $alink = '/all-tasks/' . $host . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                //setSessionVar('alltasks', 't');
                //setSessionVar('exam_link', $taskfor . ',' . $row->R_ID);
                if ($student_year == $row->R_GRADE_YEAR) {
                    $isall_student = $row->R_IS_ALL_STUDENT;
                    if (!$isall_student) {//not all student, just selectde student
                        $students = explode(',', $row->R_SELECTED_STUDENTS);
                        if (count($students) > 0) {
                            foreach ($students as $student) {
                                if ($student == $userid) {
                                    $html .= '<tr>';
                                    $html .= '<td><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></td>'; // class="task_table_col"
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= '<tr>';
                                }
                            }
                        }
                    } else {//all student
                        $html .= '<tr>';
                        $html .= '<td><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></td>';
                        $html .= '<td>' . $tracker_name . '</td>';
                        $html .= '<td>' . $indi_name . '</td>';
                        $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                        $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                        $html .= '<tr>';
                    }
                }
            }
            $html .= '</table>';
        }
        $array = array('html' => $html, 'stmt' => $stmt);
        //return $html;
        return $array;
    }

	function show_all_task_bysearch() {
        setSessionVar('task', '');
        setSessionVar('module', '');
        setSessionVar('question', '');
        setSessionVar('question_answer', '');
        setSessionVar('prevquestion', '');
        setSessionVar('allprev', '');
        setSessionVar('skip', '');
        setSessionVar('start_time', '');
        setSessionVar('end_time', '');
        //setSessionVar('alltasks', '');
		$hostid = '';
		 $userid = getSessionVar('userid');
		$gradesLavels = $this->uri->segment(2);
		
		$moduletypesids = $this->uri->segment(3);
		if ($this->uri->total_segments() > 3) {
            $selectedUserId = $this->uri->segment(4);
			/*if(is_numeric($selectedUserId)){
				$selectedUserId = $selectedUserId;
			}else{
				$selectedUserId = 0;
			}*/
        }/* else{
			$selectedUserId = 0;
		} */
		
		if($this->uri->total_segments() > 4){
			$rssUserId = $this->uri->segment(5);
		}else{
			$rssUserId = 0;
		}
		
		$taskfor = getSessionVar('userType');
		if($taskfor == 10){
			$qstudy = true;
		}else{
			$qstudy = false;
		}
        $country = getSessionVar('country_name');
		if ($qstudy) {
            $country = '<tr>';
            $country .= '   <td class="input-box">Country</td>';
            $country .= '   <td class="input-box">' . LookupComboKeyInt('country_name', 'select * from SP_COUNTRY_LIST_PRC', 'R_ID', 'R_COUNTRY_NAME', '', '', '', 'active-select-normal') . '</td>';
            $country .= '</tr>';
        } else {
            $country = '';
        }
        $this->mysmarty->assign('country', $country);

		
		if($taskfor==10){
			
			//$stmt = 'select * from SP_QUIZ_MODULE_LIST_SEARCH_PRC_QSTDYS(' . myInteger($userid) . ',' . myInteger($gradesLavels) . ','. myInteger($moduletypesids) .','. myString($selectedUserId) .')'; //for qstudy
			//$stmt = 'select * from SP_QUIZ_MODULE_LIST_SEARCH_PRCG(' . myInteger($userid) . ',' . myInteger($gradesLavels) . ','. myInteger($moduletypesids) .')';	
		}else{	
			//$stmt = 'select * from SP_QUIZ_MODULE_LIST_SEARCH_PRCG(' . myInteger($userid) . ',' . myInteger($gradesLavels) . ','. myInteger($moduletypesids) .')';	
		}
		$stmt = 'select * from SP_QUIZ_MODULE_LIST_SEARCH_PRCG(' . myInteger($userid) . ',' . myInteger($gradesLavels) . ','. myInteger($moduletypesids) .','. myString($selectedUserId) .')';
		$query = $this->db->query($stmt);
/* 		print_r($query);
		exit;
		print_r($query->result());exit; */
		$newrr = array();
			if($rssUserId!=0){	
				$selcttedstdnt = array();
				foreach($query->result() as $trow){
					if($rssUserId == $trow->R_SELECTED_STUDENTS){
						$newrr[] = $trow;
					}else{
						$selcttedstdnt = explode(',',$trow->R_SELECTED_STUDENTS);
						if(in_array($rssUserId,$selcttedstdnt)){
							$newrr[] = $trow;
						}
					}	
				}
			}else{
				$newrr = $query->result();
			} 
		//$newrr = $query->result();	
        $load = $this->user_account->load($userid);
        if ($load) {
            $parent_id = $this->user_account->host_id;
            $student_year = $this->user_account->student_year;
            if (!$hostid) {
                $parent_load = $this->user_account->load($parent_id);
            } else {
                $parent_load = $this->user_account->load($userid);
            }
            $school_id = $this->user_account->school_id;
            $school_load = $this->user_account->load($school_id);
            $corpo_id = $this->user_account->corporate_id;
            $corpo_load = $this->user_account->load($corpo_id);
            switch ($taskfor) {
                case 4:
                    if ($parent_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Tutor :' . $username;
                        } else {
                            $username = 'Tutor';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = '';
                        $logo = '';
                    }
                    break;

                case 6:
                    if ($school_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'School :' . $username;
                        } else {
                            $username = 'School';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'School';
                        $logo = '';
                    }
                    break;

                case 8:
                    if ($corpo_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname; // . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Corporate :' . $username;
                        } else {
                            $username = 'Corporate';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'Corporate';
                        $logo = '';
                    }
                    break;

                case 10:
                    $username = 'Q-Study';
                    $logo = '';
                    break;
            }
			$html = '<div id="rsindividualStudents" style="text-align:center;"><a href="javascript:fn_select_student_gradeSearch_users('.$gradesLavels.','.$moduletypesids.',\''.$selectedUserId.'\')" class="vinstdnt">View Individual Student</a></div>';
            $html .= '<div id="task_shortview">';
            //$html .= $this->search_area($hostid);
            //$html .= '<div class="header">' . $this->user_account->getlogo($logo);
            //$html .= '<div class="title">' . $username . '</div>';
           // $html .= '</div>';
            if (!$hostid) {
             //   $html .= '<div class="subjectnames">' . $this->getSubjectNames($hostid) . '</div>';
            } else {
              //  $html .= '<div class="subjectnames_' . $hostid . '">' . $this->getSubjectNames($hostid) . '</div>';
            }
			if ($newrr) {
				
                if (!$hostid) {
                    $html .= '<div class="table_div">';
                } else {
                    $html .= '<div class="table_div_' . $hostid . '">';
                }
                $html .= '<table class="task_table">';
                $html .= '<thead>';
                $html .= '<tr>';
                $html .= '<th>Sl No</th>'; // class="task_table_header"
                $html .= '<th>Module Name</th>'; // class="task_table_header"
                $html .= '<th>Tracker Name</th>';
                $html .= '<th>Individual Name</th>';
                $html .= '<th>Subject</td>';
                $html .= '<th>Chapter</th>';
                $html .= '<th>&nbsp;</th>';
                $html .= '</tr>';
                $html .= '</thead>';
				$zk = 1;
				//print_r($newrr);exit;
                foreach ($newrr as $row) {
					
				//print_r($row);exit;
                    $FirstQnumbers = explode(',',$row->R_QUESTIONS);
					$FirstQnumber = $FirstQnumbers[0];
                    $tracker_name = $row->R_TRACKER_NAME;
                    $indi_name = $row->R_INDIVIDUAL_NAME;
                    $this->deleteByModule($row->R_ID);
                    $first_quiz = $this->getFirstQuizId($taskfor, $row->R_ID, 't', '', $hostid);
                    $alink = '/all-tasks/' . $hostid . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                            $html .= '<tr data-attrs="'.$row->R_ID.'" class="defaultview trdflt'.$row->R_ID.'">';
                            $html .= '<td>'.$zk.'</td>';
                            $html .= '<td>' . $row->R_MODULES_NAME . '</td>';
                            $html .= '<td>' . $tracker_name . '</td>';
                            $html .= '<td>' . $indi_name . '</td>';
                            $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                            $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                            $html .= '<td>&nbsp;</td>';
                            $html .= '</tr>';
							$html .= '<tr data-attrs="'.$row->R_ID.'" class="editviewrs tredt'.$row->R_ID.'" style="display:none">';
							$html .= '<td><select class="modules_idrsup'.$row->R_ID.'" name="SL_NO">';
							$ji = 1;
							foreach($newrr as $irows){ 
							$html .='<option data_attr="'.$irows->R_SL_NO.'" value="'.$irows->R_ID.'">'.$ji.'</option>';
							$ji++; } 
							$html .='</select></td>';
							$html .= '<td><input type="text" class="rsinputtext modules_namersup'.$row->R_ID.'" name="modules_name" value="'.$row->R_MODULES_NAME.'"></td>';
							$html .= '<td><input type="text" class="rsinputtext tracker_namersup'.$row->R_ID.'" name="tracker_name" value="'.$tracker_name.'"></td>';
							$html .= '<td><input type="text" class="rsinputtext individual_namersup'.$row->R_ID.'" name="individual_name" value="'.$indi_name.'"></td>';
							$html .= '<td><a class="edit" href="/module-edit/'.$row->R_ID.'">re-edit</a>&nbsp;<a href="javascript:fn_simpleupdatemodules('.$row->R_ID.')" class="save">save</a></td>';
							$html .= '<td><a class="preview" href="/previewall/'.$row->R_ID.'/'.$FirstQnumber.'">preview</a>&nbsp;<a href="javascript:fn_cancleltr('.$row->R_ID.')" class="cancel">cancel</a></td>';
							if(!$qstudy){
								$html .= '<td><a class="preview" href="javascript:fn_duplicate_module_bysearch('.$row->R_ID.', 0)">Duplicate</a><a class="cancel" href="javascript:fn_delete_data_bysearch('.$row->R_ID.', \'Module Delete\', \'quiz_assign\', \'table_module\', \'module_table\')">Delete</a></td>';
							}else{
								$html .= '<td><a class="preview" href="javascript:fn_duplicate_module_bysearch('.$row->R_ID.', 1)">Duplicate</a><a class="cancel" href="javascript:fn_delete_data_bysearch('.$row->R_ID.', \'Module Delete\', \'quiz_assign\', \'table_module\', \'module_table\')">Delete</a></td>';
							}

							
							$html .= '</tr>';
					$zk++;
                }
                $html .= '</table>';
                $html .= '</div>';
            }
            $html .= '</div>';
            /* } else {
              $html = $this->user_account->error_msg;
              } */
        } else {
            $html = $this->user_account->error_msg;
        }

        //for hightlight row
        $script = '<script>' . $this->config->item('CRLF');
        $script .= '    $(function() {' . $this->config->item('CRLF');
        $script .= '        $(\'tr\').hover(function(){' . $this->config->item('CRLF');
        $script .= '            $(this).addClass(\'hightlight_cell\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().addClass(\'highlight_tr\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().addClass(\'highlight_td\');' . $this->config->item('CRLF');
        $script .= '            },function(){' . $this->config->item('CRLF');
        $script .= '            $(this).removeClass(\'hightlight_cell\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().removeClass(\'highlight_tr\');' . $this->config->item('CRLF');
        $script .= '            $(this).parent().removeClass(\'highlight_td\');' . $this->config->item('CRLF');
        $script .= '        }' . $this->config->item('CRLF');
        $script .= '        );' . $this->config->item('CRLF');
        $script .= '    });' . $this->config->item('CRLF');
        $script .= '</script>' . $this->config->item('CRLF');
        //return $script . $html;
        return $html;
    }/*rs nw*/
	
	
	
    function search_all_task($taskfor = 0, $host = 0) {
        setSessionVar('alltasks', 't');
        $userid = getSessionVar('userid');
        $country = getSessionVar('country_name');
        switch ($taskfor) {
            case 4:
                $stmt = 'select * from SP_ALL_TASK_LIST_TUTOR(' . myInteger($host) . ',' . myInteger($userid) . ')';
                $hostname = 'Tutor';
                break;

            case 6:
                $stmt = 'select * from SP_ALL_TASK_LIST_SCHOOL(' . myInteger($userid) . ')';
                $hostname = 'School';
                break;

            case 8:
                $stmt = 'select * from SP_ALL_TASK_LIST_CORP(' . myInteger($userid) . ')';
                $hostname = 'School';
                break;

            case 10:
                $stmt = 'select * from SP_ALL_TASK_LIST_QSTUDY(' . myInteger($userid) . ',' . myInteger($country) . ')';
                $hostname = 'QStudy';
                break;

            default:
                $stmt = 'select * from SP_ALL_TASK_LIST_QSTUDY(' . myInteger($userid) . ',' . myInteger($country) . ')';
                $hostname = 'QStudy';
                break;
        }
        $student_year = getSessionVar('student_year');
        $html = '';
        $query = $this->db->query($stmt);
        if ($query) {
            $html .= '<table class="task_table">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th>Module Name</th>'; // class="task_table_header"
            $html .= '<th>Tracker Name</th>';
            $html .= '<th>Individual Name</th>';
            $html .= '<th>Subject</td>';
            $html .= '<th>Chapter</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            foreach ($query->result() as $row) {
                $tracker_name = $row->R_TRACKER_NAME;
                $indi_name = $row->R_INDIVIDUAL_NAME;
                $this->deleteByModule($row->R_ID);
                $first_quiz = $this->getFirstQuizId($taskfor, $row->R_ID, 't', 0, $host);
                $alink = '/all-tasks/' . $host . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                //setSessionVar('alltasks', 't');
                //setSessionVar('exam_link', $taskfor . ',' . $row->R_ID);
                if ($student_year == $row->R_GRADE_YEAR) {
                    $isall_student = $row->R_IS_ALL_STUDENT;
                    if (!$isall_student) {//not all student, just selectde student
                        $students = explode(',', $row->R_SELECTED_STUDENTS);
                        if (count($students) > 0) {
                            foreach ($students as $student) {
                                if ($student == $userid) {
                                    $html .= '<tr>';
                                    $html .= '<td><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></td>'; // class="task_table_col"
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= '<tr>';
                                }
                            }
                        }
                    } else {//all student
                        $html .= '<tr>';
                        $html .= '<td><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></td>';
                        $html .= '<td>' . $tracker_name . '</td>';
                        $html .= '<td>' . $indi_name . '</td>';
                        $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                        $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                        $html .= '<tr>';
                    }
                }
            }
            $html .= '</table>';
        }
        $array = array('html' => $html, 'stmt' => $stmt);
        //return $html;
        return $array;
    }
	
    function search_all_task_evestudy($taskfor = 0, $hostid = 0) {
        setSessionVar('alltasks', 't');
        $userid = getSessionVar('userid');
        $country = getSessionVar('country_name');
	switch($taskfor){
		case 4:
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 6:
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 8:
			
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 10:
			if($hostid==0){
				$gethost = $this->db->query("select USER_ID from USER_ACCOUNT where USER_TYPE=10")->row();
				if($gethost){
					$hostid = $gethost->USER_ID;
				}
			}
			
			$country = getSessionVar('country_name');
            $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and COUNTRY_ID='".$country."' and FOR_TRIAL IS NULL and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
                }  else {
					//for trail
                    $rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') and COUNTRY_ID='".$country."' and FOR_TRIAL=1 order by QUIZ_MODULE.ID ASC")->result();
                }
				
                $hostname = 'QStudy';
			break;
	}
        $student_year = getSessionVar('student_year');
        $html = '';

		$html .= '<table id="task_table" class="treetable rs_mytask_table dataTable" style="border: 1px solid #C7DAF4;">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Module Name</th>'; // class="task_table_header"
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Tracker Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Individual Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Subject</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Chapter</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';

           // $query = $this->db->query($rtmt);
			
            $i = 1;
            $j = 1;
            $k = 1;
			$skl=1;
            if ($rtmt) {				
                foreach ($rtmt as $row) {
					/* $is_rept_complte = $this->check_is_complete_or_repeat($row->ID);
					$is_repts_res = $this->check_is_completed_or_not_complete($row->ID,$hostid); */
					$is_rept_complte = $this->check_is_completed_or_not_complete($row->ID,$hostid,$taskfor);
					if($is_rept_complte!=1){
						
						$tracker_name = $row->TRACKER_NAME;
						$indi_name = $row->INDIVIDUAL_NAME;

						//first create when module hasn't complete
						$first_quiz = $this->getFirstQuizId($taskfor, $row->ID, '', 1, $hostid);
					   
						$alink = '/my-exam/' . $hostid . '/' . $taskfor . '/' . $row->ID . '/' . $first_quiz . '/0';
						   
						if($skl==1){
							$active_class = 'active_val';
						}else{
							$active_class = 'active_val_not';
						}					   
						
						if($student_year == $row->GRADE_YEAR) {
							if($is_rept_complte==0){
								//echo '<pre>';
								//print_r($row);
								
							$html .= "<tr data-tt-id='" . $i . "'>";
							$html .= '<td class="'.$active_class.'" style="width:30%"><span class=\'file\'><a href="javascript:fn_everydaystudyvalidity(\'' . $alink . '\','.$k.')" data_attr_rowid="'.$k.'" class="alink rseveopen rsalink_'.$k.'">' . $row->MODULES_NAME . '</a></span></td>';
							$html .= '<td>' . $tracker_name . '</td>';
							$html .= '<td>' . $indi_name . '</td>';
							$html .= '<td>' . $row->SUBJECT_NAME . '</td>';
							$html .= '<td>' . $row->CHAPTER_NAME . '</td>';
							$html .= '<tr>';
								$k++;
							}elseif($is_rept_complte==2){
								$html .= "<tr data-tt-id='" . $i . "'>";
								$html .= '<td style="width:30%"><span class=\'complete\'>' . $row->MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->CHAPTER_NAME . '</td>';
                                    //$html .= $this->getRepeatedQuestions($row->ID, $i);
                                    $html .= $this->getRepeatedQuestions_list_byrs($row->ID,$hostid,$taskfor, $i);
									
									$html .= '</tr>';
							}
						
						$i++;
						$j++;
						$skl++;
						}
					  
						
					}
                }//end of foreach loop
            }
            $html .= '</tbody>';
            //$html .= '</li>';
            //$html .= '</ul>';
            $html .= '</table>';
		$stmt='';
        $array = array('html' => $html, 'stmt' => $stmt);
        //return $html;
        return $array;
    }
	
    function search_all_tasks_evestudy($asubject = '', $achapter = '', $taskfor = 0, $hostid = 0) {
        setSessionVar('alltasks', 't');
        $userid = getSessionVar('userid');
        $country = getSessionVar('country_name');
	switch($taskfor){
		case 4:
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and QUIZ_MODULE.SUBJECT_NAME='".$asubject."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 6:
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and QUIZ_MODULE.SUBJECT_NAME='".$asubject."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 8:
			
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and QUIZ_MODULE.SUBJECT_NAME='".$asubject."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 10:
			if($hostid==0){
				$gethost = $this->db->query("select USER_ID from USER_ACCOUNT where USER_TYPE=10")->row();
				if($gethost){
					$hostid = $gethost->USER_ID;
				}
			}
			
			$country = getSessionVar('country_name');
            $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."'  and QUIZ_MODULE.SUBJECT_NAME='".$asubject."' and COUNTRY_ID='".$country."' and FOR_TRIAL IS NULL and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
                }  else {
					//for trail
                    $rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."'  and QUIZ_MODULE.SUBJECT_NAME='".$asubject."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') and COUNTRY_ID='".$country."' and FOR_TRIAL=1 order by QUIZ_MODULE.ID ASC")->result();
                }
				
                $hostname = 'QStudy';
			break;
	}
        $student_year = getSessionVar('student_year');
        $html = '';

		$html .= '<table id="task_table" class="treetable rs_mytask_table dataTable" style="border: 1px solid #C7DAF4;">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Module Name</th>'; // class="task_table_header"
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Tracker Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Individual Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Subject</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Chapter</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';

			// $query = $this->db->query($stmt);
			//echo $this->db->last_query();exit;
            $i = 1;
            $j = 1;
            $k = 1;
			$skl=1;
            if ($rtmt) {				
                foreach ($rtmt as $row) {
					/* $is_rept_complte = $this->check_is_complete_or_repeat($row->ID);
					$is_repts_res = $this->check_is_completed_or_not_complete($row->ID,$hostid); */
					$is_rept_complte = $this->check_is_completed_or_not_complete($row->ID,$hostid,$taskfor);
					if($is_rept_complte!=1){
						
						$tracker_name = $row->TRACKER_NAME;
						$indi_name = $row->INDIVIDUAL_NAME;

						//first create when module hasn't complete
						$first_quiz = $this->getFirstQuizId($taskfor, $row->ID, '', 1, $hostid);
					   
						$alink = '/my-exam/' . $hostid . '/' . $taskfor . '/' . $row->ID . '/' . $first_quiz . '/0';
						   
						if($skl==1){
							$active_class = 'active_val';
						}else{
							$active_class = 'active_val_not';
						}					   
						
						if($student_year == $row->GRADE_YEAR) {
							if($is_rept_complte==0){
								//echo '<pre>';
								//print_r($row);
								
							$html .= "<tr data-tt-id='" . $i . "'>";
							$html .= '<td class="'.$active_class.'" style="width:30%"><span class=\'file\'><a href="javascript:fn_everydaystudyvalidity(\'' . $alink . '\','.$k.')" data_attr_rowid="'.$k.'" class="alink rseveopen rsalink_'.$k.'">' . $row->MODULES_NAME . '</a></span></td>';
							$html .= '<td>' . $tracker_name . '</td>';
							$html .= '<td>' . $indi_name . '</td>';
							$html .= '<td>' . $row->SUBJECT_NAME . '</td>';
							$html .= '<td>' . $row->CHAPTER_NAME . '</td>';
							$html .= '<tr>';
								$k++;
							}elseif($is_rept_complte==2){
								$html .= "<tr data-tt-id='" . $i . "'>";
								$html .= '<td style="width:30%"><span class=\'complete\'>' . $row->MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->CHAPTER_NAME . '</td>';
                                    //$html .= $this->getRepeatedQuestions($row->ID, $i);
                                    $html .= $this->getRepeatedQuestions_list_byrs($row->ID,$hostid,$taskfor, $i);
									
									$html .= '</tr>';
							}
						
						$i++;
						$j++;
						$skl++;
						}
					  
						
					}
                }//end of foreach loop
            }
            $html .= '</tbody>';
            //$html .= '</li>';
            //$html .= '</ul>';
            $html .= '</table>';
		$stmt ='';
        $array = array('html' => $html, 'stmt' => $stmt);
        //return $html;
        return $array;
    }

    function getQuestionIds($all = '', $ataskfor = '', $amodule_id = '', $ahostid = 0) {
        if (!$ataskfor) {
            $taskfor = $this->uri->segment(3);
        } else {
            $taskfor = $ataskfor;
        }
        if (!$amodule_id) {
            $module_id = $this->uri->segment(4);
        } else {
            $module_id = $amodule_id;
        }
        if (!$ahostid) {
            $hostid = $this->uri->segment(2);
        } else {
            $hostid = $ahostid;
        }
        $userid = getSessionVar('userid');
        $current_date = getSessionVar('current_date');
        $country = getSessionVar('country_name');
        $account_type = getSessionVar('account_type');
        if (!$account_type){
            if (!$all) {
                $stmt = 'select * from SP_GET_MODULE_QUESTIONS(' . myInteger($userid) . ',' . myInteger($taskfor)
                        . ',' . myInteger($module_id) . ', NULL' . ',' . myDate($current_date) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
            } else {
                $stmt = 'select * from SP_GET_MODULE_ALL_QUESTIONS(' . myInteger($userid) . ',' . myInteger($taskfor) . ',' .
                        myInteger($module_id) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
            }
        }  else {            
            if (!$all) {
                $stmt = 'select * from SP_GET_MODULE_QUESTIONS_TR(' . myInteger($userid) . ',' . myInteger($taskfor)
                        . ',' . myInteger($module_id) . ', NULL' . ',' . myDate($current_date) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
            } else {
                $stmt = 'select * from SP_GET_MODULE_ALL_QUESTIONS_TR(' . myInteger($userid) . ',' . myInteger($taskfor) . ',' .
                        myInteger($module_id) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
            }
        }
        //var_dump($stmt);
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                if ($row->R_QUESTIONS) {
                    $questionid = $row->R_QUESTIONS;
                    $return = 't';
                } else {
                    $questionid = '';
                    $return = 'f';
                }
            } else {
                $questionid = 'not row ' . $stmt;
                $return = '';
            }
        } else {
            $questionid = 'not query ' . $stmt;
            $return = 'f';
        }
        $questions = explode(',', $questionid);
        //asort($questions);
        $allquestion = array_values($questions);
        $newquestion = implode(',', $allquestion);
        //var_dump($newquestion);
        $qust_array = array('result' => $return, 'questionid' => $newquestion);
        return $qust_array;
    }

    function getQuestionIdsSpecial($ataskfor = '', $amodule_id = '', $ahostid = 0) {
        if (!$ataskfor) {
            $taskfor = $this->uri->segment(3);
        } else {
            $taskfor = $ataskfor;
        }
        if (!$amodule_id) {
            $module_id = $this->uri->segment(4);
        } else {
            $module_id = $amodule_id;
        }
        if (!$ahostid) {
            $hostid = $this->uri->segment(2);
        } else {
            $hostid = $ahostid;
        }
        $userid = getSessionVar('userid');
        $current_date = getSessionVar('current_date');
        $country = getSessionVar('country_name');
        $account_type = getSessionVar('account_type');
        if (!$account_type){
            $stmt = 'select * from SP_GET_MODULE_QUESTIONS_SE(' . myInteger($userid) . ',' . myInteger($taskfor)
                    . ',' . myInteger($module_id) . ', NULL' . ',' . myDate($current_date) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
        }  else {            
            $stmt = 'select * from SP_GET_MODULE_QUESTIONS_SETR(' . myInteger($userid) . ',' . myInteger($taskfor)
                    . ',' . myInteger($module_id) . ', NULL' . ',' . myDate($current_date) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
        }
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                if ($row->R_QUESTIONS) {
                    $questionid = $row->R_QUESTIONS;
                    $return = 't';
                } else {
                    $questionid = '';
                    $return = 'f';
                }
            } else {
                $questionid = 'not row ' . $stmt;
                $return = '';
            }
        } else {
            $questionid = 'not query ' . $stmt;
            $return = 'f';
        }
        $questions = explode(',', $questionid);
        //asort($questions);
        $allquestion = array_values($questions);
        $newquestion = implode(',', $allquestion);
        //var_dump($newquestion);
        $qust_array = array('result' => $return, 'questionid' => $newquestion);
        return $qust_array;
    }

    function getFirstQuizId($task, $module, $all = '', $alist = 0, $hostid = 0) {
        $userid = getSessionVar('userid');
        $current_date = getSessionVar('current_date');
        $country = getSessionVar('country_name');
        $account_type = getSessionVar('account_type');
        if (!$account_type){
            if (!$all) {
                $stmt = 'select * from SP_GET_MODULE_QUESTIONS(' . myInteger($userid) . ',' . myInteger($task) .
                        ',' . myInteger($module) . ',' . myInteger($alist) . ',' . myDate($current_date) . ',' . myInteger($country) .
                        ',' . myInteger($hostid) . ')';
            } else {
                $stmt = 'select * from SP_GET_MODULE_ALL_QUESTIONS(' . myInteger($userid) . ',' . myInteger($task) . ',' .
                        myInteger($module) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
            }
        }  else {            
            if (!$all) {
                $stmt = 'select * from SP_GET_MODULE_QUESTIONS_TR(' . myInteger($userid) . ',' . myInteger($task) .
                        ',' . myInteger($module) . ',' . myInteger($alist) . ',' . myDate($current_date) . ',' . myInteger($country) .
                        ',' . myInteger($hostid) . ')';
            } else {
                $stmt = 'select * from SP_GET_MODULE_ALL_QUESTIONS_TR(' . myInteger($userid) . ',' . myInteger($task) . ',' .
                        myInteger($module) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
            }
        }
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                if ($row->R_QUESTIONS) {
                    $questionid = $row->R_QUESTIONS;
                } else {
                    $questionid = '';
                }
            } else {
                $questionid = '';
            }
        } else {
            $questionid = '';
        }
        if ($questionid) {
            $quiz_id = explode(',', $questionid);
        } else {
            $quiz_id = array();
        }
        //asort($quiz_id);
        if (count($quiz_id) > 0) {
            //asort($quiz_id);
            $allquestion = array_values($quiz_id);
            return $allquestion[0];
        } else {
            return '';
        }
    }

    function getFirstQuizIdse($task, $module, $all = '', $alist = 0, $hostid = 0) {
        $userid = getSessionVar('userid');
        $current_date = getSessionVar('current_date');
        $country = getSessionVar('country_name');
        $account_type = getSessionVar('account_type');
        if (!$account_type){
            if (!$all) {
                $stmt = 'select * from SP_GET_MODULE_QUESTIONS_SE(' . myInteger($userid) . ',' . myInteger($task) .
                        ',' . myInteger($module) . ',' . myInteger($alist) . ',' . myDate($current_date) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
            } else {
                $stmt = 'select * from SP_GET_MODULE_ALL_QUESTIONS(' . myInteger($userid) . ',' . myInteger($task) . ',' .
                        myInteger($module) . ',' . myInteger($country) . ')';
            }
        }  else {            
            if (!$all) {
                $stmt = 'select * from SP_GET_MODULE_QUESTIONS_SETR(' . myInteger($userid) . ',' . myInteger($task) .
                        ',' . myInteger($module) . ',' . myInteger($alist) . ',' . myDate($current_date) . ',' . myInteger($country) . ',' . myInteger($hostid) . ')';
            } else {
                $stmt = 'select * from SP_GET_MODULE_ALL_QUESTIONS_TR(' . myInteger($userid) . ',' . myInteger($task) . ',' .
                        myInteger($module) . ',' . myInteger($country) . ')';
            }
        }
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                if ($row->R_QUESTIONS) {
                    $questionid = $row->R_QUESTIONS;
                } else {
                    $questionid = '';
                }
            } else {
                $questionid = '';
            }
        } else {
            $questionid = '';
        }
        if ($questionid) {
            $quiz_id = explode(',', $questionid);
        } else {
            $quiz_id = array();
        }
        //asort($quiz_id);
        if (count($quiz_id) > 0) {
            //asort($quiz_id);
            $allquestion = array_values($quiz_id);
            return $allquestion[0];
        } else {
            return '';
        }
    }

    function getAnsweredQuestion($aQuestionId = 0) {
        $module = getSessionVar('module');
        if (!$module) {
            $module_id = $this->uri->segment(3);
        } else {
            $module_id = $module;
        }
        $userid = getSessionVar('userid');
        $stmt = 'select * from SP_GET_ANSWERED_QUESTION(' . $userid . ',' . $module_id . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            $rcount = $query->row();
            if ($rcount) {
                foreach ($query->result() as $row) {
                    if ($row->R_QUESTION_ID == $aQuestionId) {
                        $return = array('id' => $row->R_QUESTION_ID, 'answered' => 't', 'right' => $row->R_ANSWER_RIGHT, 'mark' => $row->R_ANSWER_MARK_OBTAINED);
                    } else {
                        $return = array('id' => $row->R_QUESTION_ID, 'answered' => 'f', 'right' => $row->R_ANSWER_RIGHT, 'mark' => $row->R_ANSWER_MARK_OBTAINED);
                    }
                }
            } else {
                $return = array('id' => '', 'answered' => 'f', 'right' => '&nbsp;', 'mark' => '&nbsp;');
            }
        } else {
            $return = array('id' => '', 'answered' => 'f', 'right' => '&nbsp;', 'mark' => '&nbsp;');
        }
        return $return;
    }

    function questionIsAnswered($aquestionid = 0, $aModuleId = 0) {
		$userid = getSessionVar('userid');
        $stmt = 'select R_RESULT from SP_QUESTION_IS_ANSWERED(' . myInteger($aquestionid) . ',' . myInteger($aModuleId) . ',' . myInteger($userid) . ')';
        $query = $this->db->query($stmt);
		
        if ($query) {
            $row = $query->row();
			
            if ($row) {
                $result = $row->R_RESULT;
                if ($result) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getAnswerMark($aquestionid = 0, $amark = 0, $atype = 0) {
        if ($atype != 9) {
            if ($this->questionIsAnswered($aquestionid)) {
                return $amark;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    function getBackButton() {
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $current_question = $this->uri->segment(4);
        $prev_question = $this->uri->segment(5);

        $userid = getSessionVar('userid');

        $question_ids = $this->getQuestionIds();

        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];
            $question_array = explode(',', $questions);
            if ($prev_question != '0') {
                for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                    if ($question_array[$j] == $prev_question) {
                        $pos = $j;
                        if (isset($question_array[$pos - 1])) {
                            $pre_question = $question_array[$pos - 1];
                            $post_question = $question_array[$pos];
                            $link = "javascript:fn_select_question($taskfor, $module_id, $pre_question, $post_question)";
                            $back = '<a href="' . $link . '" class="back_button">Back</a>';
                        } else {
                            $back = '';
                        }
                        /* var_dump($pos . ' ' . $question_array[$pos] . ' ' . $question_array[$pos -1]);
                          //var_dump($question_array[$pos]);
                          $pre_question = $question_array[$pos];
                          if ($pos != 0){
                          $post_question = $question_array[$pos -1];
                          }  else {
                          $post_question = $question_array[$pos];
                          } */
                    } else {
                        $pre_question = '0';
                        $post_question = '0';
                    }
                    //if ($question_array[$j] == $current_question) {
                    /* $pos = $j;
                      if ($pos != 0) {
                      $pre_question = $question_array[$pos - 1];
                      $post_question = $question_array[$pos];
                      if ($post_question == $question_array[0]){
                      $post_question = '0';
                      }  else {
                      $post_question = $question_array[$pos];
                      }
                      } else {
                      $pre_question = $question_array[$pos];
                      $post_question = '0'; //$question_array[0];
                      }
                      break; */
                    /* } else {
                      $pos = 0;
                      $post_question = '0';
                      $pre_question = '0';
                      var_dump('not post ' . $pos);
                      } */
                }
                /* if ($pre_question) {
                  $link = "javascript:fn_select_question($taskfor, $module_id, $pre_question, $post_question)";
                  $back = '<a href="' . $link . '" class="back_button">Back</a>';
                  } else {
                  $back = '';
                  } */
            } else {
                $back = '';
            }

            /* $stmt = 'select * from SP_GET_QUESTIONS_SCORE(' . myString($questions) . ')';
              $query = $this->db->query($stmt);
              if ($query) {
              foreach ($query->result() as $row) {
              if ($prev_question != '0') {
              for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
              if ($question_array[$j] == $current_question) {
              $pos = $j;
              if ($pos != 0) {
              $post_question = $question_array[$pos - 1];
              } else {
              $post_question = '0'; //$question_array[0];
              }
              break;
              } else {
              $pos = 0;
              $post_question = '0';
              }
              }
              if ($post_question) {
              $link = "javascript:fn_select_question($taskfor, $module_id, $post_question, $row->R_QUIZ_ANSWER_ID)";
              $back = '<a href="' . $link . '" class="back_button">Back</a>';
              } else {
              $back = '';
              }
              } else {
              $back = '';
              }
              }
              } else {
              $back = '';
              } */
        } else {
            $back = '';
        }
        /* for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
          if ($question_array[$j] == $current_question) {
          $pos = $j;
          if ($pos != 0) {
          $post_question = $question_array[$pos + 1];
          } else {
          $post_question = ''; //$question_array[0];
          }
          break;
          } else {
          $pos = 0;
          $post_question = '';
          }
          }
          if ($post_question) {
          $link = "javascript:fn_select_question($taskfor, $module_id, $post_question, $current_question)";
          $back = '<a href="' . $link . '" class="back_button">Back</a>';
          } else {
          $back = '';
          } */
        /* } else {
          $back = '';
          } */
        /* if ($question_ids['result'] == 't') {
          $questions = $question_ids['questionid'];
          $question_array = explode(',', $questions);
          for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
          if ($question_array[$j] == $current_question) {
          $pos = $j;
          if ($pos != 0) {
          $post_question = $question_array[$pos + 1];
          } else {
          $post_question = $current_question;
          }
          //$post_question = $pos;
          break;
          } else {
          $pos = 0;
          $post_question = '0';
          }
          }
          } */
        /* if ($prev_question == '0') {
          $back = '';
          } else {
          $link = "javascript:fn_select_question($taskfor, $module_id, $prev_question, $post_question)";
          $back = '<a href="' . $link . '" class="back_button">Back</a>';
          } */
        return $back;
    }

    function generateSLNo($questions) {
        $module_id = $this->uri->segment(3);
        $stmt = 'select * from SP_QUESTION_SCORE(' . myString($questions) . ')';
        $query = $this->db->query($stmt);
        $slno = array();
        if ($query) {
            foreach ($query->result() as $row) {
                if (!in_array($row->R_SL_NO, $slno)) {
                    array_push($slno, $row->R_SL_NO);
                } else {
                    array_push($slno, '');
                }
            }
            $result = $slno;
        } else {
            $result = array();
        }
        return $result;
    }

    function generateSLNoPreview() {
        $module_id = $this->uri->segment(3);
        $userid = $this->uri->segment(6);
        $stmt = "select * from SP_GET_PROGRESS_SHEET($userid, $module_id)";
        $query = $this->db->query($stmt);
        $slno = array();
        if ($query) {
            foreach ($query->result() as $row) {
                if (!in_array($row->R_SL_NO, $slno)) {
                    array_push($slno, $row->R_SL_NO);
                } else {
                    array_push($slno, '');
                }
            }
            $result = $slno;
        } else {
            $result = array();
        }
        return $result;
    }

    function getQuestionAnswer($aModuleid = 0, $aQuestionid = 0, $aUserId = 0) {
        $stmt = 'select * from SP_GET_ANSWER_FOR_QUESTION(' . myInteger($aModuleid)
                . ',' . myInteger($aQuestionid) . ',' . myInteger($aUserId) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = array('answer_right' => $row->R_ANSWER_RIGHT,
                    'mark_obtained' => $row->R_ANSWER_MARK_OBTAINED,
                    'question_id' => $aQuestionid,
                    'answer_id' => $row->R_ID);
            } else {
                $return = array();
            }
        } else {
            $return = array();
        }
        return $return;
    }
	function getQuestionsScore_mdfy($ataskfor = '', $amodule_id = '', $acurrent_question = ''){
		if (!$ataskfor) {
            $taskfor = $this->uri->segment(3);
        } else {
            $taskfor = $ataskfor;
        }
        if (!$amodule_id) {
            $module_id = $this->uri->segment(4);
        } else {
            $module_id = $amodule_id;
        }
        if (!$acurrent_question) {
            $current_question = $this->uri->segment(5);
        } else {
            $current_question = $acurrent_question;
        }

        $userid = getSessionVar('userid');

        //load module for getting modules_name
        $load_module = $this->quiz_module->load($module_id);
        if ($load_module) {
            $modules_name = 'Module Name: ' . $this->quiz_module->modules_name;
        } else {
            $modules_name = '';
        }

        $hostid = $this->uri->segment(2);

        $question_ids = $this->getQuestionIds('', $taskfor, $amodule_id);
        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];
            //var_dump($this->generateSLNo($questions));
            $slno = $this->generateSLNo($questions);
			//echo $questions;
			//echo '<br>';
            $stmt = 'select * from SP_QUESTION_SCORE(' . myString($questions) . ')';
            $query = $this->db->query($stmt);
			$custarr = array();
			foreach($query->result() as $rows){
				$custarr[$rows->R_QUIZ_ANSWER_ID] = $rows; 
			}
			//print_r($query->result());exit;
            $html = '<table class="table_scoreboard">';
            $html .= '<tr class="firsttr">';
            //$html .= '<td class="table_header" colspan="5">' . $modules_name . '</td>';
             $html .= '<td class="table_header top_m_head" colspan="5"><span class="rmnames">' . $modules_name . '</span><span class="rsocress"></span></td>';
            $html .= '</tr>';
            $html .= '<tr class="rsscrehdr">';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';
  
            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
            $html .= '<td class="table_header">Description</td>';
            $html .= '</tr>';
            $prevquestion = getSessionVar('prevquestion');
            $i = 1;
            $question_array = explode(',', $questions);
            //ksort($question_array);
            //print_r($questions);
            if ($query) {
                $k = 0;
                foreach ($custarr as $row) {
                    $answer = $this->getQuestionAnswer($module_id, $row->R_QUIZ_ANSWER_ID, $userid);
                    if ($answer) {
                        $answer_right = $answer['answer_right'];
                        $mark_obtained = $answer['mark_obtained'];
                        $answer_id = $answer['answer_id'];
                    } else {
                        $answer_right = '';
                        $mark_obtained = '';
                        $answer_id = '';
                    }
                    
     if($mark_obtained!=''){
        $frac_check_m = $this->convert_decimal_to_fraction($mark_obtained);
            if (strpos($frac_check_m, '/') !== false) {
                $rslts_m = explode('/', $frac_check_m);
                $scnd_main_m = $rslts_m[0];
                $scnd_top_m = $rslts_m[1];
                $scnd_bottom_m = $rslts_m[2];
                $mark_obtained = '<table class="rsscondryTable std_score">';
                $mark_obtained .='<tbody>';
                $mark_obtained .='<tr>';
                $mark_obtained .='<td rowspan="2" class="scandry_mainval std_score_main">';
                $mark_obtained .=$scnd_main_m;
                $mark_obtained .='</td>';
                $mark_obtained .='<td style="border-bottom:solid 1px" class="scndry_top ">';
                $mark_obtained .=$scnd_top_m;
                $mark_obtained .='</td>';
                $mark_obtained .='</tr>';
                $mark_obtained .='<tr>';           
                $mark_obtained .='<td class="scndry_bottom">';
                $mark_obtained .= $scnd_bottom_m;
                $mark_obtained .='</td>';
                $mark_obtained .='</tr>';
                $mark_obtained .='</tbody>';
                $mark_obtained .='</table>';
            }else{
                $mark_obtained = $mark_obtained; 
            }
    }
        $frac_check = $this->convert_decimal_to_fraction($row->R_ASSIGN_SCORE);
        if (strpos($frac_check, '/') !== false) {
            $rslts = explode('/', $frac_check);
            $scnd_main = $rslts[0];
            $scnd_top = $rslts[1];
            $scnd_bottom = $rslts[2];
            $qscore_is = '<table class="rsscondryTable std_score">';
            $qscore_is .='<tbody>';
            $qscore_is .='<tr>';
            $qscore_is .='<td rowspan="2" class="scandry_mainval std_score_main">';
            $qscore_is .=$scnd_main;
            $qscore_is .='</td>';
            $qscore_is .='<td style="border-bottom:solid 1px" class="scndry_top ">';
            $qscore_is .=$scnd_top;
            $qscore_is .='</td>';
            $qscore_is .='</tr>';
            $qscore_is .='<tr>';           
            $qscore_is .='<td class="scndry_bottom">';
            $qscore_is .= $scnd_bottom;
            $qscore_is .='</td>';
            $qscore_is .='</tr>';
            $qscore_is .='</tbody>';
            $qscore_is .='</table>';
        }else{
            $qscore_is = $row->R_ASSIGN_SCORE; 
        }                
        
                    $ques_id = $row->R_QUIZ_ANSWER_ID;
                    $this->assign_score[] = array('id' => $row->R_QUIZ_ANSWER_ID, 'score' => $row->R_ASSIGN_SCORE);
					switch ($answer_right) {
                            case '1':
								$ans_status = '<img src="/images/tick.png" alt="Answer Correct"/>';
								$answerd = true;
							break;
							
							case '0':
								$ans_status = '<img src="/images/invalid.png" alt="Answer Invalid"/>';
								$answerd = true;
							break;
							default:
								$ans_status = '';
								$answerd = false;
							break;
					}
				   
					if(!$answerd){
					
					
				for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
					if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
						$pos = $j;
						if ($pos != 0) {
							$prev_question = $question_array[$pos - 1];
						} else {
							$prev_question = '0'; //$question_array[0];
						}
						break;
					} else {
						$pos = 0;
						$prev_question = '0';
					}
				}
                $link = "javascript:fn_select_question($hostid, $taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question)";
					}else{
						$link= '';	
					}			
					

                    if ($current_question != $row->R_QUIZ_ANSWER_ID) {
						$html .= '<tr class="normal_rows_prq">';
						
						
						if($row->R_QUIZ_TYPE != 9){
							$html .= '<td class="row_right_wong rsprgress_td">'.$ans_status.'</td>';
						}else{
							$html .= '<td class="row_right_wong rsprgress_td rsqdata pending_prq" rsmid="'.$module_id.'" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">Pending</td>';
						}
						
						if($link!=''){
							$html .= '<td class="row_right_wong slno table_row rsprgress_td"><a class="alink" href="'.$link.'">'.$i.'</a></td>';
						}else{
							$html .= '<td class="row_right_wong slno rsprgress_td">'.$i.'</td>';
						}
						
						
						if($row->R_QUIZ_TYPE != 9){
							
						$html .= '<td class="table_row_bold">' . $qscore_is . '</td>';
                        $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '">' . $mark_obtained . '</td>';					
						 $html .= '<td class="table_row_bold"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
							
							
						} else {
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
						}
						
						
						$html .= '</tr>';
					} else {
						$html .= '<tr class="rows_prq_highlight">';	
							
							
						if($row->R_QUIZ_TYPE != 9){
							$html .= '<td class="row_right_wong rsprgress_td">'.$ans_status.'</td>';
						}else{
							$html .= '<td class="row_right_wong rsprgress_td rsqdata pending_prq" rsmid="'.$module_id.'" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">Pending</td>';
						}
						
						if($link!=''){
							$html .= '<td class="row_right_wong slno table_row rsprgress_td"><a class="alink" href="'.$link.'">'.$i.'</a></td>';
						}else{
							$html .= '<td class="row_right_wong slno rsprgress_td">'.$i.'</td>';
						}
						
						
						if($row->R_QUIZ_TYPE != 9){
							
						$html .= '<td class="table_row_bold">' . $qscore_is . '</td>';
                        $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '">' . $mark_obtained . '</td>';					
						 $html .= '<td class="table_row_bold"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
							
							
						} else {
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
						}
							
						
						
						$html .= '</tr>';
					}
					$i++;
				}
                $html .= '</table>';
            } else {
                $html = $stmt;
            }
            return $html;
        } else {
            return $question_ids['questionid'];
        }				
	}
    function getQuestionsScore($ataskfor = '', $amodule_id = '', $acurrent_question = '') {
        if (!$ataskfor) {
            $taskfor = $this->uri->segment(3);
        } else {
            $taskfor = $ataskfor;
        }
        if (!$amodule_id) {
            $module_id = $this->uri->segment(4);
        } else {
            $module_id = $amodule_id;
        }
        if (!$acurrent_question) {
            $current_question = $this->uri->segment(5);
        } else {
            $current_question = $acurrent_question;
        }

        $userid = getSessionVar('userid');

        //load module for getting modules_name
        $load_module = $this->quiz_module->load($module_id);
        if ($load_module) {
            $modules_name = 'Module Name: ' . $this->quiz_module->modules_name;
        } else {
            $modules_name = '';
        }

        $hostid = $this->uri->segment(2);

        $question_ids = $this->getQuestionIds('', $taskfor, $amodule_id);
        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];
            //var_dump($this->generateSLNo($questions));
            $slno = $this->generateSLNo($questions);
			//echo $questions;
			//echo '<br>';
            $stmt = 'select * from SP_QUESTION_SCORE(' . myString($questions) . ')';
            $query = $this->db->query($stmt);
			//print_r($query->result());exit;
            $html = '<table class="table">';
            $html .= '<tr>';
            $html .= '<td class="table_header" colspan="5">' . $modules_name . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';
            $html .= '<td class="table_header">Description</td>';
            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
            $html .= '</tr>';
            $prevquestion = getSessionVar('prevquestion');
            $i = 1;
            $question_array = explode(',', $questions);
            //ksort($question_array);
            //print_r($questions);
            if ($query) {
                $k = 0;
                foreach ($query->result() as $row) {
                    $answer = $this->getQuestionAnswer($module_id, $row->R_QUIZ_ANSWER_ID, $userid);
                    if ($answer) {
                        $answer_right = $answer['answer_right'];
                        $mark_obtained = $answer['mark_obtained'];
                        $answer_id = $answer['answer_id'];
                    } else {
                        $answer_right = '';
                        $mark_obtained = '';
                        $answer_id = '';
                    }
                    $ques_id = $row->R_QUIZ_ANSWER_ID;
                    $this->assign_score[] = array('id' => $row->R_QUIZ_ANSWER_ID, 'score' => $row->R_ASSIGN_SCORE);
                    $html .= '<tr>';
                    if ($current_question != $row->R_QUIZ_ANSWER_ID) {
                        switch ($answer_right) {
                            case '1':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_right" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="rsqdata table_row_right_no_border" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = true;
                                break;

                            case '0':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_wrong" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    if ($slno[$k] != '') {
                                        $html .= '<td style="width:30px" class="rsqdata table_row_pending" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">P</td>';
                                    } else {
                                        $html .= '<td style="width:30px" class="rsqdata table_row_pending" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                    }
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = true;
                                break;

                            default:
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '"></td>';
                                } else {
                                    $html .= '<td style="width:30px" class="rsqdata table_row_noborder_left" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="' . $row->R_QUIZ_ANSWER_ID . '">&nbsp; Q</td>';
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_noborder_left" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = false;
                                break;
                        }
                        if (!$answerd) {
                            for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                                if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
                                    $pos = $j;
                                    if ($pos != 0) {
                                        $prev_question = $question_array[$pos - 1];
                                    } else {
                                        $prev_question = '0'; //$question_array[0];
                                    }
                                    break;
                                } else {
                                    $pos = 0;
                                    $prev_question = '0';
                                }
                            }
                            $link = "javascript:fn_select_question($hostid, $taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question)";
                            //$html .= '<td class="table_row"><a class="alink" href="' . $link . '">' . $row->R_SL_NO . '</a></td>';
                            if ($row->R_QUIZ_TYPE != 9) {
                                $html .= '<td class="table_row"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            } else {
                                $html .= '<td class="table_row_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            }
                        } else {
                            //$html .= '<td class="table_row">' . $row->R_SL_NO . '</td>';
                            if ($row->R_QUIZ_TYPE != 9) {
                                $html .= '<td class="table_row">' . $slno[$k] . '</td>';
                            } else {
                                $html .= '<td class="table_row_noborder">' . $slno[$k] . '</td>';
                            }
                        }

                      //  $html .= '<td class="table_row">' . $row->R_ASSIGN_DESCRIPTION . '</td>';
                        $html .= '<td class="table_row_bold" style="text-align:right;padding-right:2px;">' . $row->R_ASSIGN_SCORE . '</td>';
                        $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
						
						if ($row->R_QUIZ_TYPE != 9) {
						 $html .= '<td class="table_row_highlight"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
						}else{
							$html .= '<td class="table_row_highlight"></td>';
						}
						
                        $html .= '</tr>';
                        $i++;
                    } else {
                        switch ($answer_right) {
                            case '1':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_right_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="rsqdata table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = true;
                                break;

                            case '0':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_wrong_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    if ($slno[$k] != '') {
                                        $html .= '<td style="width:30px" class="rsqdata table_row_pending_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">P</td>';
                                    } else {
                                        $html .= '<td style="width:30px" class="rsqdata table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                    }
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = true;
                                break;

                            default:
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="rsqdata table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = false;
                                break;
                        }
                        if (!$answerd) {
                            for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                                if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
                                    $pos = $j;
                                    if ($pos != 0) {
                                        $prev_question = $question_array[$pos - 1];
                                    } else {
                                        $prev_question = '0'; //$question_array[0];
                                    }
                                    break;
                                } else {
                                    $pos = 0;
                                    $prev_question = '0';
                                }
                            }
                            $link = "javascript:fn_select_question($hostid, $taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question)";
                            //$html .= '<td class="table_row_highlight"><a class="alink" href="' . $link . '">' . $row->R_SL_NO . '</a></td>';
                            //if ($slno[$k] != '') {
                            if ($row->R_QUIZ_TYPE != 9) {
                                $html .= '<td class="table_row_highlight"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            } else {
                                $html .= '<td class="table_row_highlight_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            }
                            /* } else {
                              $html .= '<td class="table_row_highlight_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                              } */
                        } else {
                            //$html .= '<td class="table_row_highlight">' . $row->R_SL_NO . '</td>';
                            if ($row->R_QUIZ_TYPE != 9) {
                                $html .= '<td class="table_row_highlight">' . $slno[$k] . '</td>';
                            } else {
                                $html .= '<td class="table_row_highlight_noborder">' . $slno[$k] . '</td>';
                            }
                        }

                       // $html .= '<td class="table_row_highlight">' . $row->R_ASSIGN_DESCRIPTION . '</td>';
                        $html .= '<td class="table_row_bold_highlight" style="text-align:right;padding-right:2px;">' . $row->R_ASSIGN_SCORE . '</td>';
                        $html .= '<td class="table_row_bold_highlight" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
						
						if ($row->R_QUIZ_TYPE != 9) {
							$html .= '<td class="table_row_highlight"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
						} else {
							$html .= '<td class="table_row_highlight"></td>';
						}
					
                        $html .= '</tr>';
                        $i++;
                    }
                    $k++;
                }
                $html .= '</table>';
            } else {
                $html = $stmt;
            }
            return $html;
        } else {
            return $question_ids['questionid'];
        }
    }

/* return fraction value as string */
function convert_decimal_to_fraction($decimal){
    if($decimal!='0.00'){
        $big_fraction = $this->float2rat($decimal);
        $num_array = explode('/', $big_fraction);
        $numerator = $num_array[0];
        $denominator = $num_array[1];
        $whole_number = floor( $numerator / $denominator );
        $numerator = $numerator % $denominator;
        if($numerator == 0){
            return $whole_number;
        }else if ($whole_number == 0){
            return $numerator . '/' . $denominator;
        }else{
            return $whole_number . '/' . $numerator . '/' . $denominator;
        }
    }else{
        return '0.00';
    }
    
}

function float2rat($n, $tolerance = 1.e-6) {
    $h1=1; $h2=0;
    $k1=0; $k2=1;
    $b = 1/$n;
    do {
        $b = 1/$b;
        $a = floor($b);
        $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
        $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
        $b = $b-$a;
    } while (abs($n-$h1/$k1) > $n*$tolerance);
    return "$h1/$k1";
}    
/* return fraction value as string */    
    
    
    
function getQuestionsScoreSpecial_modify($ataskfor = '', $amodule_id = '', $acurrent_question = ''){
		
		if (!$ataskfor) {
            $taskfor = $this->uri->segment(3);
        } else {
            $taskfor = $ataskfor;
        }
        if (!$amodule_id) {
            $module_id = $this->uri->segment(4);
        } else {
            $module_id = $amodule_id;
        }
        if (!$acurrent_question) {
            $current_question = $this->uri->segment(5);
        } else {
            $current_question = $acurrent_question;
        }

        $userid = getSessionVar('userid');

        //load module for getting modules_name
        $load_module = $this->quiz_module->load($module_id);
        if ($load_module) {
            $modules_name = 'Module Name: ' . $this->quiz_module->modules_name;
        } else {
            $modules_name = '';
        }
        $hostid = $this->uri->segment(2);
        $question_ids = $this->getQuestionIdsSpecial($ataskfor, $amodule_id);
        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];
            //var_dump($this->generateSLNo($questions));
            $slno = $this->generateSLNo($questions);
            $stmt = 'select * from SP_QUESTION_SCORE(' . myString($questions) . ')';
            $query = $this->db->query($stmt);
			$custarr = array();
			//print_r($query->result());exit;
			foreach($query->result() as $rows){
				$custarr[$rows->R_QUIZ_ANSWER_ID] = $rows; 
			}
            $html = '<table class="table_scoreboard">';
            $html .= '<tr class="firsttr">';
            //$html .= '<td class="table_header" colspan="5">' . $modules_name . '</td>';
             $html .= '<td class="table_header top_m_head" colspan="5"><span class="rmnames">' . $modules_name . '</span><span class="rsocress"></span></td>';
            $html .= '</tr>';
            $html .= '<tr class="rsscrehdr">';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';

            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
			$html .= '<td class="table_header">Description</td>';
            $html .= '</tr>';
            $prevquestion = getSessionVar('prevquestion');
            $i = 1;
            $question_array = explode(',', $questions);
            //ksort($question_array);
            //print_r($questions);
            if ($query) {
                $k = 0;
                foreach ($custarr as $row) {
                    $answer = $this->getQuestionAnswer($module_id, $row->R_QUIZ_ANSWER_ID, $userid);
                    if ($answer) {
                        $answer_right = $answer['answer_right'];
                        $mark_obtained = $answer['mark_obtained'];
                        $answer_id = $answer['answer_id'];
                    } else {
                        $answer_right = '';
                        $mark_obtained = '';
                        $answer_id = '';
                    }
                    
                    if($mark_obtained!=''){
                        $frac_check_m = $this->convert_decimal_to_fraction($mark_obtained);
                                    if (strpos($frac_check_m, '/') !== false) {
                                        $rslts_m = explode('/', $frac_check_m);
                                        $scnd_main_m = $rslts_m[0];
                                        $scnd_top_m = $rslts_m[1];
                                        $scnd_bottom_m = $rslts_m[2];
                                        $mark_obtained = '<table class="rsscondryTable std_score">';
                                        $mark_obtained .='<tbody>';
                                        $mark_obtained .='<tr>';
                                        $mark_obtained .='<td rowspan="2" class="scandry_mainval std_score_main">';
                                        $mark_obtained .=$scnd_main_m;
                                        $mark_obtained .='</td>';
                                        $mark_obtained .='<td style="border-bottom:solid 1px" class="scndry_top ">';
                                        $mark_obtained .=$scnd_top_m;
                                        $mark_obtained .='</td>';
                                        $mark_obtained .='</tr>';
                                        $mark_obtained .='<tr>';           
                                        $mark_obtained .='<td class="scndry_bottom">';
                                        $mark_obtained .= $scnd_bottom_m;
                                        $mark_obtained .='</td>';
                                        $mark_obtained .='</tr>';
                                        $mark_obtained .='</tbody>';
                                        $mark_obtained .='</table>';
                                    }else{
                                        $mark_obtained = $mark_obtained; 
                                    }
                    }
                    
                    
                    $ques_id = $row->R_QUIZ_ANSWER_ID;
                    $this->assign_score[] = array('id' => $row->R_QUIZ_ANSWER_ID, 'score' => $row->R_ASSIGN_SCORE);
                   
					switch ($answer_right) {
                            case '1':
								$ans_status = '<img src="/images/tick.png" alt="Answer Correct"/>';
								$answerd = true;
							break;
							
							case '0':
								$ans_status = '<img src="/images/invalid.png" alt="Answer Invalid"/>';
								$answerd = true;
							break;
							default:
								$ans_status = '';
								$answerd = false;
							break;
					}
				   
					if(!$answerd){
						
						for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                                if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
                                    $pos = $j;
                                    if ($pos != 0) {
                                        $prev_question = $question_array[$pos - 1];
                                    } else {
                                        $prev_question = '0'; //$question_array[0];
                                    }
                                    break;
                                } else {
                                    $pos = 0;
                                    $prev_question = '0';
                                }
                            }
                            $link = "javascript:fn_select_question_special($hostid, $taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question)";
						
						
					}else{
						$link = '';
					}
                                        
                                        
                                    $frac_check = $this->convert_decimal_to_fraction($row->R_ASSIGN_SCORE);
                                    //print_r($frac_check);
                                   // exit;
                                    if (strpos($frac_check, '/') !== false) {
                                        $rslts = explode('/', $frac_check);
                                        if(count($rslts)>=3){
                                            $scnd_main = $rslts[0];
                                            $scnd_top = $rslts[1];
                                            $scnd_bottom = $rslts[2]; 
                                        }else{
                                            $scnd_main = '';
                                            $scnd_top = $rslts[0];
                                            $scnd_bottom = $rslts[1];
                                        }
                                        
                                        $qscore_is = '<table class="rsscondryTable std_score">';
                                        $qscore_is .='<tbody>';
                                        $qscore_is .='<tr>';
                                        $qscore_is .='<td rowspan="2" class="scandry_mainval std_score_main">';
                                        $qscore_is .=$scnd_main;
                                        $qscore_is .='</td>';
                                        $qscore_is .='<td style="border-bottom:solid 1px" class="scndry_top ">';
                                        $qscore_is .=$scnd_top;
                                        $qscore_is .='</td>';
                                        $qscore_is .='</tr>';
                                        $qscore_is .='<tr>';           
                                        $qscore_is .='<td class="scndry_bottom">';
                                        $qscore_is .= $scnd_bottom;
                                        $qscore_is .='</td>';
                                        $qscore_is .='</tr>';
                                        $qscore_is .='</tbody>';
                                        $qscore_is .='</table>';
                                        
                                    }else{
                                        $qscore_is = $row->R_ASSIGN_SCORE; 
                                    }
                                    
					if ($current_question != $row->R_QUIZ_ANSWER_ID) {
						$html .= '<tr class="normal_rows_prq">';
						
						if($row->R_QUIZ_TYPE != 9){
							$html .= '<td class="row_right_wong rsprgress_td">'.$ans_status.'</td>';
						}else{
							$html .= '<td class="row_right_wong rsprgress_td rsqdata pending_prq" rsmid="'.$module_id.'" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">Pending</td>';
						}
						
						if($link!=''){
							$html .= '<td class="row_right_wong slno table_row rsprgress_td"><a class="alink" href="'.$link.'">'.$i.'</a></td>';
						}else{
							$html .= '<td class="row_right_wong slno rsprgress_td">'.$i.'</td>';
						}
						
						
						if($row->R_QUIZ_TYPE != 9){
							
						$html .= '<td class="table_row_bold">' . $qscore_is . '</td>';
                        $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '">' . $mark_obtained . '</td>';					
						 $html .= '<td class="table_row_bold"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
							
							
						} else {
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
						}
					$html .= '</tr>';	
					   
					} else {
						$html .= '<tr class="rows_prq_highlight">';
											
						if($row->R_QUIZ_TYPE != 9){
							$html .= '<td class="row_right_wong rsprgress_td">'.$ans_status.'</td>';
						}else{
							$html .= '<td class="row_right_wong rsprgress_td rsqdata pending_prq" rsmid="'.$module_id.'" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">Pending</td>';
						}
						
						if($link!=''){
							$html .= '<td class="row_right_wong slno table_row rsprgress_td"><a class="alink" href="'.$link.'">'.$i.'</a></td>';
						}else{
							$html .= '<td class="row_right_wong slno rsprgress_td">'.$i.'</td>';
						}
						
						
						if($row->R_QUIZ_TYPE != 9){
							
						$html .= '<td class="table_row_bold">' . $qscore_is . '</td>';
                        $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '">' . $mark_obtained . '</td>';					
						 $html .= '<td class="table_row_bold"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
							
							
						} else {
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
							
							
						}
					$html .= '</tr>';		
					   
					}
				   
					$i++;
				}	
						$html .= '</table>';
			}else {
				$html = $stmt;
			}
				return $html;
		}else {
            return $question_ids['questionid'];
        }
	}
    function getQuestionsScoreSpecial($ataskfor = '', $amodule_id = '', $acurrent_question = '') {
		
        if (!$ataskfor) {
            $taskfor = $this->uri->segment(3);
        } else {
            $taskfor = $ataskfor;
        }
        if (!$amodule_id) {
            $module_id = $this->uri->segment(4);
        } else {
            $module_id = $amodule_id;
        }
        if (!$acurrent_question) {
            $current_question = $this->uri->segment(5);
        } else {
            $current_question = $acurrent_question;
        }

        $userid = getSessionVar('userid');

        //load module for getting modules_name
        $load_module = $this->quiz_module->load($module_id);
        if ($load_module) {
            $modules_name = 'Module Name: ' . $this->quiz_module->modules_name;
        } else {
            $modules_name = '';
        }
        $hostid = $this->uri->segment(2);
        $question_ids = $this->getQuestionIdsSpecial($ataskfor, $amodule_id);
        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];
            //var_dump($this->generateSLNo($questions));
            $slno = $this->generateSLNo($questions);
			//echo $questions;
			//echo '<br>';
            $stmt = 'select * from SP_QUESTION_SCORE(' . myString($questions) . ')';
            $query = $this->db->query($stmt);
			//print_r($query->result());exit;
            $html = '<table class="table">';
            $html .= '<tr>';
            $html .= '<td class="table_header" colspan="5">' . $modules_name . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';

            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
			$html .= '<td class="table_header">Description</td>';
            $html .= '</tr>';
            $prevquestion = getSessionVar('prevquestion');
            $i = 1;
            $question_array = explode(',', $questions);
            //ksort($question_array);
            //print_r($questions);
            if ($query) {
                $k = 0;
                foreach ($query->result() as $row) {
                    $answer = $this->getQuestionAnswer($module_id, $row->R_QUIZ_ANSWER_ID, $userid);
                    if ($answer) {
                        $answer_right = $answer['answer_right'];
                        $mark_obtained = $answer['mark_obtained'];
                        $answer_id = $answer['answer_id'];
                    } else {
                        $answer_right = '';
                        $mark_obtained = '';
                        $answer_id = '';
                    }
                    $ques_id = $row->R_QUIZ_ANSWER_ID;
                    $this->assign_score[] = array('id' => $row->R_QUIZ_ANSWER_ID, 'score' => $row->R_ASSIGN_SCORE);
                    $html .= '<tr>';
                    if ($current_question != $row->R_QUIZ_ANSWER_ID) {
						
                        switch ($answer_right) {
                            case '1':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_right" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="rsqdata  table_row_right_no_border" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = true;
                                break;

                            case '0':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_wrong" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    if ($slno[$k] != '') {
                                        $html .= '<td style="width:30px" class="rsqdata table_row_pending" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">P </td>';
                                    } else {
                                        $html .= '<td style="width:30px" class="rsqdata table_row_pending" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                    }
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = true;
                                break;

                            default:
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '"></td>';
                                } else {
                                    $html .= '<td style="width:30px" class="rsqdata table_row_noborder_left" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp; Q</td>';
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_noborder_left" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = false;
                                break;
                        }
                        if (!$answerd) {
                            for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                                if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
                                    $pos = $j;
                                    if ($pos != 0) {
                                        $prev_question = $question_array[$pos - 1];
                                    } else {
                                        $prev_question = '0'; //$question_array[0];
                                    }
                                    break;
                                } else {
                                    $pos = 0;
                                    $prev_question = '0';
                                }
                            }
                            $link = "javascript:fn_select_question_special($hostid, $taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question)";
                            //$html .= '<td class="table_row"><a class="alink" href="' . $link . '">' . $row->R_SL_NO . '</a></td>';
                            if ($row->R_QUIZ_TYPE != 9) {
                                $html .= '<td class="table_row"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            } else {
                                $html .= '<td class="table_row_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            }
                        } else {
                            //$html .= '<td class="table_row">' . $row->R_SL_NO . '</td>';
                            if ($row->R_QUIZ_TYPE != 9) {
                                $html .= '<td class="table_row">' . $slno[$k] . '</td>';
                            } else {
                                $html .= '<td class="table_row_noborder">' . $slno[$k] . '</td>';
                            }
                        }

                       // $html .= '<td class="table_row">' . $row->R_ASSIGN_DESCRIPTION . '</td>';
                        $html .= '<td class="table_row_bold" style="text-align:right;padding-right:2px;">' . $row->R_ASSIGN_SCORE . '</td>';
                        $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
						
						if ($row->R_QUIZ_TYPE != 9) {
						 $html .= '<td class="table_row_highlight"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
						}else{
							$html .= '<td class="table_row_highlight"></td>';
						}
						
                        $html .= '</tr>';
                        $i++;
                    } else {
						
                        switch ($answer_right) {
                            case '1':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_right_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="rsqdata table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = true;
                                break;

                            case '0':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_wrong_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    if ($slno[$k] != '') {
                                        $html .= '<td style="width:30px" class="rsqdata table_row_pending_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">P</td>';
                                    } else {
                                        $html .= '<td style="width:30px" class="rsqdata table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp;</td>';
                                    }
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = true;
                                break;

                            default:
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="rsqdata table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">&nbsp; RQ</td>';
                                }
                                /* } else {
                                  $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                  } */
                                $answerd = false;
                                break;
                        }
                        if (!$answerd) {
                            for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                                if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
                                    $pos = $j;
                                    if ($pos != 0) {
                                        $prev_question = $question_array[$pos - 1];
                                    } else {
                                        $prev_question = '0'; //$question_array[0];
                                    }
                                    break;
                                } else {
                                    $pos = 0;
                                    $prev_question = '0';
                                }
                            }
                            $link = "javascript:fn_select_question_special($hostid, $taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question)";
                            //$html .= '<td class="table_row_highlight"><a class="alink" href="' . $link . '">' . $row->R_SL_NO . '</a></td>';
                            //if ($slno[$k] != '') {
                            if ($row->R_QUIZ_TYPE != 9) {
                                $html .= '<td class="table_row_highlight"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            } else {
                                $html .= '<td class="table_row_highlight_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            }
                            /* } else {
                              $html .= '<td class="table_row_highlight_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                              } */
                        } else {
                            //$html .= '<td class="table_row_highlight">' . $row->R_SL_NO . '</td>';
                            if ($row->R_QUIZ_TYPE != 9) {
                                $html .= '<td class="table_row_highlight">' . $slno[$k] . '</td>';
                            } else {
                                $html .= '<td class="table_row_highlight_noborder">' . $slno[$k] . '</td>';
                            }
                        }

                       
                        $html .= '<td class="table_row_bold_highlight" style="text-align:right;padding-right:2px;">' . $row->R_ASSIGN_SCORE . '</td>';
                        $html .= '<td class="table_row_bold_highlight" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
						if ($row->R_QUIZ_TYPE != 9) {
						 $html .= '<td class="table_row_highlight"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
						}else{
							$html .= '<td class="table_row_highlight"></td>';
						}
                        $html .= '</tr>';
                        $i++;
                    }
                    $k++;
                }
                $html .= '</table>';
            } else {
                $html = $stmt;
            }
            return $html;
        } else {
            return $question_ids['questionid'];
        }
    }

    function getQuizId($all = '', $special = '') {
        $hostid = $this->uri->segment(2);
        if (!$special) {
			
            //$all = '', $ataskfor = '', $amodule_id = '', $hostid = 0
            $question_ids = $this->getQuestionIds($all, '', '', $hostid);
        } else {
            $question_ids = $this->getQuestionIdsSpecial($all, '', '', $hostid);
        }
        if ($question_ids['result'] == 't') {
            $quizs = explode(',', $question_ids['questionid']);
            //asort($quizs);
            if ($quizs) {
                $this->quiz_ids = $quizs;
                //asort($this->quiz_ids);
            } else {
                $this->quiz_ids = '';
            }
        } else {
            $this->quiz_ids = '';
        }
        //var_dump($this->quiz_ids);
    }

    function getQuizCategoryId($aQuestionId = '') {
        $load = $this->question_answer->load($aQuestionId);
        if ($load) {
            $return = $this->question_answer->quiz_category_id;
        } else {
            $return = '';
        }
        return $return;
    }

    function getQuestion($aid = '') {
        $question_get = $this->question_answer->load($aid); //getQuestionAsView($aid);
        if ($question_get) {
            $question = $this->question_answer->question;
        } else {
            $question = '';
        }
        return $question;
    }

    function QuestionAnswered($all = '', $special = '') {
        /* $module = getSessionVar('module');
          if (!$module) {
          $module_id = $this->uri->segment(3);
          } else {
          $module_id = $module;
          } */
        $module_id = $this->uri->segment(4);
        $userid = getSessionVar('userid');
        $stmt = 'select * from SP_GET_ANSWERED_QUESTION(' . $userid . ',' . $module_id . ')';
        $query = $this->db->query($stmt);
        $questions = '';
        if ($query) {
            $rcount = $query->row();
            if ($rcount) {
                foreach ($query->result() as $row) {
                    if ($questions == '') {
                        $questions = $row->R_QUESTION_ID;
                    } else {
                        $questions .= ',' . $row->R_QUESTION_ID;
                    }
                }
            } else {
                $questions = '';
            }
        } else {
            $questions = '';
        }
        $this->getQuizId($all, $special);
        $newquiz = array();
		
        if ($questions) {
		
            $allQuestions = explode(',', $questions);
            $uquestion = array_unique($allQuestions);
            foreach ($uquestion as $question) {
                /* if(($key = array_search($question, $this->quiz_ids)) !== false) {
                  unset($this->quiz_ids[$key]);
                  } */
                foreach (array_keys($this->quiz_ids, $question, true) as $key) {
                    unset($this->quiz_ids[$key]);
                }
            }
        }
		/* echo 123;
		print_r($this->quiz_ids);
		exit; */
		if($this->quiz_ids){
			$newquiz = array_values($this->quiz_ids);
		}
        
        $this->quiz_ids = @$newquiz;
        //asort($this->quiz_ids);
    }

    function displayToolSpecial($taskfor, $module_id, $question_id) {
		
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        //$this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $alink = "javascript:fn_skip_video_special($taskfor, $module_id, $question_id, 0)";
        $skip = '<td><a href="' . $alink . '" class="button_no_img">Skip</a></td>';
        $alink = '/special-recent-task/' . $taskfor;
        $back = '<td><a href="' . $alink . '" class="back_button">Back</a></td>';
$this->mysmarty->assign('top_tutorial', '');
        $this->mysmarty->assign('preview_link', $skip . $back);
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('question', '');
        $this->mysmarty->assign('quiz_score', '');
        $this->mysmarty->assign('instruction', '1');
    }

    function displayToolNew($taskfor, $module_id, $question_id, $all = '') {
		
        $host = $this->uri->segment(2);
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        //$this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $alink = "javascript:fn_skip_video($host, $taskfor, $module_id, $question_id, 0)";
        $skip = '<td><a href="' . $alink . '" class="button_no_img">Skip</a></td>';
        if (!$all) {
            $alink = '/my-recent-task/' . $taskfor;
            $back = '<td><a href="' . $alink . '" class="back_button">Back</a></td>';
        } else {
            $alink = '/all-task/' . $taskfor;
            $back = '<td><a href="' . $alink . '" class="back_button">Back</a></td>';
        }
		$this->mysmarty->assign('top_tutorial', '');
		$this->mysmarty->assign('qcalculates', '');
		$this->mysmarty->assign('qtimes', '');
        $this->mysmarty->assign('preview_link', $skip . $back);
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('question', '');
        $this->mysmarty->assign('quiz_score', '');
        $this->mysmarty->assign('instruction', '1');
    }

    function displayTool($taskfor, $module_id, $question_id) {
        $html = '<form method="post" name="f_answer_video" class="text" id="f_answer_video" enctype="" style="margin: 0px">';
        $html .= '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<li class="tool_logout">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<a href="javascript:fn_Logout()" class="main_logout"><span>Logout</span></a>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</li>' . $this->config->item('CRLF');

        $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<div id="canvas_div">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="rightside" class="fluid"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="footer_holder_exam" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="fluid footer_buttons">' . $this->config->item('CRLF');

        $alink = "javascript:fn_skip_video($taskfor, $module_id, $question_id, 0)";
        $skip = '<a href="' . $alink . '" class="button_no_img">Skip</a>';
        $alink = '/my-task/' . $taskfor;
        $back = '<a href="' . $alink . '" class="back_button">Back</a>';
        /*
          $html .= $this->config->item('four_tab') . '<table class="quiz_answer">' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '<tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $back . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $skip . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '</tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('four_tab') . '</table>' . $this->config->item('CRLF');
         */
        $html .= $this->config->item('six_tab') . $back . $this->config->item('CRLF');
        $html .= $this->config->item('six_tab') . $skip . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div class="validation-error" id="td_error" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div id="qstudy_footer" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<footer id="footer" class="fluid"> </footer>' . $this->config->item('CRLF');
        $html .= '</div>';
        $html .= '</form>';
        return $html;
    }

    function showVideo($all = '') {
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $question_id = $this->uri->segment(4);
        $module_files = $this->quiz_module_files->load($module_id);
        if ($module_files) {
            $no_of_files = $this->quiz_module_files->no_of_files;
            $video_files = $this->quiz_module_files->video_files;
            $image_files = $this->quiz_module_files->image_files;
            if ($video_files) {
                $this->mysmarty->assign('quiz_id', $question_id);
                $quiz_id = $this->getQuizCategoryId($question_id);
                $this->mysmarty->assign('script', ''); //$this->makeCSS($quiz_id));
                $this->mysmarty->assign('equation_style', '');
                $this->mysmarty->assign('equation_view', '');
                $this->mysmarty->assign('incl_form', 'incl_student_video.tpl');
                $this->mysmarty->assign('incl_form_answer', 'incl_student_image.tpl');
                /* $alink = "javascript:fn_skip_video($taskfor, $module_id, $question_id, 0)";
                  $skip = '<a href="' . $alink . '" class="button_no_img" style="margin:140px 0 0 90px;z-index:0">Skip</a>';
                  $this->mysmarty->assign('skip', $skip);
                  $alink = '/my-task/' . $taskfor;
                  $back = '<a href="' . $alink . '" class="back_button" style="margin:140px 0 0 90px;z-index:0">Back</a>';
                  $this->mysmarty->assign('back', $back); */
                $this->mysmarty->assign('videos', $this->quiz_module_files->getVideos($module_id));
                $this->mysmarty->assign('images', $this->quiz_module_files->getImages($module_id));
                $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
                $this->mysmarty->assign('quiz_category', '');
                return $this->displayTool($taskfor, $module_id, $question_id);
            } else {
                setSessionVar('skip', 't');
                return $this->showExamBoardTools($all);
            }
        } else {
            setSessionVar('skip', 't');
            return $this->showExamBoardTools($all);
        }
    }

    function showVideonew($all = '') {
		
        $taskfor = $this->uri->segment(3);
        $module_id = $this->uri->segment(4);
        $question_id = $this->uri->segment(5);
        $module_files = $this->quiz_module_files->load($module_id);
        if ($module_files) {
			
            $no_of_files = $this->quiz_module_files->no_of_files;
            $video_files = $this->quiz_module_files->video_files;
            $image_files = $this->quiz_module_files->image_files;
            if (($video_files) || ($image_files)) {
                $this->mysmarty->assign('quiz_id', $question_id);
                $quiz_id = $this->getQuizCategoryId($question_id);
                $this->mysmarty->assign('script', ''); //$this->makeCSS($quiz_id));
                $this->mysmarty->assign('equation_style', '');
                $this->mysmarty->assign('equation_view', '');
                if ($video_files) {
                    $this->mysmarty->assign('incl_form', 'incl_student_video.tpl');
                } else {
                    $this->mysmarty->assign('incl_form', '');
                }
                if ($image_files) {
                    $this->mysmarty->assign('incl_form_answer', 'incl_student_image.tpl');
                } else {
                    $this->mysmarty->assign('incl_form_answer', '');
                }
                $this->mysmarty->assign('videos', $this->quiz_module_files->getVideos($module_id));
                $this->mysmarty->assign('images', $this->quiz_module_files->getImages($module_id));
                $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
                $this->mysmarty->assign('quiz_category', '');
                return $this->displayToolNew($taskfor, $module_id, $question_id, $all);
            } else {
                setSessionVar('skip', 't');
                return $this->showExamBoardToolsNew($all);
            }
        } else {
            setSessionVar('skip', 't');
            return $this->showExamBoardToolsNew($all);
        }
    }

    function showExamBoardToolsNew($all = '') {
		
		$html = '';
		
		if($all!=''){
           $exam_is = 1;
		}else{
            $exam_is = 0;
		}
		
        $skip = getSessionVar('skip');
		$this->mysmarty->assign('top_tutorial', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_score', '');
        if (!$skip) {
            return $this->showVideonew($all);
        } else {
            $hostid = $this->uri->segment(2);
            $taskfor = $this->uri->segment(3);
            $module_id = $this->uri->segment(4);
            $question_id = $this->uri->segment(5);
            $prev_question = $this->uri->segment(5);

            $all = getSessionVar('alltasks');
            $this->QuestionAnswered($all);
            $this->mysmarty->assign('quiz_id', $question_id);
            $this->mysmarty->assign('answer', '');
            $this->mysmarty->assign('workout', '');
            $this->mysmarty->assign('workout_draw', '');
            $quiz_id = $this->getQuizCategoryId($question_id);
			
			
			
            $userid = getSessionVar('userid');
            $quiz = $this->quiz_category->load($quiz_id);
            if ($quiz) {
                $quiz_name = $this->quiz_category->quiz_name;
            } else {
                $quiz_name = '';
            }
            $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
            if (count($this->quiz_ids) > 1) {
				
                $is_answered = $this->questionIsAnswered($question_id, $module_id);
				/* if($is_answered){
					echo 'anserd';
				}else{
					echo 'not answrd';
				}
				echo $this->quiz_ids[1].' of'.$this->quiz_ids[0];
				exit; */
				 /* new condition for jumping question */
				if($this->quiz_ids[0]==$question_id){
					$next_ques = $this->quiz_ids[1];
				}else{
					$next_ques = $this->quiz_ids[0];
				}
				
				
                if (!$all) {
                    if (!$is_answered) {
                        if (count($this->quiz_ids) > 1) {
                            //$next_link = 'javascript:fn_next_question(' . getSessionVar('exam_link') . ',' . $next_ques . ',' . $prev_question . ')';
                            $next_link = 'javascript:fn_next_question(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',0)';
                            $elapse_link = '               fn_next_question(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',1);';
$next = '<a href="' . $next_link . '" class="next_button"><span>Next</span></a>';
$next_submit = '<a href="' . $next_link . '" class="next_button"><span>Submit</span></a>';

                        } else {
                            setSessionVar('allprev', '');
                            $next_link = 'javascript:fn_complete_question(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                            $elapse_link = '               javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
 $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
 $next_submit = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
 
                        }
                    } else {
                        $next = '';
                        $elapse_link = '';
                    }
                } else {
                    if (count($this->quiz_ids) > 1) {
                        $next_link = 'javascript:fn_next_question(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',0)';
                        $elapse_link = '               javascript:fn_next_question(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',1);';
                        $next = '<a href="' . $next_link . '" class="next_button"><span>Next</span></a>';
						
						/*changes*/
						 // rhm
$next_submit = '<a class="finish" href="'.$next_link.'"><span>Submit</span></a>';//rhm	
$pre_links = '/all-task/' . $taskfor ;

						/*changes*/
						
                    } else {
                        setSessionVar('allprev', '');
                        $next_link = 'javascript:fn_complete_question(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                        $elapse_link = '           javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
                        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
						/*changes*/
						 // rhm
$next_submit = '<a class="finish" href="'.$next_link.'"><span>Submit</span></a>';//rhm	
$pre_links = '/all-task/' . $taskfor ;
                    

						/*changes*/
                    }
                }
            } else {
                setSessionVar('allprev', '');
                $is_answered = $this->questionIsAnswered($question_id, $module_id);
                if (!$is_answered) {
                    $next_link = 'javascript:fn_complete_question(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                    $elapse_link = '           javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
                } else {
                    $next_link = '/answer-complete/' . $hostid . '/' . $taskfor . '/' . $module_id;
                    $elapse_link = '/answer-complete/' . $hostid . '/' . $taskfor . '/' . $module_id;
                }

                $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
				
				/* changes */
				// rhm
					$next_submit = '<a class="finish" href="'.$next_link.'"><span>Submit</span></a>';//rhm	
				
						$pre_links = '/all-task/' . $taskfor;

				/*changes*/
				
            }
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }

            //set time elapse for the question
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }
            $this->DisplayAnswerBoard($quiz_id);
            $this->mysmarty->assign('question', $this->getQuestion($question_id));

            //set timer in the question
			
			/*
            if ($time_elapse) {
                $html .= '<script>' . $this->config->item('CRLF');
                $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
                $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
                $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
                $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
                $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
                $html .= '  })' . $this->config->item('CRLF');
                
                $html .= '  .addListener(' . $this->config->item('CRLF');
                $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
                $html .= '          if (value == 0){' . $this->config->item('CRLF');
                $html .= $elapse_link;
                $html .= '          }' . $this->config->item('CRLF');
                $html .= '      }' . $this->config->item('CRLF');
                $html .= '  );' . $this->config->item('CRLF');
                $html .= '</script>' . $this->config->item('CRLF');
            } else {
                $html = '';
            }
			*/
			
			$qinfo = $this->question_answer->getallinfo_question($question_id);

			$rqtimes = $qinfo->time_elapse;
            $show_clock = 0;
            if($rqtimes!=''){
                $show_clock = 1;
            }
			$rqtimes_formated = gmdate("H:i:s", $rqtimes);
			$this->mysmarty->assign('qtimes',$rqtimes);
            //$this->mysmarty->assign('show_clock',$show_clock);
			$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
		
    if($exam_is==1){
      $top_tutorial = '<a class="finish" href="/all-task/' . $taskfor .'"><span>Index </span></a>';  
    }else{
       $top_tutorial = '<a class="finish rsdontgoback" href="/my-recent-task/' . $taskfor .'"><span>Index </span></a>'; 
    }                    

	$top_prev = '<a class="back_button rsdontgoback_backbtns" href="#">Back</a>';//rhm
	$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
			
			
			
/*changes*/
			$this->mysmarty->assign('next_submit',  $next_submit ); //rhm submit btn
           $this->mysmarty->assign('top_tutorial',$top_tutorial ); //rhm top_tutorial btn
           $this->mysmarty->assign('top_prev',  $top_prev ); //rhm top_tutorial btn
           $this->mysmarty->assign('top_next',  $top_next ); //rhm top_tutorial btn
/*changes*/
            $this->mysmarty->assign('question_no', '');
            $this->mysmarty->assign('save_link', '');
            $this->mysmarty->assign('cancel_link', '');
            $this->mysmarty->assign('preview_link', '<td>' . $next . '</td>');
            $this->mysmarty->assign('section_title', '');
            $this->mysmarty->assign('timerelapse', $html);
            $this->mysmarty->assign('grade_year', '');
            $this->mysmarty->assign('subject_name', '');
            $this->mysmarty->assign('chapter', '');
            $this->mysmarty->assign('score', '');
            $this->mysmarty->assign('time', $time_elapse);
            $this->mysmarty->assign('script', '');
            //$this->mysmarty->assign('quiz_score', $this->getQuestionsScore());
            $this->mysmarty->assign('quiz_score', $this->getQuestionsScore_mdfy());
            $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        }
    }
	
	function get_next_rs_questionofmodule($array, $key) {
		
	   $currentKey = key($array);
	   $next_rs_question = 0;
	   while ($currentKey !== null && $currentKey != $key) {
		   next($array);
		   $currentKey = key($array);
	   }
	  $next_rs_question = next($array);
	  
	   if($next_rs_question==''){
		   $next_rs_question = 0;
	   }
	   return $next_rs_question;
	}
	
    function opensource_question_details_by_admin($all = '') {		
        $skip = getSessionVar('skip');
		$quiz_list_completed = array();
		$this->mysmarty->assign('top_tutorial', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('timerelapse','');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_score', '');
        if (!$skip) {
            return $this->showVideonew($all);
        } else {
            $hostid = $this->uri->segment(2);
            $taskfor = $this->uri->segment(3);
            $module_id = $this->uri->segment(4);
            $question_id = $this->uri->segment(5);
            $prev_question = $this->uri->segment(5);

            $all = getSessionVar('alltasks');
           // $this->QuestionAnswered($all);
            $this->mysmarty->assign('quiz_id', $question_id);
            $this->mysmarty->assign('answer', '');
            $this->mysmarty->assign('workout', '');
            $this->mysmarty->assign('workout_draw', '');
            $quiz_id = $this->getQuizCategoryId($question_id);

            $userid = getSessionVar('userid');
            $quiz = $this->quiz_category->load($quiz_id);
            if ($quiz) {
                $quiz_name = $this->quiz_category->quiz_name;
            } else {
                $quiz_name = '';
            }
            $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
            
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }

            //set time elapse for the question
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }
            $this->DisplayAnswerBoard($quiz_id);
            $this->mysmarty->assign('question', $this->getQuestion($question_id));

			$qinfo = $this->question_answer->getallinfo_question($question_id);
			$rqtimes = $qinfo->time_elapse;
			$rqtimes_formated = gmdate("H:i:s", $rqtimes);
			$this->mysmarty->assign('qtimes',$rqtimes);
			$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
			
			$stmter = $this->db->query("select QUESTIONS from QUIZ_MODULE where ID='".$module_id."'")->row();
			$quiz_list_all = explode(',',$stmter->QUESTIONS);
			
			$keysC = array_search($question_id,$quiz_list_all);
			$next_q = $this->get_next_rs_questionofmodule($quiz_list_all, $keysC);
			if($next_q!=0){
				$next_link = '/opensource-module-question-details-admin/'.$hostid.'/'.$taskfor.'/'.$module_id.'/'.$next_q.'/0';
				$top_next = '<a class="next_button" href="'.$next_link.'">Next</a>';
			}else{
				$next_link = '/dictionary-opensource-modules';
				$top_next = '<a class="finish" href="'.$next_link.'">Finish</a>';
			}
			

			$top_tutorial = '<a class="finish" href="/dictionary-opensource-modules"><span>Index </span></a>'; 
			
			$next_submit = '<a class="finish" href="javascript:fn_check_opensource_q_answer()"><span>Submit </span></a>';  
                   
	
        
        
	$top_prev = '<a class="back_button rsdontgoback_backbtns" href="javascript: history.go(-1)">Back</a>';//rhm
	//rhm
			
			
			
/*changes*/
			$this->mysmarty->assign('next_submit',  '' ); //rhm submit btn $next_submit
           $this->mysmarty->assign('top_tutorial',$top_tutorial ); //rhm top_tutorial btn
           $this->mysmarty->assign('top_prev',  $top_prev ); //rhm top_tutorial btn
           $this->mysmarty->assign('top_next',  $top_next ); //rhm top_tutorial btn
/*changes*/
            $this->mysmarty->assign('question_no', '');
            $this->mysmarty->assign('save_link', '');
            $this->mysmarty->assign('cancel_link', '');
            $this->mysmarty->assign('preview_link', '<td>' . @$next . '</td>');
            $this->mysmarty->assign('section_title', '');
            $this->mysmarty->assign('timerelapse', @$html);
            $this->mysmarty->assign('grade_year', '');
            $this->mysmarty->assign('subject_name', '');
            $this->mysmarty->assign('chapter', '');
            $this->mysmarty->assign('score', '');
            $this->mysmarty->assign('time', $time_elapse);
            $this->mysmarty->assign('script', '');
            //$this->mysmarty->assign('quiz_score', $this->getQuestionsScore());
            $this->mysmarty->assign('quiz_score', $this->getQuestionsScore_mdfy());
            $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        }
    }
	
    function showExamBoardToolsNew_opensource_module($all = '') {
		
        $skip = getSessionVar('skip');
		$this->mysmarty->assign('top_tutorial', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_score', '');
        if (!$skip) {
            return $this->showVideonew($all);
        } else {
            $hostid = $this->uri->segment(2);
            $taskfor = $this->uri->segment(3);
            $module_id = $this->uri->segment(4);
            $question_id = $this->uri->segment(5);
            $prev_question = $this->uri->segment(6);

            $all = getSessionVar('alltasks');
			
            //$this->QuestionAnswered($all);
            $this->mysmarty->assign('quiz_id', $question_id);
            $this->mysmarty->assign('answer', '');
            $this->mysmarty->assign('workout', '');
            $this->mysmarty->assign('workout_draw', '');
            $quiz_id = $this->getQuizCategoryId($question_id);

            $userid = getSessionVar('userid');
            $quiz = $this->quiz_category->load($quiz_id);
            if ($quiz) {
                $quiz_name = $this->quiz_category->quiz_name;
            } else {
                $quiz_name = '';
            }
            $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
            
			/* next question link */
			
			$stmter = $this->db->query("select QUESTIONS from QUIZ_MODULE where ID='".$module_id."'")->row();
			$quiz_list_all = explode(',',$stmter->QUESTIONS);
			
			$keysC = array_search($question_id,$quiz_list_all);
			$next_q = $this->get_next_rs_questionofmodule($quiz_list_all, $keysC);
			if($next_q!=0){
				$next_link = '/all-tasks-opensource/'.$hostid.'/'.$taskfor.'/'.$module_id.'/'.$next_q.'/0';
				$next_submit = '<a class="finish" href="javascript:fn_next_question_op_modules('.$hostid.','.$taskfor.','.$module_id.','.$next_q.','.$question_id.',0)" class="submit">Submit</a>';
			}else{
				$next_submit = '<a class="finish" href="javascript:fn_complete_question_op_modules('.$hostid.','.$taskfor.','.$module_id.','.$question_id.','.$question_id.',0)" class="submit">Submit</a>';	
			}
			/* next question link */
	
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }

            //set time elapse for the question
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }
			
            $this->DisplayAnswerBoard($quiz_id);
            $this->mysmarty->assign('question', $this->getQuestion($question_id));

            //set timer in the question
            if ($time_elapse) {
                $html = '<script>' . $this->config->item('CRLF');
                $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
                $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
                $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
                $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
                $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
                $html .= '  })' . $this->config->item('CRLF');
                //addlistner
                $html .= '  .addListener(' . $this->config->item('CRLF');
                $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
                $html .= '          if (value == 0){' . $this->config->item('CRLF');
                $html .= $elapse_link;
                $html .= '          }' . $this->config->item('CRLF');
                $html .= '      }' . $this->config->item('CRLF');
                $html .= '  );' . $this->config->item('CRLF');
                $html .= '</script>' . $this->config->item('CRLF');
            } else {
                $html = '';
            }
			
			
			$qinfo = $this->question_answer->getallinfo_question($question_id);
			$rqtimes = $qinfo->time_elapse;
			$rqtimes_formated = gmdate("H:i:s", $rqtimes);
			$this->mysmarty->assign('qtimes',$rqtimes);
			$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
		
    
      $top_tutorial = '<a class="finish" href="/all-task-opensouce/16"><span>Index </span></a>';  
	$top_prev = '<a class="back_button rsdontgoback_backbtns" href="javascript: history.go(-1)">Back</a>';//rhm
	$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';
			$this->mysmarty->assign('next_submit',  $next_submit ); //rhm submit btn
           $this->mysmarty->assign('top_tutorial',$top_tutorial ); //rhm top_tutorial btn
           $this->mysmarty->assign('top_prev',  $top_prev ); //rhm top_tutorial btn
           $this->mysmarty->assign('top_next',  $top_next ); //rhm top_tutorial btn
/*changes*/
            $this->mysmarty->assign('question_no', '');
            $this->mysmarty->assign('save_link', '');
            $this->mysmarty->assign('cancel_link', '');
            $this->mysmarty->assign('preview_link', '<td>' . @$next . '</td>');
            $this->mysmarty->assign('section_title', '');
            $this->mysmarty->assign('timerelapse', @$html);
            $this->mysmarty->assign('grade_year', '');
            $this->mysmarty->assign('subject_name', '');
            $this->mysmarty->assign('chapter', '');
            $this->mysmarty->assign('score', '');
            $this->mysmarty->assign('time', $time_elapse);
            $this->mysmarty->assign('script', '');
            //$this->mysmarty->assign('quiz_score', $this->getQuestionsScore());
            $this->mysmarty->assign('quiz_score', $this->getQuestionsScore_mdfy());
            $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        }
    }

    function showVideospecial($all = '') {
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $question_id = $this->uri->segment(4);
        $module_files = $this->quiz_module_files->load($module_id);
        if ($module_files) {
            $no_of_files = $this->quiz_module_files->no_of_files;
            $video_files = $this->quiz_module_files->video_files;
            $image_files = $this->quiz_module_files->image_files;
            if (($video_files) || ($image_files)) {
                $this->mysmarty->assign('quiz_id', $question_id);
                $quiz_id = $this->getQuizCategoryId($question_id);
                $this->mysmarty->assign('script', ''); //$this->makeCSS($quiz_id));
                $this->mysmarty->assign('equation_style', '');
                $this->mysmarty->assign('equation_view', '');
                if ($video_files) {
                    $this->mysmarty->assign('incl_form', 'incl_student_video.tpl');
                } else {
                    $this->mysmarty->assign('incl_form', '');
                }
                if ($image_files) {
                    $this->mysmarty->assign('incl_form_answer', 'incl_student_image.tpl');
                } else {
                    $this->mysmarty->assign('incl_form_answer', '');
                }
                /* $alink = "javascript:fn_skip_video($taskfor, $module_id, $question_id, 0)";
                  $skip = '<a href="' . $alink . '" class="button_no_img" style="margin:140px 0 0 90px;z-index:0">Skip</a>';
                  $this->mysmarty->assign('skip', $skip);
                  $alink = '/my-task/' . $taskfor;
                  $back = '<a href="' . $alink . '" class="back_button" style="margin:140px 0 0 90px;z-index:0">Back</a>';
                  $this->mysmarty->assign('back', $back); */
                $this->mysmarty->assign('videos', $this->quiz_module_files->getVideos($module_id));
                $this->mysmarty->assign('images', $this->quiz_module_files->getImages($module_id));
                $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
                $this->mysmarty->assign('quiz_category', '');
                return $this->displayToolSpecial($taskfor, $module_id, $question_id);
            } else {
                setSessionVar('skip', 't');
                return $this->showExamBoardToolsSpecial($all);
            }
        } else {
            setSessionVar('skip', 't');
            return $this->showExamBoardToolsSpecial($all);
        }
    }

    function showExamBoardToolsSpecial($all = '') {
		$this->mysmarty->assign('top_tutorial', '');//new add
		$this->mysmarty->assign('next_submit', '');//new add
        $skip = getSessionVar('skip');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_score', '');
        if (!$skip) {
            return $this->showVideospecial();
        } else {
            $hostid = $this->uri->segment(2);
            $taskfor = $this->uri->segment(3);
            $module_id = $this->uri->segment(4);
            $question_id = $this->uri->segment(5);
            $prev_question = $this->uri->segment(5);
			
			
						/* rs new changes */
				$smpts = $this->db->query("SELECT * FROM QUIZ_MODULE WHERE ID='".$module_id."'");
				$row = $smpts->row();
				$current_time = new DateTime(); 
				$current_time = $current_time->format('H:i:s');
				$curdate = date('Y-m-d');
				$userid = getSessionVar('userid');
				if($row->SPECIFIC_DATES_FROM==$row->SPECIFIC_DATES_TO){
					if($row->NON_SPECIFIC_TIME!=''){
						$smpts_times = $this->db->query("SELECT * FROM RSEXAM_TIME WHERE MODULE_ID='".$module_id."' AND USER_ID='".$userid."' AND EXAM_DATE='".$curdate."'");
						$rowtime = $smpts_times->row();
						if($current_time>$rowtime->EXAM_TIMES){
							$sptime = 0;
							echo '<script>
					alert("your assignment time is over");
					window.location.href="/special-recent-task/'.$taskfor.'";
					</script>';
						}else{
							$sptime = 1;
						}
					}else{
						if(($current_time>=$row->FROM_TIME) && ($current_time<=$row->TO_TIME)){
							$sptime = 1;
						}else{
							if($current_time<$row->FROM_TIME){
								$sptime = 0;
								$isexpired = 0;
								echo '<script>
alert("You can not reach to your assignment time");
window.location.href="/special-recent-task/'.$taskfor.'";
</script>';
							} else {
								$sptime = 0;
								$isexpired = 1;
								echo '<script>
								alert("your assignment time is expired");
								window.location.href="/special-recent-task/'.$taskfor.'";
								</script>';
							}
						}
					}
					
				}else{
					//new dif time 
						if($curdate<$row->SPECIFIC_DATES_FROM){
								$sptime = 0;
								$isexpired = 0;
								echo '<script>
alert("You can not reach to your Exam Date");
window.location.href="/special-recent-task/'.$taskfor.'";
</script>';
							} 
							
							if($curdate>$row->SPECIFIC_DATES_TO){
								$sptime = 0;
								$isexpired = 0;
								echo '<script>
alert("your exam Date is over");
window.location.href="/special-recent-task/'.$taskfor.'";
</script>';
							} 
					//new dif time 
				} 
			/* rs new changes */
			
			
			
            $all = getSessionVar('alltasks');
            $this->QuestionAnswered($all, 't');
            $this->mysmarty->assign('quiz_id', $question_id);
            $this->mysmarty->assign('answer', '');
            $this->mysmarty->assign('workout', '');
            $this->mysmarty->assign('workout_draw', '');
            $quiz_id = $this->getQuizCategoryId($question_id);

            $userid = getSessionVar('userid');
            $quiz = $this->quiz_category->load($quiz_id);
            if ($quiz) {
                $quiz_name = $this->quiz_category->quiz_name;
            } else {
                $quiz_name = '';
            }
            $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
            if (count($this->quiz_ids) > 1) {
                $is_answered = $this->questionIsAnswered($question_id, $module_id);
                $next_ques = $this->quiz_ids[1];
                if (!$all) {
                    if (!$is_answered) {
                        if (count($this->quiz_ids) > 1) {
                            //$next_link = 'javascript:fn_next_question(' . getSessionVar('exam_link') . ',' . $next_ques . ',' . $prev_question . ')';
                            $next_link = 'javascript:fn_next_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',0)';
                            $elapse_link = '               fn_next_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',1);';
                            $next = '<a href="' . $next_link . '" class="next_button"><span>Next</span></a>';

	$next_submit = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
			
							
							
                        } else {
                            setSessionVar('allprev', '');
                            $next_link = 'javascript:fn_complete_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                            $elapse_link = '               javascript:fn_complete_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
                            $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
							
							
							$next_submit = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
			
							
                        }
                    } else {
                        $next = '';
                        $elapse_link = '';
                    }
                } else {
                    if (count($this->quiz_ids) > 1) {
                        $next_link = 'javascript:fn_next_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',0)';
                        $elapse_link = '               javascript:fn_next_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',1);';
              
			 $next = '<a href="' . $next_link . '" class="next_button"><span>Next</span></a>';
			  $next_submit = '<a href="' . $next_link . '" class="next_button"><span>Submit</span></a>';
		
                    } else {
                        setSessionVar('allprev', '');
                        $next_link = 'javascript:fn_complete_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                        $elapse_link = '           javascript:fn_complete_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
                       
					   $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
			$next_submit = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
			
                    }
                }
            } else {
                setSessionVar('allprev', '');
                $is_answered = $this->questionIsAnswered($question_id, $module_id);
                if (!$is_answered) {
                    $next_link = 'javascript:fn_complete_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                    $elapse_link = '           javascript:fn_complete_question_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
                } else {
                    $next_link = '/answer-complete-special/' . $hostid . '/' . $taskfor . '/' . $module_id;
                    $elapse_link = '/answer-complete-special/' . $hostid . '/' . $taskfor . '/' . $module_id;
                }

               
			   $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
			   $next_submit = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';	
				
				
				
            }
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }

            //set time elapse for the question
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }
            $this->DisplayAnswerBoard($quiz_id);
            $this->mysmarty->assign('question', $this->getQuestion($question_id));

            //set timer in the question
            if ($time_elapse) {
                $html = '<script>' . $this->config->item('CRLF');
                $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
                $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
                $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
                $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
                $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
                $html .= '  })' . $this->config->item('CRLF');
                //addlistner
                $html .= '  .addListener(' . $this->config->item('CRLF');
                $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
                $html .= '          if (value == 0){' . $this->config->item('CRLF');
                $html .= $elapse_link;
                $html .= '          }' . $this->config->item('CRLF');
                $html .= '      }' . $this->config->item('CRLF');
                $html .= '  );' . $this->config->item('CRLF');
                $html .= '</script>' . $this->config->item('CRLF');
            } else {
                $html = '';
            }

			
			
			$qinfo = $this->question_answer->getallinfo_question($question_id);
			$rqtimes = $qinfo->time_elapse;
			$rqtimes_formated = gmdate("H:i:s", $rqtimes);
			$this->mysmarty->assign('qtimes',$rqtimes);
			$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
			
			$top_prev = '<a class="back_button rsdontgoback_backbtns" href="#">Back</a>';//rhm
			$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
			$top_tutorial = '<a class="finish rsdontgoback" href="/special-recent-task/' . $taskfor.'"><span>Index</span></a>';
			
/*changes*/
			$this->mysmarty->assign('next_submit',@$next_submit ); //rhm submit btn
			$this->mysmarty->assign('top_tutorial',@$top_tutorial ); //rhm top_tutorial btn
			$this->mysmarty->assign('top_prev',@$top_prev ); //rhm top_tutorial btn
			$this->mysmarty->assign('top_next',@$top_next ); //rhm top_tutorial btn
/*changes*/
			
			
			
            $this->mysmarty->assign('question_no', '');
            $this->mysmarty->assign('save_link', '');
            $this->mysmarty->assign('cancel_link', '');
            $this->mysmarty->assign('preview_link', '<td>' . $next . '</td>');
            $this->mysmarty->assign('section_title', '');
            $this->mysmarty->assign('timerelapse', $html);
            $this->mysmarty->assign('grade_year', '');
            $this->mysmarty->assign('subject_name', '');
            $this->mysmarty->assign('chapter', '');
            $this->mysmarty->assign('score', '');
            $this->mysmarty->assign('time', $time_elapse);
            $this->mysmarty->assign('script', '');
           // $this->mysmarty->assign('quiz_score', $this->getQuestionsScoreSpecial());
            $this->mysmarty->assign('quiz_score', $this->getQuestionsScoreSpecial_modify());
            $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        }
    }

    function showExamBoardTools($all = '') {
        $skip = getSessionVar('skip');
        if (!$skip) {
            return $this->showVideo();
        } else {
            //$time = date('H:i:s', time());
            //var_dump($all);
            $taskfor = $this->uri->segment(2);
            $module_id = $this->uri->segment(3);
            $question_id = $this->uri->segment(4);
            $prev_question = $this->uri->segment(4);
            $all = getSessionVar('alltasks');
            $this->QuestionAnswered($all);
            $this->mysmarty->assign('quiz_id', $question_id);
            $this->mysmarty->assign('answer', '');
            $this->mysmarty->assign('workout', '');
            $this->mysmarty->assign('workout_draw', '');
            $quiz_id = $this->getQuizCategoryId($question_id);

            $userid = getSessionVar('userid');
            //get the whiteboard ids from quiz_category table as per selected quiz
            $quiz = $this->quiz_category->load($quiz_id);
            if ($quiz) {
                $whiteboard_ids = $this->quiz_category->whiteboard;
                $quiz_name = $this->quiz_category->quiz_name;
            } else {
                $whiteboard_ids = '';
                $quiz_name = '';
            }
            $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
            //$next_link = getSessionVar('exam_link');// . '|' . $this->quiz_ids;   
            //if (!$selQuestion) {
            //array_splice($this->quiz_ids, 0, 1);
            //print_r($this->quiz_ids);
            if (count($this->quiz_ids) > 1) {
                $is_answered = $this->questionIsAnswered($question_id, $module_id);
                $next_ques = $this->quiz_ids[1];
                if (!$all) {
                    if (!$is_answered) {
                        if (count($this->quiz_ids) > 1) {
                            //$next_link = 'javascript:fn_next_question(' . getSessionVar('exam_link') . ',' . $next_ques . ',' . $prev_question . ')';
                            $next_link = 'javascript:fn_next_question(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',0)';
                            $elapse_link = '               fn_next_question(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',1);';
                            $next = '<a href="' . $next_link . '" class="next_button"><span>Next</span></a>';
                        } else {
                            setSessionVar('allprev', '');
                            $next_link = 'javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                            $elapse_link = '               javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
                            $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
                        }
                    } else {
                        $next = '';
                        $elapse_link = '';
                    }
                } else {
                    if (count($this->quiz_ids) > 1) {
                        //$next_link = 'javascript:fn_next_question(' . getSessionVar('exam_link') . ',' . $next_ques . ',' . $prev_question . ')';
                        $next_link = 'javascript:fn_next_question(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',0)';
                        $elapse_link = '               javascript:fn_next_question(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' . $prev_question . ',1);';
                        $next = '<a href="' . $next_link . '" class="next_button"><span>Next</span></a>';
                    } else {
                        setSessionVar('allprev', '');
                        $next_link = 'javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                        $elapse_link = '           javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
                        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
                    }
                }
            } else {
                setSessionVar('allprev', '');
                $is_answered = $this->questionIsAnswered($question_id, $module_id);
                if (!$is_answered) {
                    $next_link = 'javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',0)';
                    $elapse_link = '           javascript:fn_complete_question(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $this->uri->segment(5) . ',1);';
                } else {
                    $next_link = '/answer-complete/' . $taskfor . '/' . $module_id;
                    $elapse_link = '/answer-complete/' . $taskfor . '/' . $module_id;
                }

                $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
            }
            //set time elapse for the question
            $time_elapse = $this->question_answer->getTimeElpase($question_id);
            if ($time_elapse) {
                $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
            } else {
                $this->mysmarty->assign('time_elapse', '');
            }

            $stmt = 'select * from SP_GET_WHITEBOARD_COUNT(' . myString($whiteboard_ids) . ')';
            $query = $this->db->query($stmt);
            if ($query) {
                $row = $query->row();
                if ($row) {
                    $hasWhiteBoard = $row->R_RESULT;
                } else {
                    $hasWhiteBoard = '';
                }
            } else {
                $hasWhiteBoard = '';
            }

            $html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
            $html .= '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
            $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
            if ($hasWhiteBoard) {
                $html .= $this->config->item('three_tab') . '<li class="tool_top_date" id="li_date"></li>' . $this->config->item('CRLF');
            } else {
                $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
            }
            //first get the top toolbar
            $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_H(' . myString($whiteboard_ids) . ')';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
            } else {
                $html .= 'error ' . $stmt;
            }
            $html .= $this->config->item('two_tab') . '<li class="tool_logout">' . $this->config->item('CRLF');
            $html .= $this->config->item('three_tab') . '<a href="javascript:fn_Logout()" class="main_logout"><span>Logout</span></a>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '</li>' . $this->config->item('CRLF');

            $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
            //$html .= $this->config->item('one_tab') . '<div class="pin" title="Hide Tool Panel" id="pin_tool" onclick="fn_pin_tool()">pin</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('one_tab') . '<div id="canvas_div">' . $this->config->item('CRLF');
            /* if ($time_elapse){
              $html .= $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
              } */
            $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid">' . $this->config->item('CRLF');
            $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');

            //only for arrow
            $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
            } else {
                $html .= 'error ' . $stmt;
            }

            //get the vertical toolbar
            $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_V(' . myString($whiteboard_ids) . ')';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
            } else {
                $html .= 'error ' . $stmt;
            }

            $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '<div id="rightside" class="fluid">' . $this->config->item('CRLF');
            if (!$all) {
                //$html .= $this->config->item('two_tab') . '<div id="rightside" class="fluid">' . $this->config->item('CRLF');
                $html .= $this->config->item('three_tab') . '<div class="fluid rightside_score" id="score">' . $this->config->item('CRLF');
                $html .= $this->config->item('four_tab') . '<div class="fluid math_tool" id="math_button">Score' . $this->config->item('CRLF');
                $html .= $this->config->item('four_tab') . $this->getQuestionsScore() . $this->config->item('CRLF');
                $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
                $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
            }
            $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
            //display the quiz controls
            $this->DisplayQuizControls($quiz_id);
            /*
              $this->mysmarty->assign('incl_form', 'inc_student_answer.tpl');
              $this->mysmarty->assign('incl_form_answer', 'incl_answer.tpl');
             */
            $this->mysmarty->assign('question', $this->getQuestion($question_id));
            //$html .= $this->config->item('two_tab') . $this->DisplayQuizControls($quiz_id) . $this->config->item('CRLF');
            //end of display quiz controls
            $html .= $this->config->item('two_tab') . '<div id="footer_holder_exam" class="fluid">' . $this->config->item('CRLF');
            $html .= $this->config->item('three_tab') . '<div class="fluid footer_buttons">' . $this->config->item('CRLF');

            $quiz_option = $this->getQuizOption($quiz_id);

            //$save_uri = '\'quiz-save-data\',\'f_quiz\'';
            if ($question_id == '-1') {
                $a_controller = "'" . '/quiz-save-data/' . $quiz_id . "'," . "'f_quiz'," . $quiz_id . ',' . $quiz_option;
            } else {
                $a_controller = "'" . '/quiz-edit-data/' . $quiz_id . '/' . $question_id . "'," . "'f_quiz'," .
                        $quiz_id . ',' . $quiz_option . ',' . $question_id;
            }
            //$save = '<a href="javascript:fn_save_form_data(' . $a_controller . ')" class="save"><span>Save</span></a>';
            //$back = $this->getBackButton();

            /* $html .= $this->config->item('four_tab') . '<table class="quiz_answer">' . $this->config->item('CRLF');
              $html .= $this->config->item('five_tab') . '<tr>' . $this->config->item('CRLF');
              $html .= $this->config->item('six_tab') . '<td class="input-box">' . $next . '</td>' . $this->config->item('CRLF');
              $html .= $this->config->item('five_tab') . '</tr>' . $this->config->item('CRLF');
              $html .= $this->config->item('four_tab') . '</table>' . $this->config->item('CRLF'); */

            $html .= $this->config->item('six_tab') . $next . $this->config->item('CRLF');

            $html .= $this->config->item('three_tab') . '<div class="validation-error" id="td_error" style="display:none"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');

            $html .= $this->config->item('three_tab') . '<div id="qstudy_footer" class="fluid">' . $this->config->item('CRLF');
            $html .= $this->config->item('four_tab') . '<div id="pages" class="fluid">Page' . $this->config->item('CRLF');
            $html .= $this->config->item('five_tab') . '<div class="current_page" id="curr_page">1</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('five_tab') . '<div class="page_of">of</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('five_tab') . '<div class="num_page" id="page_num">1</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('five_tab') . '<div class="key_pages_plus" title="Add new page" onclick="fn_addnewpage()"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('five_tab') . '<div class="key_pages_minus" title="Remove Current Page" id="remove_page" onclick="fn_removepage()" style="display:none"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('five_tab') . '<div class="key_pages_next" title="Next Page" id="next_page" onclick="fn_nextpage()" style="display:none"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('five_tab') . '<div class="key_pages_prev" title="Previous Page" id="prev_page" onclick="fn_prevpage()" style="display:none"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF');
            $html .= $this->config->item('one_tab') . '<footer id="footer" class="fluid"> </footer>' . $this->config->item('CRLF');
            $html .= '</div>' . $this->config->item('CRLF');

            //set timer in the question
            if ($elapse_link) {
                if ($time_elapse) {
                    $html .= $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
                    $html .= '<script>' . $this->config->item('CRLF');
                    $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
                    $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
                    $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
                    $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
                    $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
                    $html .= '  })' . $this->config->item('CRLF');
                    //addlistner
                    $html .= '  .addListener(' . $this->config->item('CRLF');
                    $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
                    $html .= '          if (value == 0){' . $this->config->item('CRLF');
                    $html .= $elapse_link . $this->config->item('CRLF');
                    //$html .= '              $("#time_elapse_timer").TimeCircles().restart();' . $this->config->item('CRLF');
                    $html .= '          }' . $this->config->item('CRLF');
                    $html .= '      }' . $this->config->item('CRLF');
                    $html .= '  );' . $this->config->item('CRLF');
                    $html .= '</script>' . $this->config->item('CRLF');
                }
            }

            $this->mysmarty->assign('script', $this->makeCSS($quiz_id));

            /* $this->mysmarty->assign('equation_view', $this->getEquationWindow($question_id));
              $style = $this->equations->getEquationsStyles() . $this->config->item('CRLF') . $this->equations->getEquationsDetailsClass();
              $style .= $this->equations_details->getEquationsDetailsStyle();
              $this->mysmarty->assign('equation_style', $style); */


            //$this->mysmarty->assign('validate_form', $this->formValidation());
            $html .= '</form>';
            return $html;
        }
    }

    function getQuestionQuizCategory($question_id = 0) {
        $quiz_id = $this->getQuizCategoryId($question_id);
        //get the quiz_type from quiz_category table as per selected quiz
        $quizload = $this->quiz_category->load($quiz_id);
        if ($quizload) {
            $quiz_type = $this->quiz_category->quiz_type;
        } else {
            $quiz_type = '';
        }
        if ($quiz_type != '') {
            $quiz_option = $this->quiz_typem->load($quiz_type);
            $quiz_type_option = $this->quiz_typem->quiz_option;
            if ($quiz_type_option) {
                $return = $quiz_type_option;
            } else {
                $return = '';
            }
        } else {
            $return = '';
        }
        return $return;
    }

    function DisplayAnswerBoard($quiz_id = 0, $showanswer = '') {
        //get the quiz_type from quiz_category table as per selected quiz
        $quizload = $this->quiz_category->load($quiz_id);
        if ($quizload) {
            $quiz_type = $this->quiz_category->quiz_type;
            $quiz_name = $this->quiz_category->quiz_name;
            $this->quiz_type = $quiz_type;
        } else {
            $quiz_type = '';
            $quiz_name = '';
            $this->quiz_type = 0;
        }
        if ($quiz_type == '') {
            $html = '<div id="quiz_type_notset">You didn\'t set quiz type for this quiz. Please select it in Admin Module.</div>';
            $this->mysmarty->assign('incl_form', '');
            $this->mysmarty->assign('instruction', '0');
        } else {
            $quiz_option = $this->quiz_typem->load($quiz_type);
            if ($quiz_option) {
                $quiz_type_option = $this->quiz_typem->quiz_option;
                if ($quiz_type_option) {
                    $this->mysmarty->assign('quiz_category', $quiz_type_option);
                    $this->mysmarty->assign('instruction', '0');
                    //show quiz information as per the option set in quiz_type table
					//echo $quiz_type_option;exit;
                    switch ($quiz_type_option) {
                        case 1; //Text
                        case 3:
                            $this->mysmarty->assign('incl_form', 'incl_student_answer_new.tpl');
                            $this->mysmarty->assign('incl_form_answer', 'incl_answer_new.tpl');
                            $this->mysmarty->assign('assignment_score', '');
                            /* if (!$showanswer){
                              $this->mysmarty->assign('answer', '');
                              $this->mysmarty->assign('workout', '');
                              } */
                            //$this->mysmarty->assign('error_msg', '<div class="valid_error"></div>');
                            break;
                        case 2://Text, Multiple Option
                            $this->mysmarty->assign('incl_form', 'incl_answer_true_false_new.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $a_array = array('True' => '1', 'False' => '0');
                            if (!$showanswer) {
                                $answer = ComboboxElement('answer', $a_array, '', '', 'active-select');
                            } else {
                                $module_id = $this->uri->segment(3);
                                $question_id = $this->uri->segment(4);
                                $userid = $this->uri->segment(6);
                                $student_answer = $this->getStudentAnswer($userid, $module_id, $question_id);
                                $answer = ComboboxElement('answer', $a_array, $student_answer['answer'], '', 'active-select');
                            }
                            //textinput('answer', 'active-input-border', '', '150px', '60', '', '', '', 'enter answer');
                            $this->mysmarty->assign('answer', $answer);
                            break;
                        /* case 3://Equations
                          $this->mysmarty->assign('incl_form', 'incl_ckeditor_text.tpl');
                          $this->mysmarty->assign('incl_form_answer', 'incl_ckeditor_answer.tpl');
                          break; */
                        case 4://Text, Choice
                            $this->mysmarty->assign('incl_form', 'incl_answer_multiple_choice_new.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_choices', 'incl_answer_choices.tpl');
                            //$this->mysmarty->assign('response', 'Choices');
                            $this->mysmarty->assign('response', $quiz_name);
                            $this->mysmarty->assign('creative', 'f');
                            $this->setMultipleChoiceValues_progress();
                            break;
                        case 5://Multiple Option
                            $this->mysmarty->assign('incl_form', 'incl_answer_multiple_choice_new.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_choices', 'incl_answer_multiple_choices_new.tpl');
                            //$this->mysmarty->assign('response', 'Responses');
                            $this->mysmarty->assign('response', $quiz_name);
                            $this->setMultipleChoiceValues_progress();
                            $this->setMultipleResponseAnswer($showanswer);
                            break;
                        case 6: //Multiple Response
                            $this->mysmarty->assign('incl_form', '');
                            $this->mysmarty->assign('incl_form_answer', '');
                            break;
                        case 7://matching
                            $this->mysmarty->assign('incl_form', 'incl_answer_matching_new.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->setMatchingValues();
                            break;
                        case 8://Vocabulary
                            $this->mysmarty->assign('incl_form', 'incl_answer_vocabulary_new.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_img', 'incl_answer_vocabulary_img_new.tpl');
                            $this->setMultipleChoiceValues_progress();
                            $this->setVocabularyText();
                            break;
                        case 9://Different in Score
                            $this->mysmarty->assign('incl_form', 'incl_answer_assignment_new.tpl');
                            $this->mysmarty->assign('incl_form_answer', 'incl_answer_new.tpl');
                            $assignment_score = 't';
                            $this->mysmarty->assign('assignment_score', $assignment_score);
                            $this->setAssignScores();
                            break;
                        case 10://skip
							
                            $this->mysmarty->assign('incl_form', 'incl_answer_skip_new.tpl');//incl_answer_multiple_choice_new.tpl
                            $this->mysmarty->assign('incl_form_answer','');
                            $this->mysmarty->assign('incl_form_choices','incl_answer_skip.tpl');//incl_answer_choices.tpl;
                            //$this->mysmarty->assign('response', 'Skips');
                            $this->mysmarty->assign('response', $quiz_name);
                            $this->mysmarty->assign('creative', 'f');
                            //$this->setMultipleChoiceValues_progress();
                            $this->setskipsValues_progress();
                            break;
                        case 11://Creative
                            $this->mysmarty->assign('incl_form', 'incl_answer_multiple_choice_new.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_choices', 'incl_answer_choices.tpl');
                            //$this->mysmarty->assign('response', 'Creative');
                            $this->mysmarty->assign('response', $quiz_name);
                            $this->mysmarty->assign('creative', 't');
                            $this->setMultipleChoiceValues_progress();
                            break;
                        default :
                            $this->mysmarty->assign('incl_form', '');
                            $this->mysmarty->assign('incl_form_answer', '');
                            break;
                    }

                    $html = '';
                    //{if $incl_form != ''}{include file="$incl_form"}{/if}
                    //{include file="incl_ckeditor.tpl"}
                } else {
                    $html = '<div id="quiz_type_option_notset">You didn\'t set quiz type options for this quiz. Please select it in Admin Module.</div>';
                    $this->mysmarty->assign('incl_form', '');
                    $this->canShowSave = false;
                }
            } else {
                $html = '';
            }
        }
        $this->mysmarty->assign('equation_view', $this->getEquationWindow());
        $style = $this->equations->getEquationsStyles() . $this->config->item('CRLF') . $this->equations->getEquationsDetailsClass();
        $style .= $this->equations_details->getEquationsDetailsStyle();
        $this->mysmarty->assign('equation_style', $style);

        return $html;
    }

    function DisplayQuizControls($quiz_id = 0, $showanswer = '') {
        //get the quiz_type from quiz_category table as per selected quiz
        $quizload = $this->quiz_category->load($quiz_id);
        if ($quizload) {
            $quiz_type = $this->quiz_category->quiz_type;
            $quiz_name = $this->quiz_category->quiz_name;
            $this->quiz_type = $quiz_type;
        } else {
            $quiz_type = '';
            $quiz_name = '';
            $this->quiz_type = 0;
        }
        if ($quiz_type == '') {
            $html = '<div id="quiz_type_notset">You didn\'t set quiz type for this quiz. Please select it in Admin Module.</div>';
            $this->mysmarty->assign('incl_form', '');
        } else {
            $quiz_option = $this->quiz_typem->load($quiz_type);
            if ($quiz_option) {
                $quiz_type_option = $this->quiz_typem->quiz_option;
                if ($quiz_type_option) {
                    $this->mysmarty->assign('quiz_category', $quiz_type_option);
                    //show quiz information as per the option set in quiz_type table
                    switch ($quiz_type_option) {
                        case 1; //Text
                        case 3:
                            $this->mysmarty->assign('incl_form', 'inc_student_answer.tpl');
                            $this->mysmarty->assign('incl_form_answer', 'incl_answer.tpl');
                            /* if (!$showanswer){
                              $this->mysmarty->assign('answer', '');
                              $this->mysmarty->assign('workout', '');
                              } */
                            //$this->mysmarty->assign('error_msg', '<div class="valid_error"></div>');
                            break;
                        case 2://Text, Multiple Option
                            $this->mysmarty->assign('incl_form', 'incl_answer_true_false.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $a_array = array('True' => '1', 'False' => '0');
                            if (!$showanswer) {
                                $answer = ComboboxElement('answer', $a_array, '', '', 'active-select');
                            } else {
                                $module_id = $this->uri->segment(3);
                                $question_id = $this->uri->segment(4);
                                $userid = $this->uri->segment(6);
                                $student_answer = $this->getStudentAnswer($userid, $module_id, $question_id);
                                $answer = ComboboxElement('answer', $a_array, $student_answer['answer'], '', 'active-select');
                            }
                            //textinput('answer', 'active-input-border', '', '150px', '60', '', '', '', 'enter answer');
                            $this->mysmarty->assign('answer', $answer);
                            break;
                        /* case 3://Equations
                          $this->mysmarty->assign('incl_form', 'incl_ckeditor_text.tpl');
                          $this->mysmarty->assign('incl_form_answer', 'incl_ckeditor_answer.tpl');
                          break; */
                        case 4://Text, Choice
                            $this->mysmarty->assign('incl_form', 'incl_answer_multiple_choice.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_choices', 'incl_answer_choices.tpl');
                            //$this->mysmarty->assign('response', 'Choices');
                            $this->mysmarty->assign('response', $quiz_name);
                            $this->mysmarty->assign('creative', 'f');
                            $this->setMultipleChoiceValues();
                            break;
                        case 5://Multiple Option
                            $this->mysmarty->assign('incl_form', 'incl_answer_multiple_choice.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_choices', 'incl_answer_multiple_choices.tpl');
                            //$this->mysmarty->assign('response', 'Responses');
                            $this->mysmarty->assign('response', $quiz_name);
                            $this->setMultipleChoiceValues();
                            $this->setMultipleResponseAnswer($showanswer);
                            break;
                        case 6://Multiple Response
                            $this->mysmarty->assign('incl_form', '');
                            $this->mysmarty->assign('incl_form_answer', '');
                            break;
                        case 7://matching
                            $this->mysmarty->assign('incl_form', 'incl_answer_matching.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->setMatchingValues();
                            break;
                        case 8://Vocabulary
                            $this->mysmarty->assign('incl_form', 'incl_answer_vocabulary.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_img', 'incl_answer_vocabulary_img.tpl');
                            $this->setMultipleChoiceValues();
                            $this->setVocabularyText();
                            break;
                        case 9://Different in Score
                            $this->mysmarty->assign('incl_form', 'incl_answer_assignment.tpl');
                            $this->mysmarty->assign('incl_form_answer', 'incl_answer.tpl');
                            $this->setAssignScores();
                            break;
                        case 10://skip
                            $this->mysmarty->assign('incl_form', 'incl_answer_multiple_choice.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_choices', 'incl_answer_choices.tpl');
                            //$this->mysmarty->assign('response', 'Skips');
                            $this->mysmarty->assign('response', $quiz_name);
                            $this->mysmarty->assign('creative', 'f');
                            $this->setMultipleChoiceValues();
                            break;
                        case 11://Creative
                            $this->mysmarty->assign('incl_form', 'incl_answer_multiple_choice.tpl');
                            $this->mysmarty->assign('incl_form_answer', '');
                            $this->mysmarty->assign('incl_form_choices', 'incl_answer_choices.tpl');
                            //$this->mysmarty->assign('response', 'Creative');
                            $this->mysmarty->assign('response', $quiz_name);
                            $this->mysmarty->assign('creative', 't');
                            $this->setMultipleChoiceValues();
                            break;
                        default :
                            $this->mysmarty->assign('incl_form', '');
                            $this->mysmarty->assign('incl_form_answer', '');
                            break;
                    }

                    $html = '';
                    //{if $incl_form != ''}{include file="$incl_form"}{/if}
                    //{include file="incl_ckeditor.tpl"}
                } else {
                    $html = '<div id="quiz_type_option_notset">You didn\'t set quiz type options for this quiz. Please select it in Admin Module.</div>';
                    $this->mysmarty->assign('incl_form', '');
                    $this->canShowSave = false;
                }
            } else {
                $html = '';
            }
        }
        $this->mysmarty->assign('equation_view', $this->getEquationWindow());
        $style = $this->equations->getEquationsStyles() . $this->config->item('CRLF') . $this->equations->getEquationsDetailsClass();
        $style .= $this->equations_details->getEquationsDetailsStyle();
        $this->mysmarty->assign('equation_style', $style);

        return $html;
    }

    function setMultipleChoiceValues() {
        /* if ($this->uri->total_segments() == 3) {
          $question_id = $this->uri->segment(4);
          } else {
          $question_id = '';
          } */
        if ($this->uri->total_segments() == 3) {
            $question_id = $this->uri->segment(3);
        }elseif ($this->uri->total_segments() == 4) {
            $question_id = $this->uri->segment(4);
        } else {
            $question_id = $this->uri->segment(5);
        }
        if ($question_id) {
            $lmChoice = $this->quiz_multiple_choice->load($question_id);
            if ($lmChoice) {
                $rows = $this->quiz_multiple_choice->rows_number;
                $this->mysmarty->assign('rows_number', $this->quiz_multiple_choice->rows_number);
                $this->mysmarty->assign('question_creative', $this->quiz_multiple_choice->question_creative);
            } else {
                $rows = 0;
                $this->mysmarty->assign('rows_number', '');
                $this->mysmarty->assign('question_creative', '');
            }
            $lmChoices = $this->quiz_multiple_choice->loadChoices($question_id);

            if ($lmChoices) {
                if ($rows > 0) {
                    for ($i = 1; $i <= $rows; $i++) {
                        $this->mysmarty->assign('rchoice_' . $i, $this->quiz_multiple_choice->choices_selecteds[$i - 1]);
                    }
                    for ($i = $rows + 1; $i <= 20; $i++) {
                        $this->mysmarty->assign('rchoice_' . $i, '');
                    }
                } else {
                    for ($i = 1; $i <= 20; $i++) {
                        $this->mysmarty->assign('rchoice_' . $i, '');
                    }
                }
            }
        } else {
            $this->mysmarty->assign('rows_number', '');
            $this->mysmarty->assign('question_creative', '');
            for ($i = 1; $i <= 20; $i++) {
                $this->mysmarty->assign('rchoice_' . $i, '');
            }
        }
    }

    function setMultipleChoiceValues_progress() {
        /* if ($this->uri->total_segments() == 3) {
          $question_id = $this->uri->segment(4);
          } else {
          $question_id = '';
        } */
        if ($this->uri->total_segments() == 3) {
            $question_id = $this->uri->segment(3);
        }elseif ($this->uri->total_segments() == 4) {
            $question_id = $this->uri->segment(4);
        }elseif ($this->uri->total_segments() == 6) {
            if($this->uri->segment(1)=='show-answer' || $this->uri->segment(1)=='students-progress'){
               $question_id = $this->uri->segment(4); 
            }else{
                $question_id = $this->uri->segment(5);
            }
            
        }  else {
            $question_id = $this->uri->segment(5);
        }
        
        
        //$question_id = $this->uri->segment(4);
       //echo $question_id;exit;
        if ($question_id) {
            $lmChoice = $this->quiz_multiple_choice->load($question_id);
            if ($lmChoice) {
                $rows = $this->quiz_multiple_choice->rows_number;
                $this->mysmarty->assign('rows_number', $this->quiz_multiple_choice->rows_number);
                $this->mysmarty->assign('question_creative', $this->quiz_multiple_choice->question_creative);
            } else {
                $rows = 0;
                $this->mysmarty->assign('rows_number', '');
                $this->mysmarty->assign('question_creative', '');
            }
            $lmChoices = $this->quiz_multiple_choice->loadChoices($question_id);

            if ($lmChoices) {
                if ($rows > 0) {
                    for ($i = 1; $i <= $rows; $i++) {
                        $this->mysmarty->assign('rchoice_' . $i, $this->quiz_multiple_choice->choices_selecteds[$i - 1]);
                    }
                    for ($i = $rows + 1; $i <= 20; $i++) {
                        $this->mysmarty->assign('rchoice_' . $i, '');
                    }
                } else {
                    for ($i = 1; $i <= 20; $i++) {
                        $this->mysmarty->assign('rchoice_' . $i, '');
                    }
                }
            }
        } else {
            $this->mysmarty->assign('rows_number', '');
            $this->mysmarty->assign('question_creative', '');
            for ($i = 1; $i <= 20; $i++) {
                $this->mysmarty->assign('rchoice_' . $i, '');
            }
        }
    }
	
    function setskipsValues_progress() {
        /* if ($this->uri->total_segments() == 3) {
          $question_id = $this->uri->segment(4);
          } else {
          $question_id = '';
        } */
        if ($this->uri->total_segments() == 3) {
            $question_id = $this->uri->segment(3);
        }elseif ($this->uri->total_segments() == 4) {
            $question_id = $this->uri->segment(4);
        }elseif ($this->uri->total_segments() == 6) {
            if($this->uri->segment(1)=='show-answer' || $this->uri->segment(1)=='students-progress'){
               $question_id = $this->uri->segment(4); 
            }else{
                $question_id = $this->uri->segment(5);
            }
            
        }  else {
            $question_id = $this->uri->segment(5);
        }
        
        
        //$question_id = $this->uri->segment(4);
       //echo $question_id;exit;
        if ($question_id) {
			
			$rslt = $this->db->query("select * from QUIZ_SKIP_QUES where QUIZ_ANSWER_ID='".$question_id."'")->row();
			if($rslt){
				$colrows = explode('_',$rslt->NUM_COL_ROW);
				$this->mysmarty->assign('numofcol',$colrows[0]);
				$this->mysmarty->assign('numofrow',$colrows[1]);
				$this->mysmarty->assign('allitems',$rslt->ALL_ITEMS);
				$this->mysmarty->assign('answeritems',$rslt->ANS_ITEMS);
				$this->mysmarty->assign('questionitems',$rslt->QUES_ITEMS);
			}
			
        } else {
           
			$this->mysmarty->assign('numofcol',1);
			$this->mysmarty->assign('numofrow',1);
			$this->mysmarty->assign('allitems','');
			$this->mysmarty->assign('answeritems','');
			$this->mysmarty->assign('questionitems','');
		   
        }
    }

    function setMultipleResponseAnswer($showanswer = '') {
        $module_id = $this->uri->segment(4);
        if ($this->uri->total_segments() == 4) {
            $question_id = $this->uri->segment(4);
        } else {
            $question_id = $this->uri->segment(5);
        }

        $userid = getSessionVar('userid');
        if ($showanswer) {
            $answer = $this->getStudentAnswer($userid, $module_id, $question_id);
            if (preg_match('/' . '1' . '/i', $answer['answer'])) {
                $chk1 = '<input type="checkbox" name="chk_1" id="chk_1" onClick="fn_check(1)" checked>';
            } else {
                $chk1 = '<input type="checkbox" name="chk_1" id="chk_1" onClick="fn_check(1)">';
            }
            if (preg_match('/' . '2' . '/i', $answer['answer'])) {
                $chk2 = '<input type="checkbox" name="chk_2" id="chk_2" onClick="fn_check(2)" checked>';
            } else {
                $chk2 = '<input type="checkbox" name="chk_2" id="chk_2" onClick="fn_check(2)">';
            }
            if (preg_match('/' . '3' . '/i', $answer['answer'])) {
                $chk3 = '<input type="checkbox" name="chk_3" id="chk_3" onClick="fn_check(3)" checked>';
            } else {
                $chk3 = '<input type="checkbox" name="chk_3" id="chk_3" onClick="fn_check(3)">';
            }
            if (preg_match('/' . '4' . '/i', $answer['answer'])) {
                $chk4 = '<input type="checkbox" name="chk_4" id="chk_4" onClick="fn_check(4)" checked>';
            } else {
                $chk4 = '<input type="checkbox" name="chk_4" id="chk_4" onClick="fn_check(4)">';
            }
            if (preg_match('/' . '5' . '/i', $answer['answer'])) {
                $chk5 = '<input type="checkbox" name="chk_5" id="chk_5" onClick="fn_check(5)" checked>';
            } else {
                $chk5 = '<input type="checkbox" name="chk_5" id="chk_5" onClick="fn_check(5)">';
            }
            if (preg_match('/' . '6' . '/i', $answer['answer'])) {
                $chk6 = '<input type="checkbox" name="chk_6" id="chk_6" onClick="fn_check(6)" checked>';
            } else {
                $chk6 = '<input type="checkbox" name="chk_1" id="chk_6" onClick="fn_check(6)">';
            }
            if (preg_match('/' . '7' . '/i', $answer['answer'])) {
                $chk7 = '<input type="checkbox" name="chk_7" id="chk_7" onClick="fn_check(7)" checked>';
            } else {
                $chk7 = '<input type="checkbox" name="chk_7" id="chk_7" onClick="fn_check(7)">';
            }
            if (preg_match('/' . '8' . '/i', $answer['answer'])) {
                $chk8 = '<input type="checkbox" name="chk_8" id="chk_8" onClick="fn_check(8)" checked>';
            } else {
                $chk8 = '<input type="checkbox" name="chk_8" id="chk_8" onClick="fn_check(8)">';
            }
            if (preg_match('/' . '9' . '/i', $answer['answer'])) {
                $chk9 = '<input type="checkbox" name="chk_9" id="chk_9" onClick="fn_check(9)" checked>';
            } else {
                $chk9 = '<input type="checkbox" name="chk_9" id="chk_9" onClick="fn_check(9)">';
            }
            if (preg_match('/' . '10' . '/i', $answer['answer'])) {
                $chk10 = '<input type="checkbox" name="chk_10" id="chk_10" onClick="fn_check(10)" checked>';
            } else {
                $chk10 = '<input type="checkbox" name="chk_10" id="chk_10" onClick="fn_check(10)">';
            }
            if (preg_match('/' . '11' . '/i', $answer['answer'])) {
                $chk11 = '<input type="checkbox" name="chk_11" id="chk_11" onClick="fn_check(11)" checked>';
            } else {
                $chk11 = '<input type="checkbox" name="chk_11" id="chk_11" onClick="fn_check(11)">';
            }
            if (preg_match('/' . '12' . '/i', $answer['answer'])) {
                $chk12 = '<input type="checkbox" name="chk_12" id="chk_12" onClick="fn_check(12)" checked>';
            } else {
                $chk12 = '<input type="checkbox" name="chk_12" id="chk_12" onClick="fn_check(12)">';
            }
            if (preg_match('/' . '13' . '/i', $answer['answer'])) {
                $chk13 = '<input type="checkbox" name="chk_13" id="chk_13" onClick="fn_check(13)" checked>';
            } else {
                $chk13 = '<input type="checkbox" name="chk_13" id="chk_13" onClick="fn_check(13)">';
            }
            if (preg_match('/' . '14' . '/i', $answer['answer'])) {
                $chk14 = '<input type="checkbox" name="chk_14" id="chk_14" onClick="fn_check(14)" checked>';
            } else {
                $chk14 = '<input type="checkbox" name="chk_14" id="chk_14" onClick="fn_check(14)">';
            }
            if (preg_match('/' . '15' . '/i', $answer['answer'])) {
                $chk15 = '<input type="checkbox" name="chk_15" id="chk_15" onClick="fn_check(15)" checked>';
            } else {
                $chk15 = '<input type="checkbox" name="chk_15" id="chk_15" onClick="fn_check(15)">';
            }
            if (preg_match('/' . '16' . '/i', $answer['answer'])) {
                $chk16 = '<input type="checkbox" name="chk_16" id="chk_16" onClick="fn_check(16)" checked>';
            } else {
                $chk16 = '<input type="checkbox" name="chk_16" id="chk_16" onClick="fn_check(16)">';
            }
            if (preg_match('/' . '17' . '/i', $answer['answer'])) {
                $chk17 = '<input type="checkbox" name="chk_17" id="chk_17" onClick="fn_check(17)" checked>';
            } else {
                $chk17 = '<input type="checkbox" name="chk_17" id="chk_17" onClick="fn_check(17)">';
            }
            if (preg_match('/' . '18' . '/i', $answer['answer'])) {
                $chk18 = '<input type="checkbox" name="chk_18" id="chk_18" onClick="fn_check(18)" checked>';
            } else {
                $chk18 = '<input type="checkbox" name="chk_18" id="chk_18" onClick="fn_check(18)">';
            }
            if (preg_match('/' . '19' . '/i', $answer['answer'])) {
                $chk19 = '<input type="checkbox" name="chk_19" id="chk_19" onClick="fn_check(19)" checked>';
            } else {
                $chk19 = '<input type="checkbox" name="chk_19" id="chk_19" onClick="fn_check(19)">';
            }
            if (preg_match('/' . '20' . '/i', $answer['answer'])) {
                $chk20 = '<input type="checkbox" name="chk_20" id="chk_20" onClick="fn_check(20)" checked>';
            } else {
                $chk20 = '<input type="checkbox" name="chk_20" id="chk_20" onClick="fn_check(20)">';
            }
        } else {
            $chk1 = '<input type="checkbox" name="chk_1" id="chk_1" onClick="fn_check(1)">';
            $chk2 = '<input type="checkbox" name="chk_2" id="chk_2" onClick="fn_check(2)">';
            $chk3 = '<input type="checkbox" name="chk_3" id="chk_3" onClick="fn_check(3)">';
            $chk4 = '<input type="checkbox" name="chk_4" id="chk_4" onClick="fn_check(4)">';
            $chk5 = '<input type="checkbox" name="chk_5" id="chk_5" onClick="fn_check(5)">';
            $chk6 = '<input type="checkbox" name="chk_1" id="chk_6" onClick="fn_check(6)">';
            $chk7 = '<input type="checkbox" name="chk_7" id="chk_7" onClick="fn_check(7)">';
            $chk8 = '<input type="checkbox" name="chk_8" id="chk_8" onClick="fn_check(8)">';
            $chk9 = '<input type="checkbox" name="chk_9" id="chk_9" onClick="fn_check(9)">';
            $chk10 = '<input type="checkbox" name="chk_10" id="chk_10" onClick="fn_check(10)">';
            $chk11 = '<input type="checkbox" name="chk_11" id="chk_11" onClick="fn_check(11)">';
            $chk12 = '<input type="checkbox" name="chk_12" id="chk_12" onClick="fn_check(12)">';
            $chk13 = '<input type="checkbox" name="chk_13" id="chk_13" onClick="fn_check(13)">';
            $chk14 = '<input type="checkbox" name="chk_14" id="chk_14" onClick="fn_check(14)">';
            $chk15 = '<input type="checkbox" name="chk_15" id="chk_15" onClick="fn_check(15)">';
            $chk16 = '<input type="checkbox" name="chk_16" id="chk_16" onClick="fn_check(16)">';
            $chk17 = '<input type="checkbox" name="chk_17" id="chk_10" onClick="fn_check(17)">';
            $chk18 = '<input type="checkbox" name="chk_18" id="chk_10" onClick="fn_check(18)">';
            $chk19 = '<input type="checkbox" name="chk_19" id="chk_10" onClick="fn_check(19)">';
            $chk20 = '<input type="checkbox" name="chk_20" id="chk_20" onClick="fn_check(20)">';
        }
        $this->mysmarty->assign('chk1', $chk1);
        $this->mysmarty->assign('chk2', $chk2);
        $this->mysmarty->assign('chk3', $chk3);
        $this->mysmarty->assign('chk4', $chk4);
        $this->mysmarty->assign('chk5', $chk5);
        $this->mysmarty->assign('chk6', $chk6);
        $this->mysmarty->assign('chk7', $chk7);
        $this->mysmarty->assign('chk8', $chk8);
        $this->mysmarty->assign('chk9', $chk9);
        $this->mysmarty->assign('chk10', $chk10);
        $this->mysmarty->assign('chk11', $chk11);
        $this->mysmarty->assign('chk12', $chk12);
        $this->mysmarty->assign('chk13', $chk13);
        $this->mysmarty->assign('chk14', $chk14);
        $this->mysmarty->assign('chk15', $chk15);
        $this->mysmarty->assign('chk16', $chk16);
        $this->mysmarty->assign('chk17', $chk17);
        $this->mysmarty->assign('chk18', $chk18);
        $this->mysmarty->assign('chk19', $chk19);
        $this->mysmarty->assign('chk20', $chk20);
    }

    function setMatchingValues() {
        if ($this->uri->total_segments() == 3) {
            $question_id = $this->uri->segment(3);
        }elseif ($this->uri->total_segments() == 4) {
            $question_id = $this->uri->segment(4);
        } else {
            $question_id = $this->uri->segment(5);
        }

        if ($question_id) {
            $lmChoice = $this->quiz_matching->load($question_id);
            if ($lmChoice) {
                $rows = $this->quiz_matching->rows_number;
                $this->mysmarty->assign('rows_number', $this->quiz_matching->rows_number);
            } else {
                $rows = 0;
                $this->mysmarty->assign('rows_number', '');
            }
            $lmChoices = $this->quiz_matching->loadChoices($question_id);
            if ($lmChoices) {
                if ($rows > 0) {
                    for ($i = 1; $i <= $rows; $i++) {
                        $this->mysmarty->assign('rleft_matching_' . $i, $this->quiz_matching->left_matchings[$i - 1]);
                        $this->mysmarty->assign('rright_matching_' . $i, $this->quiz_matching->right_matchings[$i - 1]);
                    }
                    for ($i = $rows + 1; $i <= 20; $i++) {
                        $this->mysmarty->assign('rleft_matching_' . $i, '');
                        $this->mysmarty->assign('rright_matching_' . $i, '');
                    }
                } else {
                    for ($i = 1; $i <= 20; $i++) {
                        $this->mysmarty->assign('rleft_matching_' . $i, '');
                        $this->mysmarty->assign('rright_matching_' . $i, '');
                    }
                }
            }
        } else {
            $this->mysmarty->assign('rows_number', '');
            for ($i = 1; $i <= 20; $i++) {
                $this->mysmarty->assign('rleft_matching_' . $i, '');
                $this->mysmarty->assign('rright_matching_' . $i, '');
            }
        }
    }

    function setAssignScores() {
        if ($this->uri->total_segments() == 3) {
            $question_id = $this->uri->segment(3);
        }elseif ($this->uri->total_segments() == 4) {
            $question_id = $this->uri->segment(4);
        } else {
            $question_id = $this->uri->segment(5);
        }
        if ($question_id) {
            $lmChoice = $this->quiz_assign_score->load($question_id);
            if ($lmChoice) {
                $rows = $this->quiz_assign_score->rows_number;
                $this->mysmarty->assign('rows_number', $this->quiz_assign_score->rows_number);
            } else {
                $rows = 0;
                $this->mysmarty->assign('rows_number', '');
            }
            $lmChoices = $this->quiz_assign_score->loadAll($question_id);
            if ($lmChoices) {
                if ($rows > 0) {
                    for ($i = 1; $i <= $rows; $i++) {
                        $this->mysmarty->assign('score_description_' . $i, $this->quiz_assign_score->assign_descriptions[$i - 1]);
                        $this->mysmarty->assign('score_' . $i, $this->quiz_assign_score->assign_scores[$i - 1]);
                    }
                    for ($i = $rows + 1; $i <= 20; $i++) {
                        $this->mysmarty->assign('score_description_' . $i, '');
                        $this->mysmarty->assign('score_' . $i, '');
                    }
                } else {
                    for ($i = 1; $i <= 20; $i++) {
                        $this->mysmarty->assign('score_description_' . $i, '');
                        $this->mysmarty->assign('score_' . $i, '');
                    }
                }
            } else {
                for ($i = 1; $i <= 20; $i++) {
                    $this->mysmarty->assign('score_description_' . $i, $this->quiz_assign_score->error_msg);
                    $this->mysmarty->assign('score_' . $i, '');
                }
            }
        } else {
            $this->mysmarty->assign('rows_number', '');
            for ($i = 1; $i <= 20; $i++) {
                $this->mysmarty->assign('score_description_' . $i, '');
                $this->mysmarty->assign('score_' . $i, '');
            }
        }
    }

    function getVideo($avideo = '') {
        $afile = $avideo;
        //$mystring = $afile;
        $findme = '"';
        $pos = strpos($afile, $findme);
        $length = strrpos($afile, $findme);
        $afile = substr($afile, $pos + 1, $length + 1);
        $length = strrpos($afile, $findme);
        $afile = substr($afile, 0, $length);
        $atext = $avideo;
        $start = strrpos($atext, $findme);
        $atext = substr($atext, $start + 2);
        $findme = '<';
        $start = strpos($atext, $findme);
        $atext = substr($atext, 0, $start);

        $link = '<a class="alink" href="javascript:fn_play_video(\'' . $atext . '\')"><div class="video_big"></div></a>';
        //$link = '<div class="video_big" onClick="fn_play_video(\'' . $atext . '\')"></div>';
        return $link;
    }

    function getAudio($audio = '') {
        $afile = $audio;
        
        $isaudio = strpos($afile, 'href');

        //$mystring = $afile;
        if ($isaudio) {
            $findme = '"';
            $pos = strpos($afile, $findme);
            $length = strrpos($afile, $findme);
            $afile = substr($afile, $pos + 1, $length + 1);
            $length = strrpos($afile, $findme);
            $afile = substr($afile, 0, $length);
            /*rs get generate file name*/
            $myfilearr = explode('/',$afile);
            $generated_file  = end($myfilearr);
           
            /*rs get generate file name*/
            $atext = $audio;
            $start = strrpos($atext, $findme);
            $atext = substr($atext, $start + 2);
            $findme = '<';
            $start = strpos($atext, $findme);
            $atext = substr($atext, 0, $start);
            
            $link = '<a class="alink" href="javascript:fn_play_sound_answer(\'' . $generated_file . '\')"><div class="audio_big"></div></a>';
        } else {
            $atext = $afile;
            $pos = strpos($atext, '>');
            $length = strrpos($atext, '<');
            $atext = substr($atext, $pos + 1, $length);
            $length = strrpos($atext, '<');
            $atext = substr($atext, 0, $length);
            $link = '<a class="alink" href="javascript:fn_textToSpeech(\'' . $atext . '\')"><div class="audio_big"></div></a>';
        }

        //$link = '<div class="audio_big" onClick="fn_play_sound_answer(\'' . $atext . '\')"></div>';
        return $link;
    }

    function setVocabularyText() {
        if ($this->uri->total_segments() == 3) {
            $question_id = $this->uri->segment(3);
        } elseif ($this->uri->total_segments() == 4) {
            $question_id = $this->uri->segment(4);
        } elseif ($this->uri->total_segments() == 6) {
            if($this->uri->segment(1)=='show-answer' || $this->uri->segment(1)=='students-progress'){
               $question_id = $this->uri->segment(4); 
            }else{
                $question_id = $this->uri->segment(5);
            }
            
        } else {
            $question_id = $this->uri->segment(5);
        }

        if ($question_id) {
            $vocabulary = $this->vocabulary->load($question_id);
            if ($vocabulary) {
                $this->mysmarty->assign('word_to_search', $this->vocabulary->word_to_search);
                $this->mysmarty->assign('definition', $this->vocabulary->definition);
                $this->mysmarty->assign('parts_of_speech', $this->vocabulary->parts_of_speech);
                $this->mysmarty->assign('synonym', $this->vocabulary->synonym);
                $this->mysmarty->assign('antonym', $this->vocabulary->antonym);
                $this->mysmarty->assign('near_antonym', $this->vocabulary->near_antonym);
                $this->mysmarty->assign('orig_sentence', $this->vocabulary->orig_sentence);
                if ($this->vocabulary->audio_file) {
                    $this->mysmarty->assign('audio_file', $this->getAudio($this->vocabulary->audio_file));
                } else {
                    $this->mysmarty->assign('audio_file', '');
                }
                if ($this->vocabulary->video_file) {
                    $this->mysmarty->assign('video_file', $this->getVideo($this->vocabulary->video_file));
                } else {
                    $this->mysmarty->assign('video_file', '');
                }
            } else {
                $this->mysmarty->assign('word_to_search', '');
                $this->mysmarty->assign('definition', '');
                $this->mysmarty->assign('parts_of_speech', '');
                $this->mysmarty->assign('synonym', '');
                $this->mysmarty->assign('antonym', '');
                $this->mysmarty->assign('near_antonym', '');
                $this->mysmarty->assign('orig_sentence', '');
                $this->mysmarty->assign('audio_file', '');
                $this->mysmarty->assign('video_file', '');
            }
        } else {
            $this->mysmarty->assign('word_to_search', '');
            $this->mysmarty->assign('definition', '');
            $this->mysmarty->assign('parts_of_speech', '');
            $this->mysmarty->assign('synonym', '');
            $this->mysmarty->assign('antonym', '');
            $this->mysmarty->assign('near_antonym', '');
            $this->mysmarty->assign('orig_sentence', '');
            $this->mysmarty->assign('audio_file', '');
            $this->mysmarty->assign('video_file', '');
        }
    }

    function getQuizOption($quiz_id = 0) {
        $quizload = $this->quiz_category->load($quiz_id);
        if ($quizload) {
            $quiz_type = $this->quiz_category->quiz_type;
            $this->quiz_type = $quiz_type;
        } else {
            $quiz_type = '';
            $this->quiz_type = 0;
        }
        if ($quiz_type == '') {
            $html = '';
        } else {
            $quiz_option = $this->quiz_typem->load($quiz_type);
            if ($quiz_option) {
                $quiz_type_option = $this->quiz_typem->quiz_option;
                $html = $quiz_type_option;
            } else {
                $html = $quiz_type_option;
            }
        }
        return $html;
    }

    function makeCSS($quiz_id = '') {
        //$quiz_id = $this->uri->segment(2);
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
        } else {
            $whiteboard_ids = '';
        }
        //first get the top toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_H(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        $html = '<style>' . $this->config->item('CRLF');
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:inline-block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-right:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin:-5px 40px 10px -30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . ':hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                //for the active
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:inline-block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-right:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin:-5px 40px 10px -30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #AEAEAE;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active:hover{' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //get the vertical toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_V(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-left:-33px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-bottom:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . ':hover{' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                //for active
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-left:-33px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-bottom:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #AEAEAE;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active:hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //for the arrow
        $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-left:-33px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-bottom:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . ':hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                //for active
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-left:-33px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-bottom:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #AEAEAE;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active:hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        $html .= '</style>';
        return $html;
    }

    function getEquationWindow($question_id = '') {
        $html = $this->getSelectedEquation();
        $html .= '<div id="customize_symbols">' . $this->config->item('CRLF');
        $html .= '<div class="text">Symbols to work with</div>' . $this->config->item('CRLF');
        //$html .= '<div class="save_button"><a class="save" href="javascript:fn_save_whiteboard_settings()">Save</a></div>' . $this->config->item('CRLF');
        //$html .= '<div class="save_button"><a class="normal_save" href="#">Save</a></div>' . $this->config->item('CRLF');
        $html .= '<div class="custom_symbol">' . $this->config->item('CRLF');
        $html .= '<ul class="custom_symbol_ul">' . $this->config->item('CRLF');
        for ($i = 1; $i <= 100; $i++) {
            if ($i <= 20) {
                $html .= '<li class="custom_symbol_li" onClick="fn_select_custom_symbol(' . $i . ')" id="cs_' . $i . '"></li>' . $this->config->item('CRLF');
            } else {
                $html .= '<li class="custom_symbol_li" onClick="fn_select_custom_symbol(' . $i . ')" id="cs_' . $i . '" style="display:none"></li>' . $this->config->item('CRLF');
            }
        }
        $html .= '</ul>';
        $html .= '</div>'; //end of customize_symbols
        $html .= '<div class="spinner">spinner</div>';

        $html .= '<div id="spinner_cs"><div id="spinval_cs" class="spinner_edit_cs">1</div>';
        $html .= '<div class="upbutton" onClick="fn_SetSpinnerVertVal(1)"></div>';
        $html .= '<div class="downbutton" onClick="fn_SetSpinnerVertVal(0)"></div>';
        $html .= '<div class="total">1-5</div>';
        $html .= '</div>'; //end of customize_symbols
        $html .= '</div>'; //end of customize_symbols div
        //equations
        /*
          $html .= '<div id="equations">' . $this->config->item('CRLF');
          //$html .= '<div class="close_eq" title="Close" onclick="fn_hide_equations()">Close</div>';
          $html .= $this->equations->getEquationsSymbols();

          //for the equations details
          $html .= $this->equations->getEquationDetails(1, 1);
          $html .= '</div>'; //end of equations
         */
        $this->mysmarty->assign('math_form', 'incl_mathquill.tpl');
        return $html;
    }

    function getSelectedEquation($question_id = '') {
        $question = $this->question_answer->load($question_id);
        $html = '<script>';
        //$html .= 'var selectedCustomSymbol=0';
        $html .= '$(function() {';
        if ($question) {
            $sel_equ = $this->question_answer->selected_equation;
            if ($sel_equ != '') {
                $html .= $this->equations_details->getSelectedEquation($sel_equ);
            }
        }
        $html .= '});';
        $html .= '</script>';
        return $html;
    }

    function getAnswerResultNew() {
        $module_id = $this->uri->segment(4);
        $userid = getSessionVar('userid');
		
        //load user account
        $user_load = $this->user_account->load($userid);
        if ($user_load) {
            $first_name = $this->user_account->user_firstname;
            $last_name = $this->user_account->user_lastname;
            $user_name = $this->user_account->user_name;
            $parent_id = $this->user_account->parent_id;
            $student_year = $this->user_account->student_year;
        } else {
            $first_name = '';
            $last_name = '';
            $user_name = '';
            $parent_id = 0;
            $student_year = 0;
        }
        //load user parent
        if ($parent_id) {
            $parent_load = $this->user_account->load($parent_id);
            if ($parent_load) {
                $mobile_no = $this->user_account->mobile_no;
                $pfirst_name = $this->user_account->user_firstname;
                $plast_name = $this->user_account->user_lastname;
                $puser_name = $this->user_account->user_name;
            } else {
                $mobile_no = '';
                $pfirst_name = '';
                $plast_name = '';
                $puser_name = '';
            }
        } else {
            $mobile_no = '';
        }

        $stmt = "select * from SP_ANSWER_RESULT($userid, $module_id)";
        
        $query = $this->db->query($stmt);

         $sql_stmt = "select QUESTION_ID,ANSWER_RIGHT from QUEST_ANS_STUDENT WHERE USER_ID='".$userid."' AND MODULE_ID='".$module_id."' AND QUIZ_CATE=9 AND ANSWER!='<p>&nbsp;</p>' GROUP BY QUESTION_ID,ANSWER_RIGHT";
        $pendings_query = $this->db->query($sql_stmt);
        $j=0;
		$k=0;
		$ttal=0;
       if($pendings_query->result()){
            foreach ($pendings_query->result() as $row_q) {
                if($row_q->ANSWER_RIGHT!=1){
                     $j++;         
                }else{
                  $k++;   
                }
                $ttal++;
            }
        }
        
        if ($query) {
            $row = $query->row();
            if ($row) {
                $mark = $row->R_MARK;
                $org_mark = $row->R_ORG_MARK;
                $total_question = $row->R_TOTAL_QUESTION;
                /*if ($mark > 0) {
                    $percentage = myMoney(($mark * 100) / $org_mark, 2);
                } else {
                    $percentage = 0;
                }*/
                $percentage = $row->R_RESULT;
                $module_name = $row->R_MODULES_NAME;
                $answer_date = $row->R_ANSWER_DATE;
                $obtained = (int) $row->R_RESULT;
                $html = '<div id="result">';
                $html .= $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                
                 $html .='<br>Your total pending question is '.$ttal.'. Wrong answer '.$j.' and right answer '.$k;
                
                $html .= '</div>';
                //prepare text for sms
                $start_time = getSessionVar('start_time');
				
                $start_time = strtotime($start_time);
				
				
                $end_time = getSessionVar('end_time');
				
                $end_time = strtotime($end_time);
				
                $elpased = $end_time - $start_time;
				/* convert to time */
				$time = $elpased;
				$seconds = $time % 60;
				$time = ($time - $seconds) / 60;
				$minutes = $time % 60;
				$hours = ($time - $minutes) / 60;
				$elapsed_time = $hours.':'.$minutes.':'.$seconds;
				/* convert to time */
				
                $sms_text = $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
               $sms_text .= 'in module ' . $module_name . ' held on ' . $answer_date . ' elapsed ' . $elapsed_time;
			  
                $sms_text .= $this->config->item('CRLF');
                $sms_text .= 'From Q-Study';

                $all = getSessionVar('alltasks');
                //check if module has send_sms checked to send sms
                $load_module = $this->quiz_module->load($module_id);
                if ($load_module) {
                    $send_sms = $this->quiz_module->send_sms;
                } else {
                    $send_sms = 0;
                }
				
                if ($send_sms) {
                    $sms_send = $this->sms_list->isSMSSend($userid, $module_id);
                    if (!$all) {
                        if (!$sms_send) {
                            $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                            if ($smssend) {
                                $html .= '<div class="result_error">';
                                $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                                $html .= '</div>';
                            } else {
                                $html .= '<div class="result_error">';
                                $html .= $this->error_msg;
                                $html .= '</div>';
                            }
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        }
                    } else {
                        $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                        if ($smssend) {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->error_msg;
                            $html .= '</div>';
                        }
                    }
                } else {
                    $html .= '<div class="result_error">';
                    $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                    $html .= '</div>';
                }
            } else {
                //$html = 'error occur in getting row. ' . $stmt;
                $mark = 0;
                $org_mark = 0;
                $total_question = 0;
                if ($mark > 0) {
                    $percentage = myMoney(($mark * 100) / $org_mark, 2);
                } else {
                    $percentage = 0;
                }
				
                $module_name = '';
                $answer_date = '';
                $obtained = 0;
                $html = '<div id="result">';
               // $html .= $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                //$html .= $mark . ' ' . $org_mark . ' ' . $total_question . ' ' . $row->R_RESULT;
                //$html .= 'in module ' . $module_name . ' held on ' . $answer_date;
                 $html .='<br>Your total pending question is '.$ttal.'. Wrong answer '.$j.' and right answer '.$k;
                
                $html .= '</div>';
                //prepare text for sms
                $start_time = getSessionVar('start_time');
                $start_time = strtotime($start_time);
                $end_time = getSessionVar('end_time');
                $end_time = strtotime($end_time);
                $elpased = $end_time - $start_time;
				
				$time = $elpased;
				
				$seconds = $time % 60;
				$time = ($time - $seconds) / 60;
				$minutes = $time % 60;
				$hours = ($time - $minutes) / 60;
				$elapsed_time = $hours.':'.$minutes.':'.$seconds;
				

                $sms_text = $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                $sms_text .= 'in module ' . $module_name . ' held on ' . $answer_date . ' elapsed ' . $elapsed_time;
                $sms_text .= $this->config->item('CRLF');
                $sms_text .= 'From Q-Study';

                $all = getSessionVar('alltasks');
                //check if module has send_sms checked to send sms
                $load_module = $this->quiz_module->load($module_id);
                if ($load_module) {
                    $send_sms = $this->quiz_module->send_sms;
                } else {
                    $send_sms = 0;
                }
				
                if ($send_sms) {
                    $sms_send = $this->sms_list->isSMSSend($userid, $module_id);
                    if (!$all) {
                        if (!$sms_send) {
                            $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                            if ($smssend) {
                                $html .= '<div class="result_error">';
                                $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                                $html .= '</div>';
                            } else {
                                $html .= '<div class="result_error">';
                                $html .= $this->error_msg;
                                $html .= '</div>';
                            }
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        }
                    } else {
                        $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                        if ($smssend) {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->error_msg;
                            $html .= '</div>';
                        }
                    }
                } else {
                    $html .= '<div class="result_error">';
                    $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                    $html .= '</div>';
                }
            }
        } else {
            $html = 'error in statement. ' . $stmt;
        }
		$this->mysmarty->assign('top_tutorial', '');
        $this->mysmarty->assign('qcalculates', '');
		$this->mysmarty->assign('qtimes', '');
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        $this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('incl_form', 'incl_answer_complete_new.tpl');
        $this->mysmarty->assign('answer_complete', $html);
        $this->mysmarty->assign('instruction', '0');
    }
    function getAnswerResultNew_op_module() {
        $module_id = $this->uri->segment(4);
        $userid = getSessionVar('userid');
		
        //load user account
        $user_load = $this->user_account->load($userid);
        if ($user_load) {
            $first_name = $this->user_account->user_firstname;
            $last_name = $this->user_account->user_lastname;
            $user_name = $this->user_account->user_name;
            $parent_id = $this->user_account->parent_id;
            $student_year = $this->user_account->student_year;
        } else {
            $first_name = '';
            $last_name = '';
            $user_name = '';
            $parent_id = 0;
            $student_year = 0;
        }
        //load user parent
        if ($parent_id) {
            $parent_load = $this->user_account->load($parent_id);
            if ($parent_load) {
                $mobile_no = $this->user_account->mobile_no;
                $pfirst_name = $this->user_account->user_firstname;
                $plast_name = $this->user_account->user_lastname;
                $puser_name = $this->user_account->user_name;
            } else {
                $mobile_no = '';
                $pfirst_name = '';
                $plast_name = '';
                $puser_name = '';
            }
        } else {
            $mobile_no = '';
        }

        $stmt = "select * from SP_ANSWER_RESULT($userid, $module_id)";
        
        $query = $this->db->query($stmt);

         $sql_stmt = "select QUESTION_ID,ANSWER_RIGHT from QUEST_ANS_STUDENT WHERE USER_ID='".$userid."' AND MODULE_ID='".$module_id."' AND QUIZ_CATE=9 AND ANSWER!='<p>&nbsp;</p>' GROUP BY QUESTION_ID,ANSWER_RIGHT";
        $pendings_query = $this->db->query($sql_stmt);
        $j=0;
       $k=0;
       $ttal=0;
       if($pendings_query->result()){
            foreach ($pendings_query->result() as $row_q) {
                if($row_q->ANSWER_RIGHT!=1){
                     $j++;         
                }else{
                  $k++;   
                }
                $ttal++;
            }
        }
        
        if ($query) {
            $row = $query->row();
            if ($row) {
                $mark = $row->R_MARK;
                $org_mark = $row->R_ORG_MARK;
                $total_question = $row->R_TOTAL_QUESTION;
                /*if ($mark > 0) {
                    $percentage = myMoney(($mark * 100) / $org_mark, 2);
                } else {
                    $percentage = 0;
                }*/
                $percentage = $row->R_RESULT;
                $module_name = $row->R_MODULES_NAME;
                $answer_date = $row->R_ANSWER_DATE;
                $obtained = (int) $row->R_RESULT;
                $html = '<div id="result">';
                $html .= $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                //$html .= $mark . ' ' . $org_mark . ' ' . $total_question . ' ' . $row->R_RESULT;
                //$html .= 'in module ' . $module_name . ' held on ' . $answer_date;
                 $html .='<br>Your total pending question is '.$ttal.'. Wrong answer '.$j.' and right answer '.$k;
                
                $html .= '</div>';
                //prepare text for sms
                $start_time = getSessionVar('start_time');
                $start_time = strtotime($start_time);
                $end_time = getSessionVar('end_time');
                $end_time = strtotime($end_time);
                $elpased = $end_time - $start_time;

				$time = $elpased;
				
				$seconds = $time % 60;
$time = ($time - $seconds) / 60;
$minutes = $time % 60;
$hours = ($time - $minutes) / 60;
$elapsed_time = $hours.':'.$minutes.':'.$seconds;
				
                $sms_text = $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                $sms_text .= 'in module ' . $module_name . ' held on ' . $answer_date . ' elapsed ' . $elapsed_time;
                $sms_text .= $this->config->item('CRLF');
                $sms_text .= 'From Q-Study';

                $all = getSessionVar('alltasks');
                //check if module has send_sms checked to send sms
                $load_module = $this->quiz_module->load($module_id);
                if ($load_module) {
                    $send_sms = $this->quiz_module->send_sms;
                } else {
                    $send_sms = 0;
                }
				
            } else {
                //$html = 'error occur in getting row. ' . $stmt;
                $mark = 0;
                $org_mark = 0;
                $total_question = 0;
                if ($mark > 0) {
                    $percentage = myMoney(($mark * 100) / $org_mark, 2);
                } else {
                    $percentage = 0;
                }
                $module_name = '';
                $answer_date = '';
                $obtained = 0;
                $html = '<div id="result">';
               // $html .= $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                //$html .= $mark . ' ' . $org_mark . ' ' . $total_question . ' ' . $row->R_RESULT;
                //$html .= 'in module ' . $module_name . ' held on ' . $answer_date;
                 $html .='<br>Your total pending question is '.$ttal.'. Wrong answer '.$j.' and right answer '.$k;
                
                $html .= '</div>';
                //prepare text for sms
                $start_time = getSessionVar('start_time');
                $start_time = strtotime($start_time);
                $end_time = getSessionVar('end_time');
                $end_time = strtotime($end_time);
                $elpased = $end_time - $start_time;
				
				$time = $elpased;	
				$seconds = $time % 60;
				$time = ($time - $seconds) / 60;
				$minutes = $time % 60;
				$hours = ($time - $minutes) / 60;
				$elapsed_time = $hours.':'.$minutes.':'.$seconds;
				
                $sms_text = $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                $sms_text .= 'in module ' . $module_name . ' held on ' . $answer_date . ' elapsed ' . $elapsed_time;
                $sms_text .= $this->config->item('CRLF');
                $sms_text .= 'From Q-Study';

                $all = getSessionVar('alltasks');
                //check if module has send_sms checked to send sms
                $load_module = $this->quiz_module->load($module_id);
                if ($load_module) {
                    $send_sms = $this->quiz_module->send_sms;
                } else {
                    $send_sms = 0;
                }
				
            }
			
			$html .= '<div class="result_error">';
                                $html .= $this->getErrorRevisionLists_opensorce_mod($student_year, $obtained, $send_sms);
                                $html .= '</div>';
			
        } else {
            $html = 'error in statement. ' . $stmt;
        }
		$this->mysmarty->assign('top_tutorial', '');
                	$this->mysmarty->assign('qcalculates', '');
	$this->mysmarty->assign('qtimes', '');
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        $this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('incl_form', 'incl_answer_complete_new.tpl');
        $this->mysmarty->assign('answer_complete', $html);
        $this->mysmarty->assign('instruction', '0');
    }

    function getAnswerResultSpecial() {
        $module_id = $this->uri->segment(4);
        $userid = getSessionVar('userid');
        //load user account
        $user_load = $this->user_account->load($userid);
        if ($user_load) {
            $first_name = $this->user_account->user_firstname;
            $last_name = $this->user_account->user_lastname;
            $user_name = $this->user_account->user_name;
            $parent_id = $this->user_account->parent_id;
            $student_year = $this->user_account->student_year;
        } else {
            $first_name = '';
            $last_name = '';
            $user_name = '';
            $parent_id = 0;
            $student_year = 0;
        }
        //load user parent
        if ($parent_id) {
            $parent_load = $this->user_account->load($parent_id);
            if ($parent_load) {
                $mobile_no = $this->user_account->mobile_no;
                $pfirst_name = $this->user_account->user_firstname;
                $plast_name = $this->user_account->user_lastname;
                $puser_name = $this->user_account->user_name;
            } else {
                $mobile_no = '';
                $pfirst_name = '';
                $plast_name = '';
                $puser_name = '';
            }
        } else {
            $mobile_no = '';
        }

        $stmt = "select * from SP_ANSWER_RESULT($userid, $module_id)";
        $query = $this->db->query($stmt);
        $sql_stmt = "select QUESTION_ID,ANSWER_RIGHT from QUEST_ANS_STUDENT WHERE USER_ID='".$userid."' AND MODULE_ID='".$module_id."' AND QUIZ_CATE=9 AND ANSWER!='<p>&nbsp;</p>' GROUP BY QUESTION_ID,ANSWER_RIGHT";
        $pendings_query = $this->db->query($sql_stmt);
        $j=0;
       $k=0;
       $ttal=0;
       if($pendings_query->result()){
            foreach ($pendings_query->result() as $row_q) {
                if($row_q->ANSWER_RIGHT!=1){
                     $j++;         
                }else{
                  $k++;   
                }
                $ttal++;
            }
        }
        $html ='';
        if ($query) {
            $row = $query->row();
            if ($row) {
                
                $mark = $row->R_MARK;
                $org_mark = $row->R_ORG_MARK;
                $total_question = $row->R_TOTAL_QUESTION;
                if ($mark > 0) {
                    $percentage = myMoney(($mark * 100) / $org_mark, 2);
                } else {
                    $percentage = 0;
                }
                
                $module_name = $row->R_MODULES_NAME;
                $answer_date = $row->R_ANSWER_DATE;
                $obtained = (int) $row->R_RESULT;
                $html .= '<div id="result">';
                $html .= $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                //$html .= $mark . ' ' . $org_mark . ' ' . $total_question . ' ' . $row->R_RESULT;
                //$html .= 'in module ' . $module_name . ' held on ' . $answer_date;
                $html .='<br>Your total pending question is '.$ttal.'. Wrong answer '.$j.' and right answer '.$k;
                $html .= '</div>';
                //prepare text for sms
                $start_time = getSessionVar('start_time');
                $start_time = strtotime($start_time);
                $end_time = getSessionVar('end_time');
                $end_time = strtotime($end_time);
                $elpased = $end_time - $start_time;
$time = $elpased;
				
				$seconds = $time % 60;
$time = ($time - $seconds) / 60;
$minutes = $time % 60;
$hours = ($time - $minutes) / 60;
$elapsed_time = $hours.':'.$minutes.':'.$seconds;
				
                $sms_text = $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                $sms_text .= 'in module ' . $module_name . ' held on ' . $answer_date . ' elapsed ' . $elapsed_time;
                $sms_text .= $this->config->item('CRLF');
                $sms_text .= 'From Q-Study';

                $all = getSessionVar('alltasks');
                //check if module has send_sms checked to send sms
                $load_module = $this->quiz_module->load($module_id);
                if ($load_module) {
                    $send_sms = $this->quiz_module->send_sms;
                } else {
                    $send_sms = 0;
                }
				
				
				
                if ($send_sms) {
                    $sms_send = $this->sms_list->isSMSSend($userid, $module_id);
                    if (!$all) {
                        if (!$sms_send) {
                            $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                            if ($smssend) {
                                $html .= '<div class="result_error">';
                                $html .= $this->getErrorRevisionSpecial($student_year, $obtained, $send_sms);
                                $html .= '</div>';
                            } else {
                                $html .= '<div class="result_error">';
                                $html .= $this->error_msg;
                                $html .= '</div>';
                            }
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionSpecial($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        }
                    } else {
                        $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                        if ($smssend) {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionSpecial($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->error_msg;
                            $html .= '</div>';
                        }
                    }
                } else {
                    $html .= '<div class="result_error">';
                    $html .= $this->getErrorRevisionSpecial($student_year, $obtained, $send_sms);
                    $html .= '</div>';
                }
            } else {
                
                //$html = 'error occur in getting row. ' . $stmt;
                $mark = 0;
                $org_mark = 0;
                $total_question = 0;
                if ($mark > 0) {
                    $percentage = myMoney(($mark * 100) / $org_mark, 2);
                } else {
                    $percentage = 0;
                }
                $module_name = '';
                $answer_date = '';
                $obtained = 0;
                $html .= '<div id="result">';
               // $html .= $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                //$html .= $mark . ' ' . $org_mark . ' ' . $total_question . ' ' . $row->R_RESULT;
                //$html .= 'in module ' . $module_name . ' held on ' . $answer_date;
            $html .='<br>Your total pending question is '.$ttal.'. Wrong answer '.$j.' and right answer '.$k;
                
                $html .= '</div>';
                //prepare text for sms
                $start_time = getSessionVar('start_time');
                $start_time = strtotime($start_time);
                $end_time = getSessionVar('end_time');
                $end_time = strtotime($end_time);
                $elpased = $end_time - $start_time;

				$time = $elpased;
				
				$seconds = $time % 60;
$time = ($time - $seconds) / 60;
$minutes = $time % 60;
$hours = ($time - $minutes) / 60;
$elapsed_time = $hours.':'.$minutes.':'.$seconds;
				
                $sms_text = $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                $sms_text .= 'in module ' . $module_name . ' held on ' . $answer_date . ' elapsed ' . $elapsed_time;
                $sms_text .= $this->config->item('CRLF');
                $sms_text .= 'From Q-Study';

                $all = getSessionVar('alltasks');
                //check if module has send_sms checked to send sms
                $load_module = $this->quiz_module->load($module_id);
                if ($load_module) {
                    $send_sms = $this->quiz_module->send_sms;
                } else {
                    $send_sms = 0;
                }
                if ($send_sms) {
                    $sms_send = $this->sms_list->isSMSSend($userid, $module_id);
                    if (!$all) {
                        if (!$sms_send) {
                            $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                            if ($smssend) {
                                $html .= '<div class="result_error">';
                                $html .= $this->getErrorRevisionSpecial($student_year, $obtained, $send_sms);
                                $html .= '</div>';
                            } else {
                                $html .= '<div class="result_error">';
                                $html .= $this->error_msg;
                                $html .= '</div>';
                            }
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionSpecial($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        }
                    } else {
                        $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                        if ($smssend) {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionSpecial($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->error_msg;
                            $html .= '</div>';
                        }
                    }
                } else {
                    $html .= '<div class="result_error">';
                    $html .= $this->getErrorRevisionSpecial($student_year, $obtained, $send_sms);
                    $html .= '</div>';
                }
            }
        } else {
            $html = 'error in statement. ' . $stmt;
        }
		$this->mysmarty->assign('top_tutorial', '');
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        $this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('incl_form', 'incl_answer_complete_new.tpl');
        $this->mysmarty->assign('answer_complete', $html);
        $this->mysmarty->assign('instruction', '0');
    }

    function getAnswerResult() {
        $module_id = $this->uri->segment(3);
        $userid = getSessionVar('userid');
        //load user account
        $user_load = $this->user_account->load($userid);
        if ($user_load) {
            $first_name = $this->user_account->user_firstname;
            $last_name = $this->user_account->user_lastname;
            $user_name = $this->user_account->user_name;
            $parent_id = $this->user_account->parent_id;
            $student_year = $this->user_account->student_year;
        } else {
            $first_name = '';
            $last_name = '';
            $user_name = '';
            $parent_id = 0;
            $student_year = 0;
        }
        //load user parent
        if ($parent_id) {
            $parent_load = $this->user_account->load($parent_id);
            if ($parent_load) {
                $mobile_no = $this->user_account->mobile_no;
                $pfirst_name = $this->user_account->user_firstname;
                $plast_name = $this->user_account->user_lastname;
                $puser_name = $this->user_account->user_name;
            } else {
                $mobile_no = '';
                $pfirst_name = '';
                $plast_name = '';
                $puser_name = '';
            }
        } else {
            $mobile_no = '';
        }

        $html = '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<li class="tool_logout">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<a href="javascript:fn_Logout()" class="main_logout"><span>Logout</span></a>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</li>' . $this->config->item('CRLF');

        $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<div id="canvas_div">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');

        $stmt = "select * from SP_ANSWER_RESULT($userid, $module_id)";
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $mark = $row->R_MARK;
                $org_mark = $row->R_ORG_MARK;
                $total_question = $row->R_TOTAL_QUESTION;
                if ($mark > 0) {
                    $percentage = myMoney(($mark * 100) / $org_mark, 2);
                } else {
                    $percentage = 0;
                }
                $module_name = $row->R_MODULES_NAME;
                $answer_date = $row->R_ANSWER_DATE;
                $obtained = (int) $row->R_RESULT;
                $html .= '<div id="result">';
                $html .= $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                //$html .= $mark . ' ' . $org_mark . ' ' . $total_question . ' ' . $row->R_RESULT;
                //$html .= 'in module ' . $module_name . ' held on ' . $answer_date;
                $html .= '</div>';
                //prepare text for sms
                $start_time = getSessionVar('start_time');
                $start_time = strtotime($start_time);
                $end_time = getSessionVar('end_time');
                $end_time = strtotime($end_time);
                $elpased = $end_time - $start_time;
				
				$time = $elpased;
				
				$seconds = $time % 60;
$time = ($time - $seconds) / 60;
$minutes = $time % 60;
$hours = ($time - $minutes) / 60;
$elapsed_time = $hours.':'.$minutes.':'.$seconds;

                $sms_text = $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                $sms_text .= 'in module ' . $module_name . ' held on ' . $answer_date . ' elapsed ' . $elapsed_time;
                $sms_text .= $this->config->item('CRLF');
                $sms_text .= 'From Q-Study';

                $all = getSessionVar('alltasks');
                //check if module has send_sms checked to send sms
                $load_module = $this->quiz_module->load($module_id);
                if ($load_module) {
                    $send_sms = $this->quiz_module->send_sms;
                } else {
                    $send_sms = 0;
                }
                if ($send_sms) {
                    $sms_send = $this->sms_list->isSMSSend($userid, $module_id);
                    if (!$all) {
                        if (!$sms_send) {
                            $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                            if ($smssend) {
                                $html .= '<div class="result_error">';
                                $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                                $html .= '</div>';
                            } else {
                                $html .= '<div class="result_error">';
                                $html .= $this->error_msg;
                                $html .= '</div>';
                            }
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        }
                    } else {
                        $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                        if ($smssend) {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->error_msg;
                            $html .= '</div>';
                        }
                    }
                } else {
                    $html .= '<div class="result_error">';
                    $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                    $html .= '</div>';
                }
            } else {
                //$html = 'error occur in getting row. ' . $stmt;
                $mark = 0;
                $org_mark = 0;
                $total_question = 0;
                if ($mark > 0) {
                    $percentage = myMoney(($mark * 100) / $org_mark, 2);
                } else {
                    $percentage = 0;
                }
                $module_name = '';
                $answer_date = '';
                $obtained = 0;
                $html .= '<div id="result">';
                $html .= $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                //$html .= $mark . ' ' . $org_mark . ' ' . $total_question . ' ' . $row->R_RESULT;
                //$html .= 'in module ' . $module_name . ' held on ' . $answer_date;
                $html .= '</div>';
                //prepare text for sms
                $start_time = getSessionVar('start_time');
                $start_time = strtotime($start_time);
                $end_time = getSessionVar('end_time');
                $end_time = strtotime($end_time);
                $elpased = $end_time - $start_time;

				$time = $elpased;
				$seconds = $time % 60;
				$time = ($time - $seconds) / 60;
				$minutes = $time % 60;
				$hours = ($time - $minutes) / 60;
				$elapsed_time = $hours.':'.$minutes.':'.$seconds;
				
                $sms_text = $first_name . ' obtained ' . $percentage . '% out of ' . $total_question . ' Question' . $this->config->item('CRLF');
                $sms_text .= 'in module ' . $module_name . ' held on ' . $answer_date . ' elapsed ' .$elapsed_time;
                $sms_text .= $this->config->item('CRLF');
                $sms_text .= 'From Q-Study';

                $all = getSessionVar('alltasks');
                //check if module has send_sms checked to send sms
                $load_module = $this->quiz_module->load($module_id);
                if ($load_module) {
                    $send_sms = $this->quiz_module->send_sms;
                } else {
                    $send_sms = 0;
                }
                if ($send_sms) {
                    $sms_send = $this->sms_list->isSMSSend($userid, $module_id);
                    if (!$all) {
                        if (!$sms_send) {
                            $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                            if ($smssend) {
                                $html .= '<div class="result_error">';
                                $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                                $html .= '</div>';
                            } else {
                                $html .= '<div class="result_error">';
                                $html .= $this->error_msg;
                                $html .= '</div>';
                            }
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        }
                    } else {
                        $smssend = $this->sendsms($mobile_no, $sms_text, $pfirst_name, $plast_name, $puser_name, $userid, $module_id);
                        if ($smssend) {
                            $html .= '<div class="result_error">';
                            $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                            $html .= '</div>';
                        } else {
                            $html .= '<div class="result_error">';
                            $html .= $this->error_msg;
                            $html .= '</div>';
                        }
                    }
                } else {
                    $html .= '<div class="result_error">';
                    $html .= $this->getErrorRevisionLists($student_year, $obtained, $send_sms);
                    $html .= '</div>';
                }
            }
        } else {
            $html = 'error in statement. ' . $stmt;
        }

        $html .= $this->config->item('two_tab') . '<div id="footer_holder_exam" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="fluid footer_buttons">' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div id="qstudy_footer" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<footer id="footer" class="fluid"> </footer>' . $this->config->item('CRLF');
        $html .= '</div>' . $this->config->item('CRLF');

        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        $this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        return $html;
    }

    function sendsms($asendto = '', $asmstext = '', $afirstname = '', $alastname = '', $ausername = '', $auserid = 0, $amoduleid = 0) {
        $load_sms = $this->sms_settings->load();
        if ($load_sms) {
            $apikey = $this->sms_settings->api_key;
            $api_secrect = $this->sms_settings->api_secrect;
            $sms_from = $this->sms_settings->sms_from;
        } else {
            $apikey = '';
            $api_secrect = '';
            $sms_from = '';
        }
        $sms_settings = $this->sms_settings->hasSettings();
        if ($sms_settings) {
            $this->load->library('MyNexmo');
            $this->mynexmo = new NexmoMessage($apikey, $api_secrect);
            // load library
            //$this->load->library('nexmo');
            // set response format: xml or json, default json
            //$this->nexmo->set_format('json');
            //set nexmo apikey and apisecret;
            //$this->nexmo->apikey = $apikey;
            //$this->nexmo->api_secret = $api_secrect;
			
			/* check if australia phn*/
			$substrs = substr($asendto,0,4);
			$rest = substr($asendto, 4);
			if($substrs=='+610'){
				$asendto = '+61'.$rest;
			}
			/* check if australia phn*/
			
            $ato = $asendto;
            $afrom = $sms_from;
            $atext = $asmstext;
            /* $message = array(
              'text' => $asmstext
              );
              print_r($message); */
            //var_dump($apikey . ' secret: ' . $api_secrect . ' from: ' . $sms_from . ' send to: ' . $asendto);
            $sms = $this->mynexmo->sendText($ato, $afrom, $atext);
            /* $response = $this->nexmo->send_message($sms_from, $asendto, $message);
              echo "<h1>Text Message</h1>";
              $this->nexmo->d_print($response);
              echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>"; */
            /* $response = $this->nexmo->get_balance();
              echo "<h1>Account - Get Balance</h1>";
              $this->nexmo->d_print($response);
              echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>"; */



            //set response
            $messagecount = '';
            $to = '';
            $messageprice = '';
            $status = '';
            $messageid = '';
            $remainingbalance = '';
            $network = '';
            $cost = '';
            $errortext = '';
            /* var_dump($sms);
			echo 'send to'.$ato;
			exit; */
            if ($sms) {
                $normal = array();
                foreach ($sms as $key => $value) {
                    if (!is_array($value)) {
                        $normal[] = array($key => $value);
                    } else {
                        foreach ($value as $valkey => $valValue) {
                            $normal[] = array($valkey => $valValue);
                        }
                    }
                }
                foreach ($normal as $nkey => $nval) {
                    foreach ($nval as $nvalkey => $nvalvalue) {
                        if (!is_object($nvalvalue)) {
                            switch ($nvalkey) {
                                case 'messagecount':
                                    $messagecount = $nvalvalue;
                                    break;
                                case 'to':
                                    $to = $nvalvalue;
                                    break;
                                case 'messageprice':
                                    $messageprice = $nvalvalue;
                                    break;
                                case 'status':
                                    $status = $nvalvalue;
                                    break;
                                case 'messageid':
                                    $messageid = $nvalvalue;
                                    break;
                                case 'remainingbalance':
                                    $remainingbalance = $nvalvalue;
                                    break;
                                case 'network':
                                    $network = $nvalvalue;
                                    break;
                                case 'cost':
                                    $cost = $nvalvalue;
                                    break;
                                case 'errortext':
                                    $errortext = $nvalvalue;
                                    break;
                            }
                            //echo '<br>';
                            //echo $nvalkey . ' = ' . $nvalvalue;
                        } else {
                            foreach ($nvalvalue as $nvalvaluekey => $nvalvalueValue) {
                                switch ($nvalvaluekey) {
                                    case 'messagecount':
                                        $messagecount = $nvalvalueValue;
                                        break;
                                    case 'to':
                                        $to = $nvalvalueValue;
                                        break;
                                    case 'messageprice':
                                        $messageprice = $nvalvalueValue;
                                        break;
                                    case 'status':
                                        $status = $nvalvalueValue;
                                        break;
                                    case 'messageid':
                                        $messageid = $nvalvalueValue;
                                        break;
                                    case 'remainingbalance':
                                        $remainingbalance = $nvalvalueValue;
                                        break;
                                    case 'network':
                                        $network = $nvalvalueValue;
                                        break;
                                    case 'cost':
                                        $cost = $nvalvalueValue;
                                        break;
                                    case 'errortext':
                                        $errortext = $nvalvalueValue;
                                        break;
                                }
                            }
                        }
                    }
                }
            } else {
                $errortext = $this->mynexmo->error_msg;
            }

            switch ($status) {
                case '0':
                    $status = 'Success';
                    break;
                case '1':
                    $status = 'You have exceeded the submission capacity allowed on this account, please back-off and retry';
                    break;
                case '2':
                    $status = 'Your request is incomplete and missing some mandatory parameters';
                    break;
                case '3':
                    $status = 'The value of one or more parameters is invalid';
                    break;
                case '4':
                    $status = 'The api_key / api_secret you supplied is either invalid or disabled';
                    break;
                case '5':
                    $status = 'An error has occurred in the nexmo platform whilst processing this message';
                    break;
                case '6':
                    $status = 'The Nexmo platform was unable to process this message, for example, an un-recognized number prefix';
                    break;
                case '7':
                    $status = 'The number you are trying to submit to is blacklisted and may not receive messages';
                    break;
                case '8':
                    $status = 'The api_key you supplied is for an account that has been barred from submitting messages';
                    break;
                case '9':
                    $status = 'Your pre-pay account does not have sufficient credit to process this message';
                    break;
                case '10':
                    $status = 'The number of simultaneous connections to the platform exceeds the capabilities of your account';
                    break;
                case '11':
                    $status = 'This account is not provisioned for REST submission, you should use SMPP instead';
                    break;
                case '12':
                    $status = 'Applies to Binary submissions, where the length of the UDH and the message body combined exceed 140 octets';
                    break;
                case '13':
                    $status = 'Message was not submitted because there was a communication failure';
                    break;
                case '14':
                    $status = 'Message was not submitted due to a verification failure in the submitted signature';
                    break;
                case '15':
                    $status = 'The sender address (from parameter) was not allowed for this message. Restrictions may apply depending on the destination';
                    break;
                case '16':
                    $status = 'The ttl parameter values is invalid';
                    break;
                case '19':
                    $status = 'Your request makes use of a facility that is not enabled on your account';
                    break;
                case '20':
                    $status = 'The message class value supplied was out of range (0 - 3)';
                    break;
            }
            $this->sms_list->id = -1;
            $this->sms_list->send_to_first_name = $afirstname;
            $this->sms_list->send_to_last_name = $alastname;
            $this->sms_list->send_to_user_name = $ausername;
            $this->sms_list->send_to_mobile = $asendto;
            $this->sms_list->remain_balance = $remainingbalance;
            $this->sms_list->cost = $messageprice;
            $this->sms_list->sms_status = $status;
            $this->sms_list->send_from = $sms_from;
            $this->sms_list->user_id = $auserid;
            $this->sms_list->module_id = $amoduleid;
            if ($errortext == '') {
                $save_sms = $this->sms_list->save();
                if ($save_sms) {
                    return true;
                } else {
                    $this->error_msg = $this->sms_list->error_msg;
                    return false;
                }
            } else {
                $this->error_msg = $errortext;
                return false;
            }
        } else {
            $this->error_msg = $this->sms_settings->error_msg;
            return false;
        }
        /* if ($errortext == '') {
          $array = array('result' => 't', 'to' => $to, 'status' => $status, 'error' => '', 'remain' => $remainingbalance);
          } else {
          $array = array('result' => 'f', 'to' => $to, 'status' => $status, 'error' => $errortext);
          }
          echo json_encode($array); */

        /*
          response
          messagecount = 1
          to = 8801714094710
          messageprice = 0.00700000
          status = 0
          messageid = 0300000011B8ADED
          remainingbalance = 1.87400000
          network = 47001
          cost = 0.007
          errortext
         */
    }
	
    function sendsms_parents($asendto = '', $asmstext = '', $afirstname = '', $alastname = '', $ausername = '', $auserid = 0, $amoduleid = 0) {

        $load_sms = $this->sms_settings->load();
        if ($load_sms) {
            $apikey = $this->sms_settings->api_key;
            $api_secrect = $this->sms_settings->api_secrect;
            $sms_from = $this->sms_settings->sms_from;
        } else {
            $apikey = '';
            $api_secrect = '';
            $sms_from = '';
        }
        $sms_settings = $this->sms_settings->hasSettings();
        if ($sms_settings) {
            $this->load->library('MyNexmo');
            $this->mynexmo = new NexmoMessage($apikey, $api_secrect);
           
			/* check if australia phn*/
			$substrs = substr($asendto,0,4);
			$rest = substr($asendto, 4);
			if($substrs=='+610'){
				$asendto = '+61'.$rest;
			}
			/* check if australia phn*/
			
            $ato = $asendto;
            $afrom = $sms_from;
            $atext = $asmstext;
            /* $message = array(
              'text' => $asmstext
              );
              print_r($message); */
            //var_dump($apikey . ' secret: ' . $api_secrect . ' from: ' . $sms_from . ' send to: ' . $asendto);
            $sms = $this->mynexmo->sendText($ato, $afrom, $atext);
            /* $response = $this->nexmo->send_message($sms_from, $asendto, $message);
              echo "<h1>Text Message</h1>";
              $this->nexmo->d_print($response);
              echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>"; */
            /* $response = $this->nexmo->get_balance();
              echo "<h1>Account - Get Balance</h1>";
              $this->nexmo->d_print($response);
              echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>"; */



            //set response
            $messagecount = '';
            $to = '';
            $messageprice = '';
            $status = '';
            $messageid = '';
            $remainingbalance = '';
            $network = '';
            $cost = '';
            $errortext = '';
            /* var_dump($sms);
			echo 'send to'.$ato;
			exit; */
            if ($sms) {
				$d= date("Y-m-d");
				/* new code for tracking sms */
				$this->db->query("INSERT INTO PARENTS_SMS(USER_ID,SEND_DATE)values(".$auserid.",".$d.")");
				/* new code for tracking sms */
				
                $normal = array();
                foreach ($sms as $key => $value) {
                    if (!is_array($value)) {
                        $normal[] = array($key => $value);
                    } else {
                        foreach ($value as $valkey => $valValue) {
                            $normal[] = array($valkey => $valValue);
                        }
                    }
                }
                foreach ($normal as $nkey => $nval) {
                    foreach ($nval as $nvalkey => $nvalvalue) {
                        if (!is_object($nvalvalue)) {
                            switch ($nvalkey) {
                                case 'messagecount':
                                    $messagecount = $nvalvalue;
                                    break;
                                case 'to':
                                    $to = $nvalvalue;
                                    break;
                                case 'messageprice':
                                    $messageprice = $nvalvalue;
                                    break;
                                case 'status':
                                    $status = $nvalvalue;
                                    break;
                                case 'messageid':
                                    $messageid = $nvalvalue;
                                    break;
                                case 'remainingbalance':
                                    $remainingbalance = $nvalvalue;
                                    break;
                                case 'network':
                                    $network = $nvalvalue;
                                    break;
                                case 'cost':
                                    $cost = $nvalvalue;
                                    break;
                                case 'errortext':
                                    $errortext = $nvalvalue;
                                    break;
                            }
                            //echo '<br>';
                            //echo $nvalkey . ' = ' . $nvalvalue;
                        } else {
                            foreach ($nvalvalue as $nvalvaluekey => $nvalvalueValue) {
                                switch ($nvalvaluekey) {
                                    case 'messagecount':
                                        $messagecount = $nvalvalueValue;
                                        break;
                                    case 'to':
                                        $to = $nvalvalueValue;
                                        break;
                                    case 'messageprice':
                                        $messageprice = $nvalvalueValue;
                                        break;
                                    case 'status':
                                        $status = $nvalvalueValue;
                                        break;
                                    case 'messageid':
                                        $messageid = $nvalvalueValue;
                                        break;
                                    case 'remainingbalance':
                                        $remainingbalance = $nvalvalueValue;
                                        break;
                                    case 'network':
                                        $network = $nvalvalueValue;
                                        break;
                                    case 'cost':
                                        $cost = $nvalvalueValue;
                                        break;
                                    case 'errortext':
                                        $errortext = $nvalvalueValue;
                                        break;
                                }
                            }
                        }
                    }
                }
            } else {
                $errortext = $this->mynexmo->error_msg;
            }

            switch ($status) {
                case '0':
                    $status = 'Success';
                    break;
                case '1':
                    $status = 'You have exceeded the submission capacity allowed on this account, please back-off and retry';
                    break;
                case '2':
                    $status = 'Your request is incomplete and missing some mandatory parameters';
                    break;
                case '3':
                    $status = 'The value of one or more parameters is invalid';
                    break;
                case '4':
                    $status = 'The api_key / api_secret you supplied is either invalid or disabled';
                    break;
                case '5':
                    $status = 'An error has occurred in the nexmo platform whilst processing this message';
                    break;
                case '6':
                    $status = 'The Nexmo platform was unable to process this message, for example, an un-recognized number prefix';
                    break;
                case '7':
                    $status = 'The number you are trying to submit to is blacklisted and may not receive messages';
                    break;
                case '8':
                    $status = 'The api_key you supplied is for an account that has been barred from submitting messages';
                    break;
                case '9':
                    $status = 'Your pre-pay account does not have sufficient credit to process this message';
                    break;
                case '10':
                    $status = 'The number of simultaneous connections to the platform exceeds the capabilities of your account';
                    break;
                case '11':
                    $status = 'This account is not provisioned for REST submission, you should use SMPP instead';
                    break;
                case '12':
                    $status = 'Applies to Binary submissions, where the length of the UDH and the message body combined exceed 140 octets';
                    break;
                case '13':
                    $status = 'Message was not submitted because there was a communication failure';
                    break;
                case '14':
                    $status = 'Message was not submitted due to a verification failure in the submitted signature';
                    break;
                case '15':
                    $status = 'The sender address (from parameter) was not allowed for this message. Restrictions may apply depending on the destination';
                    break;
                case '16':
                    $status = 'The ttl parameter values is invalid';
                    break;
                case '19':
                    $status = 'Your request makes use of a facility that is not enabled on your account';
                    break;
                case '20':
                    $status = 'The message class value supplied was out of range (0 - 3)';
                    break;
            }
            $this->sms_list->id = -1;
            $this->sms_list->send_to_first_name = $afirstname;
            $this->sms_list->send_to_last_name = $alastname;
            $this->sms_list->send_to_user_name = $ausername;
            $this->sms_list->send_to_mobile = $asendto;
            $this->sms_list->remain_balance = $remainingbalance;
            $this->sms_list->cost = $messageprice;
            $this->sms_list->sms_status = $status;
            $this->sms_list->send_from = $sms_from;
            $this->sms_list->user_id = $auserid;
            $this->sms_list->module_id = $amoduleid;
            if ($errortext == '') {
                $save_sms = $this->sms_list->save();
                if ($save_sms) {
                    return true;
                } else {
                    $this->error_msg = $this->sms_list->error_msg;
                    return false;
                }
            } else {
                $this->error_msg = $errortext;
                return false;
            }
        } else {
            $this->error_msg = $this->sms_settings->error_msg;
            return false;
        }
        /* if ($errortext == '') {
          $array = array('result' => 't', 'to' => $to, 'status' => $status, 'error' => '', 'remain' => $remainingbalance);
          } else {
          $array = array('result' => 'f', 'to' => $to, 'status' => $status, 'error' => $errortext);
          }
          echo json_encode($array); */

        /*
          response
          messagecount = 1
          to = 8801714094710
          messageprice = 0.00700000
          status = 0
          messageid = 0300000011B8ADED
          remainingbalance = 1.87400000
          network = 47001
          cost = 0.007
          errortext
         */
    }

    function getErrorRevisionLists($student_year = 0, $obtained = 0, $send_sms = 0) {
		
        $taskfor = $this->uri->segment(3);
        $module_id = $this->uri->segment(4);
        $hostid = $this->uri->segment(2);
        $userid = getSessionVar('userid');
        $stmt = "select * from SP_GET_ERROR_REVISION($userid, $module_id)";
        $query = $this->db->query($stmt);
        if ($query) {
            $html = '<ul>';
            $i = 0;
			$rt= 0;
			$rw_c = count($query->result());
            foreach ($query->result() as $row) {
				$r_rev_c = 0;
				$r_rev_c = $this->getErrorRevCount($module_id, $row->R_QUESTION_ID);
                $link = '/error-revision/' . $hostid . '/' . $taskfor . '/' . $module_id . '/' . $row->R_QUESTION_ID;
				if($r_rev_c<=3){
                $html .= '<li class="quiz_list_li_question_e"><a class="quiz_list_li_question_a_e" href="' . $link . '">' . $row->R_SL_NO . '</a>';
                $err_rev = $this->getErrorRevCount($module_id, $row->R_QUESTION_ID);
                if ($err_rev) {
                    $rev = $err_rev . ' time(s)';
                } else {
                    $rev = '';
                }
                $html .= '<div class="err_revisioned">' . $rev . '</div></li>';
				}else{
					$rt++;
				}
                $i++;
            }
			
            $html .= '</ul>';
            if ($i > 0) {
                $html .= '<div class="error_rev">You have wrong answer more than three times for this you have to repeat them tomorrow.</div>';
				if($rt==$rw_c){
					$next_link = '/my-recent-task/' . $taskfor;
                    $next = '<a href="' . $next_link . '" class="finish"><span>Finish</span></a>';
                        $html .= $next;
				}
            } else {
                $all = getSessionVar('alltasks');
                if (!$all) {
                    $module_up = $this->quiz_module->AddStudentInModule($module_id, $userid);
                    if ($module_up) {
                        $next_link = '/my-recent-task/' . $taskfor;
                        $next = '<a href="' . $next_link . '" class="finish"><span>Finish</span></a>';
                        $html .= $next;
                    } else {
                        $html .= $this->quiz_module->error_msg;
                    }
                } else {
                    $next_link = '/all-task/' . $taskfor;
                    $next = '<a href="' . $next_link . '" class="finish"><span>Finish</span></a>';
                    $html .= $next;
                }
            }
            //show the dialogue as per student year and errors
            $dialog = $this->dialogue->showDialogue($student_year, $obtained);
            if ($dialog) {
                $html .= $dialog;
            }

            if (!$send_sms) {
                $html .= '<div class="information">No SMS send as Response to SMS is not set.</div>';
            }
        }
        return $html;
    }
	
    function getErrorRevisionLists_opensorce_mod($student_year = 0, $obtained = 0, $send_sms = 0) {
        $taskfor = $this->uri->segment(3);
        $module_id = $this->uri->segment(4);
        $hostid = $this->uri->segment(2);
        $userid = getSessionVar('userid');
        $stmt = "select * from SP_GET_ERROR_REVISION($userid, $module_id)";
        $query = $this->db->query($stmt);
        if ($query) {
            $html = '<ul>';
            $i = 0;
			
            foreach ($query->result() as $row) {
                $link = '/error-revision-opensource-module/' . $hostid . '/' . $taskfor . '/' . $module_id . '/' . $row->R_QUESTION_ID;
                $html .= '<li class="quiz_list_li_question_e"><a class="quiz_list_li_question_a_e" href="' . $link . '">' . $row->R_SL_NO . '</a>';
                $err_rev = $this->getErrorRevCount($module_id, $row->R_QUESTION_ID);
                if ($err_rev) {
                    $rev = $err_rev . ' time(s)';
                } else {
                    $rev = '';
                }
                $html .= '<div class="err_revisioned">' . $rev . '</div></li>';
                $i++;
            }
            $html .= '</ul>';
            if ($i > 0) {
                $html .= '<div class="error_rev">In above question(s) you have answered wrong. Please error revision them.</div>';
            } else {
                $all = getSessionVar('alltasks');
                if (!$all) {
                    $module_up =1; //$this->quiz_module->AddStudentInModule($module_id, $userid); //student answered updatating off for opensource
                    if ($module_up) {
                        $next_link = '/all-task-opensouce/' . $taskfor;
                        $next = '<a href="' . $next_link . '" class="finish"><span>Finish</span></a>';
                        $html .= $next;
                    } else {
                        $html .= $this->quiz_module->error_msg;
                    }
                } else {
                    $next_link = '/all-task-opensouce/' . $taskfor;
                    $next = '<a href="' . $next_link . '" class="finish"><span>Finish</span></a>';
                    $html .= $next;
                }
            }
            //show the dialogue as per student year and errors
            $dialog = $this->dialogue->showDialogue($student_year, $obtained);
            if ($dialog) {
                $html .= $dialog;
            }

            if (!$send_sms) {
                $html .= '<div class="information">No SMS send as Response to SMS is not set.</div>';
            }
        }
        return $html;
    }

    function getErrorRevisionSpecial($student_year = 0, $obtained = 0, $send_sms = 0) {
       
        $hostid = $this->uri->segment(2);
        $taskfor = $this->uri->segment(3);
        $module_id = $this->uri->segment(4);
        $userid = getSessionVar('userid');
        $stmt = "select * from SP_GET_ERROR_REVISION($userid, $module_id)";
        $query = $this->db->query($stmt);
        if ($query) {
            $html = '<ul>';
            $i = 0;
            foreach ($query->result() as $row) {
                $link = '/error-revision-special/' . $hostid . '/' . $taskfor . '/' . $module_id . '/' . $row->R_QUESTION_ID;
				
	$q_count = $this->getErrorRevCount($module_id, $row->R_QUESTION_ID);
	if($q_count<=3){			
		$html .= '<li class="quiz_list_li_question_e"><a class="quiz_list_li_question_a_e" href="' . $link . '">' . $row->R_SL_NO . '</a>';
		$err_rev = $this->getErrorRevCount($module_id, $row->R_QUESTION_ID);
		if ($err_rev) {
			$rev = $err_rev . ' time(s)';
		} else {
			$rev = '';
		}
		$html .= '<div class="err_revisioned">' . $rev . '</div></li>';
		$i++;
	}
            }
            $html .= '</ul>';
			$rrr = getSessionVar('alltasks');

            if ($i > 0) {
                $html .= '<div class="error_rev">In above question(s) you have answered wrong. Please error revision them.</div>';
            } else {
                $all = getSessionVar('alltasks');
                if (!$all) {
                    $module_up = $this->quiz_module->AddStudentInModule($module_id, $userid);
                    if ($module_up) {
                        $next_link = '/special-recent-task/' . $taskfor;
                        $next = '<a href="' . $next_link . '" class="finish"><span>Finish</span></a>';
                        $html .= $next;
                    } else {
                        $html .= $this->quiz_module->error_msg;
                    }
                } else {
                    $next_link = '/special-recent-task/' . $taskfor;
                    $next = '<a href="' . $next_link . '" class="finish"><span>Finish</span></a>';
                    $html .= $next;
                }
            }
            //show the dialogue as per student year and errors
            $dialog = $this->dialogue->showDialogue($student_year, $obtained);
            if ($dialog) {
                $html .= $dialog;
            }

            if (!$send_sms) {
                $html .= '<div class="information">No SMS send as Response to SMS is not set.</div>';
            }
            /* if ($i == 0) {
              $module_up = $this->quiz_module->AddStudentInModule($module_id, $userid);
              if ($module_up) {
              $all = getSessionVar('alltasks');
              if (!$all) {
              $next_link = '/my-task/' . $taskfor;
              } else {
              $next_link = '/all-task/' . $taskfor;
              }
              $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
              $html .= $next;
              }
              } */
        }
        return $html;
    }

    function getErrorRevCount($amodule, $aquestion) {
        $stmt = "select * from SP_GET_ANS_ERR_REV_COUNT($amodule, $aquestion)";
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = $row->R_RESULT;
            } else {
                $return = '';
            }
        } else {
            $return = '';
        }
        return $return;
    }

    function getErrorRev($amodule, $aquestion) {
        $stmt = "select * from SP_GET_ANSWER_ERR_REV($amodule, $aquestion)";
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = $row->R_RESULT;
            } else {
                $return = '';
            }
        } else {
            $return = '';
        }
        return $return;
    }

    function showErrorRevisionNew() {
        
        $this->mysmarty->assign('top_tutorial', '');//new add
		$this->mysmarty->assign('next_submit', '');//new add
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        //$this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('question', '');
        $this->mysmarty->assign('quiz_score', '');


        $taskfor = $this->uri->segment(3);
        $module_id = $this->uri->segment(4);
        $question_id = $this->uri->segment(5);
        $hostid = $this->uri->segment(2);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
        //set answer as error revision
        $revision = $this->getErrorRev($module_id, $question_id);
        setSessionVar('error_revision', $revision);

        //set button
        //answer-complete/4/34
        $next_link = 'javascript:fn_error_revision(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $revision . ',0)';
        $elapse_link = 'fn_error_revision(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $revision . ',1)';
        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
$next_submit = '<a class="finish" href="'.$next_link.'"><span>Submit</span></a>';//rhm	
        //set time elapse for the question
        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }
        //display the quiz controls
        $this->DisplayAnswerBoard($quiz_id);
        //set timer in the question
        if ($time_elapse) {
            $html = $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
            $html .= '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
	
        //new added
    			
            $qinfo = $this->question_answer->getallinfo_question($question_id);
            $rqtimes = $qinfo->time_elapse;
            $rqtimes_formated = gmdate("H:i:s", $rqtimes);
            $this->mysmarty->assign('qtimes',$rqtimes);
            $this->mysmarty->assign('qcalculates',$qinfo->question_calculator);

            $top_prev = '<a class="back_button rsdontgoback_backbtns" href="#">Back</a>';//rhm
            $top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
           $top_tutorial = '<a class="finish rsdontgoback" href="/my-task/' . $taskfor .'"><span>Index </span></a>';

            /*changes*/
            $this->mysmarty->assign('next_submit',@$next_submit ); //rhm submit btn
            $this->mysmarty->assign('top_tutorial',@$top_tutorial ); //rhm top_tutorial btn
            $this->mysmarty->assign('top_prev',@$top_prev ); //rhm top_tutorial btn
            $this->mysmarty->assign('top_next',@$top_next ); //rhm top_tutorial btn
            /*changes*/
        //new added
        
        
        
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        $this->mysmarty->assign('quiz_score', '');
        $this->mysmarty->assign('script', '');
        //$this->mysmarty->assign('quiz_id', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $next . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', $time_elapse);
        $this->mysmarty->assign('timerelapse', $html);
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        //$this->mysmarty->assign('incl_form', 'incl_answer_complete_new.tpl');        
    }
	
    function showErrorRevisionNew_opensource_module() {
        
        $this->mysmarty->assign('top_tutorial', '');//new add
		$this->mysmarty->assign('next_submit', '');//new add
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        //$this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('question', '');
        $this->mysmarty->assign('quiz_score', '');


        $taskfor = $this->uri->segment(3);
        $module_id = $this->uri->segment(4);
        $question_id = $this->uri->segment(5);
        $hostid = $this->uri->segment(2);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
        //set answer as error revision
        $revision = $this->getErrorRev($module_id, $question_id);
        setSessionVar('error_revision', $revision);

        //set button
        //answer-complete/4/34
        $next_link = 'javascript:fn_error_revision_opensource_modules(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $revision . ',0)';
        $elapse_link = 'fn_error_revision_opensource_modules(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $revision . ',1)';
        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
$next_submit = '<a class="finish" href="'.$next_link.'"><span>Submit</span></a>';//rhm	
        //set time elapse for the question
        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }
        //display the quiz controls
        $this->DisplayAnswerBoard($quiz_id);
        //set timer in the question
        if ($time_elapse) {
            $html = $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
            $html .= '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
	
        //new added
    			
            $qinfo = $this->question_answer->getallinfo_question($question_id);
            $rqtimes = $qinfo->time_elapse;
            $rqtimes_formated = gmdate("H:i:s", $rqtimes);
            $this->mysmarty->assign('qtimes',$rqtimes);
            $this->mysmarty->assign('qcalculates',$qinfo->question_calculator);

            $top_prev = '<a class="back_button rsdontgoback_backbtns" href="javascript: history.go(-1)">Back</a>';//rhm
            $top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
           $top_tutorial = '<a class="finish" href="/all-task-opensouce/' . $taskfor .'"><span>Index </span></a>';

            /*changes*/
            $this->mysmarty->assign('next_submit',@$next_submit ); //rhm submit btn
            $this->mysmarty->assign('top_tutorial',@$top_tutorial ); //rhm top_tutorial btn
            $this->mysmarty->assign('top_prev',@$top_prev ); //rhm top_tutorial btn
            $this->mysmarty->assign('top_next',@$top_next ); //rhm top_tutorial btn
            /*changes*/
        //new added
        
        
        
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        $this->mysmarty->assign('quiz_score', '');
        $this->mysmarty->assign('script', '');
        //$this->mysmarty->assign('quiz_id', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $next . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', $time_elapse);
        $this->mysmarty->assign('timerelapse', $html);
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        //$this->mysmarty->assign('incl_form', 'incl_answer_complete_new.tpl');        
    }

    function showErrorRevisionSpecial() {
        $this->mysmarty->assign('top_tutorial', '');//new add
		$this->mysmarty->assign('next_submit', '');//new add
        // new add by rs
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_id', '');
        //$this->mysmarty->assign('incl_form_answer', '');
        $this->mysmarty->assign('quiz_category', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('question', '');
        $this->mysmarty->assign('quiz_score', '');

        $hostid = $this->uri->segment(2);
        $taskfor = $this->uri->segment(3);
        $module_id = $this->uri->segment(4);
        $question_id = $this->uri->segment(5);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
        //set answer as error revision
        $revision = $this->getErrorRev($module_id, $question_id);
        setSessionVar('error_revision', $revision);

        //set button
        //answer-complete/4/34
        $next_link = 'javascript:fn_error_revision_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $revision . ',0)';
        $elapse_link = 'fn_error_revision_special(' . $hostid . ',' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $revision . ',1)';
        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';
$next_submit = '<a class="finish" href="'.$next_link.'"><span>Submit</span></a>';//rhm	
        //set time elapse for the question
        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }
        //display the quiz controls
        $this->DisplayAnswerBoard($quiz_id);
        //set timer in the question
        if ($time_elapse) {
            $html = $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
            $html .= '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
        
        //new added

            $qinfo = $this->question_answer->getallinfo_question($question_id);
            $rqtimes = $qinfo->time_elapse;
            $rqtimes_formated = gmdate("H:i:s", $rqtimes);
            $this->mysmarty->assign('qtimes',$rqtimes);
            $this->mysmarty->assign('qcalculates',$qinfo->question_calculator);

            $top_prev = '<a class="back_button rsdontgoback_backbtns" href="#">Back</a>';//rhm
            $top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
            $top_tutorial = '<a class="finish rsdontgoback" href="/special-recent-task/' . $taskfor.'"><span>Index</span></a>';

            /*changes*/
            $this->mysmarty->assign('next_submit',@$next_submit ); //rhm submit btn
            $this->mysmarty->assign('top_tutorial',@$top_tutorial ); //rhm top_tutorial btn
            $this->mysmarty->assign('top_prev',@$top_prev ); //rhm top_tutorial btn
            $this->mysmarty->assign('top_next',@$top_next ); //rhm top_tutorial btn
            /*changes*/
        //new added
        
        
        
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        $this->mysmarty->assign('quiz_score', '');
        $this->mysmarty->assign('script', '');
        //$this->mysmarty->assign('quiz_id', '');
        $this->mysmarty->assign('equation_style', '');
        $this->mysmarty->assign('equation_view', '');
        $this->mysmarty->assign('user_type', '');
        $this->mysmarty->assign('time_elapse', '');
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $next . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('time_elapse', $time_elapse);
        $this->mysmarty->assign('timerelapse', $html);
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        //$this->mysmarty->assign('incl_form', 'incl_answer_complete_new.tpl');        
    }

    function showErrorRevision() {
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $question_id = $this->uri->segment(4);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
        //set answer as error revision
        $revision = $this->getErrorRev($module_id, $question_id);
        setSessionVar('error_revision', $revision);

        //set button
        //answer-complete/4/34
        $next_link = 'javascript:fn_error_revision(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $revision . ',0)';
        $elapse_link = 'fn_error_revision(' . $taskfor . ',' . $module_id . ',' . $question_id . ',' . $revision . ',1)';
        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';

        //set time elapse for the question
        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $stmt = 'select * from SP_GET_WHITEBOARD_COUNT(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $hasWhiteBoard = $row->R_RESULT;
            } else {
                $hasWhiteBoard = '';
            }
        } else {
            $hasWhiteBoard = '';
        }

        $html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
        $html .= '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
        if ($hasWhiteBoard) {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date" id="li_date"></li>' . $this->config->item('CRLF');
        } else {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
        }
        //first get the top toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_H(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }
        $html .= $this->config->item('two_tab') . '<li class="tool_logout">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<a href="javascript:fn_Logout()" class="main_logout"><span>Logout</span></a>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</li>' . $this->config->item('CRLF');

        $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
        //$html .= $this->config->item('one_tab') . '<div class="pin" title="Hide Tool Panel" id="pin_tool" onclick="fn_pin_tool()">pin</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<div id="canvas_div">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');

        //only for arrow
        $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //get the vertical toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_V(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        //display the quiz controls
        $this->DisplayQuizControls($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        //end of display quiz controls
        $html .= $this->config->item('two_tab') . '<div id="footer_holder_exam" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="fluid footer_buttons">' . $this->config->item('CRLF');

        $quiz_option = $this->getQuizOption($quiz_id);

        //$save_uri = '\'quiz-save-data\',\'f_quiz\'';
        if ($question_id == '-1') {
            $a_controller = "'" . '/quiz-save-data/' . $quiz_id . "'," . "'f_quiz'," . $quiz_id . ',' . $quiz_option;
        } else {
            $a_controller = "'" . '/quiz-edit-data/' . $quiz_id . '/' . $question_id . "'," . "'f_quiz'," .
                    $quiz_id . ',' . $quiz_option . ',' . $question_id;
        }
        /*
          $html .= $this->config->item('four_tab') . '<table class="quiz_answer">' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '<tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $next . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '</tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('four_tab') . '</table>' . $this->config->item('CRLF');
         */
        $html .= $this->config->item('six_tab') . $next . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="validation-error" id="td_error" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div id="qstudy_footer" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '<div id="pages" class="fluid">Page' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="current_page" id="curr_page">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="page_of">of</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="num_page" id="page_num">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_plus" title="Add new page" onclick="fn_addnewpage()"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_minus" title="Remove Current Page" id="remove_page" onclick="fn_removepage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_next" title="Next Page" id="next_page" onclick="fn_nextpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_prev" title="Previous Page" id="prev_page" onclick="fn_prevpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<footer id="footer" class="fluid"> </footer>' . $this->config->item('CRLF');
        $html .= '</div>' . $this->config->item('CRLF');

        $this->mysmarty->assign('script', $this->makeCSS($quiz_id));

        //set timer in the question
        if ($time_elapse) {
            $html .= $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
            $html .= '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        }

        $html .= '</form>';
        return $html;
    }

    function showRepeatedBoardNew() {
		 $html = '';
        //new fix bug
        $this->mysmarty->assign('top_tutorial','');
        $this->mysmarty->assign('qcalculates','');
        $this->mysmarty->assign('qtimes','');
        $this->mysmarty->assign('next_submit','');
        //new bug fix
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_score', '');
        $this->mysmarty->assign('instruction', '');
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $question_id = $this->uri->segment(4);
        $questionfor_id = $this->uri->segment(5);
        $repeateddate = $this->uri->segment(6);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        //set button
        //answer-complete/4/34
        $next_link = 'javascript:fn_repeat(' . $taskfor . ',' . $module_id . ','
                . $question_id . ',' . $questionfor_id . ',' . $userid . ',\'' . $repeateddate . '\',0)';
        $elapse_link = 'fn_repeat(' . $taskfor . ',' . $module_id . ','
                . $question_id . ',' . $questionfor_id . ',' . $userid . ',\'' . $repeateddate . '\',1)';
        $next = '<td><a href="' . $next_link . '" class="finish"><span>Submit</span></a></td>';

        $back = '<td><a href="/my-recent-task/' . $taskfor . '" class="back_button">Back</a></td>';

        //set time elapse for the question
        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }
        //display the quiz controls
        $this->DisplayAnswerBoard($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        //set timer in the question
        if ($time_elapse) {
            $html .= $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
            $html .= '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
		$this->mysmarty->assign('next_submit', $next ); 
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', $back);
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('timerelapse', $html);
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', $time_elapse);
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('quiz_score', '');
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
		
    }

    function showRepeatedBoard() {
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $question_id = $this->uri->segment(4);
        $questionfor_id = $this->uri->segment(5);
        $repeateddate = $this->uri->segment(6);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        //set button
        //answer-complete/4/34
        $next_link = 'javascript:fn_repeat(' . $taskfor . ',' . $module_id . ','
                . $question_id . ',' . $questionfor_id . ',' . $userid . ',\'' . $repeateddate . '\',0)';
        $elapse_link = 'fn_repeat(' . $taskfor . ',' . $module_id . ','
                . $question_id . ',' . $questionfor_id . ',' . $userid . ',\'' . $repeateddate . '\',1)';
        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';

        $back = '<a href="/my-task/' . $taskfor . '" class="back_button">Back</a>';
        ;
        //set time elapse for the question
        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $stmt = 'select * from SP_GET_WHITEBOARD_COUNT(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $hasWhiteBoard = $row->R_RESULT;
            } else {
                $hasWhiteBoard = '';
            }
        } else {
            $hasWhiteBoard = '';
        }

        $html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
        $html .= '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
        if ($hasWhiteBoard) {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date" id="li_date"></li>' . $this->config->item('CRLF');
        } else {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
        }
        //first get the top toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_H(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }
        $html .= $this->config->item('two_tab') . '<li class="tool_logout">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<a href="javascript:fn_Logout()" class="main_logout"><span>Logout</span></a>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</li>' . $this->config->item('CRLF');

        $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
        //$html .= $this->config->item('one_tab') . '<div class="pin" title="Hide Tool Panel" id="pin_tool" onclick="fn_pin_tool()">pin</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<div id="canvas_div">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');

        //only for arrow
        $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //get the vertical toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_V(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        //display the quiz controls
        $this->DisplayQuizControls($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        //end of display quiz controls
        $html .= $this->config->item('two_tab') . '<div id="footer_holder_exam" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="fluid footer_buttons">' . $this->config->item('CRLF');

        $quiz_option = $this->getQuizOption($quiz_id);

        //$save_uri = '\'quiz-save-data\',\'f_quiz\'';
        if ($question_id == '-1') {
            $a_controller = "'" . '/quiz-save-data/' . $quiz_id . "'," . "'f_quiz'," . $quiz_id . ',' . $quiz_option;
        } else {
            $a_controller = "'" . '/quiz-edit-data/' . $quiz_id . '/' . $question_id . "'," . "'f_quiz'," .
                    $quiz_id . ',' . $quiz_option . ',' . $question_id;
        }
        /*
          $html .= $this->config->item('four_tab') . '<table class="quiz_answer">' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '<tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $back . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $next . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '</tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('four_tab') . '</table>' . $this->config->item('CRLF');
         */
        $html .= $this->config->item('six_tab') . $next . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="validation-error" id="td_error" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div id="qstudy_footer" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '<div id="pages" class="fluid">Page' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="current_page" id="curr_page">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="page_of">of</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="num_page" id="page_num">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_plus" title="Add new page" onclick="fn_addnewpage()"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_minus" title="Remove Current Page" id="remove_page" onclick="fn_removepage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_next" title="Next Page" id="next_page" onclick="fn_nextpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_prev" title="Previous Page" id="prev_page" onclick="fn_prevpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<footer id="footer" class="fluid"> </footer>' . $this->config->item('CRLF');
        $html .= '</div>' . $this->config->item('CRLF');

        $this->mysmarty->assign('script', $this->makeCSS($quiz_id));

        //set timer in the question
        if ($time_elapse) {
            $html .= $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
            $html .= '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        }

        $html .= '</form>';
        return $html;
    }

    function getAnswerDetailByModule($amodule = 0, $auserid = 0) {
        $stmt = "select * from SP_GET_ANS_DETAILS($amodule, $auserid)";
        $query = $this->db->query($stmt);
        if ($query) {
            $html = '<ul class="quiz_table_ul">' . $this->config->item('CRLF');
            $html .= '<div class="testBody">' . $this->config->item('CRLF');
            foreach ($query->result() as $row) {
                $html .= '<li class="quiz_table_row_li">' . $this->config->item('CRLF');
                $html .= '<span style="width:150px">' . $row->R_SEND_TO_FIRST_NAME . '</span>' . $this->config->item('CRLF');
                $html .= '<span style="width:250px">' . $row->R_SEND_TO_LAST_NAME . '</span>' . $this->config->item('CRLF');

                $html .= '<span style="width:250px">' . $row->R_SEND_TO_USER_NAME . '</span>' . $this->config->item('CRLF');
                $html .= '<span style="width:250px">' . $row->R_SEND_ON . '</span>' . $this->config->item('CRLF');
                $html .= '<span style="width:250px">' . $row->R_REMAIN_BALANCE . '</span>' . $this->config->item('CRLF');
                //$html .= '<span style="width:250px">' . $row->R_COST . '</span>' . $this->config->item('CRLF');
                $html .= '<span style="width:250px">' . $row->R_SMS_STATUS . '</span>' . $this->config->item('CRLF');
                $html .= '</li>' . $this->config->item('CRLF');
            }
            $html .= '</div>' . $this->config->item('CRLF');
            $html .= '</ul>' . $this->config->item('CRLF');
        }
    }

    function ResetQuizIds() {
        $this->getQuizIdShow();
        var_dump($this->quiz_ids);
        array_splice($this->quiz_ids, 0, 1);
        //return $this->quiz_ids;
    }

    function getStudentAnswer($userid, $module_id, $question_id) {
        $stmt = "select * from SP_GET_STUDENT_ANSWER($userid, $module_id, $question_id)";
        //only for blob fields
        $dbh = ibase_connect($this->config->item('DB_DATABASE'), $this->config->item('DB_USER'), $this->config->item('DB_PASS'));

        // Get result
        $result = ibase_query($dbh, $stmt);

        // Loop through results
        $return = array();
        while ($row = ibase_fetch_object($result, IBASE_TEXT)) {
            $return = array('answer' => $row->R_ANSWER, 'workout' => $row->R_WORKOUT, 'workout_draw' => $row->R_WORKOUT_DRAW);
        }
        ibase_free_result($result);
        ibase_close($dbh);
        return $return;
    }

    function showAnswerBoardToolsNew() {
        setSessionVar('alltasks', '');
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $question_id = $this->uri->segment(4);
        $prev_question = $this->uri->segment(5);
        $next_start = getSessionVar('next_start');
        $this->getQuizIdShow();
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
        $this->mysmarty->assign('time_elapse', '');

        $userid = $this->uri->segment(6);
		//$r = $this->db->query("select * from QUEST_ANS_STUDENT where MODULE_ID=634")->row();
		//print_r($r);exit;
        $student_answer = $this->getStudentAnswer($userid, $module_id, $question_id);
       
		
        $this->mysmarty->assign('answer', @$student_answer['answer']);
        $this->mysmarty->assign('workout', @$student_answer['workout']);
        $this->mysmarty->assign('workout_draw', @$student_answer['workout_draw']);
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $quiz_name = '';
        }

        $next_ques = getSessionVar('next_id');
        if (!$next_ques) {
            if (count($this->quiz_ids) > 1) {
                array_splice($this->quiz_ids, 0, 1);
                $next_ques = $this->quiz_ids[0];
            } else {
                $next_ques = '';
            }
            $this->getQuizIdShow();
            $mquest_ids = implode(',', $this->quiz_ids);
        } else {
            $quiz_ids = getSessionVar('quiz_ids');
            if (count($quiz_ids) > 1) {
                $mquest_ids = implode(',', $quiz_ids);
                $next_ques = getSessionVar('next_id');
            } else {
                $next_ques = '';
            }
        }

        $mbackquest_ids = implode(',', $this->back_quiz_ids);

        if ($next_ques) {
            $next_link = 'javascript:fn_next_question_answer(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' .
                    $question_id . ',' . $userid . ',\'' . $mquest_ids . '\',0)';
            $next = '<a href="' . $next_link . '" class="next_button">Next</a>';
            
            $save_linksval = 'javascript:fn_save_progress_byteacher(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' .
                    $question_id . ',' . $userid . ',\'' . $mquest_ids . '\',1)';
            $save = '<a href="' . $save_linksval . '" class="save">Save</a>';
            
            
        } else {
            $next = '';
            $mquest_ids = 0;  
            $next_ques = 0;
            $save_linksval = 'javascript:fn_save_progress_byteacher(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' .
                    $question_id . ',' . $userid . ',\'' . $mquest_ids . '\',0)';
            $save = '<a href="' . $save_linksval . '" class="save">Save</a>';
        }

        $submit_link = '/view-progress';
        $submit = '<a href="' . $submit_link . '" class="finish">Finish</a>';
		$rsback = '<td><a class="back_button" href="javascript: history.go(-1)">Back</a></td>';
		$back = '<td><a href="' . $submit_link . '" class="backbutton">Index</a></td>';
        if ($next) {

            $next = '<td>' . $next . '</td>';
            $save = '<td>' . $save . '</td>';
           
            $this->mysmarty->assign('preview_link', $rsback . $back . $next . $save);
        } else {
            $submit = '<td>' . $submit . '</td>';
            $save = '<td>' . $save . '</td>';
            $this->mysmarty->assign('preview_link', $rsback . $back. $submit . $save);
        }
		
        $this->mysmarty->assign('next_submit', '');/* new one */
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        $this->mysmarty->assign('script', '');
        $this->DisplayAnswerBoard($quiz_id, 't');
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        //$this->mysmarty->assign('quiz_score', $this->getQuestionsScorePreview());
        $this->mysmarty->assign('quiz_score', $this->getQuestionsScorePreview_newprogrss());
        //for pending number dialog
        /* $html .= '<div id="dlg_pending" style="display:none">';
          $html .= '<div id="pending"></div>';
          $html .= '</div>'; */
    }

    function showAnswerBoardToolsNew_studentProgress() {
        setSessionVar('alltasks', '');
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $question_id = $this->uri->segment(4);
        $prev_question = $this->uri->segment(5);
        $next_start = getSessionVar('next_start');
        $this->getQuizIdShow();
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
        $this->mysmarty->assign('time_elapse', '');

        $userid = $this->uri->segment(6);
        $student_answer = $this->getStudentAnswer($userid, $module_id, $question_id);
        //print_r($student_answer);exit;
        $this->mysmarty->assign('answer', $student_answer['answer']);
        $this->mysmarty->assign('workout', $student_answer['workout']);
        $this->mysmarty->assign('workout_draw', $student_answer['workout_draw']);
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $quiz_name = '';
        }

        $next_ques = getSessionVar('next_id');
        if (!$next_ques) {
            if (count($this->quiz_ids) > 1) {
                array_splice($this->quiz_ids, 0, 1);
                $next_ques = $this->quiz_ids[0];
            } else {
                $next_ques = '';
            }
            $this->getQuizIdShow();
            $mquest_ids = implode(',', $this->quiz_ids);
        } else {
            $quiz_ids = getSessionVar('quiz_ids');
            if (count($quiz_ids) > 1) {
                $mquest_ids = implode(',', $quiz_ids);
                $next_ques = getSessionVar('next_id');
            } else {
                $next_ques = '';
            }
        }

        $mbackquest_ids = implode(',', $this->back_quiz_ids);

        if ($next_ques) {
            $next_link = 'javascript:fn_next_question_answer_student_view(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' .
                    $question_id . ',' . $userid . ',\'' . $mquest_ids . '\',0)';
            $next = '<a href="' . $next_link . '" class="next_button">Next</a>';
           
            
            
        } else {
            $next = ''; 
        }

        $submit_link = '/student-progress';
        $submit = '<a href="' . $submit_link . '" class="finish">Finish</a>';
		$rsback = '<td><a class="back_button" href="javascript: history.go(-1)">Back</a></td>';
		$back = '<td><a href="' . $submit_link . '" class="backbutton">Index</a></td>';
        if ($next) {

            $next = '<td>' . $next . '</td>';
           
            $this->mysmarty->assign('preview_link', $rsback . $back . $next);
        } else {
            $submit = '<td>' . $submit . '</td>';
            $this->mysmarty->assign('preview_link', $rsback . $back. $submit);
        }
		
        $this->mysmarty->assign('next_submit', '');/* new one */
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('timerelapse', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
        $this->mysmarty->assign('script', '');
        $this->DisplayAnswerBoard($quiz_id, 't');
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        //$this->mysmarty->assign('quiz_score', $this->getQuestionsScorePreview());
        $this->mysmarty->assign('quiz_score', $this->getQuestionsScorePreview_newprogrss());
        //for pending number dialog
        /* $html .= '<div id="dlg_pending" style="display:none">';
          $html .= '<div id="pending"></div>';
          $html .= '</div>'; */
    }

    function showAnswerBoardTools() {
        setSessionVar('alltasks', '');
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $question_id = $this->uri->segment(4);
        $prev_question = $this->uri->segment(5);
        $next_start = getSessionVar('next_start');
        $this->getQuizIdShow();
        //var_dump($next_start);
        /* if (!$next_start){
          $this->getQuizIdShow();
          $next_ques = $this->quiz_ids[1];
          }  else {
          $this->getQuizIdShow();
          array_splice($this->quiz_ids, 0, 1);
          var_dump($this->quiz_ids);
          $next_ques = $this->quiz_ids[1];
          } */
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());
        $this->mysmarty->assign('time_elapse', '');



        $userid = $this->uri->segment(6);
        $student_answer = $this->getStudentAnswer($userid, $module_id, $question_id);
        $this->mysmarty->assign('answer', $student_answer['answer']);
        $this->mysmarty->assign('workout', $student_answer['workout']);
        $this->mysmarty->assign('workout_draw', $student_answer['workout_draw']);
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }

        $next_ques = getSessionVar('next_id');
        if (!$next_ques) {
            if (count($this->quiz_ids) > 1) {
                array_splice($this->quiz_ids, 0, 1);
                $next_ques = $this->quiz_ids[0];
            } else {
                $next_ques = '';
            }
            $this->getQuizIdShow();
            $mquest_ids = implode(',', $this->quiz_ids);
        } else {
            $quiz_ids = getSessionVar('quiz_ids');
            if (count($quiz_ids) > 1) {
                $mquest_ids = implode(',', $quiz_ids);
                $next_ques = getSessionVar('next_id');
            } else {
                $next_ques = '';
            }
        }

        $mbackquest_ids = implode(',', $this->back_quiz_ids);

        if ($next_ques) {
            $next_link = 'javascript:fn_next_question_answer(' . $taskfor . ',' . $module_id . ',' . $next_ques . ',' .
                    $question_id . ',' . $userid . ',\'' . $mquest_ids . '\',0)';
            $next = '<a href="' . $next_link . '" class="next_button">Next</a>';
        } else {
            $next = '';
        }
        /* $back_id = getSessionVar('back_id');
          if (!$back_id) {
          $back = '';
          } else {
          $back_link = '/show-answer/' . $taskfor . '/' . $module_id . '/' . $back_id . '/' . $question_id . '/' . $userid;
          //$back_link = 'javascript:fn_back_question_answer(' . $taskfor . ',' . $module_id . ',' . $back_id . ',' . $question_id . ',' . $userid . ',\'' . $mquest_ids . '\')';
          $back = '<a href="' . $back_link . '" class="back_button">Back</a>';
          } */

        $submit_link = '/view-progress';
        $submit = '<a href="' . $submit_link . '" class="finish">Submit</a>';

        $stmt = 'select * from SP_GET_WHITEBOARD_COUNT(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $hasWhiteBoard = $row->R_RESULT;
            } else {
                $hasWhiteBoard = '';
            }
        } else {
            $hasWhiteBoard = '';
        }

        $html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
        $html .= '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
        if ($hasWhiteBoard) {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date" id="li_date"></li>' . $this->config->item('CRLF');
        } else {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
        }
        //first get the top toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_H(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }
        $html .= $this->config->item('two_tab') . '<li class="tool_logout">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<a href="javascript:fn_Logout()" class="main_logout"><span>Logout</span></a>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</li>' . $this->config->item('CRLF');

        $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<div id="canvas_div">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');

        //only for arrow
        $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //get the vertical toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_V(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="rightside" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="fluid rightside_score_progress" id="score">' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '<div class="fluid math_tool_progress" id="math_button">Score' . $this->config->item('CRLF');
        //$html .= $this->config->item('four_tab') . $this->getQuestionsScoreShow() . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . $this->getQuestionsScorePreview() . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');

        //display the quiz controls
        $this->DisplayQuizControls($quiz_id, 't');
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        $html .= $this->config->item('two_tab') . '<div id="footer_holder_exam" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="fluid footer_buttons">' . $this->config->item('CRLF');

        $quiz_option = $this->getQuizOption($quiz_id);
        /*
          $html .= $this->config->item('four_tab') . '<table class="quiz_answer">' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '<tr>' . $this->config->item('CRLF');
          //$html .= $this->config->item('six_tab') . '<td class="input-box">' . $back . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $next . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $submit . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '</tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('four_tab') . '</table>' . $this->config->item('CRLF');
         */
        $html .= $this->config->item('six_tab') . $next . $this->config->item('CRLF');
        $html .= $this->config->item('six_tab') . $submit . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div class="validation-error" id="td_error" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div id="qstudy_footer" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '<div id="pages" class="fluid">Page' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="current_page" id="curr_page">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="page_of">of</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="num_page" id="page_num">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_plus" title="Add new page" onclick="fn_addnewpage()"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_minus" title="Remove Current Page" id="remove_page" onclick="fn_removepage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_next" title="Next Page" id="next_page" onclick="fn_nextpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_prev" title="Previous Page" id="prev_page" onclick="fn_prevpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<footer id="footer" class="fluid"> </footer>' . $this->config->item('CRLF');
        $html .= '</div>' . $this->config->item('CRLF');
        //for pending number dialog
        $html .= '<div id="dlg_pending" style="display:none">';
        $html .= '<div id="pending"></div>';
        $html .= '</div>';

        $this->mysmarty->assign('script', $this->makeCSS($quiz_id));
        $html .= '</form>';
        return $html;
    }

    function QuestionAnsweredShow() {
        $module_id = $this->uri->segment(3);
        $userid = $this->uri->segment(6);
        $stmt = 'select * from SP_GET_ANSWERED_QUESTION(' . $userid . ',' . $module_id . ')';
        $query = $this->db->query($stmt);
        $questions = '';
        if ($query) {
            $rcount = $query->row();
            if ($rcount) {
                foreach ($query->result() as $row) {
                    if ($questions == '') {
                        $questions = $row->R_QUESTION_ID;
                    } else {
                        $questions .= ',' . $row->R_QUESTION_ID;
                    }
                }
            } else {
                $questions = '';
            }
        } else {
            $questions = '';
        }
        $this->getQuizIdShow();
        $newquiz = array();
        if ($questions) {
            $allQuestions = explode(',', $questions);
            $uquestion = array_unique($allQuestions);
            foreach ($uquestion as $question) {
                foreach (array_keys($this->quiz_ids, $question, true) as $key) {
                    unset($this->quiz_ids[$key]);
                }
            }
        }
        $newquiz = array_values($this->quiz_ids);
        $this->quiz_ids = $newquiz;
    }

    function getQuizIdShow() {
        $question_ids = $this->getQuestionIdsShow();
        if ($question_ids['result'] == 't') {
            $quizs = explode(',', $question_ids['questionid']);
            if ($quizs) {
                $this->quiz_ids = array_unique($quizs);
            } else {
                $this->quiz_ids = '';
            }
        } else {
            $this->quiz_ids = '';
        }
    }

    function getBackQuestionShow() {
        $question_ids = $this->getQuestionIdsShow();
        if ($question_ids['result'] == 't') {
            $quizs = explode(',', $question_ids['questionid']);
            if ($quizs) {
                $this->back_quiz_ids = $quizs;
            } else {
                $this->back_quiz_ids = '';
            }
        } else {
            $this->back_quiz_ids = '';
        }
    }

    function getQuestionIdsShow($ataskfor = '', $amodule_id = '', $auserid = '') {
        if (!$ataskfor) {
            $taskfor = $this->uri->segment(2);
        } else {
            $taskfor = $ataskfor;
        }
        if (!$amodule_id) {
            $module_id = $this->uri->segment(3);
        } else {
            $module_id = $amodule_id;
        }
        if (!$auserid) {
            $userid = $this->uri->segment(6);
        } else {
            $userid = $auserid;
        }
        $usertype = getSessionVar('userType');
        if ($usertype != 2) {
            //$stmt = 'select * from SP_GET_MODULE_QUESTIONS_ANS(' . myInteger($userid) . ',' . myInteger($taskfor) . ',' . myInteger($module_id) . ')';
            $stmt = 'select * from SP_GET_MODULE_LIST_QUESTIONS(' . myInteger($module_id) . ')';
        } else {
            $child_load = $this->user_account->load($userid);
            $host_id = $this->user_account->host_id;
            $host_load = $this->user_account->load($host_id);
            $user_type = $this->user_account->user_type;
            //$stmt = 'select * from SP_GET_MODULE_QUESTIONS_ANS(' . myInteger($userid) . ',' . myInteger($user_type) . ',' . myInteger($module_id) . ')';
            $stmt = 'select * from SP_GET_MODULE_LIST_QUESTIONS(' . myInteger($module_id) . ')';
        }
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                if ($row->R_QUESTIONS) {
                    $questionid = $row->R_QUESTIONS;
                    $return = 't';
                } else {
                    $questionid = '';
                    $return = 'f';
                }
            } else {
                $questionid = 'not row ' . $stmt;
                $return = '';
            }
        } else {
            $questionid = 'not query ' . $stmt;
            $return = 'f';
        }
        $questions = explode(',', $questionid);
        //asort($questions);
        $allquestion = array_values($questions);
        $newquestion = implode(',', $allquestion);
        $qust_array = array('result' => $return, 'questionid' => $newquestion);
        return $qust_array;
    }

    function getQuestionsScoreShow($ataskfor = '', $amodule_id = '', $acurrent_question = '', $auserid = '') {
        if (!$ataskfor) {
            $taskfor = $this->uri->segment(2);
        } else {
            $taskfor = $ataskfor;
        }
        if (!$amodule_id) {
            $module_id = $this->uri->segment(3);
        } else {
            $module_id = $amodule_id;
        }
        if (!$acurrent_question) {
            $current_question = $this->uri->segment(4);
        } else {
            $current_question = $acurrent_question;
        }
        if (!$auserid) {
            $userid = $this->uri->segment(6);
        } else {
            $userid = $auserid;
        }

        $question_ids = $this->getQuestionIdsShow($ataskfor, $amodule_id, $auserid);
        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];
            //var_dump($this->generateSLNo($questions));
            $slno = $this->generateSLNo($questions);
            $stmt = 'select * from SP_QUESTION_SCORE(' . myString($questions) . ')';
            $query = $this->db->query($stmt);
            $html = '<table class="table_progress">';
            $html .= '<tr>';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';
            $html .= '<td class="table_header" style="width:15px"></td>';
            $html .= '<td class="table_header">Description</td>';
            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
            $html .= '</tr>';
            $prevquestion = getSessionVar('prevquestion');
            $i = 1;
            $question_array = explode(',', $questions);
            //ksort($question_array);
            //print_r($questions);
            if ($query) {
                $k = 0;
                foreach ($query->result() as $row) {
                    $answer = $this->getQuestionAnswer($module_id, $row->R_QUIZ_ANSWER_ID, $userid);
                    if ($answer) {
                        $answer_right = $answer['answer_right'];
                        $mark_obtained = $answer['mark_obtained'];
                        $answer_id = $answer['answer_id'];
                    } else {
                        $answer_right = '';
                        $mark_obtained = '';
                        $answer_id = '';
                    }
                    $ques_id = $row->R_QUIZ_ANSWER_ID;
                    $this->assign_score[] = array('id' => $row->R_QUIZ_ANSWER_ID, 'score' => $row->R_ASSIGN_SCORE);
                    $html .= '<tr>';
                    if ($current_question != $row->R_QUIZ_ANSWER_ID) {
                        switch ($answer_right) {
                            case '1':
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_right" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="table_row_right_no_border" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                }
                                //$answerd = true;
                                break;

                            case '0':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_wrong" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    if ($slno[$k] != '') {
                                        $html .= '<td style="width:30px" class="table_row_pending" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">P</td>';
                                    } else {
                                        $html .= '<td style="width:30px" class="table_row_pending" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                    }
                                }
                                //$answerd = true;
                                break;

                            default:
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '"></td>';
                                } else {
                                    $html .= '<td style="width:30px" class="table_row_noborder_left" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                }
                                //$answerd = false;
                                break;
                        }
                        for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                            if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
                                $pos = $j;
                                if ($pos != 0) {
                                    $prev_question = $question_array[$pos - 1];
                                } else {
                                    $prev_question = '0'; //$question_array[0];
                                }
                                break;
                            } else {
                                $pos = 0;
                                $prev_question = '0';
                            }
                        }
                        $link = "javascript:fn_select_question_progress($taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question, $userid)";
                        if ($row->R_QUIZ_TYPE != 9) {
                            $html .= '<td class="table_row"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            $html .= '<td class="table_row" id="bt_1">&nbsp;</td>';
                        } else {
                            $html .= '<td class="table_row_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            if ($slno[$k] != '') {
                                $number_set = "fn_set_number($taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $userid)";
                                $numberlink = '<div id="number_' . $row->R_QUIZ_ANSWER_ID . '" class="table_row_number_set" onClick="' . $number_set . '">number</div>';
                                $html .= '<td class="table_row_noborder">' . $numberlink . '</td>';
                            } else {
                                $html .= '<td class="table_row_noborder"">&nbsp;</td>';
                            }
                        }

                        $html .= '<td class="table_row">' . $row->R_ASSIGN_DESCRIPTION . '</td>';
                        $html .= '<td class="table_row_bold" style="text-align:right;padding-right:2px;">' . $row->R_ASSIGN_SCORE . '</td>';
                        if ($row->R_QUIZ_TYPE != 9) {
                            $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
                        } else {
                            $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '_' . $answer_id . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
                        }
                        $html .= '</tr>';
                        $i++;
                    } else {
                        switch ($answer_right) {
                            case '1':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_right_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                }
                                //$answerd = true;
                                break;

                            case '0':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_wrong_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    if ($slno[$k] != '') {
                                        $html .= '<td style="width:30px" class="table_row_pending_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">P</td>';
                                    } else {
                                        $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                    }
                                }
                                //$answerd = true;
                                break;

                            default:
                                if ($row->R_QUIZ_TYPE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_highlight" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $row->R_QUIZ_ANSWER_ID . '">&nbsp;</td>';
                                }
                                //$answerd = false;
                                break;
                        }
                        for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                            if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
                                $pos = $j;
                                if ($pos != 0) {
                                    $prev_question = $question_array[$pos - 1];
                                } else {
                                    $prev_question = '0'; //$question_array[0];
                                }
                                break;
                            } else {
                                $pos = 0;
                                $prev_question = '0';
                            }
                        }
                        $link = "javascript:fn_select_question_progress($taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question, $userid)";
                        if ($row->R_QUIZ_TYPE != 9) {
                            $html .= '<td class="table_row_highlight"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            $html .= '<td class="table_row_highlight" id="bt_1">&nbsp;</td>';
                        } else {
                            $html .= '<td class="table_row_highlight_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            if ($slno[$k] != '') {
                                $number_set = "fn_set_number($taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $userid)";
                                $numberlink = '<div id="number_' . $row->R_QUIZ_ANSWER_ID . '" class="table_row_number_set" onClick="' . $number_set . '">number</div>';
                                $html .= '<td class="table_row_highlight_noborder">' . $numberlink . '</td>';
                            } else {
                                $html .= '<td class="table_row_highlight_noborder">&nbsp;</td>';
                            }
                        }

                        $html .= '<td class="table_row_highlight">' . $row->R_ASSIGN_DESCRIPTION . '</td>';
                        $html .= '<td class="table_row_bold_highlight" style="text-align:right;padding-right:2px;">' . $row->R_ASSIGN_SCORE . '</td>';
                        if ($row->R_QUIZ_TYPE != 9) {
                            $html .= '<td class="table_row_bold_highlight" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
                        } else {
                            $html .= '<td class="table_row_bold_highlight" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '_' . $answer_id . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
                        }
                        $html .= '</tr>';
                        $i++;
                    }
                    $k++;
                }
                $html .= '</table>';
            } else {
                $html = $stmt;
            }
            return $html;
        } else {
            return $question_ids['questionid'];
        }
    }
	
	function getPendingList($aModuleId = 0, $aUserId = 0, $aQuestionId = 0) {
        $stmt = "select * from SP_GET_PENDING_ANSWER_LIST($aModuleId, $aUserId, $aQuestionId)";
        $query = $this->db->query($stmt);
        if ($query) {
            $i = 0;
            $html = '<table class="pending_table">';
            $html .= '<tr>';
            $html .= '<td class="pending_table_header">SL</td>';
            $html .= '<td class="pending_table_header">Description</td>';
            $html .= '<td class="pending_table_header_center">Score</td>';
            $html .= '<td class="pending_table_header_center">Mark</td>';
            $html .= '</tr>';
            $ids = array();
            $spinner_ids = array();
			//print_r($query->result());
			//exit;
            foreach ($query->result() as $row) {
                $ids[] = array('id' => $row->R_ID, 'score' => $row->R_ANSWER_ORIG_MARK);
                $spinner_ids[] = 'answer_mark_obtained_' . $row->R_ID;
                $html .= '<tr>';
                //answer_mark_obtained
				$html .= '<td class="pending_table_row_score assing_sll">'.$row->R_ASSIGN_SLNO.'</td>';
                $html .= '<td class="pending_table_row descrption_desc" data_attr_qdesc="' . $row->R_ASSIGN_DESCRIPTION . '"><img src="/images/notepad1.png" class="rsdemosdata_p_updates"/></td>';
                $html .= '<td class="pending_table_row_score">' . $row->R_ANSWER_ORIG_MARK . '</td>';
                $html .= '<td class="pending_table_row_spinner " id="td_' . $row->R_ID . '">' . textinput('answer_mark_obtained_' . $row->R_ID, 'active-input', '', '20', '2') . '</td>';
                $html .= '</tr>';
                $i++;
            }
            if ($i > 0) {
                $script = '<script>' . $this->config->item('CRLF');
                $script .= '$(function() {' . $this->config->item('CRLF');
                foreach ($ids as $id) {
                    $script .= '$("#answer_mark_obtained_' . $id['id'] . '").spinner();' . $this->config->item('CRLF');
                    $script .= '$("#answer_mark_obtained_' . $id['id'] . '").spinner( "option", "max",' . $id['score'] . ',"step","0.01","numberFormat","n" );' . $this->config->item('CRLF');
                    $script .= '$("#answer_mark_obtained_' . $id['id'] . '").spinner( "option","step","0.01","numberFormat","n" );' . $this->config->item('CRLF');
                    $script .= '$("#answer_mark_obtained_' . $id['id'] . '").css(\'width\', \'30px\');' . $this->config->item('CRLF');
                    $script .= '$("#answer_mark_obtained_' . $id['id'] . '").css(\'height\', \'20px\');' . $this->config->item('CRLF');
                    $script .= '$("#answer_mark_obtained_' . $id['id'] . '").css(\'font-size\', \'12px\');' . $this->config->item('CRLF');
                }
                $script .= '});' . $this->config->item('CRLF');
                $script .= '</script>';
            }
            $html .= '</table>';
            $data = $i;
            $error = '';
            $spinners = implode(',', $spinner_ids);
            $score = '';
            $rid = '';
            foreach ($ids as $id) {
                if ($score == '') {
                    $score = $id['score'];
                } else {
                    $score = $score . ',' . $id['score'];
                }
                if ($rid == '') {
                    $rid = $id['id'];
                } else {
                    $rid = $rid . ',' . $id['id'];
                }
            }
        } else {
            $error = 'Error in getting Pending List';
            $html = '';
            $data = 0;
            $spinners = '';
            $score = '';
            $rid = '';
        }
        $return = array('html' => @$script . $html, 'data' => $data, 'error' => $error, 'spinners' => $spinners, 'score' => $score, 'id' => $rid);
        return $return;
    }

    function IsPendingMarkAvailable() {
        $module_id = $this->uri->segment(3);
        $userid = $this->uri->segment(6);
        $stmt = "select * from SP_IS_PENDING_MARK($userid, $module_id)";
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = $row->R_RESULT;
            } else {
                $return = -1;
            }
        } else {
            $return = -1;
        }
        if ($return == 0) {
            return true;
        } else {
            return false;
        }
    }
	
	
		function rsgetstudent_totalMarks($taskfor,$module_id,$userid ){
		/* new ar */
        $usertype = getSessionVar('userType');
		$hostid = getSessionVar('userid');
		$question_ids = $this->getQuestionIdsShow($taskfor,$module_id,$userid);
		//$question_ids = $this->getQuestionIds('', $taskfor, $module_id);
		$custarr = array();
        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];
			
            //$stmt = "select * from SP_GET_PROGRESS_SHEET($userid, $module_id)";
            $stmt = 'select * from SP_QUESTION_SCORE(' . myString($questions) . ')';
            $query = $this->db->query($stmt);	
			foreach($query->result() as $rows){
				$custarr[$rows->R_QUIZ_ANSWER_ID] = $rows; 
			}
		}
		/* new ar */
		$tmarks = 0;
		
		if($custarr){
			foreach ($custarr as $row){
				$answer = $this->getQuestionAnswer($module_id, $row->R_QUIZ_ANSWER_ID, $userid);
				if ($answer) {
					$answer_right = $answer['answer_right'];
					$tmarks += $answer['mark_obtained'];
					$answer_id = $answer['answer_id'];
				}
			}
		}
		return $tmarks;
	}
	
	
	function getQuestionsScorePreview_newprogrss(){
		$taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $current_question = $this->uri->segment(4);

        $userid = $this->uri->segment(6);
        $usertype = getSessionVar('userType');
		$hostid = getSessionVar('userid');

        //load module for getting modules_name
        $load_module = $this->quiz_module->load($module_id);
        if ($load_module) {
            $modules_name = 'Module Name: ' . $this->quiz_module->modules_name;
        } else {
            $modules_name = '';
        }
        //$question_ids = $this->getQuestionIdsShow();
        //$question_ids = $this->getQuestionIds('', $taskfor, $module_id);
		
        $question_ids = $this->getQuestionIdsShow();
        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];

           // $slno = $this->generateSLNoPreview($userid, $module_id);
			$slno = $this->generateSLNo($questions);
            $isPending = $this->IsPendingMarkAvailable();
			
            //$stmt = "select * from SP_GET_PROGRESS_SHEET($userid, $module_id)";
            $stmt = 'select * from SP_QUESTION_SCORE(' . myString($questions) . ')';
            $query = $this->db->query($stmt);			
			$custarr = array();
			
			/* echo '<pre>';
			print_r($query->result());
			exit; */
			
			foreach($query->result() as $rows){
				$custarr[$rows->R_QUIZ_ANSWER_ID] = $rows; 
			}
            $html = '<table class="table_progress rstbles_progress" id="score_table">';
            $html .= '<tr>';
			
            if ($usertype != 2) {
                $html .= '<td class="table_header" colspan="6">' . $modules_name . '</td>';
            } else {
                $html .= '<td class="table_header" colspan="5">' . $modules_name . '</td>';
            }
			
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';
            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
			$html .= '<td class="table_header">Description</td>';
            $html .= '</tr>';
            $prevquestion = getSessionVar('prevquestion');
            $i = 1;
            $question_array = explode(',', $questions);
            if ($query) {
                $k = 0;
               // foreach ($query->result() as $row) {
                foreach ($custarr as $row){
                    $answer = $this->getQuestionAnswer($module_id, $row->R_QUIZ_ANSWER_ID, $userid);
                    if ($answer) {
                        $answer_right = $answer['answer_right'];
                        $mark_obtained = $answer['mark_obtained'];
                        $answer_id = $answer['answer_id'];
                    } else {
                        $answer_right = '';
                        $mark_obtained = '';
                        $answer_id = '';
                    }
                    
     if($mark_obtained!=''){
        $frac_check_m = $this->convert_decimal_to_fraction($mark_obtained);
            if (strpos($frac_check_m, '/') !== false) {
                $rslts_m = explode('/', $frac_check_m);
                $scnd_main_m = $rslts_m[0];
                $scnd_top_m = $rslts_m[1];
                $scnd_bottom_m = $rslts_m[2];
                $mark_obtained = '<table class="rsscondryTable std_score">';
                $mark_obtained .='<tbody>';
                $mark_obtained .='<tr>';
                $mark_obtained .='<td rowspan="2" class="scandry_mainval std_score_main">';
                $mark_obtained .=$scnd_main_m;
                $mark_obtained .='</td>';
                $mark_obtained .='<td style="border-bottom:solid 1px" class="scndry_top ">';
                $mark_obtained .=$scnd_top_m;
                $mark_obtained .='</td>';
                $mark_obtained .='</tr>';
                $mark_obtained .='<tr>';           
                $mark_obtained .='<td class="scndry_bottom">';
                $mark_obtained .= $scnd_bottom_m;
                $mark_obtained .='</td>';
                $mark_obtained .='</tr>';
                $mark_obtained .='</tbody>';
                $mark_obtained .='</table>';
            }else{
                $mark_obtained = $mark_obtained; 
            }
    }
        $frac_check = $this->convert_decimal_to_fraction($row->R_ASSIGN_SCORE);
        if (strpos($frac_check, '/') !== false) {
            $rslts = explode('/', $frac_check);
            $scnd_main = $rslts[0];
            $scnd_top = $rslts[1];
            $scnd_bottom = $rslts[2];
            $qscore_is = '<table class="rsscondryTable std_score">';
            $qscore_is .='<tbody>';
            $qscore_is .='<tr>';
            $qscore_is .='<td rowspan="2" class="scandry_mainval std_score_main">';
            $qscore_is .=$scnd_main;
            $qscore_is .='</td>';
            $qscore_is .='<td style="border-bottom:solid 1px" class="scndry_top ">';
            $qscore_is .=$scnd_top;
            $qscore_is .='</td>';
            $qscore_is .='</tr>';
            $qscore_is .='<tr>';           
            $qscore_is .='<td class="scndry_bottom">';
            $qscore_is .= $scnd_bottom;
            $qscore_is .='</td>';
            $qscore_is .='</tr>';
            $qscore_is .='</tbody>';
            $qscore_is .='</table>';
        }else{
            $qscore_is = $row->R_ASSIGN_SCORE; 
        }                
        
                    $ques_id = $row->R_QUIZ_ANSWER_ID;
                    $this->assign_score[] = array('id' => $row->R_QUIZ_ANSWER_ID, 'score' => $row->R_ASSIGN_SCORE);
					switch ($answer_right) {
                            case '1':
								$ans_status = '<img src="/images/tick.png" alt="Answer Correct"/>';
								$answerd = true;
							break;
							
							case '0':
								$ans_status = '<img src="/images/invalid.png" alt="Answer Invalid"/>';
								$answerd = true;
							break;
							default:
								$ans_status = '';
								$answerd = false;
							break;
					}
				   
					/* if(!$answerd){ */
					
					
				for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
					if ($question_array[$j] == $row->R_QUIZ_ANSWER_ID) {
						$pos = $j;
						if ($pos != 0) {
							$prev_question = $question_array[$pos - 1];
						} else {
							$prev_question = '0'; //$question_array[0];
						}
						break;
					} else {
						$pos = 0;
						$prev_question = '0';
					}
				}
                //$link = "javascript:fn_select_question($hostid, $taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question)";
				
				$link = "javascript:fn_select_question_progress($taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question, $userid)";
					
				/* 
					}else{
						$link= '';	
					} */	
				/* new added for question link */
				//$link = "javascript:fn_select_question_progress($taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $prev_question, $userid)";	
				/* new added for question link */
					
					$number_set = "fn_set_number($taskfor, $module_id, $row->R_QUIZ_ANSWER_ID, $userid)";
					

                    if ($current_question != $row->R_QUIZ_ANSWER_ID) {
						$html .= '<tr class="normal_rows_prq">';
						
						
						if($row->R_QUIZ_TYPE != 9){
							$html .= '<td class="row_right_wong rsprgress_td">'.$ans_status.'</td>';
						}else{
							$html .= '<td class="row_right_wong rsprgress_td"><a href="javascript:'.$number_set.'" class="alink pending_prq">Pending</a></td>';
						}
						
						if($link!=''){
							$html .= '<td class="row_right_wong slno table_row rsprgress_td"><a class="alink" href="'.$link.'">'.$i.'</a></td>';
						}else{
							$html .= '<td class="row_right_wong slno rsprgress_td">'.$i.'</td>';
						}
						
						
						if($row->R_QUIZ_TYPE != 9){
							
						$html .= '<td class="table_row_bold">' . $qscore_is . '</td>';
                        $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '">' . $mark_obtained . '</td>';					
						 $html .= '<td class="table_row_bold"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
							
							
						} else {
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
						}
						
						
						$html .= '</tr>';
					} else {
						$html .= '<tr class="rows_prq_highlight">';	
							
							
						if($row->R_QUIZ_TYPE != 9){
							$html .= '<td class="row_right_wong rsprgress_td">'.$ans_status.'</td>';
						}else{
							$html .= '<td class="row_right_wong rsprgress_td rsqdata pending_prq" rsmid="'.$module_id.'" rsq_id="'.$row->R_QUIZ_ANSWER_ID.'">Pending</td>';
						}
						
						if($link!=''){
							$html .= '<td class="row_right_wong slno table_row rsprgress_td"><a class="alink" href="'.$link.'">'.$i.'</a></td>';
						}else{
							$html .= '<td class="row_right_wong slno rsprgress_td">'.$i.'</td>';
						}
						
						
						if($row->R_QUIZ_TYPE != 9){
							
						$html .= '<td class="table_row_bold">' . $qscore_is . '</td>';
                        $html .= '<td class="table_row_bold" id="td_mark_' . $row->R_QUIZ_ANSWER_ID . '">' . $mark_obtained . '</td>';					
						 $html .= '<td class="table_row_bold"><img class="scroring_note" data_qid="'.$row->R_QUIZ_ANSWER_ID.'" src="/images/notepad1.png" alt="no-image"/></td>';
							
							
						} else {
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
							$html .= '<td class="table_row_bold"></td>';
						}
							
						
						
						$html .= '</tr>';
					}
					$i++;
				}
				
			   }
				$html .= '</table>';
				return $html;
			} else {
				return $question_ids['questionid'];
			}
	}
    function getQuestionsScorePreview() {
        $taskfor = $this->uri->segment(2);
        $module_id = $this->uri->segment(3);
        $current_question = $this->uri->segment(4);

        $userid = $this->uri->segment(6);
        $usertype = getSessionVar('userType');

        //load module for getting modules_name
        $load_module = $this->quiz_module->load($module_id);
        if ($load_module) {
            $modules_name = 'Module Name: ' . $this->quiz_module->modules_name;
        } else {
            $modules_name = '';
        }
        $question_ids = $this->getQuestionIdsShow();
        if ($question_ids['result'] == 't') {
            $questions = $question_ids['questionid'];

            $slno = $this->generateSLNoPreview($userid, $module_id);
            $isPending = $this->IsPendingMarkAvailable();
            $stmt = "select * from SP_GET_PROGRESS_SHEET($userid, $module_id)";
            $query = $this->db->query($stmt);

            $html = '<table class="table_progress" id="score_table">';
            $html .= '<tr>';
            if ($usertype != 2) {
                $html .= '<td class="table_header" colspan="6">' . $modules_name . '</td>';
            } else {
                $html .= '<td class="table_header" colspan="5">' . $modules_name . '</td>';
            }
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';
            if ($usertype != 2) {
                $html .= '<td class="table_header" style="width:15px" id="td_set"></td>';
            }
            $html .= '<td class="table_header">Description</td>';
            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
            $html .= '</tr>';
            $prevquestion = getSessionVar('prevquestion');
            $i = 1;
            $question_array = explode(',', $questions);
            if ($query) {
                $k = 0;
                foreach ($query->result() as $row) {
                    $ques_id = $row->R_QUESTION_ID;
                    $answer_id = $row->R_ID;
                    $mark_obtained = $row->R_ANSWER_MARK_OBTAINED;
                    $answer_right = $row->R_ANSWER_RIGHT;
                    $this->assign_score[] = array('id' => $row->R_QUESTION_ID, 'score' => $row->R_ANSWER_ORIG_MARK);
                    $html .= '<tr>';
                    if ($current_question != $row->R_QUESTION_ID) {
                        switch ($answer_right) {
                            case '1':
                                if ($row->R_QUIZ_CATE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_right" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="table_row_right_no_border" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                }
                                //$answerd = true;
                                break;

                            case '0':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_CATE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_wrong" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                } else {
                                    if ($slno[$k] != '') {
                                        if (!$isPending) {
                                            $html .= '<td style="width:30px" class="table_row_pending" id="td_okwrong_' . $answer_id . '">P</td>';
                                        } else {
                                            $html .= '<td style="width:30px" class="table_row_pending" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                        }
                                    } else {
                                        $html .= '<td style="width:30px" class="table_row_pending" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                    }
                                }
                                //$answerd = true;
                                break;

                            default:
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_CATE != 9) {
                                    $html .= '<td style="width:30px" class="table_row" id="td_okwrong_' . $answer_id . '"></td>';
                                } else {
                                    $html .= '<td style="width:30px" class="table_row_noborder_left" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                }
                                //$answerd = false;
                                break;
                        }
                        for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                            if ($question_array[$j] == $row->R_QUESTION_ID) {
                                $pos = $j;
                                if ($pos != 0) {
                                    $prev_question = $question_array[$pos - 1];
                                } else {
                                    $prev_question = '0'; //$question_array[0];
                                }
                                break;
                            } else {
                                $pos = 0;
                                $prev_question = '0';
                            }
                        }
                        $link = "javascript:fn_select_question_progress($taskfor, $module_id, $row->R_QUESTION_ID, $prev_question, $userid)";
                        if ($row->R_QUIZ_CATE != 9) {
                            $html .= '<td class="table_row"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            if ($usertype != 2) {
                                $html .= '<td class="table_row" id="bt_1">&nbsp;</td>';
                            }
                        } else {
                            $html .= '<td class="table_row_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            if ($usertype != 2) {
                                if (!$isPending) {
                                    if ($slno[$k] != '') {
                                        $number_set = "fn_set_number($taskfor, $module_id, $row->R_QUESTION_ID, $userid)";
                                        $numberlink = '<div id="number_' . $row->R_QUESTION_ID . '" class="table_row_number_set" onClick="' . $number_set . '">number</div>';
                                        $html .= '<td class="table_row_noborder" id="td_set_' . $answer_id . '">' . $numberlink . '</td>';
                                    } else {
                                        $html .= '<td class="table_row_noborder" id="td_set_' . $answer_id . '">&nbsp;</td>';
                                    }
                                } else {
                                    $html .= '<td class="table_row_noborder" id="td_set_' . $answer_id . '">&nbsp;</td>';
                                }
                            }
                        }

                        $html .= '<td class="table_row">' . $row->R_ASSIGN_DESCRIPTION . '</td>';
                        $html .= '<td class="table_row_bold" style="text-align:right;padding-right:2px;">' . $row->R_ANSWER_ORIG_MARK . '</td>';
                        if ($row->R_QUIZ_CATE != 9) {
                            $html .= '<td class="table_row_bold" id="td_mark_' . $answer_id . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
                        } else {
                            $html .= '<td class="table_row_bold" id="td_mark_' . $answer_id . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
                        }
                        $html .= '</tr>';
                        $i++;
                    } else {
                        switch ($answer_right) {
                            case '1':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_CATE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_right_highlight" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                }
                                //$answerd = true;
                                break;

                            case '0':
                                //if ($slno[$k] != '') {
                                if ($row->R_QUIZ_CATE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_wrong_highlight" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                } else {
                                    if ($slno[$k] != '') {
                                        if (!$isPending) {
                                            $html .= '<td style="width:30px" class="table_row_pending_highlight" id="td_okwrong_' . $answer_id . '">P</td>';
                                        } else {
                                            $html .= '<td style="width:30px" class="table_row_pending_highlight" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                        }
                                    } else {
                                        $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                    }
                                }
                                //$answerd = true;
                                break;

                            default:
                                if ($row->R_QUIZ_CATE != 9) {
                                    $html .= '<td style="width:30px" class="table_row_highlight" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                } else {
                                    $html .= '<td style="width:30px" class="table_row_highlight_noborder" id="td_okwrong_' . $answer_id . '">&nbsp;</td>';
                                }
                                //$answerd = false;
                                break;
                        }
                        for ($j = 0, $l = count($question_array); $j < $l; ++$j) {
                            if ($question_array[$j] == $row->R_QUESTION_ID) {
                                $pos = $j;
                                if ($pos != 0) {
                                    $prev_question = $question_array[$pos - 1];
                                } else {
                                    $prev_question = '0'; //$question_array[0];
                                }
                                break;
                            } else {
                                $pos = 0;
                                $prev_question = '0';
                            }
                        }
                        $link = "javascript:fn_select_question_progress($taskfor, $module_id, $row->R_QUESTION_ID, $prev_question, $userid)";
                        if ($row->R_QUIZ_CATE != 9) {
                            $html .= '<td class="table_row_highlight"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            if ($usertype != 2) {
                                $html .= '<td class="table_row_highlight" id="bt_1">&nbsp;</td>';
                            }
                        } else {
                            $html .= '<td class="table_row_highlight_noborder"><a class="alink" href="' . $link . '">' . $slno[$k] . '</a></td>';
                            if ($usertype != 2) {
                                if (!$isPending) {
                                    if ($slno[$k] != '') {
                                        $number_set = "fn_set_number($taskfor, $module_id, $row->R_QUESTION_ID, $userid)";
                                        $numberlink = '<div id="number_' . $row->R_QUESTION_ID . '" class="table_row_number_set" onClick="' . $number_set . '">number</div>';
                                        $html .= '<td class="table_row_highlight_noborder" id="td_set_' . $answer_id . '">' . $numberlink . '</td>';
                                    } else {
                                        $html .= '<td class="table_row_highlight_noborder" id="td_set_' . $answer_id . '">&nbsp;</td>';
                                    }
                                } else {
                                    $html .= '<td class="table_row_highlight_noborder" id="td_set_' . $answer_id . '">&nbsp;</td>';
                                }
                            }
                        }

                        $html .= '<td class="table_row_highlight">' . $row->R_ASSIGN_DESCRIPTION . '</td>';
                        $html .= '<td class="table_row_bold_highlight" style="text-align:right;padding-right:2px;">' . $row->R_ANSWER_ORIG_MARK . '</td>';
                        if ($row->R_QUIZ_CATE != 9) {
                            $html .= '<td class="table_row_bold_highlight" id="td_mark_' . $answer_id . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
                        } else {
                            $html .= '<td class="table_row_bold_highlight" id="td_mark_' . $answer_id . '" style="text-align:right;padding-right:2px;">' . $mark_obtained . '</td>';
                        }
                        $html .= '</tr>';
                        $i++;
                    }
                    $k++;
                }
                $html .= '</table>';
            } else {
                $html = $stmt;
            }
            return $html;
        } else {
            return $question_ids['questionid'];
        }
    }

    function updatePendingMarks($aid = 0, $aValues = 0) {
        $stmt = "select R_RESULT from SP_UPDATE_PENDING_MARK($aid, $aValues)";
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $return = $row->R_RESULT;
                if ($return) {
                    return true;
                } else {
                    $this->error_msg = 'Problem in updating Mark';
                    return false;
                }
            } else {
                $this->error_msg = 'Can\'t updating Mark';
                return false;
            }
        } else {
            $this->error_msg = 'Error in updating Mark';
            return false;
        }
    }

    function show_preview_title() {
        $title = 'Q-Study | Answer Preview';
        return $title;
    }

    function show_preview_page_title() {
        return 'Preview';
    }
	function getscorboard(){
		$rsMuduleId = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);

		$load_module = $this->quiz_module->load($rsMuduleId);
        if ($load_module) {
            $modules_name = 'Module Name: ' . $this->quiz_module->modules_name;
        } else {
            $modules_name = '';
        }
		
		$quiz_id = $this->getQuizCategoryId($question_id);
		$cate_id = $quiz_id;
		$quiz_options = $this->question_answer->getQuizOption($cate_id);
		
		$stmt = "SELECT * FROM QUESTION_ANSWER LEFT JOIN QUIZ_ASSIGN_SCORE ON QUESTION_ANSWER.ID=QUIZ_ANSWER_ID WHERE QUESTION_ANSWER.ID='".$question_id."'";
		
		$result = $this->db->query($stmt);
		
			$html = '<table class="table">';
            $html .= '<tr>';
            $html .= '<td class="table_header" colspan="5">' . $modules_name . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';
            $html .= '<td class="table_header">Description</td>';
            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
            $html .= '</tr>';
			$totals = 0;
			foreach($result->result() as $rows){
				$html .= '<tr>';
				$html .= '<td class="table_row_highlight"></td>';
				if($quiz_options!=9){
				$html .= '<td class="table_row_highlight">1</td>';
				
				$html .= '<td class="table_row_highlight"><img class="scroring_note" data_qid="'.$rows->QUIZ_ANSWER_ID.'"  src="/images/notepad1.png" alt="no-image"/></td>';
				
				}else{
					$html .= '<td class="table_row_highlight">'.$rows->ASSIGN_SLNO.'</td>';
					
					$html .= '<td class="table_row_highlight"><img class="scroring_note" data_scrore_desc="'.$rows->ASSIGN_DESCRIPTION.'" src="/images/notepad1.png" alt="no-image"/></td>';
				}
				
				$html .= '<td class="table_row_highlight">'.$rows->ASSIGN_SCORE.'</td>';
				$html .= '<td class="table_row_highlight"></td>';
				$html .= '</tr>';
				$totals += $rows->ASSIGN_SCORE;
			}
			
            $html .= '</table>';
		$html .= '<p>Total Mark:'.$totals.'</p>';
		return $html;
		
	}
function showPreviewAnswerBoard() {
        $cate_id = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        //set button
        //answer-complete/4/34
        $alink = $this->uri->segment(1);
        if ($alink != 'preview-from-main') {
            $submit_link = 'javascript:fn_preview_question(' . $cate_id . ',' . $question_id . ',0)';
            $elapse_link = 'fn_preview_question(' . $cate_id . ',' . $question_id . ',1)';
            $submit = '<td><a href="' . $submit_link . '" class="finish"><span>Submit</span></a></td>';
            $next_submit = '<td><a href="' . $submit_link . '" class="finish"><span>Submit</span></a></td>';
			
			
			
        } else {
            $submit_link = 'javascript:fn_preview_from_question(' . $cate_id . ',' . $question_id . ',0)';
            $elapse_link = 'fn_preview_from_question(' . $cate_id . ',' . $question_id . ',1)';
            $submit = '<a href="' . $submit_link . '" class="finish"><span>Submit</span></a>';
            $next_submit = '<a href="' . $submit_link . '" class="finish"><span>Submit</span></a>';
			
			
			
			
        }

        //set time elapse for the question
        /* $time_elapse = $this->question_answer->getTimeElpase($question_id);
          if ($time_elapse) {
          $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
          } else {
          $this->mysmarty->assign('time_elapse', '');
          } */
		  
		$qinfo = $this->question_answer->getallinfo_question($question_id);
		$rqtimes = $qinfo->time_elapse;
		$rqtimes_formated = gmdate("H:i:s", $rqtimes);
		$this->mysmarty->assign('qtimes',$rqtimes);
		$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
		
		$previewallcancel = '/quiz-edit/'.$cate_id.'/'.$question_id;
		
		$top_prev = '<a class="back_button" href="javascript: history.go(-1)">Back</a>';//rhm
		$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
		$top_tutorial = '<a class="finish" href="'.$previewallcancel.'"><span>Index</span></a>';  
		  
		  
        $this->DisplayAnswerBoard($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        $this->mysmarty->assign('next_submit', $next_submit);
        $this->mysmarty->assign('top_tutorial', $top_tutorial);
		$this->mysmarty->assign('top_prev',  $top_prev); //rhm top_tutorial btn
        $this->mysmarty->assign('top_next',  $top_next); //rhm top_tutorial btn
		
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $submit . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');
		$this->mysmarty->assign('scoreboard',$this->getscorboard());
        //set timer in the question
        if ($time_elapse) {
            $html = '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('timerelapse', $html);
        //$this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');
        $this->mysmarty->assign('quiz_score', $this->getQuestionsScore());
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
    }
	

//rs new changes
    function showPreviewAnswerBoard_all_q() {
		
        $rsMuduleId = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);
		
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
		$cate_id = $quiz_id;
		$reslt = $this->question_answer->getQuizOption($cate_id);

        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
		
        if ($quiz) {
           $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

		
        //set button
        //answer-complete/4/34
        $alink = $this->uri->segment(1);
        if ($alink != 'preview-from-main') {
            $submit_link = 'javascript:fn_preview_question_previews(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ',0)';
            $elapse_link = 'fn_preview_question_previews(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ',1)';
            $previewallcancel = '/module-edit/'.$rsMuduleId;/*11-22-16 rs changes*/
            $submit = '<td><a href="' . $submit_link . '" class="next_button"><span>Next</span></a></td>';
            $submit .= '<td><a href="' . $previewallcancel . '" class="cancel"><span>Cancel</span></a></td>';
			
			$next_submit = '<a href="' . $submit_link . '" class="next_button"><span>submit</span></a>';
			
        }

        //set time elapse for the question
        /* $time_elapse = $this->question_answer->getTimeElpase($question_id);
          if ($time_elapse) {
          $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
          } else {
          $this->mysmarty->assign('time_elapse', '');
          } */
		$qinfo = $this->question_answer->getallinfo_question($question_id);
		$rqtimes = $qinfo->time_elapse;
		$rqtimes_formated = gmdate("H:i:s", $rqtimes);
		$this->mysmarty->assign('qtimes',$rqtimes);
		$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
		 
		  
		$top_prev = '<a class="back_button" href="javascript: history.go(-1)">Back</a>';//rhm
		$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
		$top_tutorial = '<a class="finish" href="'.$previewallcancel.'"><span>Index</span></a>';
		  
        $this->DisplayAnswerBoard($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));

        $this->mysmarty->assign('next_submit', $next_submit);
        $this->mysmarty->assign('top_tutorial', $top_tutorial);
		$this->mysmarty->assign('top_prev',  $top_prev); //rhm top_tutorial btn
        $this->mysmarty->assign('top_next',  $top_next); //rhm top_tutorial btn
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $submit . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');

		$this->mysmarty->assign('scoreboard',$this->getscorboard());
        //set timer in the question
        if ($time_elapse) {
            $html = '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('timerelapse', $html);
        //$this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');
        $this->mysmarty->assign('quiz_score', $this->getQuestionsScore());
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
    }
	
    function showPreviewAnswerBoard_all_q_opensrce_admin() {
		
        $rsMuduleId = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);
		
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
		$cate_id = $quiz_id;
		$reslt = $this->question_answer->getQuizOption($cate_id);
		$rUser = $this->db->query("select USER_ID from QUIZ_MODULE where ID='".$rsMuduleId."'")->row();
        $userid = $rUser->USER_ID;
		//getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
		
        if ($quiz) {
           $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

		
        //set button
        //answer-complete/4/34
        $alink = $this->uri->segment(1);
        if ($alink != 'preview-from-main') {
            $submit_link = 'javascript:fn_preview_question_previews_opnse_adminview(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ',0)';
            $elapse_link = 'fn_preview_question_previews_opnse_adminview(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ',1)';
            $previewallcancel = '/admin-preview-module/'.$rsMuduleId.'/'.$userid;/*11-22-16 rs changes*/
            $submit = '<td><a href="' . $submit_link . '" class="next_button"><span>Next</span></a></td>';
            $submit .= '<td><a href="' . $previewallcancel . '" class="cancel"><span>Cancel</span></a></td>';
			
			$next_submit = '<a href="' . $submit_link . '" class="next_button"><span>submit</span></a>';
			
        }
		$qinfo = $this->question_answer->getallinfo_question($question_id);
		$rqtimes = $qinfo->time_elapse;
		$rqtimes_formated = gmdate("H:i:s", $rqtimes);
		$this->mysmarty->assign('qtimes',$rqtimes);
		$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
		 
		  
		$top_prev = '<a class="back_button" href="javascript: history.go(-1)">Back</a>';//rhm
		$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
		$top_tutorial = '<a class="finish" href="'.$previewallcancel.'"><span>Index</span></a>';
		  
        $this->DisplayAnswerBoard($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));

        $this->mysmarty->assign('next_submit', $next_submit);
        $this->mysmarty->assign('top_tutorial', $top_tutorial);
		$this->mysmarty->assign('top_prev',  $top_prev); //rhm top_tutorial btn
        $this->mysmarty->assign('top_next',  $top_next); //rhm top_tutorial btn
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $submit . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');

		$this->mysmarty->assign('scoreboard',$this->getscorboard());
        //set timer in the question
        if ($time_elapse) {
            $html = '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('timerelapse', $html);
        //$this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');
        $this->mysmarty->assign('quiz_score', $this->getQuestionsScore());
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
    }
	
    function showPreviewAnswerBoard_all_q_opensrce_tutors() {
		
        $rsMuduleId = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);
		
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
		$cate_id = $quiz_id;
		$reslt = $this->question_answer->getQuizOption($cate_id);
		$rUser = $this->db->query("select USER_ID from QUIZ_MODULE where ID='".$rsMuduleId."'")->row();
        $userid = $rUser->USER_ID;
		//getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
		
        if ($quiz) {
           $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

		
        //set button
        //answer-complete/4/34
        $alink = $this->uri->segment(1);
        if ($alink != 'preview-from-main') {
            $submit_link = 'javascript:fn_preview_question_previews_opnse_tutorsview(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ',0)';
            $elapse_link = 'fn_preview_question_previews_opnse_tutorsview(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ',1)';
            $previewallcancel = '/tutors-preview-module/'.$rsMuduleId.'/'.$userid;/*11-22-16 rs changes*/
            $submit = '<td><a href="' . $submit_link . '" class="next_button"><span>Next</span></a></td>';
            $submit .= '<td><a href="' . $previewallcancel . '" class="cancel"><span>Cancel</span></a></td>';
			
			$next_submit = '<a href="' . $submit_link . '" class="next_button"><span>submit</span></a>';
			
        }
		$qinfo = $this->question_answer->getallinfo_question($question_id);
		$rqtimes = $qinfo->time_elapse;
		$rqtimes_formated = gmdate("H:i:s", $rqtimes);
		$this->mysmarty->assign('qtimes',$rqtimes);
		$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
		 
		  
		$top_prev = '<a class="back_button" href="javascript: history.go(-1)">Back</a>';//rhm
		$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
		$top_tutorial = '<a class="finish" href="'.$previewallcancel.'"><span>Index</span></a>';
		  
        $this->DisplayAnswerBoard($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));

        $this->mysmarty->assign('next_submit', $next_submit);
        $this->mysmarty->assign('top_tutorial', $top_tutorial);
		$this->mysmarty->assign('top_prev',  $top_prev); //rhm top_tutorial btn
        $this->mysmarty->assign('top_next',  $top_next); //rhm top_tutorial btn
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $submit . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');

		$this->mysmarty->assign('scoreboard',$this->getscorboard());
        //set timer in the question
        if ($time_elapse) {
            $html = '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
		
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('timerelapse', $html);
        //$this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');
        $this->mysmarty->assign('quiz_score', $this->getQuestionsScore());
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
    }
	
    function showPreviewAnswerBoard_all_q_opensrce_qstudys() {
		
        $rsMuduleId = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);
        $job_id = $this->uri->segment(4);
		
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
		$cate_id = $quiz_id;
		$reslt = $this->question_answer->getQuizOption($cate_id);
		$rUser = $this->db->query("select USER_ID from QUIZ_MODULE where ID='".$rsMuduleId."'")->row();
        $userid = $rUser->USER_ID;
		//getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
		
        if ($quiz) {
           $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

		
        //set button
        //answer-complete/4/34
        $alink = $this->uri->segment(1);
        if ($alink != 'preview-from-main') {
            $submit_link = 'javascript:fn_preview_question_previews_opnse_qstudysview(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ','.$job_id.',0)';
            $elapse_link = 'fn_preview_question_previews_opnse_qstudysview(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ','.$job_id.',1)';
            $previewallcancel = '/proposal-preview-module/'.$rsMuduleId.'/'.$userid.'/'.$job_id;/*11-22-16 rs changes*/
            $submit = '<td><a href="' . $submit_link . '" class="next_button"><span>Next</span></a></td>';
            $submit .= '<td><a href="' . $previewallcancel . '" class="cancel"><span>Cancel</span></a></td>';
			
			$next_submit = '<a href="' . $submit_link . '" class="next_button"><span>submit</span></a>';
			
        }
		$qinfo = $this->question_answer->getallinfo_question($question_id);
		$rqtimes = $qinfo->time_elapse;
		$rqtimes_formated = gmdate("H:i:s", $rqtimes);
		$this->mysmarty->assign('qtimes',$rqtimes);
		$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
		 
		  
		$top_prev = '<a class="back_button" href="javascript: history.go(-1)">Back</a>';//rhm
		$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
		$top_tutorial = '<a class="finish" href="'.$previewallcancel.'"><span>Index</span></a>';
		  
        $this->DisplayAnswerBoard($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));

        $this->mysmarty->assign('next_submit', $next_submit);
        $this->mysmarty->assign('top_tutorial', $top_tutorial);
		$this->mysmarty->assign('top_prev',  $top_prev); //rhm top_tutorial btn
        $this->mysmarty->assign('top_next',  $top_next); //rhm top_tutorial btn
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $submit . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');

		$this->mysmarty->assign('scoreboard',$this->getscorboard());
        //set timer in the question
        if ($time_elapse) {
            $html = '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
		
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('timerelapse', $html);
        //$this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');
        $this->mysmarty->assign('quiz_score', $this->getQuestionsScore());
        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
    }
	
	
	
    function showPreviewAnswerBoard_all_q_opensrce_users() {
		
        $rsMuduleId = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);
		
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
		$cate_id = $quiz_id;
		$reslt = $this->question_answer->getQuizOption($cate_id);
		$rUser = $this->db->query("select USER_ID from QUIZ_MODULE where ID='".$rsMuduleId."'")->row();
        $userid = $rUser->USER_ID;
		//getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
		
        if ($quiz) {
           $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

		
        //set button
        //answer-complete/4/34
        $alink = $this->uri->segment(1);
        if ($alink != 'preview-from-main') {
            $submit_link = 'javascript:fn_preview_question_previews_opnse_usersview(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ',0)';
            $elapse_link = 'fn_preview_question_previews_opnse_usersview(' . $cate_id . ',' . $question_id . ',' . $rsMuduleId . ',1)';
            $previewallcancel = '/users-preview-opensource/'.$rsMuduleId.'/'.$userid;/*11-22-16 rs changes*/
            $submit = '<td><a href="' . $submit_link . '" class="next_button"><span>Next</span></a></td>';
            $submit .= '<td><a href="' . $previewallcancel . '" class="cancel"><span>Cancel</span></a></td>';
			
			$next_submit = '<a href="' . $submit_link . '" class="next_button"><span>submit</span></a>';
			
        }
		$qinfo = $this->question_answer->getallinfo_question($question_id);
		$rqtimes = $qinfo->time_elapse;
		$rqtimes_formated = gmdate("H:i:s", $rqtimes);
		$this->mysmarty->assign('qtimes',$rqtimes);
		$this->mysmarty->assign('qcalculates',$qinfo->question_calculator);
		 
		  
		$top_prev = '<a class="back_button" href="javascript: history.go(-1)">Back</a>';//rhm
		$top_next = '<a class="next_button" href="javascript: history.go(+1)">Next</a>';//rhm
		$top_tutorial = '<a class="finish" href="'.$previewallcancel.'"><span>Index</span></a>';
		

        $this->DisplayAnswerBoard($quiz_id);
		
        $this->mysmarty->assign('question', $this->getQuestion($question_id));

        $this->mysmarty->assign('next_submit', $next_submit);
        $this->mysmarty->assign('top_tutorial', $top_tutorial);
		$this->mysmarty->assign('top_prev',  $top_prev); //rhm top_tutorial btn
        $this->mysmarty->assign('top_next',  $top_next); //rhm top_tutorial btn
        $this->mysmarty->assign('question_no', '');
        $this->mysmarty->assign('save_link', '');
        $this->mysmarty->assign('cancel_link', '');
        $this->mysmarty->assign('preview_link', '<td>' . $submit . '</td>');
        $this->mysmarty->assign('section_title', '');
        $this->mysmarty->assign('grade_year', '');
        $this->mysmarty->assign('subject_name', '');
        $this->mysmarty->assign('chapter', '');
        $this->mysmarty->assign('score', '');
        $this->mysmarty->assign('time', '');
        $this->mysmarty->assign('script', '');

		$this->mysmarty->assign('scoreboard',$this->getscorboard());
		
        //set timer in the question
        if ($time_elapse) {
            $html = '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        } else {
            $html = '';
        }
        $this->mysmarty->assign('script', '');
        $this->mysmarty->assign('timerelapse', $html);
        //$this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $this->mysmarty->assign('quiz_score', $this->getQuestionsScore());

        $this->mysmarty->assign('selected_equation', $this->getSelectedEquation($question_id));
		
    }
//rs new changes
    function showPreviewBoardTools() {
        $cate_id = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        //set button
        //answer-complete/4/34
        $next_link = 'javascript:fn_preview_question(' . $cate_id . ',' . $question_id . ',0)';
        $elapse_link = 'fn_preview_question(' . $cate_id . ',' . $question_id . ',1)';
        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';

        //set time elapse for the question
        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $stmt = 'select * from SP_GET_WHITEBOARD_COUNT(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $hasWhiteBoard = $row->R_RESULT;
            } else {
                $hasWhiteBoard = '';
            }
        } else {
            $hasWhiteBoard = '';
        }

        $html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
        $html .= '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
        if ($hasWhiteBoard) {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date" id="li_date"></li>' . $this->config->item('CRLF');
        } else {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
        }
        //first get the top toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_H(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }
        $html .= $this->config->item('two_tab') . '<li class="tool_logout">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<a href="javascript:fn_Logout()" class="main_logout"><span>Logout</span></a>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</li>' . $this->config->item('CRLF');

        $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
        //$html .= $this->config->item('one_tab') . '<div class="pin" title="Hide Tool Panel" id="pin_tool" onclick="fn_pin_tool()">pin</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<div id="canvas_div">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');

        //only for arrow
        $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //get the vertical toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_V(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="rightside" class="fluid"></div>' . $this->config->item('CRLF');
        //display the quiz controls
        $this->DisplayQuizControls($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        //end of display quiz controls
        $html .= $this->config->item('two_tab') . '<div id="footer_holder_exam" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="fluid footer_buttons">' . $this->config->item('CRLF');

        $quiz_option = $this->getQuizOption($quiz_id);

        //$save_uri = '\'quiz-save-data\',\'f_quiz\'';
        if ($question_id == '-1') {
            $a_controller = "'" . '/quiz-save-data/' . $quiz_id . "'," . "'f_quiz'," . $quiz_id . ',' . $quiz_option;
        } else {
            $a_controller = "'" . '/quiz-edit-data/' . $quiz_id . '/' . $question_id . "'," . "'f_quiz'," .
                    $quiz_id . ',' . $quiz_option . ',' . $question_id;
        }
        /*
          $html .= $this->config->item('four_tab') . '<table class="quiz_answer">' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '<tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $next . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '</tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('four_tab') . '</table>' . $this->config->item('CRLF');
         */
        $html .= $this->config->item('six_tab') . $next . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="validation-error" id="td_error" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div id="qstudy_footer" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '<div id="pages" class="fluid">Page' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="current_page" id="curr_page">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="page_of">of</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="num_page" id="page_num">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_plus" title="Add new page" onclick="fn_addnewpage()"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_minus" title="Remove Current Page" id="remove_page" onclick="fn_removepage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_next" title="Next Page" id="next_page" onclick="fn_nextpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_prev" title="Previous Page" id="prev_page" onclick="fn_prevpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<footer id="footer" class="fluid"> </footer>' . $this->config->item('CRLF');
        $html .= '</div>' . $this->config->item('CRLF');

        $this->mysmarty->assign('script', $this->makeCSS($quiz_id));

        //set timer in the question
        if ($time_elapse) {
            $html .= $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
            $html .= '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        }

        $html .= '</form>';
        return $html;
    }

    function showPreviewFromBoardTools() {
        $cate_id = $this->uri->segment(2);
        $question_id = $this->uri->segment(3);
        $this->mysmarty->assign('quiz_id', $question_id);
        $quiz_id = $this->getQuizCategoryId($question_id);
        $userid = getSessionVar('userid');
        //get the whiteboard ids from quiz_category table as per selected quiz
        $quiz = $this->quiz_category->load($quiz_id);
        if ($quiz) {
            $whiteboard_ids = $this->quiz_category->whiteboard;
            $quiz_name = $this->quiz_category->quiz_name;
        } else {
            $whiteboard_ids = '';
            $quiz_name = '';
        }
        $this->mysmarty->assign('answer', '');
        $this->mysmarty->assign('workout', '');
        $this->mysmarty->assign('workout_draw', '');

        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $this->mysmarty->assign('user_type', $this->user_account->getUserTypeOf());

        //set button
        //answer-complete/4/34
        $next_link = 'javascript:fn_preview_from_question(' . $cate_id . ',' . $question_id . ',0)';
        $elapse_link = 'fn_preview_from_question(' . $cate_id . ',' . $question_id . ',1)';
        $next = '<a href="' . $next_link . '" class="finish"><span>Submit</span></a>';

        //set time elapse for the question
        $time_elapse = $this->question_answer->getTimeElpase($question_id);
        if ($time_elapse) {
            $this->mysmarty->assign('time_elapse', $time_elapse . ';' . $elapse_link);
        } else {
            $this->mysmarty->assign('time_elapse', '');
        }

        $stmt = 'select * from SP_GET_WHITEBOARD_COUNT(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $hasWhiteBoard = $row->R_RESULT;
            } else {
                $hasWhiteBoard = '';
            }
        } else {
            $hasWhiteBoard = '';
        }

        $html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
        $html .= '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
        if ($hasWhiteBoard) {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date" id="li_date"></li>' . $this->config->item('CRLF');
        } else {
            $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
        }
        //first get the top toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_H(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }
        $html .= $this->config->item('two_tab') . '<li class="tool_logout">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<a href="javascript:fn_Logout()" class="main_logout"><span>Logout</span></a>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</li>' . $this->config->item('CRLF');

        $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
        //$html .= $this->config->item('one_tab') . '<div class="pin" title="Hide Tool Panel" id="pin_tool" onclick="fn_pin_tool()">pin</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<div id="canvas_div">' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');

        //only for arrow
        $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //get the vertical toolbar
        $stmt = 'select * from SP_GET_WHITEBOARD_BY_QUIZ_V(' . myString($whiteboard_ids) . ')';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('three_tab') . '<li class="' .
                        $row->R_TOOL_NAME . '" title="' .
                        $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '<div id="rightside" class="fluid"></div>' . $this->config->item('CRLF');
        //display the quiz controls
        $this->DisplayQuizControls($quiz_id);
        $this->mysmarty->assign('question', $this->getQuestion($question_id));
        //end of display quiz controls
        $html .= $this->config->item('two_tab') . '<div id="footer_holder_exam" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="fluid footer_buttons">' . $this->config->item('CRLF');

        $quiz_option = $this->getQuizOption($quiz_id);

        //$save_uri = '\'quiz-save-data\',\'f_quiz\'';
        if ($question_id == '-1') {
            $a_controller = "'" . '/quiz-save-data/' . $quiz_id . "'," . "'f_quiz'," . $quiz_id . ',' . $quiz_option;
        } else {
            $a_controller = "'" . '/quiz-edit-data/' . $quiz_id . '/' . $question_id . "'," . "'f_quiz'," .
                    $quiz_id . ',' . $quiz_option . ',' . $question_id;
        }
        /*
          $html .= $this->config->item('four_tab') . '<table class="quiz_answer">' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '<tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('six_tab') . '<td class="input-box">' . $next . '</td>' . $this->config->item('CRLF');
          $html .= $this->config->item('five_tab') . '</tr>' . $this->config->item('CRLF');
          $html .= $this->config->item('four_tab') . '</table>' . $this->config->item('CRLF');
         */
        $html .= $this->config->item('six_tab') . $next . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<div class="validation-error" id="td_error" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');

        $html .= $this->config->item('three_tab') . '<div id="qstudy_footer" class="fluid">' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '<div id="pages" class="fluid">Page' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="current_page" id="curr_page">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="page_of">of</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="num_page" id="page_num">1</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_plus" title="Add new page" onclick="fn_addnewpage()"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_minus" title="Remove Current Page" id="remove_page" onclick="fn_removepage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_next" title="Next Page" id="next_page" onclick="fn_nextpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('five_tab') . '<div class="key_pages_prev" title="Previous Page" id="prev_page" onclick="fn_prevpage()" style="display:none"></div>' . $this->config->item('CRLF');
        $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<footer id="footer" class="fluid"> </footer>' . $this->config->item('CRLF');
        $html .= '</div>' . $this->config->item('CRLF');

        $this->mysmarty->assign('script', $this->makeCSS($quiz_id));

        //set timer in the question
        if ($time_elapse) {
            $html .= $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
            $html .= '<script>' . $this->config->item('CRLF');
            $html .= '  $("#time_elapse_timer").TimeCircles({' . $this->config->item('CRLF');
            $html .= '      count_past_zero: false,' . $this->config->item('CRLF');
            $html .= '      time: { Days: { show: false },' . $this->config->item('CRLF');
            $html .= '      Hours: { show: false },' . $this->config->item('CRLF');
            $html .= '      Minutes: {show:false} }' . $this->config->item('CRLF');
            $html .= '  })' . $this->config->item('CRLF');
            //addlistner
            $html .= '  .addListener(' . $this->config->item('CRLF');
            $html .= '      function(unit,value,total) {' . $this->config->item('CRLF');
            $html .= '          if (value == 0){' . $this->config->item('CRLF');
            $html .= $elapse_link;
            $html .= '          }' . $this->config->item('CRLF');
            $html .= '      }' . $this->config->item('CRLF');
            $html .= '  );' . $this->config->item('CRLF');
            $html .= '</script>' . $this->config->item('CRLF');
        }

        $html .= '</form>';
        return $html;
    }

    function getDrawWhiteboard() {
        $stmt = 'select * from SP_WHITEBOARD_COUNT_DRAW';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $hasWhiteBoard = $row->R_RESULT;
            } else {
                $hasWhiteBoard = '';
            }
        } else {
            $hasWhiteBoard = '';
        }
        $user_type = $this->user_account->getUserTypeOf();
        //echo $user_type ; die;
        //$html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
        $html = '<div id="workout_draw" class="clearfix">' . $this->config->item('CRLF');

        if ($user_type == 2) {
            $html .= $this->config->item('one_tab') . '<header class="header fluid">' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
            $stmt = 'select * from SP_WHITEBOARD_H_DRAW';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
                $html .= '<div class="linewidth">Line Width:';
                $html .= textinput('line_width', 'line_width', 1);
                //$html .= '<div class="line_width">1</div>';
                /* $html .= '<div class="spinbox">';
                  $html .= '<div class="down-triangle" onclick="fn_show_slider()"></div>';
                  $html .= '<div class="spinnerbox">';
                  $html .= '<div class="sliderarea" id="line_slider">';
                  //$html .= '<div class="spin" onclick="fn_spin()"></div>';
                  $html .= '</div>';
                  $html .= '</div>'; */
                $html .= '</div>';
                //$html .= '</div>';
            } else {
                $html .= 'error ' . $stmt;
            }

            $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('one_tab') . '</header>'; //header div end
            $html .= $this->config->item('two_tab') . '<div class="leftside fluid">' . $this->config->item('CRLF');
            $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');
			
            //get the vertical toolbar
            $stmt = 'select * from SP_WHITEBOARD_V_DRAW';
            $query = $this->db->query($stmt);
            if ($query) {
				
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
            } else {
                $html .= 'error ' . $stmt;
            }

            $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF'); //end of leftside
            $html .= $this->config->item('one_tab') . '<div class="canvas_div">' . $this->config->item('CRLF');
            $html .= '<canvas id="can_1"  width="730px" height="580px"  style=margin:0px 0 0 0px;">html5 canvas not supported</canvas>';
            //$html .= '<canvas id="can_1"  width="100%" height="100%"  style=margin:0px 0 0 0px;">html5 canvas not supported</canvas>';
            $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF'); //end of canvas_div
            //for horizontal hand
            $html .= '<div id="horizontal_hand" class="horz_hand" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand_part" class="horz_hand_part" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand1" class="horz_hand" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand_part1" class="horz_hand_part" style="display:none">hand</div>';

            $html .= '<div id="colorbox">'; //start of color box
            $html .= '<div class="color_box" style="display:none">';
            $html .= '<div class="color_box_header">Choose Color</div>';
            $html .= '<div class="close_box" onclick="fn_close_colorbox()" title="Close">Close</div>';
            $html .= '<div class="main_color" id="main_color_box">' . $this->getColorBoxMainButton();
            $html .= '<div class="button" onclick="fn_show_more_color()" id="more_color" style="margin:">more color</div>';
            $html .= '</div>'; //end main_color
            $html .= '<div class="color_button" id="main_button">' . $this->getColorButtons() . '</div>';
            $html .= '</div>'; //end color_box
            $html .= '</div>'; //end colorbox
            //start on text box
            $html .= '<div class="text_box" style="display:none">';
            $html .= '<div class="header">Choose Font</div>';
            $html .= '<div class="close_box" id="close_font_box" onClick="fn_close_fontbox()">Close</div>';

            $html .= '<div class="spinner"><div id="spinval" class="spinner_edit">15</div>';
            $html .= '<div class="upbutton" onClick="fn_SetSpinnerVal(1)"></div>';
            $html .= '<div class="downbutton" onClick="fn_SetSpinnerVal(0)"></div>';
            $html .= '</div>';
            $html .= '<div class="fontname"><div id="font_name" class="fontname_edit"></div>';
            $html .= '<div class="upbutton" onClick="fn_getFontNames()"></div>';
            $html .= '<div id="list_fonts" class="lstFont input-box"></div>';
            $html .= '</div>';
            $html .= '<div class="keys_ul_div">';
            $html .= '<ul class="keys_ul">';
            $html .= '<li class="keys_li" id="key_bold" onclick="fn_bold()" style="font-weight:bold;border-top-left-radius:4px; border-bottom-left-radius:4px;behavior: url(/pie/PIE.htc);">B</li>';
            $html .= '<li class="keys_li" id="key_underline" onclick="fn_underline()" style="text-decoration:underline;">U</li>';
            $html .= '<li class="keys_li" id="key_italic" onclick="fn_italic()" style="font-style:italic">I</li>';
            $html .= '<li class="keys_li" id="key_strikeout" onclick="fn_strikeout()" style="text-decoration:line-through;border-top-right-radius:4px; border-bottom-right-radius:4px;behavior: url(/pie/PIE.htc);">abc</li>';
            $html .= '</ul>';
            $html .= '</div>'; //end of keys
            $html .= '</div>'; //end of text_box
            //start on text box
            $html .= '<div class="draw_text_box">';
            $html .= '<div class="header">Enter your Text</div>';
            $html .= '<div class="textbox"><input id="textobject" name="textobject" class="active_input_border" style="width:140px" value=""></div>';
            $html .= '<div class="border_bottom"></div>';
            $html .= '<div class="button_area_cancel"><a class="button_no_img" href="javascript:fn_cancel_text()">Cancel</a></div>';
            $html .= '<div class="button_area"><a class="button_no_img" style="width:40px;height:15px" href="javascript:fn_enter_text()">OK</a></div>';


            $style = $this->makeCSS_draw() . $this->config->item('CRLF');
            $script = '<script>';
            $script .= '$(function() {';
            $script .= '    $("#line_width").spinner({ max: 100, min:1 });';
            $script .= '});';
            $script .= '</script>';
        } else {
            //$html .= $this->config->item('one_tab') . '</header>'; //header div end
            //$html .= $this->config->item('two_tab') . '<div class="leftside fluid"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('one_tab') . '<div class="canvas_div_normal">' . $this->config->item('CRLF');
            $html .= '<canvas id="can_1"  width="730px" height="580px"  style=margin:0px 0 0 0px;">html5 canvas not supported</canvas>';
            $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF'); //end of canvas_div
            $style = '';
            $script = '';
        }
        $html .= '</div>'; //workout_draw end
        return $script . $style . $html;
    }

    function getDrawWhiteboard_pgress() {
        $stmt = 'select * from SP_WHITEBOARD_COUNT_DRAW';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $hasWhiteBoard = $row->R_RESULT;
            } else {
                $hasWhiteBoard = '';
            }
        } else {
            $hasWhiteBoard = '';
        }
       // $user_type = $this->user_account->getUserTypeOf();

        //$html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
        $html = '<div id="workout_draw" class="clearfix">' . $this->config->item('CRLF');

            $html .= $this->config->item('one_tab') . '<header class="header fluid">' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
            $stmt = 'select * from SP_WHITEBOARD_H_DRAW';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
                $html .= '<div class="linewidth">Line Width:';
                $html .= textinput('line_width', 'line_width', 1);
                //$html .= '<div class="line_width">1</div>';
                /* $html .= '<div class="spinbox">';
                  $html .= '<div class="down-triangle" onclick="fn_show_slider()"></div>';
                  $html .= '<div class="spinnerbox">';
                  $html .= '<div class="sliderarea" id="line_slider">';
                  //$html .= '<div class="spin" onclick="fn_spin()"></div>';
                  $html .= '</div>';
                  $html .= '</div>'; */
                $html .= '</div>';
                //$html .= '</div>';
            } else {
                $html .= 'error ' . $stmt;
            }

            $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
            $html .= $this->config->item('one_tab') . '</header>'; //header div end
            $html .= $this->config->item('two_tab') . '<div class="leftside fluid">' . $this->config->item('CRLF');
            $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');
            //get the vertical toolbar
            $stmt = 'select * from SP_WHITEBOARD_V_DRAW';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
            } else {
                $html .= 'error ' . $stmt;
            }

            $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF'); //end of leftside
            $html .= $this->config->item('one_tab') . '<div class="canvas_div">' . $this->config->item('CRLF');
            $html .= '<canvas id="can_1"  width="730px" height="580px"  style=margin:0px 0 0 0px;">html5 canvas not supported</canvas>';
            //$html .= '<canvas id="can_1"  width="100%" height="100%"  style=margin:0px 0 0 0px;">html5 canvas not supported</canvas>';
            $html .= $this->config->item('one_tab') . '</div>' . $this->config->item('CRLF'); //end of canvas_div
            //for horizontal hand
            $html .= '<div id="horizontal_hand" class="horz_hand" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand_part" class="horz_hand_part" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand1" class="horz_hand" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand_part1" class="horz_hand_part" style="display:none">hand</div>';

            $html .= '<div id="colorbox">'; //start of color box
            $html .= '<div class="color_box" style="display:none">';
            $html .= '<div class="color_box_header">Choose Color</div>';
            $html .= '<div class="close_box" onclick="fn_close_colorbox()" title="Close">Close</div>';
            $html .= '<div class="main_color" id="main_color_box">' . $this->getColorBoxMainButton();
            $html .= '<div class="button" onclick="fn_show_more_color()" id="more_color" style="margin:">more color</div>';
            $html .= '</div>'; //end main_color
            $html .= '<div class="color_button" id="main_button">' . $this->getColorButtons() . '</div>';
            $html .= '</div>'; //end color_box
            $html .= '</div>'; //end colorbox
            //start on text box
            $html .= '<div class="text_box" style="display:none">';
            $html .= '<div class="header">Choose Font</div>';
            $html .= '<div class="close_box" id="close_font_box" onClick="fn_close_fontbox()">Close</div>';

            $html .= '<div class="spinner"><div id="spinval" class="spinner_edit">15</div>';
            $html .= '<div class="upbutton" onClick="fn_SetSpinnerVal(1)"></div>';
            $html .= '<div class="downbutton" onClick="fn_SetSpinnerVal(0)"></div>';
            $html .= '</div>';
            $html .= '<div class="fontname"><div id="font_name" class="fontname_edit"></div>';
            $html .= '<div class="upbutton" onClick="fn_getFontNames()"></div>';
            $html .= '<div id="list_fonts" class="lstFont input-box"></div>';
            $html .= '</div>';
            $html .= '<div class="keys_ul_div">';
            $html .= '<ul class="keys_ul">';
            $html .= '<li class="keys_li" id="key_bold" onclick="fn_bold()" style="font-weight:bold;border-top-left-radius:4px; border-bottom-left-radius:4px;behavior: url(/pie/PIE.htc);">B</li>';
            $html .= '<li class="keys_li" id="key_underline" onclick="fn_underline()" style="text-decoration:underline;">U</li>';
            $html .= '<li class="keys_li" id="key_italic" onclick="fn_italic()" style="font-style:italic">I</li>';
            $html .= '<li class="keys_li" id="key_strikeout" onclick="fn_strikeout()" style="text-decoration:line-through;border-top-right-radius:4px; border-bottom-right-radius:4px;behavior: url(/pie/PIE.htc);">abc</li>';
            $html .= '</ul>';
            $html .= '</div>'; //end of keys
            $html .= '</div>'; //end of text_box
            //start on text box
            $html .= '<div class="draw_text_box">';
            $html .= '<div class="header">Enter your Text</div>';
            $html .= '<div class="textbox"><input id="textobject" name="textobject" class="active_input_border" style="width:140px" value=""></div>';
            $html .= '<div class="border_bottom"></div>';
            $html .= '<div class="button_area_cancel"><a class="button_no_img" href="javascript:fn_cancel_text()">Cancel</a></div>';
            $html .= '<div class="button_area"><a class="button_no_img" style="width:40px;height:15px" href="javascript:fn_enter_text()">OK</a></div>';


            $style = $this->makeCSS_draw() . $this->config->item('CRLF');
            $script = '<script>';
            $script .= '$(function() {';
            $script .= '    $("#line_width").spinner({ max: 100, min:1 });';
            $script .= '});';
            $script .= '</script>';
        
        $html .= '</div>'; //workout_draw end
        return $script . $style . $html;
    }

    function getDrawWhiteboard1() {
        $stmt = 'select * from SP_WHITEBOARD_COUNT_DRAW';
        $query = $this->db->query($stmt);
        if ($query) {
            $row = $query->row();
            if ($row) {
                $hasWhiteBoard = $row->R_RESULT;
            } else {
                $hasWhiteBoard = '';
            }
        } else {
            $hasWhiteBoard = '';
        }
        $user_type = $this->user_account->getUserTypeOf();

        //$html = '<form method="post" name="f_question_answer" class="text" id="f_question_answer" enctype="" style="margin: 0px">';
        $html = '<div class="gridContainer clearfix">' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<header id="header" class="fluid">' . $this->config->item('CRLF');
        if ($user_type == 2) {
            $html .= $this->config->item('two_tab') . '<ul>' . $this->config->item('CRLF');
            /* if ($hasWhiteBoard) {
              $html .= $this->config->item('three_tab') . '<li class="tool_top_date" id="li_date"></li>' . $this->config->item('CRLF');
              } else {
              $html .= $this->config->item('three_tab') . '<li class="tool_top_date_empty" id="li_date"></li>' . $this->config->item('CRLF');
              } */
            //first get the top toolbar
            $stmt = 'select * from SP_WHITEBOARD_H_DRAW';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
            } else {
                $html .= 'error ' . $stmt;
            }

            $html .= $this->config->item('two_tab') . '</ul>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '<div id="scalculator" style="display:none"></div>' . $this->config->item('CRLF');
        }
        $html .= $this->config->item('one_tab') . '</header>' . $this->config->item('CRLF');
        //$html .= $this->config->item('one_tab') . '<div class="pin" title="Hide Tool Panel" id="pin_tool" onclick="fn_pin_tool()">pin</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('one_tab') . '<div id="canvas_div" style="height:620px">' . $this->config->item('CRLF');
        /* if ($time_elapse){
          $html .= $this->config->item('two_tab') . '<div id="time_elapse_timer" data-timer="' . $time_elapse . '"></div>' . $this->config->item('CRLF');
          } */
        $html .= $this->config->item('two_tab') . '<div id="leftside" class="fluid" style="height:620px">' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '<ul>' . $this->config->item('CRLF');

        //only for arrow
        /* $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
          $query = $this->db->query($stmt);
          if ($query) {
          foreach ($query->result() as $row) {
          $html .= $this->config->item('three_tab') . '<li class="' .
          $row->R_TOOL_NAME . '" title="' .
          $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
          }
          } else {
          $html .= 'error ' . $stmt;
          } */
        if ($user_type == 2) {
            //get the vertical toolbar
            $stmt = 'select * from SP_WHITEBOARD_V_DRAW';
            $query = $this->db->query($stmt);
            if ($query) {
                foreach ($query->result() as $row) {
                    $html .= $this->config->item('three_tab') . '<li class="' .
                            $row->R_TOOL_NAME . '" title="' .
                            $row->R_TOOL_TITLE . '"  onclick="fn_' . $row->R_TOOL_NAME . '()" id="' . $row->R_TOOL_NAME . '"></li>' . $this->config->item('CRLF');
                }
            } else {
                $html .= 'error ' . $stmt;
            }

            $html .= $this->config->item('three_tab') . '</ul>' . $this->config->item('CRLF');
            $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        }
        //$html .= '<div id="main_canvas">';
        //canvas area
        //$html .= '<div id="canvas_area">';
        if ($user_type == 2) {
            $html .= '<canvas id="can_1" width="765px" height="600px" style="position:absolute;border:0px solid;">html5 canvas not supported</canvas>';
        } else {
            $html .= '<canvas id="can_1" width="765px" height="600px" style="position:absolute;border:0px solid;left:60px;top:0px">html5 canvas not supported</canvas>';
        }
        //$html .= '</div>'; //end of canvas_area
        //$html .= '</div>'; //end of main canvas_area
        if ($user_type == 2) {
            $html .= $this->config->item('two_tab') . '<div id="rightside_draw" class="fluid">' . $this->config->item('CRLF');
        } else {
            $html .= $this->config->item('two_tab') . '<div id="rightside_draw" class="fluid">h' . $this->config->item('CRLF');
        }
        $html .= $this->config->item('four_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('three_tab') . '</div>' . $this->config->item('CRLF');
        //$html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        $html .= $this->config->item('two_tab') . '</div>' . $this->config->item('CRLF');
        if ($user_type == 2) {
            //for horizontal hand
            $html .= '<div id="horizontal_hand" class="horz_hand" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand_part" class="horz_hand_part" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand1" class="horz_hand" style="display:none">hand</div>';
            $html .= '<div id="horizontal_hand_part1" class="horz_hand_part" style="display:none">hand</div>';

            $html .= '<div id="colorbox">'; //start of color box
            $html .= '<div id="color_box" style="display:none">';
            $html .= '<div class="color_box_header">Choose Color</div>';
            $html .= '<div class="close_box" onclick="fn_close_colorbox()" title="Close">Close</div>';
            $html .= '<div class="main_color" id="main_color_box">' . $this->getColorBoxMainButton();
            $html .= '<div class="button" onclick="fn_show_more_color()" id="more_color" style="margin:">more color</div>';
            $html .= '</div>'; //end main_color
            $html .= '<div class="color_button" id="main_button">' . $this->getColorButtons() . '</div>';
            $html .= '</div>'; //end color_box
            $html .= '</div>'; //end colorbox
            //start on text box
            $html .= '<div id="text_box" style="display:none">';
            $html .= '<div class="header">Choose Font</div>';
            $html .= '<div class="close_box" id="close_font_box" onClick="fn_close_fontbox()">Close</div>';

            $html .= '<div id="spinner"><div id="spinval" class="spinner_edit">15</div>';
            $html .= '<div class="upbutton" onClick="fn_SetSpinnerVal(1)"></div>';
            $html .= '<div class="downbutton" onClick="fn_SetSpinnerVal(0)"></div>';
            $html .= '</div>';
            $html .= '<div id="fontname"><div id="font_name" class="fontname_edit"></div>';
            $html .= '<div class="upbutton" onClick="fn_getFontNames()"></div>';
            $html .= '<div id="list_fonts" class="lstFont input-box"></div>';
            $html .= '</div>';
            $html .= '<div class="keys_ul_div">';
            $html .= '<ul class="keys_ul">';
            $html .= '<li class="keys_li" id="key_bold" onclick="fn_bold()" style="font-weight:bold;border-top-left-radius:4px; border-bottom-left-radius:4px;behavior: url(/pie/PIE.htc);">B</li>';
            $html .= '<li class="keys_li" id="key_underline" onclick="fn_underline()" style="text-decoration:underline;">U</li>';
            $html .= '<li class="keys_li" id="key_italic" onclick="fn_italic()" style="font-style:italic">I</li>';
            $html .= '<li class="keys_li" id="key_strikeout" onclick="fn_strikeout()" style="text-decoration:line-through;border-top-right-radius:4px; border-bottom-right-radius:4px;behavior: url(/pie/PIE.htc);">abc</li>';
            $html .= '</ul>';
            $html .= '</div>'; //end of keys
            $html .= '</div>'; //end of text_box
            //start on text box
            $html .= '<div id="draw_text_box">';
            $html .= '<div class="header">Enter your Text</div>';
            $html .= '<div class="textbox"><input id="textobject" name="textobject" class="active_input_border" style="width:140px" value=""></div>';
            $html .= '<div class="border_bottom"></div>';
            $html .= '<div class="button_area_cancel"><a class="button_no_img" href="javascript:fn_cancel_text()">Cancel</a></div>';
            $html .= '<div class="button_area"><a class="button_no_img" style="width:40px;height:15px" href="javascript:fn_enter_text()">OK</a></div>';
            $html .= '</div>';
            $style = $this->makeCSS_draw() . $this->config->item('CRLF');
        } else {
            $style = '';
        }
        return $style . $html;
    }

    function makeCSS_draw() {
        //first get the top toolbar
        /*
          .arrow{
          background:url(../image/arrow.png) no-repeat;
          background-size:24px 24px;
          background-position:center center;
          background-color: rgba(243, 243, 243, 0.6);
          color: rgba(243, 243, 243, 0.6);
          border-radius:4px;
          width:30px;
          height:30px;
          -moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);
          -webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);
          box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);
          cursor:pointer;
          }
          .arrow:hover{
          background-color: rgba(161, 206, 215, 0.6);
          color: rgba(161, 206, 215, 0.6);
          -moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);
          -webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);
          box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);
          }

         */
        $stmt = 'select * from SP_WHITEBOARD_H_DRAW';
        $query = $this->db->query($stmt);
        $html = '<style>' . $this->config->item('CRLF');
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:inline-block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-right:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin:-5px 40px 10px -30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat;' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background-image:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat;' . $this->config->item('CRLF');


                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),#ffffff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-moz-linear-gradient(top,  #ffffff 0%, #f1f1f1 50%, #e1e1e1 51%, #f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(50%,#f1f1f1), color-stop(51%,#e1e1e1), color-stop(100%,#f6f6f6));' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-webkit-linear-gradient(top,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-o-linear-gradient(top,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-ms-linear-gradient(top,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),linear-gradient(to bottom,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#ffffff\', endColorstr=\'#f6f6f6\',GradientType=0 );' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-size:24px 24px, auto auto;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-repeat: no-repeat, no-repeat;' . $this->config->item('CRLF');

                //$html .= $this->config->item('two_tab') . 'background-color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');


                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . ':hover{' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE_HOVER . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE_HOVER . ') no-repeat;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                //for the active
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:inline-block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-right:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin:-5px 40px 10px -30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #AEAEAE;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active:hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE_HOVER . ') no-repeat #F68D20;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //get the vertical toolbar
        $stmt = 'select * from SP_WHITEBOARD_V_DRAW';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-left:-33px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-bottom:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat;' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');                                
                //$html .= $this->config->item('two_tab') . 'background-color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),#ffffff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-moz-linear-gradient(top,  #ffffff 0%, #f1f1f1 50%, #e1e1e1 51%, #f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(50%,#f1f1f1), color-stop(51%,#e1e1e1), color-stop(100%,#f6f6f6));' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-webkit-linear-gradient(top,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-o-linear-gradient(top,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),-ms-linear-gradient(top,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background: url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . '),linear-gradient(to bottom,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#ffffff\', endColorstr=\'#f6f6f6\',GradientType=0 );' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-size:24px 24px, auto auto;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-repeat: no-repeat, no-repeat;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . ':hover{' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE_HOVER . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE_HOVER . ') no-repeat;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                //for active
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-left:-33px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-bottom:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #AEAEAE;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active:hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE_HOVER . ') no-repeat #F68D20;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        //for the arrow
        $stmt = 'select * from SP_GET_WHITEBOARD_ARROW';
        $query = $this->db->query($stmt);
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-left:-33px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-bottom:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat;' . $this->config->item('CRLF');

                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(243, 243, 243, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . ':hover{' . $this->config->item('CRLF');
                //$html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-size:24px 24px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-position:center center;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background-color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'color: rgba(161, 206, 215, 0.6);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-moz-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . '-webkit-box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'box-shadow: inset 0 -1px 1px rgba(200,200,210,0.5);' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                //for active
                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'display:block;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'border-radius:3px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'width:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'height:30px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:#fff;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-left:-33px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'margin-bottom:10px;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #AEAEAE;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');

                $html .= $this->config->item('one_tab') . '.' . $row->R_TOOL_NAME . '_active:hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('two_tab') . 'background:url(/images/whiteboard_icons/' . $row->R_TOOL_IMAGE . ') no-repeat #F68D20;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . '}' . $this->config->item('CRLF');
            }
        } else {
            $html .= 'error ' . $stmt;
        }

        $html .= $this->getColorBoxStyle();
        $html .= $this->getColorBoxButtonStyle();
        $html .= $this->getColorBoxMoreStyle();

        $html .= '</style>';
        return $html;
    }

    function getColorBoxMainButton() {
        $stmt = 'select * from SP_COLOR_BOX_LIST_PRC(1)';
        $query = $this->db->query($stmt);
        $html = '';
        if ($query) {
            foreach ($query->result() as $row) {
               // $html .= $this->config->item('CRLF') . '<div class="' . $row->R_BUTTON_NAME . '" id="' . $row->R_BUTTON_NAME . '" title="' . $row->R_BUTTON_COLOR . '">' . $row->R_BUTTON_NAME . '</div>';
            }
        } else {
            $html = 'error';
        }
        return $html;
    }

    function getColorButtons() {
        $stmt = 'select * from SP_COLOR_BOX_LIST_PRC_ALL';
        $query = $this->db->query($stmt);
        $html = '<ul class="color_button_ul" id="color_list">';
        if ($query) {
            foreach ($query->result() as $row) {
                if ($row->R_BUTTON_TYPE == '3') {
                    $html .= $this->config->item('CRLF') . '<li class="' . $row->R_BUTTON_NAME . '" id="' . $row->R_BUTTON_NAME
                            . '" onclick="fn_select_color(\'' . $row->R_BUTTON_COLOR . '\')">' . $row->R_BUTTON_NAME . '</li>';
                } else {
                    $html .= $this->config->item('CRLF') . '<li class="' . $row->R_BUTTON_NAME . '" id="' . $row->R_BUTTON_NAME
                            . '" onclick="fn_select_color(\'' . $row->R_BUTTON_COLOR . '\')" style="display:none">' . $row->R_BUTTON_NAME . '</li>';
                }
                /* if ($row->R_BUTTON_TYPE == '3'){
                  $html .= $this->config->item('CRLF'). '<div class="' . $row->R_BUTTON_NAME . '" id="' . $row->R_BUTTON_NAME
                  . '" onclick="fn_select_color(\'' . $row->R_BUTTON_COLOR . '\')">' . $row->R_BUTTON_NAME .'</div>';
                  }  else {
                  $html .= $this->config->item('CRLF'). '<div class="' . $row->R_BUTTON_NAME . '" id="' . $row->R_BUTTON_NAME
                  . '" onclick="fn_select_color(\'' . $row->R_BUTTON_COLOR . '\')" style="display:none">' . $row->R_BUTTON_NAME .'</div>';
                  } */
            }
            $html .= '</ul>';
        } else {
            $html = 'error';
        }
        return $html;
    }

    function getLessMoreButtons($amore_less = '') {
        if ($amore_less == 'more') {
            $stmt = 'select * from SP_COLOR_BOX_LIST_PRC(4)';
            $stmt1 = 'select * from SP_COLOR_BOX_LIST_PRC(3)';
        } else {
            $stmt = 'select * from SP_COLOR_BOX_LIST_PRC(3)';
            $stmt1 = 'select * from SP_COLOR_BOX_LIST_PRC(4)';
        }
        $query = $this->db->query($stmt);
        $visible_button = array();
        if ($query) {
            foreach ($query->result() as $row) {
                $visible_button[] = $row->R_BUTTON_NAME;
            }
        } else {
            $visible_button = 'error';
        }

        $query_nv = $this->db->query($stmt1);
        $nvisible_button = array();
        if ($query_nv) {
            foreach ($query_nv->result() as $row) {
                $nvisible_button[] = $row->R_BUTTON_NAME;
            }
        } else {
            $nvisible_button = 'error';
        }
        $array = array('visible' => $visible_button, 'not_visible' => $nvisible_button);
        return $array;
    }

    function getColorBoxMoreButtons($amore_less = '') {
        if ($amore_less == 'm') {
            $html = '<div id="color_box_more">';
            $html .= '<div class="color_box_header">Choose Color</div>';
            $html .= '<div class="close_box" onclick="fn_close_colorbox()" title="Close">Close</div>';
            $html .= '<div class="main_color">' . $this->getColorBoxMainButton();
            $html .= '<div class="button" onclick="fn_show_more_color()" id="more_color">less color</div>';
            $html .= '</div>'; //end main_color
            $html .= '<div class="color_button">' . $this->getColorBoxButtons(4) . '</div>';
            $html .= '</div>'; //end color_box_more
        } else {
            $html = '<div id="color_box">';
            $html .= '<div class="color_box_header">Choose Color</div>';
            $html .= '<div class="close_box" onclick="fn_close_colorbox()" title="Close">Close</div>';
            $html .= '<div class="main_color">' . $this->getColorBoxMainButton();
            $html .= '<div class="button" onclick="fn_show_more_color()" id="more_color">more color</div>';
            $html .= '</div>'; //end main_color
            $html .= '<div class="color_button">' . $this->getColorBoxButtons() . '</div>';
            $html .= '</div>'; //end color_box
        }
        $script = '<script>';
        $script .= '$(function() {';
        $script .= '$( "#color_box" ).draggable({ cursor: "move", cursorAt: { top: 25, left: 25 } });';
        $script .= '$( "#color_box_more" ).draggable({ cursor: "move", cursorAt: { top: 25, left: 25 } });';
        $script .= 'var pos = $("#tool_color").offset();';
        $script .= '$("#color_box_more").css({left:120, top:pos.top - 50});';
        $script .= '$("#color_box").css({left:120, top:pos.top - 50});';
        $script .= '});';
        $script .= '</script>';
        return $html . $script;
    }

    function getColorBoxStyle() {
        //first make the main buttons
        $stmt = 'select * from SP_COLOR_BOX_LIST_PRC(1)';
        $query = $this->db->query($stmt);
        $html = '';
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('CRLF') . '.color_box .' . $row->R_BUTTON_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'background:url(/images/keyboard/' . $row->R_BUTTON_IMAGE . ') no-repeat;' . $this->config->item('CRLF');
                //$html .= $this->config->item('one_tab') . 'left:80px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'height:' . $row->R_BUTTON_HEIGHT . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'width:' . $row->R_BUTTON_WIDTH . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'color:transparent;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'font-size:0px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'left:40px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'margin:4px 10px 0 0;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'display:inline-block;' . $this->config->item('CRLF');
                $html .= '}' . $this->config->item('CRLF');
            }
        } else {
            $html = 'error';
        }
        return $html;
    }

    function getColorBoxButtonStyle() {
        //first make the main buttons
        //$stmt = 'select * from SP_COLOR_BOX_LIST_PRC(3)';
        $stmt = 'select * from SP_COLOR_BOX_LIST_PRC_ALL';
        $query = $this->db->query($stmt);
        $html = '';
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('CRLF') . '.color_box .' . $row->R_BUTTON_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'background:' . $row->R_BUTTON_COLOR . ';' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'border:1px solid #D6D6D6;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'height:' . $row->R_BUTTON_HEIGHT . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'width:' . $row->R_BUTTON_WIDTH . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'color:transparent;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'font-size:0px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'position:relative;' . $this->config->item('CRLF');
                //$html .= $this->config->item('one_tab') . 'margin:' . $row->R_BUTTON_TOP . 'px -2px 0 0;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'behavior: url(/pie/PIE.htc);' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'margin-right:-2px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'margin-right:0px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'margin-bottom:4px;' . $this->config->item('CRLF');
                //$html .= $this->config->item('one_tab') . 'margin:1px -5px -2px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'top:' . $row->R_DRAW_BUTTON_TOP . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'display:inline-block;' . $this->config->item('CRLF');
                $html .= '}' . $this->config->item('CRLF');

                $html .= $this->config->item('CRLF') . '.color_box .' . $row->R_BUTTON_NAME . ':hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('CRLF') . '-webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.075),0 0 8px rgba(82, 168, 236, 0.6);';
                $html .= $this->config->item('CRLF') . '-moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.075),0 0 8px rgba(82, 168, 236, 0.6);';
                $html .= $this->config->item('CRLF') . 'box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.075),0 0 8px rgba(82, 168, 236, 0.6);';
                $html .= '}' . $this->config->item('CRLF');
            }
        } else {
            $html = 'error';
        }
        return $html;
    }

    function getColorBoxMoreStyle() {
        //first make the main buttons
        $stmt = 'select * from SP_COLOR_BOX_LIST_PRC(1)';
        $query = $this->db->query($stmt);
        $html = '';
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('CRLF') . '#color_box_more .' . $row->R_BUTTON_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'background:url(/images/keyboard/' . $row->R_BUTTON_IMAGE . ') no-repeat;' . $this->config->item('CRLF');
                //$html .= $this->config->item('one_tab') . 'left:80px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'height:' . $row->R_BUTTON_HEIGHT . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'width:' . $row->R_BUTTON_WIDTH . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'color:transparent;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'font-size:0px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'left:40px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'margin:4px 10px 0 0;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'display:inline-block;' . $this->config->item('CRLF');
                $html .= '}' . $this->config->item('CRLF');
            }
        } else {
            $html = 'error';
        }
        return $html;
    }

    function getColorBoxMoreButtonStyle() {
        //first make the main buttons
        $stmt = 'select * from SP_COLOR_BOX_LIST_PRC(4)';
        $query = $this->db->query($stmt);
        $html = '';
        if ($query) {
            foreach ($query->result() as $row) {
                $html .= $this->config->item('CRLF') . '#color_box_more .' . $row->R_BUTTON_NAME . '{' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'background:' . $row->R_BUTTON_COLOR . ';' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'border:1px solid #D6D6D6;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'cursor:pointer;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'height:' . $row->R_BUTTON_HEIGHT . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'width:' . $row->R_BUTTON_WIDTH . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'color:transparent;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'font-size:0px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'position:relative;' . $this->config->item('CRLF');
                //$html .= $this->config->item('one_tab') . 'margin:' . $row->R_BUTTON_TOP . 'px -2px 0 0;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'behavior: url(/pie/PIE.htc);' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'margin-right:-2px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'top:' . $row->R_DRAW_BUTTON_TOP . 'px;' . $this->config->item('CRLF');
                $html .= $this->config->item('one_tab') . 'display:inline-block;' . $this->config->item('CRLF');
                $html .= '}' . $this->config->item('CRLF');

                $html .= $this->config->item('CRLF') . '#color_box_more .' . $row->R_BUTTON_NAME . ':hover{' . $this->config->item('CRLF');
                $html .= $this->config->item('CRLF') . '-webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.075),0 0 8px rgba(82, 168, 236, 0.6);';
                $html .= $this->config->item('CRLF') . '-moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.075),0 0 8px rgba(82, 168, 236, 0.6);';
                $html .= $this->config->item('CRLF') . 'box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.075),0 0 8px rgba(82, 168, 236, 0.6);';
                $html .= '}' . $this->config->item('CRLF');
            }
        } else {
            $html = 'error';
        }
        return $html;
    }
	
	function myEverydayTask_by_ajax(){
	
		$userid = getSessionVar('userid');
        $taskfor = $this->input->post('task_for');
        $load_timer = $this->input->post('load_timer');
        $hostid = $this->input->post('host_id');
        $current_date = getSessionVar('current_date');
        $current_date = new DateTime($current_date);
        $current_date = date_format($current_date,"Y-m-d");

        //perpare sql statement as per task
        switch ($taskfor) {

            case 4:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_TUTOR(' . myInteger($hostid) . ',' . myInteger($userid) . ',' . myDate($current_date) . ')';
                
                break;

            case 6:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_SCHOOL(' . $userid . ',' . myDate($current_date) . ')';
                break;

            case 8:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_CORP(' . myInteger($userid) . ',' . myDate($current_date) . ')';
                break;

            case 10:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_TR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                break;

            default:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_TR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                break;
        }
		
		$query = $this->db->query($stmt);
			
			/* load account */
			//load user
        $load = $this->user_account->load($userid);
        if ($load) {
            if ($hostid == 0) {
                $parent_id = $this->user_account->host_id;
            } else {
                $parent_id = $hostid;
            }
            $student_year = $this->user_account->student_year;
            $parent_load = $this->user_account->load($parent_id);
            $school_id = $this->user_account->school_id;
            $school_load = $this->user_account->load($school_id);
            $corpo_id = $this->user_account->corporate_id;
            $corpo_load = $this->user_account->load($corpo_id);
		}
		/* load account */
	
            $i = 1;
            $j = 1;
            $k = 1;
            $html = '';
				$arrays_slice = array_slice($query->result(), $load_timer, 10);
				if($arrays_slice){
				
                foreach ($arrays_slice as $row) {
				
               // foreach ($query->result() as $row) {
					
                    $hasRepeat = $this->isModuleHasRepeatation_by_ajax($row->R_ID, '', $hostid,$taskfor);
                   //$hasRepeat =false;
                    /* if (!$hasRepeat) {
                      $hasRepeat = $this->isModuleCompleted($row->R_ID);
                      } */
                    $tracker_name = $row->R_TRACKER_NAME;
                    $indi_name = $row->R_INDIVIDUAL_NAME;

                    //first create when module hasn't complete
                    $first_quiz = $this->getFirstQuizId($taskfor, $row->R_ID, '', 1, $hostid);
                   
                    if (!$hasRepeat) {
                        //if ($current_date == $row->R_MODULE_DATE){
                            if (!$hostid) {
                                $alink = '/my-exam/0/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                            } else {
                                $alink = '/my-exam/' . $hostid . '/' . $taskfor . '/' . $row->R_ID . '/' . $first_quiz . '/0';
                            }
                        /* }  else {
                            $alink = 'undone';
                        } */
                    } else {
                        $alink = 'complete';
                    }
                    if ($student_year == $row->R_GRADE_YEAR) {
                        $isall_student = $row->R_IS_ALL_STUDENT;
                        if (!$isall_student) {//not all student, just selectde student
                            $students = explode(',', $row->R_SELECTED_STUDENTS);
                            if (count($students) > 0) {
                                foreach ($students as $student) {
                                    if ($student == $userid) {
                                        $html .= "<tr data-tt-id='" . $i . "'>";
                                        switch ($alink){
                                            case 'complete':
                                                $html .= '<td style="width:30%"><span class="indenter" style="padding-left: 0px;"></span><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                                break;
                                            
                                            case 'undone':
                                                $html .= '<td style="width:30%"><span class="indenter" style="padding-left: 0px;"></span><span class=\'undone\'>' . $row->R_MODULES_NAME . '</span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                break;
                                            
                                            default :
                                                $html .= '<td style="width:30%"><span class="indenter" style="padding-left: 0px;"></span><span class=\'file\'><a href="javascript:fn_everydaystudyvalidity(\'' . $alink . '\','.$k.')" data_attr_rowid="'.$k.'" class="alink rseveopen rsalink_'.$k.'">' . $row->R_MODULES_NAME . '</a></span></td>';
                                                $html .= '<td>' . $tracker_name . '</td>';
                                                $html .= '<td>' . $indi_name . '</td>';
                                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                                $html .= '</tr>';
                                                $k++;
                                                break;
                                        }
                                        /*if ($alink) {
                                            $html .= '<td style="width:30%"><span class=\'file\'><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></span></td>';
                                            $html .= '<td>' . $tracker_name . '</td>';
                                            $html .= '<td>' . $indi_name . '</td>';
                                            $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                            $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                            $html .= '</tr>';
                                        } else {
                                            $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                            $html .= '<td>' . $tracker_name . '</td>';
                                            $html .= '<td>' . $indi_name . '</td>';
                                            $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                            $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                            $html .= '</tr>';
                                            $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                        }*/
                                        //}
                                        //$html .= '<tr>';
                                    }
                                }
                            }
                        } else {//all student
                            $html .= "<tr data-tt-id='" . $i . "'>";
                            switch ($alink){
                                case 'complete':
                                    $html .= '<td style="width:30%"><span class="indenter" style="padding-left: 0px;"></span><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                    $html .= '</tr>';
                                    break;

                                case 'undone':
                                    $html .= '<td style="width:30%"><span class="indenter" style="padding-left: 0px;"></span><span class=\'undone\'>' . $row->R_MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= '</tr>';
                                    break;

                                default :
                                    $html .= '<td style="width:30%"><span class="indenter" style="padding-left: 0px;"></span><span class=\'file\'><a href="javascript:fn_everydaystudyvalidity(\'' . $alink . '\','.$k.')" data_attr_rowid="'.$k.'" class="alink rseveopen rsalink_'.$k.'">' . $row->R_MODULES_NAME . '</a></span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                    $html .= '<tr>';
                                    $k++;
                                    break;
                            }
                            
                            /*
                            if ($alink) {
                                $html .= '<td style="width:30%"><span class=\'file\'><a href="' . $alink . '" class="alink">' . $row->R_MODULES_NAME . '</a></span></td>';
                                $html .= '<td>' . $tracker_name . '</td>';
                                $html .= '<td>' . $indi_name . '</td>';
                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                $html .= '<tr>';
                            } else {
                                $html .= '<td style="width:30%"><span class=\'complete\'>' . $row->R_MODULES_NAME . '</span></td>';
                                $html .= '<td>' . $tracker_name . '</td>';
                                $html .= '<td>' . $indi_name . '</td>';
                                $html .= '<td>' . $row->R_SUBJECT_NAME . '</td>';
                                $html .= '<td>' . $row->R_CHAPTER_NAME . '</td>';
                                $html .= $this->getRepeatedQuestions($row->R_ID, $i);
                                $html .= '</tr>';
                            }
                            */
                            //$html .= '<tr>';
                        }
                    }
                    $i++;
                    $j++;
                }//end of foreach loop
				}else{
					$html = '<tr><td colspan="5" style="text-align:center;">all task has been loaded</td></tr>';
				}
		return $html;
	}
	/* new recent task */
	function my_recent_EverydayTask(){
		    $taskfor = $this->uri->segment(2);
        $userid = getSessionVar('userid');
		$style = '';
        if ($taskfor == 4) {
			
            //for treeview
            $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
            $script .= '$("#maintask_table").treetable({ expandable: true });' . $this->config->item('CRLF');
            //$script .= '        $(".treetable").collapseAll();' . $this->config->item('CRLF');
            // Highlight selected row
            /*$script .= '            $("#maintask_table tbody").on("mousedown", "tr", function() {' . $this->config->item('CRLF');
            $script .= '            $(".selected").not(this).removeClass("selected");' . $this->config->item('CRLF');
            $script .= '            $(this).toggleClass("selected");' . $this->config->item('CRLF');
            $script .= '        });' . $this->config->item('CRLF');*/
            $script .= '</script>' . $this->config->item('CRLF');

            $stmt = 'select * from SP_GET_STUDENT_TUTORS(' . myInteger($userid) . ')';
            $html = '<table id="maintask_table" class="treetable">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #D6E4F6;color:#000">Tutor Name</th>'; // class="task_table_header"
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            $query = $this->db->query($stmt);
			$style .= '<style>' . $this->config->item('CRLF');
            if ($query) {
                foreach ($query->result() as $row) {
					
					/* new for style */
					
					$style .= '#task .table_div_' . $row->R_ID . '{' . $this->config->item('CRLF');
                    $style .= '	margin:5px 0 0 5px;' . $this->config->item('CRLF');
                    $style .= '	width:98%;' . $this->config->item('CRLF');
                    $style .= '	overflow-y:auto;' . $this->config->item('CRLF');
                    $style .= '	height:430px;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');

                    $style .= '#task .subjectnames_' . $row->R_ID . '{' . $this->config->item('CRLF');
                    $style .= '    position:relative;' . $this->config->item('CRLF');
                    $style .= '    padding-bottom:10px;' . $this->config->item('CRLF');
                    $style .= '    background:#1F3367;' . $this->config->item('CRLF');
                    $style .= '    color:#fff;' . $this->config->item('CRLF');
                    $style .= '    max-height:120px;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' ul{' . $this->config->item('CRLF');
                    $style .= '    background:#1f3367;' . $this->config->item('CRLF');
                    $style .= '    margin-top:10px;' . $this->config->item('CRLF');
                    $style .= '    margin-bottom:10px;' . $this->config->item('CRLF');
                    $style .= '    padding-bottom:10px !important;' . $this->config->item('CRLF');
                    $style .= '    width:98.6%;' . $this->config->item('CRLF');
                    $style .= '    float:left;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' .spinbox{' . $this->config->item('CRLF');
                    $style .= '   float:right;' . $this->config->item('CRLF');
                    $style .= '   margin-right:3px;' . $this->config->item('CRLF');
                    $style .= '   width:10px;' . $this->config->item('CRLF');
                    $style .= '   height:7px;' . $this->config->item('CRLF');
                    $style .= '   margin:-25px 0 0 33.24%;' . $this->config->item('CRLF');
                    $style .= '   position:absolute;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' .spinbox .leftbutton{' . $this->config->item('CRLF');
                    $style .= '    background:url(/images/back_button_w.png) no-repeat;' . $this->config->item('CRLF');
                    $style .= '    width:4px;' . $this->config->item('CRLF');
                    $style .= '    height:7px;' . $this->config->item('CRLF');
                    $style .= '    margin:0px 0 0 0px;' . $this->config->item('CRLF');
                    $style .= '    cursor:pointer;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' .spinbox .rightbutton{' . $this->config->item('CRLF');
                    $style .= '    background:url(/images/right_button_w.png) no-repeat;' . $this->config->item('CRLF');
                    $style .= '    width:4px;' . $this->config->item('CRLF');
                    $style .= '    height:7px;' . $this->config->item('CRLF');
                    $style .= '    margin:-7px 0 0 6px;' . $this->config->item('CRLF');
                    $style .= '    cursor:pointer;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' li{' . $this->config->item('CRLF');
                    $style .= '    display:inline-block;' . $this->config->item('CRLF');
                    $style .= '    margin-right:15px;' . $this->config->item('CRLF');
                    $style .= '    margin-left:0px;' . $this->config->item('CRLF');
                    $style .= '    padding-right:4px;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' li a{' . $this->config->item('CRLF');
                    $style .= '  text-decoration:none;' . $this->config->item('CRLF');
                    $style .= '  color:#fff;' . $this->config->item('CRLF');
                    $style .= '  font-size:12px;' . $this->config->item('CRLF');
                    $style .= '}';
                    $style .= '.li_active_' . $row->R_ID . '{' . $this->config->item('CRLF');
                    $style .= '    font-weight:bold;' . $this->config->item('CRLF');
                    $style .= '}' . $this->config->item('CRLF');
                    $style .= '#task .subjectnames_' . $row->R_ID . ' li a:hover{' . $this->config->item('CRLF');
                    $style .= '    color:#EF0D10;' . $this->config->item('CRLF');
                    $style .= '}';

					
					/* new for style */
					
					
					
                    $html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $row->R_USER_LASTNAME . ' ' . $row->R_USER_FIRSTNAME . '</td>';
                    $html .= '</tr>';
                    //$html .= "<tr data-tt-id='" . $row->R_ID . "'>";
                    $html .= "<tr data-tt-id='c" . $row->R_ID . "' data-tt-parent-id='" . $row->R_ID . "'>";
                    $html .= '<td style="border: 1px solid #D6E4F6">' . $this->show_my_recent_task($row->R_ID) . '</td>';
                    $html .= '</tr>';
                }
                $html .= '</tbody>';
            }
            $html .= '</table>';
			$style .= '</style>' . $this->config->item('CRLF');
        } else {
            $script = '';
			$style .= '';
            $html = $this->show_my_recent_task();
        }
		
		
        return $style .$html . $script;
	}
	
	function check_is_complete_or_repeat($module_id){
		//$module_id = 716;
		$return_value = 0;/* 0:Not completed,1:completed, 2:repeated, 3: repeated but completed. */
		$cur_date = date('m/d/Y');
		$cur_date_format = date('Y-m-d');
		$userid = getSessionVar('userid');
		$result_demo = $this->db->query("select MODULE_ID from QUEST_ANS_STUDENT where USER_ID='".$userid."' AND MODULE_ID='".$module_id."'")->row();
		
		if($result_demo){
			$demos_re = $this->db->query("select REPEAT_DATE from MODULE_REPEAT where QUIZ_MODULE_ID='".$module_id."'")->row();
			if($demos_re){
				$expld_date = explode(';',$demos_re->REPEAT_DATE);
			
				if(in_array($cur_date,$expld_date)){
					
					$demos_re_day = $this->db->query("select REPEAT_DATE from MODULE_REPEATS where QUIZ_MODULE_ID='".$module_id."' AND USER_ID='".$userid."' AND REPEAT_DATE='".$cur_date_format."'")->row();
					if($demos_re_day){
						$return_value = 1;
					}else{
						$return_value = 2;
					}
				}else{
					$return_value = 1;
				}
			}else{
				$return_value = 1;
			}
		}else{
			$return_value = 0;
		}
		return $return_value;
	}

	function check_is_completed_or_not_complete($module_id,$hostid,$taskfor){
		$userid = getSessionVar('userid');
		$C_date = date('Y-m-d');		
		$result_demo = $this->db->query("select MODULE_ID from QUEST_ANS_STUDENT where USER_ID='".$userid."' AND MODULE_ID='".$module_id."'")->row();
		
		$reslt = $this->db->query("select * from QUESTION_REPEATED where MODULE_ID='".$module_id."' and USER_ID='".$userid."' and HOST_ID='".$hostid."' and STATUS=0")->result();
		$retrn_list = array();
			if($result_demo){
/* 0:Not completed,1:completed, 2:repeated, 3: repeated but completed. */		
				if($reslt){
					foreach($reslt as $rgrow){
						$r_rptdate = explode(',',$rgrow->REPEATED_DATES);
						if(in_array($C_date,$r_rptdate)){
							
							$clss = $this->is_m_cmplete_or_not($rgrow->MODULE_ID,$rgrow->QUESTION_ID,$taskfor,$hostid);
							if($clss=='file'){
								$retrn_list = 2;
							}else{
								$retrn_list = 3;
							}
							
						}else{
							$retrn_list = 3;
						}
					}
				}else{
					$retrn_list = 1;
				}
			}else{
				$retrn_list = 0;
			}	
				
		return $retrn_list;
	}
	
	function rsquestion_repeated_list($module_id,$hostid){
		$userid = getSessionVar('userid');
		$C_date = date('Y-m-d');
		$reslt = $this->db->query("select * from QUESTION_REPEATED where MODULE_ID='".$module_id."' and USER_ID='".$userid."' and HOST_ID='".$hostid."' and STATUS=0")->result();
		$retrn_list = array();
		if($reslt){
			foreach($reslt as $rgrow){
				$r_rptdate = explode(',',$rgrow->REPEATED_DATES);
				if(in_array($C_date,$r_rptdate)){
					$retrn_list = 1;
				}else{
					$retrn_list = 0;
				}
			}
		}
		return $retrn_list;
	}	
	function is_m_cmplete_or_not($module_id,$question_id,$taskfor,$hostid){
		$userid = getSessionVar('userid');
		$C_date = date('Y-m-d');
		$reslt = $this->db->query("select * from QUEST_ANS_STUDENT where MODULE_ID='".$module_id."' and USER_ID='".$userid."' and ANSWER_DEFAULT_DATE='".$C_date."' and QUESTION_ID='".$question_id."' and ERROR_REV='-1' and ANSWER_RIGHT=0")->result();
		$nm_repeated = count($reslt);
		if($nm_repeated>=3){
			$class = 'undone';
		}else{
			$reslts_is_com = $this->db->query("select * from QUEST_ANS_STUDENT where MODULE_ID='".$module_id."' and USER_ID='".$userid."' and ANSWER_DEFAULT_DATE='".$C_date."' and QUESTION_ID='".$question_id."' and ERROR_REV='-1' and ANSWER_RIGHT=1")->row();
			if($reslts_is_com){
				$class = 'completed';
			}else{
				$class = 'file';
			}
			
		}
		return $class;
				
	}
	function getRepeatedQuestions_list_byrs($module_id,$hostid,$taskfor,$aparent){
		$userid = getSessionVar('userid');
		$C_date = date('Y-m-d');
		$reslt = $this->db->query("select * from QUESTION_REPEATED where MODULE_ID='".$module_id."' and USER_ID='".$userid."' and HOST_ID='".$hostid."' and STATUS=0")->result();
		$retrn_list = array();
		$html = '';
		if($reslt){
			$j=2;
			$m=1;
			foreach($reslt as $row){
				$r_rptdate = explode(',',$row->REPEATED_DATES);
				if(in_array($C_date,$r_rptdate)){
					$clss = $this->is_m_cmplete_or_not($row->MODULE_ID,$row->QUESTION_ID,$taskfor,$hostid);
					$html .= "<tr class='rsrepeat_questions' data-tt-id='" . $aparent . '-' . $j . "' data-tt-parent-id='" . $aparent . "'>";
					if($clss=='file'){
						 $alink = '/repeat/' . $taskfor . '/' . $row->MODULE_ID . '/' . $row->QUESTION_ID . '/' . $row->QUESTION_ID . '/' . $C_date;
					}elseif($clss=='completed'){
						$alink='#';
					}else{
						$alink='#';
					}
               
                $heldon = $row->ANSWER_DEFAULT_DATE;
                $html .= '<td colspan="5"><span class="'.$clss.'"><a href="' . $alink . '" class="alink">Repetition for [Q-' . $m . '] held on ' . $heldon . '</a></span></td>';
                $html .= '</tr>';
                $j++;
                $m++;
				}
			}
		}
		return $html;
	}
	
	function show_my_recent_task($hostid = 0){
		setSessionVar('task', '');
        setSessionVar('module', '');
        setSessionVar('question', '');
        setSessionVar('question_answer', '');
        setSessionVar('prevquestion', '');
        setSessionVar('allprev', '');
        setSessionVar('skip', '');
        setSessionVar('start_time', '');
        setSessionVar('end_time', '');
        setSessionVar('alltasks', '');

		$userid = getSessionVar('userid');
        $taskfor = $this->uri->segment(2);
        $current_date = getSessionVar('current_date');
        $current_date = new DateTime($current_date);
        $current_date = date_format($current_date,"Y-m-d");
        //perpare sql statement as per task
        switch ($taskfor) {
            case 4:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_TUTOR(' . myInteger($hostid) . ',' . myInteger($userid) . ',' . myDate($current_date) . ')';
                $hostname = 'Tutor';
                break;

            case 6:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_SCHOOL(' . $userid . ',' . myDate($current_date) . ')';
                $hostname = 'School';
                break;

            case 8:
                $stmt = 'select * from SP_QUIZ_MODULE_LIST_CORP(' . myInteger($userid) . ',' . myDate($current_date) . ')';
                $hostname = 'School';
                break;

            case 10:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_TR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                $hostname = 'QStudy';
                break;

            default:
                $country = getSessionVar('country_name');
                $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }  else {
                    $stmt = 'select * from SP_QUIZ_MODULE_LIST_QSTUDY_TR(' . myInteger($userid) . ',' . myDate($current_date) . ',' . myInteger($country, true) . ')';
                }
                $hostname = 'QStudy';
                break;
        }       
        //for treeview
        $script = $this->config->item('CRLF') . '<script>' . $this->config->item('CRLF');
        //$script .= '    $(function() {' . $this->config->item('CRLF');
        $script .= '        $("#task_table").treetable({ expandable: true });' . $this->config->item('CRLF');
        // Highlight selected row
        $script .= '        $("#task_table tbody").on("mousedown", "tr", function() {' . $this->config->item('CRLF');
        $script .= '        $(".selected").not(this).removeClass("selected");' . $this->config->item('CRLF');
        $script .= '        $(this).toggleClass("selected");' . $this->config->item('CRLF');
        $script .= '        });' . $this->config->item('CRLF');
        $script .= '</script>' . $this->config->item('CRLF');
        //load user
        $load = $this->user_account->load($userid);
        if ($load) {
            if ($hostid == 0) {
                $parent_id = $this->user_account->host_id;
            } else {
                $parent_id = $hostid;
            }
            $student_year = $this->user_account->student_year;
            $parent_load = $this->user_account->load($parent_id);
            $school_id = $this->user_account->school_id;
            $school_load = $this->user_account->load($school_id);
            $corpo_id = $this->user_account->corporate_id;
            $corpo_load = $this->user_account->load($corpo_id);
            switch ($taskfor) {
                case 4:
                    if ($parent_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname;
                                    //$username = $lastname . ' ' . $fistname . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Tutor :' . $username;
                        } else {
                            $username = 'Tutor';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = '';
                        $logo = '';
                    }
                    break;

                case 6:
                    if ($school_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname ;
                                    //$username = $lastname . ' ' . $fistname . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'School :' . $username;
                        } else {
                            $username = 'School';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'School';
                        $logo = '';
                    }
                    break;

                case 8:
                    if ($corpo_load) {
                        $lastname = $this->user_account->user_lastname;
                        $fistname = $this->user_account->user_firstname;
                        $uname = $this->user_account->user_name;
                        if ($lastname) {
                            if ($fistname) {
                                if ($uname) {
                                    $username = $lastname . ' ' . $fistname;
                                    //$username = $lastname . ' ' . $fistname . ' ' . $uname;
                                } else {
                                    $username = $lastname . ' ' . $fistname;
                                }
                            } else {
                                $username = $lastname;
                            }
                        } else {
                            $username = '';
                        }
                        if ($username) {
                            $username = 'Corporate :' . $username;
                        } else {
                            $username = 'Corporate';
                        }
                        $logo = $this->user_account->logo;
                    } else {
                        $username = 'Corporate';
                        $logo = '';
                    }
                    break;

                /* case 6:
                  $username = $this->user_account->user_lastname;
                  break; */

                case 10:
                    $username = 'Q-Study';
                    $logo = '';
                    break;
            }
			if($hostid==0){
				$getHostId = $this->db->query("select R_ID,R_PARENT_ID from SP_GET_USER_HOST('".$userid."','".$taskfor."')")->row();
				if($getHostId){
					$hostid = $getHostId->R_PARENT_ID;
				}
				
			}

/* 			
			$rtmt = $this->db->query("select * from QUIZ_MODULE where QUIZ_MODULE.ID NOT IN(select QUEST_ANS_STUDENT.MODULE_ID from QUEST_ANS_STUDENT where QUEST_ANS_STUDENT.USER_ID='".$userid."') and QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."')")->result(); */
			
/* testing my custom query */
	//echo "select * from QUIZ_MODULE where QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.ID NOT IN(select QUEST_ANS_STUDENT.MODULE_ID from QUEST_ANS_STUDENT where QUEST_ANS_STUDENT.USER_ID='".$userid."') QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."')";
	//QUIZ_MODULE.ID NOT IN(select QUEST_ANS_STUDENT.MODULE_ID from QUEST_ANS_STUDENT where QUEST_ANS_STUDENT.USER_ID='".$userid."') and
	switch($taskfor){
		case 4:
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 6:
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 8:
			
			$rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
			break;
		case 10:
			if($hostid==0){
				$gethost = $this->db->query("select USER_ID from USER_ACCOUNT where USER_TYPE=10")->row();
				if($gethost){
					$hostid = $gethost->USER_ID;
				}
				
			}
			
			$country = getSessionVar('country_name');
            $account_type = getSessionVar('account_type');
                if (!$account_type){
                    $rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and COUNTRY_ID='".$country."' and FOR_TRIAL IS NULL and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') order by QUIZ_MODULE.ID ASC")->result();
                }  else {
					//for trail
                    $rtmt = $this->db->query("select * from QUIZ_MODULE where  QUIZ_MODULE.MODULE_TYPE=1 and QUIZ_MODULE.USER_ID='".$hostid."' and (QUIZ_MODULE.IS_ALL_STUDENT=1 or QUIZ_MODULE.SELECTED_STUDENTS CONTAINING '".$userid."') and COUNTRY_ID='".$country."' and FOR_TRIAL=1 order by QUIZ_MODULE.ID ASC")->result();
                }
				
                $hostname = 'QStudy';
			break;
	}
			
            //make the top header for showing tracker name and logo
            $html = '<div id="task">';
            $html .= '<div class="header">' . $this->user_account->getlogo($logo);

            /* if ($username) {
              $html .= '<div class="title">' . $hostname . '-' . $username . '</div>';
              } else {
              $html .= '<div class="title">&nbsp;</div>';
              } */
            //$html .= '<div class="title">' . $this->show_trackername($stmt) . '</div>';
            $html .= '<div class="title">' . $username . '</div>';
			
            $html .= '</div>';
			$subjct_lst = $this->gt_userssubjects_rtmt($rtmt,$taskfor,$hostid,$student_year);
			//show subject list if it is provide by host
			if($hostid==10){
				$html .= '<div class="common_styles_subjects eve_sublist_q subjectnames_' . $hostid . '">' . $this->get_host_subject_for_student_eve($hostid) . '</div>';
			}else{
				  $html .= '<div class="common_styles_subjects eve_sublist_g subjectnames_' . $hostid . '">' . $this->getSubjectNames_users_all($subjct_lst,$hostid) . '</div>';
			}
			
			//show subject list if it is provide by host
            //make modules header
            if (!$hostid) {
				$html .= '<div class="table_div">';
			} else {
				$html .= '<div class="table_div_' . $hostid . '">';
			}
			
            $html .= '<table id="task_table" class="treetable rs_mytask_table dataTable" style="border: 1px solid #C7DAF4;">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th style="width:30%;background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Module Name</th>'; // class="task_table_header"
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Tracker Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Individual Name</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Subject</th>';
            $html .= '<th style="background: #D6E4F6;border: 1px solid #C7DAF4;color:#000">Chapter</th>';
            //$html .= '<th>Module ID</th>';
            //$html .= '<th>Has Repeat</th>';
            //$html .= '<th>User ID</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            //now make the parent tree node
            /* $html .= '<div class="progress_tree_div">';
              $html .= '<ul id="task_lists">' . $this->config->item('CRLF');
              $html .= '<li>'; */
            $html .= '<tbody>';

            $query = $this->db->query($stmt);
			
			//print_r($query->result());
			//exit;
			
            $i = 1;
            $j = 1;
            $k = 1;
			$skl=1;
            if ($query) {				
                foreach ($rtmt as $row) {
					/* $is_rept_complte = $this->check_is_complete_or_repeat($row->ID);
					$is_repts_res = $this->check_is_completed_or_not_complete($row->ID,$hostid); */
					$is_rept_complte = $this->check_is_completed_or_not_complete($row->ID,$hostid,$taskfor);
					if($is_rept_complte!=1){
						$tracker_name = $row->TRACKER_NAME;
						$indi_name = $row->INDIVIDUAL_NAME;

						//first create when module hasn't complete
						$first_quiz = $this->getFirstQuizId($taskfor, $row->ID, '', 1, $hostid);
					   
						$alink = '/my-exam/' . $hostid . '/' . $taskfor . '/' . $row->ID . '/' . $first_quiz . '/0';
						   
						if($skl==1){
							$active_class = 'active_val';
						}else{
							$active_class = 'active_val_not';
						}					   
						
						if($student_year == $row->GRADE_YEAR) {
							if($is_rept_complte==0){
								//echo '<pre>';
								//print_r($row);
								
							$html .= "<tr data-tt-id='" . $i . "'>";
							$html .= '<td class="'.$active_class.'" style="width:30%"><span class=\'file\'><a href="javascript:fn_everydaystudyvalidity(\'' . $alink . '\','.$k.')" data_attr_rowid="'.$k.'" class="alink rseveopen rsalink_'.$k.'">' . $row->MODULES_NAME . '</a></span></td>';
							$html .= '<td>' . $tracker_name . '</td>';
							$html .= '<td>' . $indi_name . '</td>';
							$html .= '<td>' . $row->SUBJECT_NAME . '</td>';
							$html .= '<td>' . $row->CHAPTER_NAME . '</td>';
							$html .= '<tr>';
								$k++;
							}elseif($is_rept_complte==2){
								$html .= "<tr data-tt-id='" . $i . "'>";
								$html .= '<td style="width:30%"><span class=\'complete\'>' . $row->MODULES_NAME . '</span></td>';
                                    $html .= '<td>' . $tracker_name . '</td>';
                                    $html .= '<td>' . $indi_name . '</td>';
                                    $html .= '<td>' . $row->SUBJECT_NAME . '</td>';
                                    $html .= '<td>' . $row->CHAPTER_NAME . '</td>';
                                    //$html .= $this->getRepeatedQuestions($row->ID, $i);
                                    $html .= $this->getRepeatedQuestions_list_byrs($row->ID,$hostid,$taskfor, $i);
									
									$html .= '</tr>';
							}
						
						$i++;
						$j++;
						$skl++;
						}
					  
						
					}
                }//end of foreach loop
            }
            $html .= '</tbody>';
            //$html .= '</li>';
            //$html .= '</ul>';
            $html .= '</table>';
			
            $html .= '</div>';
            $html .= '</div>';
        } else {
            $html = $this->user_account->error_msg;
        }
        return $html . $script;
	}
	/* new recent task */
}

