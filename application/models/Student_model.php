<?php

class Student_model extends CI_Model
{

    public function insertInfo($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function insertId($table, $data)
    {
        $this->db->insert($table, $data);

        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function getAllInfo($table)
    {
        $this->db->select('*');
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_where($select, $table, $columnName, $columnValue)
    {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($columnName, $columnValue);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSelectItem($select, $table)
    {
        $this->db->select($select);
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateInfo($table, $colName, $colValue, $data)
    {
        $this->db->where($colName, $colValue);
        $this->db->update($table, $data);
    }

    public function deleteInfo($table, $colName, $colValue)
    {
        $this->db->where($colName, $colValue);
        $this->db->delete($table);
    }

    public function getInfo($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRow($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->row_array();
    }
    
    
    //    Module Section
    public function userInfo($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_useraccount');
        
        $this->db->join('tbl_country', 'tbl_useraccount.country_id = tbl_country.id', 'LEFT');
        $this->db->where('tbl_useraccount.id', $user_id);

        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_sct_enrollment_info($stId, $sctType)
    {
        $this->db->select('tbl_useraccount.*,tbl_enrollment.sct_id,tbl_enrollment.st_id');
        $this->db->from('tbl_useraccount');
        
        $this->db->join('tbl_enrollment', 'tbl_useraccount.id=tbl_enrollment.sct_id');
        $this->db->where('tbl_useraccount.user_type', $sctType);
        $this->db->where('tbl_enrollment.st_id', $stId);
        
        return $this->db->get()->result_array();
    }
    
    public function getLinkInfo($table, $colName1, $colName2, $colValue1, $colValue2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName1, $colValue1);
        $this->db->where($colName2, $colValue2);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function delete_enrollment($userType, $user_id)
    {
        $this->db->where('sct_type', $userType);
        $this->db->where('st_id', $user_id);
        $this->db->delete('tbl_enrollment');
        return;
    }
    
    public function getStudentRefLink($stId)
    {
        $this->db->select('tbl_useraccount.*,tbl_enrollment.sct_id,tbl_enrollment.st_id');
        $this->db->from('tbl_useraccount');
        $this->db->join('tbl_enrollment', 'tbl_useraccount.id=tbl_enrollment.sct_id');
        $this->db->where('tbl_enrollment.st_id', $stId);
        return $this->db->get()->result_array();
    }

    public function studentProgress($conditions)
    {
        $res = $this->db
            ->where($conditions)
            ->get('tbl_studentprogress')
            ->result_array();

        return $res;
    }

    public function studentByClass($class)
    {
        $res = $this->db
            ->where('student_grade', $class)
            ->get('tbl_studentgrade')
            ->result_array();

        return $res;
    }

    public function studentName($studentId)
    {
        $res = $this->db
            ->select('name')
            ->where('id', $studentId)
            ->get('tbl_useraccount')
            ->result_array();

        return isset($res[0]['name'])?$res[0]['name']:'';
    }

    /**
     * Get all students of a specific tutor/school/corporate/parent.
     *
     * @param array $conditions [column_name=>value,...]
     *                          for tutor,qstudy etc:['sct_id'=>loggedUserId]
     *
     * @return array             studentIds ex:[1,2,3,4,5]
     */
    public function allStudents($conditions)
    {
        $loggedUserId = $this->session->userdata('user_id');
        $loggedUserType = $this->session->userdata('userType');
        
        if ($loggedUserType == 1) { //parent
            $res = $this->db
                ->select('id as `st_id`')
                ->where('parent_id', $loggedUserId)
                ->get('tbl_useraccount')
                ->result_array();
        } else { //corporate/school/tutor
            $res = $this->db
                ->select('st_id')
                ->where($conditions)
                ->get('tbl_enrollment')
                ->result_array();
        }

        return array_column($res, 'st_id');
    }
    
    public function all_module_by_type($tutorType, $module_type, $desired_result)
    {
        $this->db->select('tbl_module.*,tbl_subject.subject_name,tbl_chapter.chapterName,tbl_useraccount.image');
        $this->db->from('tbl_module');
        
        $this->db->join('tbl_subject', 'tbl_module.subject = tbl_subject.subject_id', 'LEFT');
        $this->db->join('tbl_chapter', 'tbl_module.chapter = tbl_chapter.id', 'LEFT');
        $this->db->join('tbl_useraccount', 'tbl_useraccount.user_type = tbl_module.user_type', 'LEFT');
        
        $this->db->where('moduleType', $module_type);
        $this->db->where_in('tbl_module.subject', $desired_result);
      
        if ($tutorType == 7) {
            $this->db->where('tbl_useraccount.user_type', $tutorType);
        } else {
            $this->db->where('tbl_module.user_id', $tutorType);
        }
      
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function subjectInfo($tutorType)
    {
        $this->db->select('*');
        $this->db->from('tbl_subject');
        
        $this->db->join('tbl_useraccount', 'tbl_subject.created_by = tbl_useraccount.id', 'LEFT');
        
        $this->db->where('tbl_useraccount.user_type', $tutorType);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_all_tutor_link($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_enrollment');
        
        $this->db->join('tbl_useraccount', 'tbl_enrollment.sct_id = tbl_useraccount.id', 'LEFT');
        
        $this->db->where('tbl_enrollment.st_id', $user_id);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }

    
    //unreleased(7-31-18)
    public function studentClass($studentId)
    {
        $res = $this->db->select('student_grade')
            ->where('id', $studentId)
            ->get('tbl_useraccount')
            ->result_array();


        return isset($res[0]['student_grade']) ? $res[0]['student_grade']:0;
    }
    //unreleased(7-31-18)
    
    
    public function get_all_tutor_link_with_module($user_id, $module_type)
    {
        $this->db->select('tbl_enrollment.*,tbl_useraccount.name, tbl_module.id AS module_id,moduleName,trackerName,individualName,subject,chapter,country,tbl_subject.subject_name,tbl_chapter.chapterName');
        $this->db->from('tbl_enrollment');
        
        $this->db->join('tbl_useraccount', 'tbl_enrollment.sct_id = tbl_useraccount.id', 'LEFT');
        $this->db->join('tbl_module', 'tbl_useraccount.id = tbl_module.user_id', 'LEFT');
        $this->db->join('tbl_subject', 'tbl_module.subject = tbl_subject.subject_id', 'LEFT');
        $this->db->join('tbl_chapter', 'tbl_module.chapter = tbl_chapter.id', 'LEFT');
        
        $this->db->where('tbl_enrollment.st_id', $user_id);
        $this->db->where('tbl_module.moduleType', $module_type);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_all_subject($user_type)
    {
        $this->db->select('subject_id');
        $this->db->from('tbl_course');
        
        $this->db->join('tbl_subject', 'tbl_subject.subject_name = tbl_course.courseName', 'LEFT');
        $this->db->join('tbl_useraccount', 'tbl_useraccount.id = tbl_subject.created_by', 'LEFT');

        $this->db->where('tbl_useraccount.user_type', $user_type);
        $this->db->distinct();
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_all_subject_for_registered_student($id)
    {
        $this->db->select('subject_id');
        $this->db->from('tbl_registered_course');
        
        $this->db->join('tbl_course', 'tbl_course.id = tbl_registered_course.course_id', 'LEFT');
        $this->db->join('tbl_subject', 'tbl_subject.subject_name = tbl_course.courseName', 'LEFT');

        $this->db->where('tbl_registered_course.user_id', $id);
        $this->db->distinct();
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    
    /**
     * Match student enrolled courses with subject table
     *
     * @param  integer $studentId logged student id
     * @return array              subjects info
     */
    public function studentSubjects($studentId)
    {
        $temp = $this->studentCourses($studentId);
        $studentCourses = array_column($temp, 'courseName');
        $studentCourses = count($studentCourses)?$studentCourses:[0];
        $res = $this->db
            ->where_in('tbl_subject.subject_name', $studentCourses)
            ->get('tbl_subject')
            ->result_array();

        return $res;
    }

    /**
     * Student enrolled courses.
     *
     * @param  integer $studentId student id
     * @return array              all courses of a student
     */
    public function studentCourses($studentId)
    {
        $res = $this->db
            ->join('tbl_course', 'tbl_course.id = tbl_registered_course.course_id', 'left')
            ->where('user_id', $studentId)
            ->get('tbl_registered_course')
            ->result_array();

        return $res;
    }

    /**
     * Get chapters of a individual/multiple subject/s.
     *
     * @param  integer $subjectId subject id/s
     * @return array              chapters
     */
    public function chaptersOfSubject($subjectId)
    {
       
        if (count($subjectId)) {
            if (count($subjectId)>1) {
                $this->db->where_in('subjectId', $subjectId);
            } else {
                $this->db->where('subjectId', $subjectId[0]);
            }
        }
        
        $res = $this->db
            ->get('tbl_chapter')
            ->result_array();

        return $res;
    }
    
    /**
     * Get all tutors info of a student
     *
     * @param  integer $studentId student id
     * @return array            tutor ids
     */
    public function allTutor($studentId)
    {
        return $this->db
            ->join('tbl_useraccount', 'tbl_useraccount.id=tbl_enrollment.sct_id', 'left')
            ->where('st_id', $studentId)
            ->get('tbl_enrollment')
            ->result_array();
    }
}
