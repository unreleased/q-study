<style>
    .fa-close{
        color: red;
    }
    .fa-check{
        color: green;
    }
</style>


<?php //echo '<pre>';print_r($question_info_left_right);die; ?>

<div class="ss_student_board">
    <div class="ss_s_b_top">
        <div class="ss_index_menu">
            <a href="<?php echo base_url().$userType.'/view_course'; ?>">Question/Module</a>
        </div>
        <div class="col-sm-6 ss_next_pre_top_menu text-left">
            <a class="btn btn_next" href="question_edit/<?php echo $question_item?>/<?php echo $question_id?>">
                <i class="fa fa-caret-left" aria-hidden="true"></i> Back
            </a>

            <a class="btn btn_next" href="#">Index  </a>
            <a class="btn btn_next" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>
        </div>
<!--        <div class="col-sm-6 ss_index_menu"> 
            <img src="assets/images/icon_timer.png" class="pull-left"><div class="ss_timer"><h1>00:00:00 </h1></div>
            
        </div>-->
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ss_s_b_main" style="min-height: 100vh">
                <div class="col-sm-12">
                    <div class="panel-group col-sm-4" id="raccordion" role="tablist" aria-multiselectable="true" style="float: right;">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  <span>Module Name: Every Sector</span></a>
                                </h4>
                            </div>
                            <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="ss_module_result">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>    
                                                    <tr>
                                                        
                                                        <th>SL</th>
                                                        <th>Mark</th>
                                                       <!--  <th>Obtained</th> -->
                                                        <th>Description</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        
                                                        <td>1</td>
                                                        <td><?php echo $question_info_total[0]['questionMarks']; ?></td>
                                                        <!-- <td><?php // echo $question_info_total[0]['questionMarks']; ?></td> -->
                                                        <td>
                                                            <a onclick="showDescription()">
                                                                <img src="assets/images/icon_details.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <span><img src="assets/images/icon_draw.png"> Instruction</span> Question
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <?php echo $question_info_left_right->questionName;?>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <?php
                $lettry_array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'k', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T');
                ?>
                <div class="col-sm-4">
                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">  Maching</a>
                                </h4>
                            </div>
                            <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="image_box_list ss_m_qu">
                                        <?php $i = 1;
                                        foreach ($question_info_left_right->left_side as $row) { ?>
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <p class="ss_lette"><?php echo $lettry_array[$i - 1]; ?></p>
                                            </div>
                                            <div class="col-xs-8">
                                                <div class="box ">
                                                    <div class="ss_w_box text-center">
                                                        <?php echo $row[0]; ?>
                                                    </div>                                                   
                                                </div>
                                            </div>
                                            <div class="col-xs-1">
                                                <p class="ss_lette" id="color_left_side_<?php echo $i; ?>">
                                                    <input type="radio" id='left_side_<?php echo $i; ?>' name="left_side_<?php echo $i; ?>" value="<?php echo $i; ?>" data-id="1" class="left" onclick="getLeftVal(this);" style="min-height: 96px;">
                                                </p>
                                            </div>
                                        </div>
                                            <?php $i++;
                                        } ?> 
                                        
                                    </div> 
                                </div>
                                
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4" style="margin-top: 10px;">     
                                    <button type="button" class="btn btn_next" id="answer_matching">submit</button>
                                </div>                                  
                                <div class="col-sm-4"></div>
                                
                            </div>
                        </div>                                                        
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne"> &nbsp </a>
                                </h4>
                            </div>
                            <div id="collapseOne2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="image_box_list ss_m_qu">
                                        <form id="answer_form">
                                            <?php $i = 1;
                                            foreach ($question_info_left_right->right_side as $row) { ?>
                                            <div class="row">

                                                <div class="col-xs-1">
                                                    <p class="ss_lette" id="color_right_side_<?php echo $i; ?>">
                                                        <input type="radio" name="right_side_<?php echo $i; ?>"  value="<?php echo $i; ?>" class="right" onclick="getRightVal(this);" style="min-height: 96px;">
                                                    </p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="box ">
                                                        <div class="ss_w_box text-center">
                                                            <p><?php echo $row[0];
                                                            echo '<br><br>'; ?></p>
                                                        </div>                                                   
                                                    </div>
                                                </div>

                                                <div class="col-xs-3">
                                                    <p class="ss_lette" style="height: 100px;vertical-align: middle;display: table-cell;">
                                                        <input type="number" class="form-control" name="answer_<?php echo $i; ?>" id="answer_<?php echo $i; ?>" data="1" onclick="getAnswer();">
                                                    </p>
                                                </div>
                                                <div class="col-xs-1">
                                                    <span class="" id="message_<?php echo $i - 1; ?>"></span>
                                                </div>
                                                <input type="hidden" name="id" value="<?php echo $question_id; ?>">
                                                <input type="hidden" name="total_ans" value="<?php echo sizeof($question_info_left_right->right_side); ?>">
                                            </div>
                                                <?php $i++;
                                            } ?>
                                        </form> 

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


            </div>

        </div>
    </div>
</div>


<!--Solution Modal-->
<div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Solution</h4>
            </div>
            <div class="modal-body row">
                <span class="ss_extar_top20">
                    <?php echo $question_info_total[0]['question_solution']?>
                </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
            </div>
        </div>
    </div>
</div>

<!--Description Modal-->
<div class="modal fade ss_modal" id="ss_info_description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Question Description</h4>
            </div>
            <div class="modal-body row">
                <span class="ss_extar_top20"><?php echo $question_info_total[0]['questionDescription']?></span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Success Modal-->
<div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_sucess.png" class="pull-left"> <span class="ss_extar_top20">Your answer is correct</span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>



<script>
    
    $('.right').attr('disabled', true);
    var left_arr = new Array();
    var right_arr = new Array();
    var color_array = new Array('red', 'green', 'blue', '#00BFFF', '#FF6347', '#708090', '#2F4F4F', '#C71585', '#8B0000', '#808000', '#FF6347', '#FF4500', '#FFD700', '#FFA500', '#228B22', '#808000', '#00FFFF', '#66CDAA', '#7B68EE', '#FF69B4');
    function getLeftVal(e)
    {
        var left_ans_val = e.value;

        left_arr.push(left_ans_val);

        $('.right').attr('disabled', false);
        $('.left').attr('disabled', true);
        //var last = left_arr.slice(-1)[0];
        var color_left = color_array[left_ans_val - 1];
        //document.getElementById("color_left_side_1").style.backgroundColor = color_left;
        document.getElementById("color_left_side_" + left_ans_val).setAttribute('style', 'background-color:' + color_left + ' !important');
        //console.log(last);
    }

    function getRightVal(e)
    {
        var last = left_arr.slice(-1)[0];
        var right_ans_val = e.value;

        document.getElementById("answer_" + right_ans_val).value = last;

        $('.right').attr('disabled', true);
        $('.left').attr('disabled', false);
        var color_right = color_array[last - 1];
        document.getElementById("color_right_side_" + right_ans_val).setAttribute('style', 'background-color:' + color_right + ' !important');
        //console.log(right_arr);
    }

    function getAnswer()
    {
        //alert(this.attr('data'));
    }
    
</script>

<script>
    $('#answer_matching').click(function () {
        var form = $("#answer_form");
        $.ajax({
            type: 'POST',
            url: 'answer_multiple_matching',
            data: form.serialize(),
            dataType: 'json',
            success: function (results) {
                var obj = (results);
                console.log(obj.flag);
                if(obj.flag == 0) {
                    $('#ss_info_worng').modal('show');
                    for (var i = 0; i < obj.student_ans.length; i++) {
                        if (obj.student_ans[i] == obj.tutor_ans[i]) {
                            $("#message_" + i).removeClass("fa fa-close");
                            $("#message_" + i).addClass("fa fa-check");
                        } else {
                            $("#message_" + i).removeClass("fa fa-check");
                            $("#message_" + i).addClass("fa fa-close");
                        }
                    }
                } if(obj.flag == 1) {
                    for (var i = 0; i < obj.student_ans.length; i++) {
                        $("#message_" + i).removeClass("fa fa-close");
                        $("#message_" + i).removeClass("fa fa-check");
                    }
                    $('#ss_info_sucesss').modal('show');
                }
//                for (var i = 0; i < obj.student_ans.length; i++)
//                {
//                    if (obj.student_ans[i] == obj.tutor_ans[i]) {
//                        $("#message_" + i).removeClass("fa fa-close");
//                        $("#message_" + i).addClass("fa fa-check");
//                    } else {
//                        $("#message_" + i).addClass("fa fa-close");
//                        $("#message_" + i).addClass("fa fa-check");
//                    }
//                }
                
            }
        });

    });
    
    function showDescription(){
        $('#ss_info_description').modal('show');
    }

</script>
