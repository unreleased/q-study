<br>
<div class="ss_student_board">
  <div class="ss_s_b_top">
    <div class="ss_index_menu">
      <a href="<?php echo base_url().$userType.'/view_course'; ?>">Question/Module</a>
    </div>
    <div class="col-sm-6 ss_next_pre_top_menu">
      <a class="btn btn_next" href="question_edit/<?php echo $question_item; ?>/<?php echo $question_id; ?>">
        <i class="fa fa-caret-left" aria-hidden="true"></i> Back
      </a>
      <a class="btn btn_next" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>
      <a class="btn btn_next workout_2 rscustomworkout" onclick="fn_show_workout_draw();">Draw <img src="assets/images/icon_draw.png"></a>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="ss_s_b_main" style="min-height: 100vh">
        <div class="col-sm-4">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span><img src="assets/images/icon_draw.png"> Instruction</span> Question</a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <div class=" math_plus">
                    <?php echo strip_tags($question_info[0]['questionName']); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <form id="true_false">
            <input type="hidden" value="<?php echo $question_id; ?>" name="question_id" id="question_id">

            <div class="t_f_button">
              <div class="checkbox">
                <label>
                  <span class="btn btn_yellow btn-lg">TRUE</span> <input type="radio" name="answer" value="1"> 
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <span class="btn btn_yellow btn-lg">FALSE</span> <input type="radio" name="answer" value="0"> 
                </label>
              </div>
            </div>


          </form>
        </div>

        <div class="col-sm-4">
          <div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  
                    <span>Module Name: Every Sector</span></a>
                  </h4>
                </div>
                <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <div class=" ss_module_result">
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>    
                            <tr>
                              
                              <th>SL</th>
                              <th>Mark</th>
                              
                              <th>Description</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              
                              <td>1</td>
                              <td><?php echo $question_info[0]['questionMarks']; ?>
                              </td>
                              
                              <td>
                                <a onclick="showDescription()" class="text-center">
                                  <img src="assets/images/icon_details.png">
                                </a>
                              </td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


            </div>
          </div>                      
          <div class="col-sm-6" style=" margin: 50px; ">
            <button class="btn btn_next" id="answer_matching">Submit</button>
          </div>                  
        </div>

      </div>
    </div>
  </div>
</div>  
</section>

<!--Description Modal-->
<div class="modal fade ss_modal" id="ss_info_description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title" id="myModalLabel">Question Description</h4>
      </div>
      <div class="modal-body row">
        <span class="ss_extar_top20"><?php echo $question_info[0]['questionDescription']?></span> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

      </div>
    </div>
  </div>
</div>


<!--Success Modal-->
<div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
      </div>
      <div class="modal-body row">
        <img src="assets/images/icon_sucess.png" class="pull-left"> <span class="ss_extar_top20">Your answer is correct</span> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

      </div>
    </div>
  </div>
</div>

<!--Solution Modal-->
<div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title" id="myModalLabel">Solution</h4>
      </div>
      <div class="modal-body row">
        <i class="fa fa-close" style="font-size:20px;color:red"></i> 
        <!--<span class="ss_extar_top20">Your answer is wrong</span>-->
        <br><?php echo strip_tags($question_info[0]['questionName']); ?>  

        <?php
        if ($question_info[0]['answer'] == 1) {
            echo 'TRUE';
        } else {
            echo 'FALSE';
        }
        ?> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
      </div>
    </div>
  </div>
</div>

<script>
  $('#answer_matching').click(function () {
    var form = $("#true_false");
    $.ajax({
      type: 'POST',
      url: 'answer_matching_true_false',
      data: form.serialize(),
      dataType: 'html',
      success: function (results) {
        if (results == 1) {
          alert('select true or false');
        } else if (results == 2) {
          $('#ss_info_sucesss').modal('show');
        } else if (results == 3) {
          $('#ss_info_worng').modal('show');
        }
      }
    });

  });
  
  function showDescription(){
    $('#ss_info_description').modal('show');
  }
</script>
