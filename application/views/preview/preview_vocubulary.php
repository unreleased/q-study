<div class="ss_student_board">
    <div class="ss_s_b_top">
        <div class="ss_index_menu">
            <a href="<?php echo base_url().$userType.'/view_course'; ?>">Question/Module</a>
        </div>
        <div class="col-sm-6 ss_next_pre_top_menu">
            <a class="btn btn_next" href="question_edit/<?php echo $question_item; ?>/<?php echo $question_id; ?>">
                <i class="fa fa-caret-left" aria-hidden="true"></i> Back
            </a>
            <a class="btn btn_next" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>
            <a class="btn btn_next" href="#">Draw <img src="<?php echo base_url();?>assets/images/icon_draw.png"></a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ss_s_b_main" style="min-height: 100vh">
                <div class="col-sm-4">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <span><img src="assets/images/icon_draw.png"> Instruction</span> Question</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="image_q_list" style="font-size: 13px;">

                                        <div class="row">
                                            <div class="col-xs-5">Word</div>
                                            <div class="col-xs-7">: ?</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">Definition</div>
                                            <div class="col-xs-7">: <?php echo $question_info->definition;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">Parts of speech</div>
                                            <div class="col-xs-7">: <?php echo $question_info->parts_of_speech;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">Synonym </div>
                                            <div class="col-xs-7">: <?php echo $question_info->synonym;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">Antonym</div>
                                            <div class="col-xs-7">: <?php echo $question_info->antonym;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">Your Sentence</div>
                                            <div class="col-xs-7">: <?php echo $question_info->sentence;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">Near Antonym</div>
                                            <div class="col-xs-7">: <?php echo $question_info->near_antonym;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">Audio File</div>
                                            <div class="col-xs-7">
                                                <img src="assets/images/aa.png" onclick="showAudio()"> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">Video file</div>
                                            <div class="col-xs-7"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <audio controls style="display: none;">
                            <source src="<?php if(isset($question_info->audioFile)){echo $question_info->audioFile;} ?>" type="audio/ogg">
                                <source src="<?php if(isset($question_info->audioFile)){echo $question_info->audioFile;} ?>" type="audio/mpeg">
                                    <source src="<?php if(isset($question_info->audioFile)){echo $question_info->audioFile;} ?>" type="audio/webm">
                                        <source src="<?php if(isset($question_info->audioFile)){echo $question_info->audioFile;} ?>" type="audio/wav">
                                            <source src="<?php if(isset($question_info->audioFile)){echo $question_info->audioFile;} ?>" type="audio/flac">
                                            </audio>

<!--                        <video width="320" height="240" controls>
                            <source src="<?php echo $question_info->videoFile; ?>" type="video/mp4">
                            <source src="movie.ogg" type="video/ogg">
                            Your browser does not support the video tag.
                        </video> -->
                    </div>

                </div>
                <div class="col-sm-4">
                    <div class="panel-group" id="saccordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#saccordion" href="#collapseTow" aria-expanded="true" aria-controls="collapseOne">   Answer</a>
                                </h4>
                            </div>
                            <div id="collapseTow" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="image_box_list_result result">
                                        <form id="answer_form">
                                            <div class="image_box_list" style="overflow: visible;">
                                                <div class="row">
                                                    <!--<div class="col-sm-2">
                                                        <p class="ss_lette"> A </p>
                                                    </div>-->

                                                    <div class="">
                                                        <div class="">
                                                            <?php foreach ($question_info->vocubulary_image as $row){?>
                                                            <div class="result_board">
                                                                <?php echo $row[0]?>
                                                            </div>
                                                            <br/>
                                                            <?php }?>
                                                            <div class="form-group" style="padding: 0px 12px;">
                                                                <input type="text" readonly class="form-control" id="exampleInputl1" name="answer">
                                                            </div>
                                                        </div>
                                                        <div class="letter_box" style="padding: 10px 12px;">
                                                            <ul>
                                                                <?php foreach (range('A', 'Z') as $char) {?>
                                                                <li> <a onclick="getLetter('<?php  echo $char;?>')" data-id="<?php  echo $char;?>"><?php  echo $char;?></a> </li>
                                                                <?php } ?>
                                                                <li> 
                                                                   <a onclick="delLetter();"><img src="assets/images/icon_l_d.png"></a> 
                                                               </li>
                                                           </ul>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                           <input type="hidden" value="<?php echo $question_id;?>" name="question_id" id="question_id">
                                           <div class="text-center">
                                            <a  class="btn btn_next"  id="answer_matching">Submit</a></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  <span>Module Name: Every Sector</span></a>
                                </h4>
                            </div>
                            <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class=" ss_module_result">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>    
                                                    <tr>
                                                        
                                                        <th>SL</th>
                                                        <th>Mark</th>
                                                        
                                                        <th>Description</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                       
                                                        <td>1</td>
                                                        <td><?php echo $question_info_s[0]['questionMarks']; ?></td>
                                                        
                                                        <td>
                                                            <a onclick="showDescription()">
                                                                <img src="assets/images/icon_details.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Description Modal-->
<div class="modal fade ss_modal" id="ss_info_description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <span class="ss_extar_top20"><?php echo $question_info_s[0]['questionDescription']?></span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>


<!--Success Modal-->
<div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_sucess.png" class="pull-left"> <span class="ss_extar_top20">Your answer is correct</span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<!--Solution Modal-->
<div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <i class="fa fa-close" style="font-size:20px;color:red"></i> 
                <span class="ss_extar_top20">
                    <?php echo $question_info_s[0]['question_solution']?>
                </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
            </div>
        </div>
    </div>
</div>


<script>
    $('#answer_matching').click(function () {
        var form = $("#answer_form");
        $.ajax({
            type: 'POST',
            url: 'answer_matching_vocabolary',
            data: form.serialize(),
            dataType: 'html',
            success: function (results) {
                if(results==1){
                    alert('write your answer');
                }else if(results==2){
                    $('#ss_info_sucesss').modal('show');
                }else if(results==3){
                    $('#ss_info_worng').modal('show');		
                }
            }
        });

    });

    function getLetter(letter)
    {
        var val = document.getElementById('exampleInputl1').value;
        var total = val + letter;
        $('#exampleInputl1').val(total);
    }
    function delLetter(){
        var val = document.getElementById('exampleInputl1').value;
        var sillyString = val.slice(0, -1);
        $('#exampleInputl1').val(sillyString);
    }

    function showAudio(){
        $("audio").show();
    }
    
    function showDescription(){
        $('#ss_info_description').modal('show');
    }
</script>