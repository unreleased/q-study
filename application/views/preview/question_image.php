<br>
<div class="ss_student_board">
  <div class="ss_s_b_top">
    <div class="ss_index_menu">
      <a href="<?php echo base_url().$userType.'/view_course'; ?>">Question/Module</a>
    </div>
    <div class="col-sm-6 ss_next_pre_top_menu">
      <a class="btn btn_next" href="question_edit/<?php echo $question_item; ?>/<?php echo $question_id; ?>">
        <i class="fa fa-caret-left" aria-hidden="true"></i> Back
      </a>
      <a class="btn btn_next" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>
      <a class="btn btn_next" href="#">Draw <img src="assets/images/icon_draw.png"></a>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="ss_s_b_main" style="min-height: 100vh">
        <div class="col-sm-4">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span><img src="assets/images/icon_draw.png"> Instruction</span> Question</a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                
                <div class="panel-body">
                  <div class=" math_plus">
                    <?php echo ($question_info[0]['questionName']); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" value="<?php echo $question_id; ?>" name="question_id" id="question_id">
        <div class="col-sm-4">
          <div class="panel-group" id="saccordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#saccordion" href="#collapseTow" aria-expanded="true" aria-controls="collapseOne">   Answer</a>
                </h4>
              </div>
              <div id="collapseTow" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <textarea name="answer" class="my_preview_textarea"></textarea>
              </div>
            </div>
          </div>
          <div class="col-sm-4"></div>
          <div class="col-sm-4">
            <button class="btn btn_next" id="answer_matching">Submit</button>
          </div>
          <div class="col-sm-4"></div>

        </div>

        <div class="col-sm-4">
          <div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  
                    <span>Module Name: Every Sector</span></a>
                  </h4>
                </div>
                <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <div class=" ss_module_result">
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>    
                            <tr>
                             
                              <th>SL</th>
                              <th>Mark</th>
                              <!-- <th>Obtained</th> -->
                              <th>Description</th>

                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td><?php echo $question_info[0]['questionMarks']; ?></td>
                              <!-- <td><?php // echo $question_info[0]['questionMarks']; ?></td> -->
                              <td><a onclick="showDescription()" class="text-center"><img src="assets/images/icon_details.png"></a></td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


            </div>
          </div>


        </div>

      </div>
    </div>
  </div>

  <!--Description Modal-->
  <div class="modal fade ss_modal" id="ss_info_description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title" id="myModalLabel">Question Description</h4>
        </div>
        <div class="modal-body row">
          <span class="ss_extar_top20"><?php echo $question_info[0]['questionDescription']?></span> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

        </div>
      </div>
    </div>
  </div>


  <!--Success Modal-->
  <div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
        </div>
        <div class="modal-body row">
          <img src="assets/images/icon_sucess.png" class="pull-left"> <span class="ss_extar_top20">Your answer is correct</span> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

        </div>
      </div>
    </div>
  </div>

  <!--Solution Modal-->
  <div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Solution</h4>
        </div>
        <div class="modal-body row">
          <i class="fa fa-close" style="font-size:20px;color:red"></i> 
          <span class="ss_extar_top20">
            <?php echo $question_info[0]['question_solution']?>
          </span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
        </div>
      </div>
    </div>
  </div>


  <script>
    $('#answer_matching').click(function () {
      var user_answer = CKEDITOR.instances.answer.getData();
      var id = $('#question_id').val();
      $.ajax({
        type: 'POST',
        url: 'answer_matching',
        data: {
          user_answer: user_answer,
          id: id
        },
        dataType: 'html',
        success: function (results) {
          if (results == 0) {
            $('#ss_info_worng').modal('show');
          } else if (results == 1) {
            $('#ss_info_sucesss').modal('show');
          }
        }
      });



    });
    
    function showDescription(){
      $('#ss_info_description').modal('show');
    }
  </script>
