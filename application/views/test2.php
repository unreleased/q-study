<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  

  <!-- dependency: React.js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react-with-addons.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react-dom.js"></script>
  <!-- leterally canvas -->
  <script src="<?php echo base_url(); ?>assets/js/html2canvas/html2canvas.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/literallycanvas/js/literallycanvas.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/literallycanvas/css/literallycanvas.css">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>


  <!-- Large modal -->

  <!-- Modal -->
<!-- <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="my-drawing">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>
      </div>  -->  
      <div id="dialog" title="Basic dialog" class="my-drawing">
      </div>      
      <div class="row">
        <div class="col-md-10">

        </div>
      </div>
      
      <!-- <div class="my-drawing"> -->


      <!-- where the widget goes. you can do CSS to it. -->
    <!-- note: as of 0.4.13, you cannot use 'literally' as the class name.
    sorry about that. -->
    <div id="imgToCapture" style="padding: 10px; background: #f5da55">
      <h4 style="color: #000; ">Hello world!</h4>
    </div>
    <button id="capture">Capture</button>
    <!-- <button id="showDraw1" data-toggle="modal" data-target="#exampleModalLong">Show Draw</button> -->
    <button id="showDraw">Show Draw</button>
    

    <!-- kick it off -->
    <script>
      var lc;

      $('#capture').on('click', function(){

        html2canvas(document.querySelector("#imgToCapture")).then(canvas => {
          console.log(canvas.toDataURL());
          var img = new Image();
          img.src = canvas.toDataURL();
          lc.saveShape(LC.createShape('Image', {x: 100, y: 100, image: img}));
          //$('.literally').literallycanvas({watermarkImage: img});
        });
      })


      //$( "#dialog" ).dialog();
      $('#showDraw').on('click', function () {
        $( "#dialog" ).dialog({
          title: "Drawing Board",
          height: 600,
          width: 900,
          buttons: { "OK": function() { $(this).dialog("close"); },"Save": function() { $(this).dialog("close"); } } 
        });

        lc = LC.init(
          document.getElementsByClassName('my-drawing')[0],
          {
            imageURLPrefix: "<?php echo base_url(); ?>assets/js/literallycanvas/img",
          });

        var x= (lc.getImage().toDataURL('image/png'));
        console.log(x);

      })
      
    </script>
  </body>



  </html>