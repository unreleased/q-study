<table class="table table-bordered">
    <tbody>
        <tr>
            <td>Country Name</td>
            <td>User Type</td>
            <td>User Name</td>
            <td>Action</td>
        </tr>
        <?php foreach ($total_registered as $row) {?>
        <tr id="<?php echo $row['id']; ?>">
            <td><a><?php echo $row['countryName'];?></a></td>
            <td><?php echo $row['userType'];?></td>
            <td id="userName"><?php echo $row['name'];?></td>
            <input type="hidden" id="usersTrialEnd" value="<?php echo strlen($row['trial_end_date'])? date('d-M-Y', strtotime($row['trial_end_date'])) : ''; ?>">
            <td>
                <?php if (!$row['suspension_status']) : ?>
                    <a  style="display: inline;" href="<?php echo base_url('Admin/suspendUser/').$row['id']; ?>"><i style="padding:0px 2px 0px 2px" data-toggle="tooltip" title="suspend" class="fa fa-pause-circle-o"></i></a>
                <?php else : ?>
                    <a  style="color:red; display: inline;" href="<?php echo base_url('Admin/unsuspendUser/').$row['id']; ?>"><i style="padding:0px 2px 0px 2px" data-toggle="tooltip" title="unsuspend" class="fa fa-play-circle-o"></i></a>
                <?php endif; ?>
                <!-- <a href style="padding:0px 2px 0px 2px; display: inline;"><i style="padding-right:2px" data-toggle="tooltip" title="delete" class="fa fa-trash"></i></a> -->
                <span class="updTrialPeriod1" data-toggle="modal" data-target="#updTrialPeriod" id="updTrialPeriod1"><i style="padding-right:2px" data-toggle="tooltip" title="Extend Trial Period" class="fa fa-wrench" ></i></span>
                <span class="updPackage" data-toggle="modal" data-target="#updPackageModal" id="updPackage"><i style="padding-right:2px" data-toggle="tooltip" title="Add Packages" class="fa fa-archive" ></i></span>
            </td>

        </tr>
        <?php }?>
    </tbody>
</table>

<!-- Update user trial period -->
<div class="modal fade" id="updTrialPeriod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Extend Trial period</h4>
      </div>
      <div class="modal-body">
            <p id="userTrialInfo"></p><br><br>
          <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <label for="recipient-name" class="control-label">Extend Trial Duration(Days)</label>
                </div>
                <div class="col-md-8">
                    <input type="hidden" id="hiddenUserId" value="">
                    <input type="number" min="1" max="30" class="form-control" id="extendAmound" placeholder="Days">
                </div>
            </div>
          </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="updTrialBtn" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>

<!-- Update user package -->
<div class="modal fade" id="updPackageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Add package</h4>
      </div>
      <div class="modal-body">
            <p id="usersPackageInfo"></p><br><br>
          <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <label for="recipient-name" class="control-label">Add packages</label>
                </div>
                <div class="col-md-8">
                    <input type="hidden" id="hiddenUserId" value="">
                    <form id="pkgSel">
                    <select class="form-control select2" multiple="multiple" id="notTakenCourses" name="notTakenCourses">
                       
                    </select>
                    </form>
                </div>
            </div>
          </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="updPkgBtn" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    //select2 effect
    $('.select2').select2();

    $('.updTrialPeriod1').on('click', function(){
        var userId = $(this).closest('tr').attr('id');
        var userName = $(this).closest('tr').find('#userName').html();
        var usersTrialEnd = $(this).closest('tr').find('#usersTrialEnd').val();
        var info = '<strong>' + userName+"'s" + ' trial period : </strong>'+usersTrialEnd ;
        $('#userTrialInfo').html(info);
        $('#hiddenUserId').val(userId);
    });

    //update trial modal button action
    $("#updTrialBtn").on('click', function(){
        var userId = $('#hiddenUserId').val();
        var extendAmount = $('#extendAmound').val();
        $.ajax({
            url:'Admin/extendTrialPeriod',
            method: 'POST',
            data : {'userId': userId, 'extendAmound': extendAmount},
            success: function(data){
                alert('User trial extended successfully');
                $('#updTrialPeriod').modal('toggle');
            },
        });
    });

    //add package icon action
    $('.updPackage').on('click', function(){
        var userId = $(this).closest('tr').attr('id');
        var userName = $(this).closest('tr').find('#userName').html();
        $('#hiddenUserId').val(userId);

        $.ajax({
            url:'Admin/usersCurrentPackages',
            method: 'POST',
            data: {'userId': userId},
            success: function(data){
                var userInfo = userName+"'s current packages : "+data;
                $('#usersPackageInfo').html(userInfo);
            }
        })
        $.ajax({
            url:'Admin/packageNotTaken',
            method: 'POST',
            data:{'userId': userId},
            success: function(data){
                $('#notTakenCourses').html(data);    
            }
        })
    });

    //package save
    $('#updPkgBtn').on('click', function(){
        var pkgSelected = $('#pkgSel').serializeArray();
        var userId =  $('#hiddenUserId').val();
        $.ajax({
            url:'Admin/addPackages',
            method: 'POST',
            data:{'userId': userId, 'pkgSelected':pkgSelected},
            success: function(data){
                alert('Package Updated Successfully');
                $('#updPackageModal').modal('toggle');
            }
        })
    });
 
</script>