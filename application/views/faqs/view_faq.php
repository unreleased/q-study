<style>
  label {
    font-size: 13px;
  }

  .user_list {
    border-color: #2F91BA;
  }
</style>


<div class="row" style="margin-top:50px;">
  <div class="col-md-4">
      <?php echo $leftnav ?>
  </div>
  
    <?php if ($this->session->flashdata('success_msg')) :?>
  <div class="col-md-8" id="flashmsg">
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success_msg'); ?>
    </div>
  </div>
    <?php endif; ?>
  <div class="col-md-8 user_list">
    <div class="panel-group " id="task_accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title text-center">
            <a role="button" data-toggle="collapse" data-parent="#task_accordion" href="#collapseOnetask" aria-expanded="true" aria-controls="collapseOne"> 
              <strong><span style="font-size : 18px; ">  <?php echo $faq['title']; ?> </span></strong>
            </a>
          </h4>
        </div>

        <form class="form-horizontal" id="editFaqForm" action= "" method="POST">
          <!-- <div class="row panel-body">
            <div class="col-sm-12 text-right">
              <button type="button" class="btn btn_next" id="" onclick = "location.reload(true)"><i class="fa fa-times" style="padding-right: 5px;"></i>Cancel</button>
              <button type="submit" class="btn btn_next" id=""><i class="fa fa-plus" style="padding-right: 5px;"></i>Save</button>
            </div>
          </div> -->

          <!-- faq submit form -->  
          <div class="row panel-body">
            <div class="col-sm-12"><?php echo $faq['body']; ?></div>
          </div>
        </form>


      </div>
    </div>

  </div>
</div>
