<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Testing</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- datatable -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

</head>
<body>
  <div class="row">
    <div class="col-12 text-center">
      <h3>Datatable test</h3>
    </div>
  </div>
  <div class="row">
  <div class="col-md-1"></div>
    <div class="col-md-10">

      <table class="table table-striped">
        <thead>
          <tr>
              <th scope="col">Date</th>
              <th scope="col">Type Of Creator</th>
              <th scope="col">Country Name</th>
              <th scope="col">Creator Name</th>
              <th scope="col">Word</th>
              <th scope="col">Total words</th>
              <th scope="col" class="text-center">View</th>
              <th scope="col" class="text-center">Select</th>
              <th scope="col" class="text-center">Delete</th> 
            </tr>
        </thead>
        <tbody>
          <!-- <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>@mdo</td>
            <td>@mdo</td>
            <td>@mdo</td>
            <td>@mdo</td>
            
            <td scope="col" class="text-center"><a href=""><span class="glyphicon glyphicon-remove"></span></a></td>
          </tr> -->
         
        </tbody>
      </table>
    </div>
  </div>


  <script>
    $('.table').dataTable({
      "serverSide": true,
      "paging": true,
      "ajax": "<?php echo base_url(); ?>CommonAccess/test2",
      "dataSrc": "tableData",
      "columns":[
        {'data':'ques_created_at'},
        {'data':'creator_type'},
        {'data':'creator_country'},
        {'data':'word_creator'},
        {'data':'word'},
        {'data':'sl'},
        {'data':'view'},
        {'data':'select'},
        {'data':'delete'},
      ]
    });
  </script>
</body>
</html>
<!-- "columns":[
        {'data':'data.ques_created_at'},
        {'data':'data.word_creator'},
        {'data':'data.creator_country'},
        {'data':'data.word_creator'},
        {'data':'data.word'},
        {'data':'data.creator_id'},
        {'data':'data.creator_id'},
        {'data':'data.creator_id'},
        {'data':'data.creator_id'},
      ] -->