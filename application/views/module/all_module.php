<div class="container top100">
  <div class="row">
    <div class="col-md-10 text-center">
      <?php if ($this->session->userdata('success_msg')) : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
          <strong><?php echo $this->session->userdata('success_msg'); ?></strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
        <?php elseif ($this->session->userdata('error_msg')) : ?>
    <div class="alert alert-danger alert-dismissible show" role="alert">
      <strong><?php echo $this->session->userdata('error_msg'); ?></strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
      <?php endif; ?>
</div>
</div>

<div class="row">

    <div class="ss_student_progress">

      <div class="search_filter">
        <form class="form-inline">
          <div class="form-group">
            <label for="exampleInputName2">Country</label>
            <div class="select">
              <select class="form-control" name="" id="">
                <?php echo $all_country; ?>
            </select>
            <!-- <input class="form-control" value="<?php //echo isset($user_info[0]['countryName']) ? $user_info[0]['countryName'] : '';?>"> -->
        </div>
    </div>
    <div class="form-group">

        <label for="exampleInputName2">Grade/Year/Lavel</label>
        <div class="select">
          <select class="form-control select-hidden">
            <option value="">Select Grade/Year/Level</option>
            <?php $grades = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; ?>
            <?php foreach ($grades as $stGrade) { ?>
            <option value="<?php echo $stGrade; ?>">
                <?php echo $stGrade; ?>
            </option>
            <?php } ?>
            <option value="13">Upper Level</option>
        </select>

    </div>
</div>
<div class="form-group">
    <label for="exampleInputEmail2">Module Type</label>
    <div class="select">
      <select class="form-control select-hidden">
        <option>Select....</option>
        <?php foreach ($all_module_type as $module_type) {?>
        <option value="<?php echo $module_type['id']?>">
            <?php echo $module_type['module_type'];?>
        </option>
        <?php }?>
    </select>

</div>
</div>
<div class="form-group">
    <label for="exampleInputEmail2">Subject</label>
    <div class="select">
      <select class="form-control select-hidden">
        <option>Select....</option>
        <?php foreach ($all_course as $course) {?>
        <option value="<?php echo $course['id']?>">
            <?php echo $course['courseName'];?>
        </option>
        <?php }?>
    </select>

</div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn_green"><i class="fa fa-search"></i>  Search</button>
</div>
<div class="form-group">
    <a href="add-module">
      <button type="button" class="btn btn_orange">
        <i class="fa fa-file"></i>  Add New
    </button>
</a>
</div>
<!-- <button type="submit" class="btn btn_green">Re-Order</button> -->
<a href="reorder-module" class="btn btn_green">Re-Order</a>
</form>
</div>
</div>

<div class="sign_up_menu">
  <div class="table-responsive">
    <table class="table table-bordered" id="module_setting">
      <thead>
        <tr>
          <th>Date</th>
          <th>Module Name</th>
          <th>Module Type</th>
          <th>Duplicate</th>
          <th>Edit</th>
          <th>Delete</th>
      </tr>
  </thead>
  <tbody>
    <?php foreach ($all_module as $module) : ?>
      <tr id="<?php echo $module['id']; ?>">
        <td><?php echo date('d-M-Y', $module['exam_date']) ?></td>
        <td id="modName">
          <a href="edit-module/<?php echo $module['id']; ?>"><?php echo $module['moduleName']; ?></a> 
      </td>
      <td>Everyday Study</td>
      <td><i class="fa fa-clipboard" id="modDuplicateIcon" data-toggle="modal" data-target="#moduleDuplicateModal" style="color:#4c8e0c;"></i></td>
      <td><a href="edit-module/<?php echo $module['id']; ?>"><i class="fa fa-pencil" style="color:#4c8e0c;"></i></a></td>
      <td><i data-toggle="modal" data-target="#moduleDelModal" class="fa fa-trash" id="dltModOpnIcon" style="color:red;"></i></td>
  </tr>
    <?php endforeach; ?>
</tbody>
</table>
</div>
</div>
</div>
</div>

<!-- delete module -->
<div class="modal fade" tabindex="-1" role="dialog" id="moduleDelModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">Really want to delete module?</h5>
    </div>
    <div class="modal-body text-center">
        <input type="hidden" value="" id="moduleToDel">
        <button type="button" class="btn btn-danger" id="moduleDltBtn">YES</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
    </div>
    <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- duplicate module -->
<div class="modal fade" tabindex="-1" role="dialog" id="moduleDuplicateModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">Really want to duplicate module?</h5>
    </div>
    <div class="modal-body text-center">
        <input type="hidden" value="" id="moduleToDel">
        <div class="row">

          <div class="col-md-10">
            <form class="form-horizontal" id="moduleDuplicateForm">
              <input type="hidden" name="origModId" id="origModId" val="">
              <div class="form-group row">
                <label for="exampleInputEmail1" class="col-sm-4">Module Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="moduleName" id="duplicateModName" required>
              </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-4" >Grade/Year/Level</label>
            <div class="col-sm-8">
              <select  id="" class="form-control" name="studentGrade" required>
                <option value="">SELECT A GRADE</option>
                <?php for ($a=1; $a<=13; $a++) : ?>
                    <?php if ($a>12) : ?>
                        <option value="<?php echo $a ?>">Upper level</option>
                    <?php else : ?>
                        <option value="<?php echo $a ?>"><?php echo $a; ?></option>
                    <?php endif; ?>
                <?php endfor; ?>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label  class="col-sm-4" >Date</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="enterDate" name="examDate" autocomplete="off" required>
      </div>
  </div>

  <div class="form-group row">
    <label for="" class="col-sm-4" >Module Type</label>
    <div class="col-sm-8">
      <select id="" class="form-control" name="moduleType" required>
        <option value="">SELECT MODULE TYPE</option>
        <?php echo $allRenderedModType; ?>
    </select>
</div>
</div>

<div class="form-group row">
    <label for="" class="col-sm-4" ></label>
    <div class="col-sm-4">
      <label class="checkbox-inline">
        <input type="checkbox" id="inlineCheckbox1" value="1" name="respToSMS"> Respond to SMS
    </label>
</div>
<div class="col-sm-4">                            
  <label class="checkbox-inline">
    <input type="checkbox" id="inlineCheckbox2" value="1" name="forAllStd"> For all student
</label>
</div>
</div>
<div class="form-group row">
    <label for="" class="col-sm-4">For individual student</label>
    <div class="col-sm-8">
      <select  id="" class="form-control" name="indivStIds">
        <?php echo $allStudents; ?>
    </select>
</div>
</div>
<div class="form-group row">
    <label for="" class="col-sm-4">Country</label>
    <div class="col-sm-8">
      <select  id="" class="form-control" name="country" required>
        <?php echo $all_country; ?>
    </select>
</div>
</div>
<div class="form-group row">
    <label for="" class="col-sm-4"></label>
    <div class="col-sm-2">
      <button type="submit" class="btn btn-primary btn-sm">YES</button>
  </div>
  <div class="col-sm-2">
      <button type="button" class="btn btn-primary btn-sm">NO</button>
  </div>
</div>
</div>



</form>
</div>

</div>

</div>
<div class="modal-footer">
</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    //set module id to modal for deletion
    $(document).on('click', '#dltModOpnIcon' ,function(){
      var moduleTodel = $(this).closest('tr').attr('id');
      $('#moduleToDel').val(moduleTodel);
      
  })

    //module delete functionality
    $('#moduleDltBtn').on('click', function(){
      var moduleId = $(this).siblings('#moduleToDel').val();
      $.ajax({
        url:'Module/deleteModule',
        method:'post',
        data:{moduleId:moduleId},
        success: function(data){
          if(data=='true'){
            alert('Module deleted successfully');
        }else{
            alert('Something is wrong');
        }
        $('#moduleDelModal').modal('toggle');
        location.reload();
    }
})
  });

    //set original module id on module duplicate modal
    $(document).on('click', '#modDuplicateIcon', function(){
      var origModId = $(this).closest('tr').attr('id');
      var origModName = $(this).closest('tr').find('#modName a').html();
      console.log(origModName);
      $('#origModId').val(origModId);
      $('#duplicateModName').val(origModName);
  })

    //module duplicate form submit
    $(document).on('submit', '#moduleDuplicateForm', function(event){
      event.preventDefault();
      var data = $(this).serialize();

      $.ajax({
        url : 'Module/moduleDuplicate',
        method : 'POST',
        data : data,
        success : function(data){
          if(data=='false'){
            alert('To keep same module name please change country or student grade.');
        } else if(data=='true') {
            alert('Module duplication complete.');
        } else {
            alert(data);
        }

        $('#moduleDuplicateModal').modal('toggle');
        location.reload();
    }
})
      console.log(data);
  });

    //date picker on duplicate module modal
    $('#enterDate').datepicker({});
</script>
