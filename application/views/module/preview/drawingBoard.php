
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<!-- Bootstrap -->
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->

<!-- jquery UI -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></scri pt>-->
  <!-- dependency: React.js -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react-with-addons.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react-dom.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/html2canvas/html2canvas.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/literallycanvas/js/literallycanvas.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/literallycanvas/css/literallycanvas.css">
  <!-- html2canvas -->
  <!-- <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>-->
  <script src="assets/js/html2canvas/html2canvas.js"></script>

  <style>
    .no-close .ui-dialog-titlebar-close {
      display: none;
    }
  </style>


  <!-- Large modal -->
<!-- <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" >

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Workout Draw</h4>
      </div>

      <div class="row">

      <div class="modal-body">

       <div id="wPaint" class="my-drawing" style="position:relative;width:800px; height:600px; background:#fff; border:solid black 1px;"></div>
       <div class="row">
        <div class="col-md-12">
          <div id="wPaint" class="my-drawing">
          </div>
        </div>
        
      </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="getQues" class="btn btn-primary">Get Question</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="setAnswer()">Set Answer</button>
      </div>

    </div>
  </div>
</div>
</div> -->

<div id="dialog" title="Basic dialog" class="my-drawing"></div>  

<script type="text/javascript">
  var lc;
  function showDrawBoard(){
    lc = LC.init(
      document.getElementsByClassName('my-drawing')[0],
      {
        imageURLPrefix: "<?php echo base_url(); ?>assets/js/literallycanvas/img",
        //imageSize: {width: 100, height: 80},
      });

    $( "#dialog" ).dialog({
      title: "Drawing Board",
      dialogClass: "no-close",
      height: 600,
      width: 900,
      buttons: [
      {
        text:"Close",
        icon: "ui-icon-heart",
        click: function() {
          lc.teardown();
          $( this ).dialog( "close" );
        }
      },
      {
        text:"Get Question",
        //id:"getQues",
        click: function() {
          getQues();
        }
      },
      {
        text:"Set Answer",
        click: function() {
          setAnswer();
        }
      },
      ]
    });

    
  }


  function setAnswer(){
    var imageData = (lc.getImage().toDataURL('image/png'));
    console.log(imageData);
    $.ajax({
      type: 'POST',
      url: 'module/get_draw_image',
      data: {
        imageData: imageData,
      },
      dataType: 'html',
      success: function (results) {
        //console.log(results);
        $("#draggable").show();
        //CKEDITOR.instances.workout.insertHtml('<img src="'+results+'">');
        $("#setWorkoutHere").html('<img src="'+results+'">');
      }
    });
  }
  //get question(modal button) activity
  /*$(document).on('click', '#getQues', function(){
    html2canvas(document.querySelector("#quesBody")).then(canvas => {
      var img = new Image();
      img.src = canvas.toDataURL();
      lc.saveShape(LC.createShape('Image', {x: 100, y: 100, image: img}));
      //console.log(canvas.toDataURL('image/png'));
    });
  })*/

  function getQues() {
    console.log('one');
    html2canvas(document.querySelector(".cke_wysiwyg_frame")).then(canvas => {
      var img = new Image();
      img.src = canvas.toDataURL('image/png');
      lc.saveShape(LC.createShape('Image', {x: 100, y: 100, image: img}));
      console.log('two');
      console.log(canvas.toDataURL());
    });
    console.log('three');
  }

</script>

