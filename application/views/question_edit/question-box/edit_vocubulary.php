<?php //echo '<pre>';print_r($question_info_ind->vocubulary_image);die;?>
<style>
	.form-group{
		display: block !important;
		margin-bottom: 10px !important;
	}
</style>

<div class="col-sm-4">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" aria-expanded="true" aria-controls="collapseOne">
                        <span onclick="setSolution()">
                            <img src="assets/images/icon_solution.png"> Solution
                        </span> Question
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body form-horizontal ss_image_add_form">
                    <!--  <form class="form-horizontal ss_image_add_form"> -->
                    <div class="form-group">
                        <label for="inputwordl3" class="col-sm-4 control-label">Word</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputword3" name="answer" value="<?php echo $question_info[0]['answer']; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDefinitionl3" class="col-sm-4 control-label">Definition</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputDefinitionl3" name="definition" value="<?php echo $question_info_ind->definition; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPartsofspeech3" class="col-sm-4 control-label">Parts of speech</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPartsofspeech3" name="parts_of_speech" value="<?php echo $question_info_ind->parts_of_speech; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSynonym3" class="col-sm-4 control-label">Synonym</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputSynonym3" name="synonym" value="<?php echo $question_info_ind->synonym; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAntonym3" class="col-sm-4 control-label">Antonym</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputAntonym3" name="antonym" value="<?php echo $question_info_ind->antonym; ?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputYourSentence3" class="col-sm-4 control-label">Your Sentence</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputYourSentence3" name="sentence" value="<?php echo $question_info_ind->sentence; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputNearAntonym3" class="col-sm-4 control-label">Near Antonym</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputNearAntonym3" name="near_antonym" value="<?php echo $question_info_ind->near_antonym; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Audio File</label>
                        <div class="col-sm-8">
                            <input type="file" id="exampleInputFile" name="audioFile">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Video file</label>
                        <div class="col-sm-8">
                            <input type="file" id="exampleInputFilevideo" name="videoFile">
                        </div>
                    </div>

                    <!--  </form> -->
                </div>
            </div>
        </div>

    </div>
</div>

<div class="col-sm-4">
    <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">  Image</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body ss_imag_add_right">

                    <div class="text-center">
                        <div class="form-group ss_h_mi">
                            <label for="exampleInputiamges1">How many images</label>
                            <div class="select">
                                <input class="form-control" type="number" value="<?php echo sizeof($question_info_ind->vocubulary_image); ?>" id="box_qty" onclick="getImageBox(this)">
                            </div>
                        </div>
                    </div>

                    <div class="image_box_list" id="image_box_list">
                        <?php
                        $i = 1;
                        $lettry_array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'k', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T');
                        foreach ($question_info_ind->vocubulary_image as $row) {
                            ?>
                            <div class="row editor_hide" id="list_box_<?php echo $i; ?>" style="display:none; margin-bottom:5px">
                                <div class="col-xs-2">
                                    <p class="ss_lette" style="min-height: 136px; line-height: 137px; ">
    <?php echo $lettry_array[$i - 1]; ?>
                                    </p>
                                </div>
                                <div class="col-xs-10">
                                    <div class="box">
                                        <textarea class="form-control vocubulary_image" name="vocubulary_image_<?php echo $i; ?>[]"><?php echo $row[0]; ?></textarea>
                                    </div>
                                </div>
                            </div>
    <?php $i++;
} ?>
                                    <?php for ($desired_i = $i; $desired_i <= 20; $desired_i++) { ?>					
                            <div class="row editor_hide" id="list_box_<?php echo $desired_i; ?>" style="display:none; margin-bottom:5px">
                                <div class="col-xs-2">
                                    <p class="ss_lette" style="min-height: 136px; line-height: 137px; ">
    <?php echo $lettry_array[$desired_i - 1]; ?>
                                    </p>
                                </div>
                                <div class="col-xs-10">
                                    <div class="box">
                                        <textarea class="form-control vocubulary_image" name="vocubulary_image_<?php echo $desired_i; ?>[]"></textarea>
                                    </div>
                                </div>
                            </div>						
<?php } ?>
                    </div>
                    <input type="hidden" name="image_quantity" id="image_quantity">			

                </div>
            </div>
        </div>


    </div>
</div>

<!-- Modal -->
<div class="modal fade ss_modal" id="ss_sucess_mess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_info.png" class="pull-left"> <span class="ss_extar_top20">Save Sucessfully</span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<script>
    var qtye = $("#box_qty").val();
//    alert(qtye);
    document.getElementById("image_quantity").value = qtye;
    common(qtye);
    function getImageBox() {
        var qty = $("#box_qty").val();
        if (qty < 1) {
            $("#box_qty").val(1);
        } else if (qty > 20) {
            $("#box_qty").val(20);
        } else {
            $('.editor_hide').hide();
            document.getElementById("image_quantity").value = qty;
            common(qty);
        }

    }
    function common(quantity)
    {
        for (var i = 1; i <= quantity; i++)
        {
            $('#list_box_' + i).show();
        }
    }
</script>

