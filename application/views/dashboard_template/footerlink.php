<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/css3-animate-it.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/counter.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/main.js"></script> -->

<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/ckeditor/config.js"></script>
<script src="<?php echo base_url(); ?>assets/ckeditor/styles.js" type="text/javascript"></script>
<!-- select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script>
    $('.mytextarea').ckeditor({
        height: 200,
        extraPlugins : 'simage',
        filebrowserBrowseUrl: '/assets/uploads?type=Images',
        filebrowserUploadUrl: 'imageUpload',
        toolbar: [
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview','Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
        '/',
                { name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] },  // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
            // Line break - next group will be placed in new line.
                '/',
        { name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] }
            ],
        allowedContent: true
    });
    
    $('.vocubulary_image').ckeditor({
        height: 60,
        extraPlugins : 'simage',
        filebrowserBrowseUrl: '/assets/uploads?type=Images',
        filebrowserUploadUrl: 'imageUpload',
        toolbar: [
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
        '/',
                { name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] },  // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
            // Line break - next group will be placed in new line.
                '/',
        { name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] }
            ],
        allowedContent: true
    });
    
    $('.assignment_textarea').ckeditor({
        extraPlugins : 'spdf,simage,sdoc',
        filebrowserBrowseUrl: '/assets/uploads?type=Images',
        filebrowserUploadUrl: 'imageUpload',
        toolbar: [
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage','SPdf','SDoc' ] },
        '/',
                { name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] },  // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
            // Line break - next group will be placed in new line.
                '/',
        { name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] }
            ],
        allowedContent: true
    });
    
    $('.multiple_choice_textarea').ckeditor({
        height: 80,
        extraPlugins : 'simage',
        filebrowserBrowseUrl: '/assets/uploads?type=Images',
        filebrowserUploadUrl: 'imageUpload',
        toolbar: [
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview','Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
        '/',
                { name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] },  // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
            // Line break - next group will be placed in new line.
                '/',
        { name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] }
            ],
        allowedContent: true
    });
    
    $('.my_preview_textarea').ckeditor({
        height: 115,
        extraPlugins : 'simage',
        filebrowserBrowseUrl: '/assets/uploads?type=Images',
        filebrowserUploadUrl: 'imageUpload',
        toolbar: [
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
        '/',
                { name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','Table','-', 'Templates','Link', 'addFile'] },  // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
            // Line break - next group will be placed in new line.
                '/',
        { name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] }
            ],
        allowedContent: true
    });
    
    $('.course_textarea').ckeditor({
        height: 50,
        extraPlugins : 'simage',
        filebrowserBrowseUrl: '/assets/uploads?type=Images',
        filebrowserUploadUrl: 'imageUpload',
        toolbar: [
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
        '/',
                { name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','Table','-', 'Templates','Link', 'addFile'] },  // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
            // Line break - next group will be placed in new line.
                '/',
        { name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] }
            ],
        allowedContent: true
    });
    
    // $('.vocubulary_image').ckeditor({
        // height: 80,
        // filebrowserBrowseUrl: '/assets/uploads?type=Images',
        // filebrowserUploadUrl: 'imageUpload',
        // allowedContent: true,
    // });

    $(document).ready(function () {
        $('#question_time').click(function () {
            if ($(this).is(':checked'))
                $("#set_time").show();
            else
                $("#set_time").hide();
        });
        $('.ss_bottom_s_course .select').on('click', function () {
            $(this).parent().find('div.active').removeClass('active');
            $(this).addClass('active');
        });
        
        $('.select2').select2({
            
        });

    });
</script>
<script>
    function chkLoginAccess() {

        var pathname = '<?php echo base_url(); ?>';
        var user_name = $("#user_name").val();
        var password = $("#password").val();
        $.ajax({
            type: 'POST',
            url: 'loginChk',
            data: {
                user_name: user_name,
                password: password
            },
            dataType: 'html',
            success: function (results) {
                if (results == 0) {
                    $("#error_msg").show();
                }
                if (results == 1) {
                    window.location.href = pathname + "dashboard";
                }
                if (results == 2) {
                    window.location.href = pathname + "dashboard";
                }

            }
        });

    }

    $('#flashmsg').fadeOut(5000);
</script>
