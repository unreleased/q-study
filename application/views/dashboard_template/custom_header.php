<meta charset="UTF-8">
<title>.:: Q-Study :: Tutor yourself...</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
<!-- Framework Css -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- Font Awesome / Icon Fonts -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/font-awesome.min.css">
<!-- Owl Carousel / Carousel- Slider -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/owl.carousel.min.css">
<!-- Animations -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/animations.min.css">
<!-- Style Theme -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<!-- Light Style Theme -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/light.css">
<!-- Responsive Theme -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/countrySelect.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/intlTelInput.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css" rel="stylesheet" />
<!-- Stripe JavaScript library -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dropzone.js"></script>

<!-- cdn -->
<!-- datatable -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- jquery ui cdn -->

<!-- select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- <link rel="stylesheet" href="http://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css"> -->


<style> 
    .ui-widget.ui-widget-content {
        border: 1px solid #c5c5c5;
        z-index: 10000 !important;
    }
</style>

<!-- font awesome-->

<!-- context menu cdn-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.4.9/jquery.autocomplete.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/main.js"></script> -->
