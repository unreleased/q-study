<style>
  td {
    border: 2px solid #f68d20 !important;
  }
  
</style>
<div class="" style="margin-left: 15px;">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-10 user_list">
      <div class="panel-group " id="task_accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title text-center">
              <img style="float:left;" src="assets/images/email-read.png" alt="" width="45px" height="45px;">
              <a role="button" data-toggle="collapse" data-parent="#task_accordion" href="#collapseOnetask" aria-expanded="true" aria-controls="collapseOne"> 
                <strong><span style="font-size : 18px; ">  Choose Message Topics </span></strong>
              </a>
            </h4>
          </div>

          <div class="row panel-body">
            <div class="col-sm-12 text-right"> 
              <button type="submit"  class="btn btn_next" id=""><i class="fa fa-plus" style="padding-right: 5px;"></i>Add New</button>
            </div> 
          </div>
          <div class="row panel-body">
            <div class="col-sm-12 text-right"> 
              <table class="table table-bordered c_shcedule">
                <thead>
                  <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Topic</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1; ?>
                    <?php foreach ($allTopics as $topic) : ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><a href="message/set/<?php echo $topic['id']; ?>"><?php echo  $topic['topic']; ?></a></td>
                      <td><a href=""><i class="fa fa-pencil"></i></a></td>
                      <td><a href=""><i class="fa fa-times"></i></a></td>
                    </tr>
                    <?php endforeach; ?>
                  
                </tbody>
              </table>
            </div>
          </div>


        </div>

      </div>

    </div>
  </div>
</div>
<script>
  //$('.table').DataTable({});
</script>
