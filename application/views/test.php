<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <!-- bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <link 
  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" 
  rel="stylesheet"  type='text/css'>

  <script src="<?php echo base_url(); ?>assets/js/wPaint/lib/jquery.1.10.2.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wPaint/lib/jquery.ui.core.1.10.3.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wPaint/lib/jquery.ui.widget.1.10.3.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wPaint/lib/jquery.ui.mouse.1.10.3.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wPaint/lib/jquery.ui.draggable.1.10.3.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/wPaint/lib/wColorPicker.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wPaint/wPaint.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wPaint/plugins/main/wPaint.menu.main.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wPaint/plugins/shapes/wPaint.menu.main.shapes.min.js"></script>
  
  <!-- html2canvas -->
  <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
  <!--   <script src="assets/js/html2canvas/html2canvas.js"></script> -->

  <!-- wPaint css-->
  <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/js/wPaint/wPaint.min.css">
  <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/js/wPaint/lib/wColorPicker.min.css">

</head>
<body>
  <div id="capture" style="padding: 10px; background: #f5da55; width:150px !important;">
    <h4 style="color: #000; ">Hello world!</h4>
  </div>
  <div id="captured" style="width:150px !important;" style="padding: 10px; background: #CACACA">

  </div>
  <button id="shutter">capture</button>
  <div class="row">
   <div class="col-2"></div>
   <div class="col-md-6">
    <div class="my-drawing">
      <div id="wPaint" style="position:relative;width:800px; height:600px; background:#fff; border:solid black 1px;">
        <div id="setQues" style="margin:0 auto; width:50%; background-color: '#fff'; text-align: center;">
        </div>
      </div>
    </div>
  </div>

  <script>
    $('#shutter').on('click', function(){
      html2canvas(document.querySelector("#capture")).then(canvas => {
        $('#wPaint').wPaint('image', canvas.toDataURL("image/png"));

      });
    })
    $.extend($.fn.wPaint.defaults, {
            mode:        'pencil',  // set mode
            lineWidth:   '4',       // starting line width
            fillStyle:   '#CACACA', // starting fill style
            strokeStyle: '#000',  // start stroke style
            fontSize       : '12',    // current font size for text input
            fontFamily     : 'Arial', // active font family for text input
    });
    $("#wPaint").wPaint({
      path: '<?php echo base_url(); ?>assets/js/wPaint/',
      onDrawDown: function (e) {
        console.log(this.settings.mode + ": " + e.pageX + ',' + e.pageY);
      },
      onDrawMove: true,
      onDrawUp: function (e) {
        console.log(this.settings.mode + ": " + e.pageX + ',' + e.pageY);
      }
    });
  </script>
</body>

</html>
