
<div class="" style="margin-left: 15px;">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8 user_list">
      <div class="panel-group " id="task_accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title text-center">
              <a role="button" data-toggle="collapse" data-parent="#task_accordion" href="#collapseOnetask" aria-expanded="true" aria-controls="collapseOne"> 
                <strong><span style="font-size : 18px; ">  All Subject & Chapters </span></strong>
              </a>
            </h4>
          </div>

          <div class="panel-body">
            <div id="accordion">
              <?php echo $allSubs; ?>
            </div>
          </div>

        </div>

      </div>

    </div>
  </div>
</div>
<script src="assets/js/sweet_alert.js"></script>
<script>
  $( function() {
    $( "#accordion" ).accordion();
  });

  $(document).on('click', '.delChapIcon', function(){
    var chapId = $(this).parent('td').attr('id');
    var obj = $(this);
    swal({
      title: "Are you sure?",
      text: "Once deleted, all question associated with this chapter will be deleted",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url:'chapter/delete/'+chapId,
          method:'get',
          success:function (data) {
            if(data=='true'){
              swal("Chapter has been deleted successfully!", {
                icon: "success",
              }).then(obj.closest('tr').fadeOut(4000))
            } else {
              swal("Chapter not deleted!", {
                icon: "warning",
              });
            }
          }
        })//end ajax
        
      } else {
        swal("Chapter not deleted.");
      }
    });
  });

  $(document).on('click', '.delSubBtn', function(){
    var subId = $(this).attr('subId');
    swal({
      title: "Are you sure?",
      text: "Remember, all chapters and question associated with this subject will be deleted",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url:'subject/delete/'+subId,
          method:'get',
          success:function (data) {
            if(data=='true'){
              swal("Subject has been deleted successfully!", {
                icon: "success",
              }).then(window.location.reload());  
            } else {
              swal("Subject not deleted!", {
                icon: "warning",
              });
            }
          }
        })//end ajax
        
      } else {
        swal("Subject not deleted.");
      }
    });
  });

</script>
