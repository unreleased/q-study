<?php

class Admin extends CI_Controller
{
    public $loggedUserId;
    public function __construct()
    {
        parent::__construct();
        
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;
        
        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }
        if ($user_type != 0) {
            redirect('welcome');
        }
        
        $this->load->library('Ajax_pagination');
        $this->load->model('admin_model');
        $this->load->model('Tutor_model');
        $this->load->model('QuestionModel');
    }
    
    /**
     * Function index() doc comment
     *
     * @return void
     */
    public function index()
    {
        $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/admin_dashboard', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    public function course_theme()
    {
        $data['all_theme'] = $this->admin_model->getAllInfo('tbl_course_theme');
        $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/theme/course_theme', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    public function save_theme()
    {
        $data['theme_name'] = $this->input->post('theme_name');
        //        print_r($data);die;
        $this->admin_model->insertInfo('tbl_course_theme', $data);
        $data['all_theme'] = $this->admin_model->getAllInfo('tbl_course_theme');
        
        $json = array();
        $json['themeDiv'] = $this->load->view('admin/theme/theme_div', $data, true);
        echo json_encode($json);
    }
    
    public function all_area()
    {
        //        $data['all_theme'] = $this->admin_model->getAllInfo('tbl_course_theme');
        $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['page'] = '';
        $data['page_section'] = '';
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/all_area/all_area', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    public function user_list()
    {

        $data['total_registered'] = $this->admin_model->total_registered();
        $data['today_registered'] = $this->admin_model->today_registered();
        $data['tutor_with_10_student'] = $this->admin_model->tutor_with_10_student();
        
        $data['tutor_with_50_vocabulary'] = $this->admin_model->tutor_with_50_vocabulary();
        date_default_timezone_set('Australia/Canberra');
        $date = date('Y-m-d');
        $data['get_todays_data'] = $this->admin_model->get_todays_data($date);
        
        $data['user_type'] = $this->admin_model->getAllInfo('tbl_usertype');
        $data['all_country'] = $this->admin_model->getAllInfo('tbl_country');
        
        $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['page'] = 'User List';
        $data['page_section'] = 'User';

        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/user/user_list', $data, true);
        $this->load->view('master_dashboard', $data);
    }


    public function userAdd()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        if (!$post) {
            //date_default_timezone_set('Australia/Canberra');
            //$date = date('Y-m-d');
            //$data['get_todays_data'] = $this->admin_model->get_todays_data($date);

            $data['user_type'] = $this->admin_model->getAllInfo('tbl_usertype');
            $data['all_country'] = $this->admin_model->getAllInfo('tbl_country');

            $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
            $data['parents'] = $this->admin_model->getInfo('tbl_useraccount', 'user_type', 1);
            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            $data['page'] = 'User List';
            $data['page_section'] = 'User';

            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('admin/user/user_add', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            print_r($clean);
            $dataToSave = [
            'user_type'=> $clean['userType'],
            'country_id' => $clean['country'],
            'children_number' => isset($clean['numOfChild']) ? $clean['numOfChild'] : null,
            'name' => $clean['name'],
            'user_email' => $clean['name'],
            'user_pawd' => $clean['password'],
            'user_mobile' => $clean['mobile'],
            'SCT_link' => $clean['refLink'],
            'created' => time(),
            'subscription_type' => 'signup',
            'parent_id' => isset($clean['parentId']) ? $clean['parentId']:null,
            'token' => null,
            'image' => null,
            'payment_status' => '',
            ];

            $a=$this->admin_model->insertId('tbl_useraccount', $dataToSave);
            echo $a;
        }
    }

    public function tutor_with_10_students()
    {
        $data['tutor_with_10_student'] = $this->admin_model->tutor_with_10_student();

        $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['page'] = 'User List';
        $data['page_section'] = 'User';

        //        echo '<pre>';print_r($data['tutor_with_10_student']);die;

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/user/tutor_with_10_students', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function tutor_create_50_vocabulary()
    {
        $data['tutor_with_50_vocabulary'] = $this->admin_model->tutor_with_50_vocabulary();

        $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['page'] = 'User List';
        $data['page_section'] = 'User';

        //        echo '<pre>';print_r($data['tutor_with_10_student']);die;

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/user/tutor_create_50_vocabulary', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function country_list()
    {
        $data['all_country'] = $this->admin_model->getAllInfo('tbl_country');
        $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['page'] = 'Country List';
        $data['page_section'] = 'Country';

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/country/country_list', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function save_country()
    {
        $data['countryName'] = $this->input->post('countryName');
        $data['countryCode'] = $this->input->post('countryCode');
        //        print_r($data);die;
        $this->admin_model->insertInfo('tbl_country', $data);
        $data['all_country'] = $this->admin_model->getAllInfo('tbl_country');

        $json = array();
        $json['countryDiv'] = $this->load->view('admin/country/country_div', $data, true);
        echo json_encode($json);
    }

    public function update_country()
    {
        $data['countryName'] = $this->input->post('countryName');
        $data['countryCode'] = $this->input->post('countryCode');
        $country_id = $this->input->post('id');
        //        print_r($data);die;
        $this->admin_model->updateInfo('tbl_country', 'id', $country_id, $data);
        $data['all_country'] = $this->admin_model->getAllInfo('tbl_country');

        $json = array();
        $json['countryDiv'] = $this->load->view('admin/country/country_div', $data, true);
        echo json_encode($json);
    }

    public function delete_country($country_id)
    {
        $this->admin_model->deleteInfo('tbl_country', 'id', $country_id);
        redirect('country_list');
    }

    public function country_wise()
    {
        $data['all_country'] = $this->admin_model->getAllInfo('tbl_country');
        $data['user_info'] = $this->admin_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['page'] = 'Country Wise';
        $data['page_section'] = 'Country';

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/schedule/country_list', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function course_schedule($country_id)
    {
        $data['country_info'] = $this->admin_model->getInfo('tbl_country', 'id', $country_id);

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['page'] = 'Country Wise';
        $data['page_section'] = 'Country';

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/schedule/country_wise_schedule', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function add_course_schedule()
    {
        $data['subscription_type'] = $this->input->post('subscription_type');
        $data['user_type'] = $this->input->post('user_type');
        $data['country_id'] = $this->input->post('country_id');

        $data['country_info'] = $this->admin_model->getInfo('tbl_country', 'id', $data['country_id']);
        $data['course_info'] = $this->admin_model->get_course($data['subscription_type'], $data['user_type'], $data['country_id']);

        //        echo '<pre>';print_r($data['course_info']);die;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['page'] = 'Country Wise';
        $data['page_section'] = 'Country';

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/schedule/add_course', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function save_course_schedule()
    {
        $data['country_id'] = $_POST['country_id'];
        $data['courseName'] = strip_tags($_POST['courseName']);
        $data['courseCost'] = $_POST['courseCost'];
        $data['is_enable'] = $_POST['is_enable'];
        $data['user_type'] = $_POST['user_type'];
        $data['subscription_type'] = $_POST['subscription_type'];

        $course_id = $_POST['course_id'];
        //        echo $course_id;print_r($data);die;
        if ($course_id != '') {
            $this->admin_model->updateInfo('tbl_course', 'id', $course_id, $data);
        } else {
            $course_id = $this->admin_model->insertId('tbl_course', $data);
        }


        $data['course_details'] = $this->admin_model->getInfo('tbl_course', 'id', $course_id);
        $data['box_num'] = $_POST['box_num'];

        $json = array();
        $json['course_id'] = $course_id;
        $json['course_content_div'] = $this->load->view('admin/schedule/course_content_div', $data, true);
        echo json_encode($json);
    }

    public function delete_course()
    {
        $course_id = $this->input->post('course_id');
        $this->admin_model->deleteInfo('tbl_course', 'id', $course_id);
    }

    /**
     * Suspend a user accound
     *
     * @param integer $userId User ID to suspend
     *
     * @return void
     */
    public function suspendUser($userId)
    {
        $dataToUpdate = ['suspension_status'=>1];
        $this->admin_model->updateInfo($table = "tbl_useraccount", "id", $userId, $dataToUpdate);
        
        $this->session->set_flashdata('success_msg', 'User suspended successfully.');
        redirect('user_list');
    }

    /**
     * Unsuspend a user
     *
     * @param integer $userId User ID to remove suspension
     *
     * @return [type]
     */
    public function unsuspendUser($userId)
    {
        $dataToUpdate = ['suspension_status'=>0];
        $this->admin_model->updateInfo($table = "tbl_useraccount", "id", $userId, $dataToUpdate);
        
        $this->session->set_flashdata('success_msg', 'Suspension removed successfully.');
        redirect('user_list');
    }

    /**
     * Extend users trial period
     * @return void [description]
     */
    public function extendTrialPeriod()
    {

        $post         = $this->input->post();
        $userId       = $post['userId'];
        $extendDays   = $post['extendAmound'];
        $user         = $this->admin_model->getInfo('tbl_useraccount', 'id', $userId);
        $currentEndDate = isset($user[0]['trial_end_date']) ? $user[0]['trial_end_date'] : date('Y-m-d');
        $dateToSet      = date("Y-m-d", strtotime($currentEndDate. ' +'. $extendDays .'days'));
        $dataToUpdate = ['trial_end_date'=>$dateToSet];
        $this->admin_model->updateInfo('tbl_useraccount', 'id', $userId, $dataToUpdate);
    }

    public function usersCurrentPackages()
    {
        $post = $this->input->post();
        $userId = $post['userId'];

        $usersCourse = $this->admin_model->getInfo('tbl_registered_course', 'user_id', $userId);
        $usersCourse = count($usersCourse)?array_column($usersCourse, 'course_id'):[];
        $courseNames = '';
        foreach ($usersCourse as $courseId) {
            $courseName = $this->admin_model->courseName($courseId);
            $courseNames .= $courseName. ', ';
        }
        $courseNames = rtrim($courseNames, ', ');
        echo $courseNames;
    }

    /**
     * All packages that not taken by user
     *
     * @return string rendered package option
     */
    public function packageNotTaken()
    {
        $post = $this->input->post();
        $userId = $post['userId'];
        $usersCourse = $this->admin_model->getInfo('tbl_registered_course', 'user_id', $userId);
        $usersCourse = count($usersCourse)?array_column($usersCourse, 'course_id'):[];
        
        //if user taken some courses then filter the result else take all courses
        if (count($usersCourse)) {
            $courseNotTakenByUser = $this->admin_model->whereNotIn('tbl_course', 'id', $usersCourse);
        } else {
            $courseNotTakenByUser = $this->admin_model->getAllInfo('tbl_course');
        }
        
        $notTakenCourses = count($courseNotTakenByUser) ? array_column($courseNotTakenByUser, 'id'):[];
        $option = '';
        foreach ($notTakenCourses as $courseId) {
            $courseName = $this->admin_model->courseName($courseId);
            $option .= '<option value="'.$courseId.'">' . $courseName . '</option>';
        }
        echo $option;
    }

    /**
     * Assign packages to a  user.
     *
     * @return void
     */
    public function addPackages()
    {
        $post = $this->input->post();
        $userId = $post['userId'];
        $pkgSelected = array_column($post['pkgSelected'], 'value');
        $dataToInsert = [];
        foreach ($pkgSelected as $pkgId) {
            $dataToInsert[] = [
            'course_id' => $pkgId,
            'user_id'   => $userId,
            'created'   => time(),
            'cost'      => 0,
            ];
        }
        $lastId  = $this->admin_model->insertBatch('tbl_registered_course', $dataToInsert);
        if ($lastId) {
            echo 'success';
        }
    }

    /**
     * show list of all q-dictionary word
     *
     * @return void
     */
    public function dictionaryWordList()
    {
        $data = [];
        /*$allWord = $this->QuestionModel->groupedWordItems();

        $data['wordChunk'] = [];
        foreach ($allWord as $word) {
            $data['wordChunk'][$word['creator_id']][] = $word;
        }*/
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('admin/q-dictionary/wordlist', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    /**
     * Get all word for ajax datatable(ajax call)
     *
     * @return string json encoded string
     */
    public function wordForDataTable()
    {
        $post = $this->input->get();
        $offset = isset($post['start']) ? $post['start']  : 0;
        $limit = isset($post['length']) ? $post['length'] : 5;
        
        $allWord = $this->QuestionModel->groupedWordItems($limit, $offset);
        
        $data['wordChunk'] = [];
        //$data=0;
        foreach ($allWord as $word) {
            $data['wordChunk'][$word['creator_id']][] = $word;
        }
        $data['data'] =[];
        foreach ($data['wordChunk'] as $userChunk) {
            $cnt=0;
            foreach ($userChunk as $word) {
                $checked = $word['word_approved'] ? "checked":"";
                $word['sl'] = ++$cnt;
                $word['ques_created_at'] = date('d M Y', strtotime($word['ques_created_at']));
                $word['view'] ='<a href="q-dictionary/approve/'.$word['word_id'].'">View</a>';
                $word['select'] = '<input class="approvalCheck" wordId="'.$word['word_id'].'" type="checkbox" name="" '.$checked.'>';
                $word['delete'] = '<a href="#" wordId="'.$word['word_id'].'" class="wordDel"> <i class="fa fa-times"></i> </a>';
                $data['data'][] = $word;
            }
        }
        //$data['draw'] =1;
        $data['recordsTotal'] =$this->QuestionModel->countDictWord();
        $data['recordsFiltered'] =$data['recordsTotal'];
        echo json_encode($data);
    }

    public function dicWordApprovePage($wordId)
    {

        $word = $this->QuestionModel->search('tbl_question', ['id'=>$wordId]);
        if (!$word) {
            show_404();
        }
        $data['word_info'] = json_decode($word[0]['questionName']);
        $data['word'] = $word[0]['answer'];
        $data['wordId'] = $wordId;
        $data['approvalStatus'] = $word[0]['word_approved'];
        $data['creator_info'] = $this->Tutor_model->tutorInfo(['id'=>$word[0]['user_id']]);
        $data['creator_info'] = $data['creator_info'][0];
        /*$data['total_items'] = count($this->QuestionModel->search('tbl_question', ['answer'=>$data['word'], 'dictionary_item'=>1]));
        $wordCount = $this->QuestionModel->search(
            'tbl_question',
            [
            'answer'=>$data['word'],
            'dictionary_item'=>1
            ]
        );
        $wordCount = count($wordCount);*/

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['pageType'] = 'q-dictionary';
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $question_box = 'preview/dictionary_word_approval';
        $data['maincontent'] = $this->load->view($question_box, $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * Word approval by admin(ajax call).
     *
     * @param int $wordId question id
     *
     * @return string      update status
     */
    public function wordApprove($wordId)
    {
        $this->QuestionModel->update('tbl_question', 'id', $wordId, ['word_approved'=>1]);
        echo 'true';
    }

    /**
     * Word reject by admin(ajax call).
     *
     * @param int $wordId question id
     *
     * @return string      update status
     */
    public function wordReject($wordId)
    {
        $this->QuestionModel->update('tbl_question', 'id', $wordId, ['word_approved'=>0]);
        echo 'true';
    }

    /**
     * Add dialogue from admin that will
     * show after giving answer from student account
     *
     * @return void
     */
    public function addDialogue()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        if (!$post) {
            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
            $data['maincontent'] = $this->load->view('dialogue/add_dialogue', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            $dataToInsert = [
            'body'=>$post['dialogue_body'],
            'show_whole_year' => isset($post['year']) ? $post['year'] : '',
            'date_to_show' => isset($post['date']) ? $post['date'] : '',
            ];

            $insId = $this->admin_model->insertInfo('dialogue', $dataToInsert);
            if ($insId) {
                $this->session->set_flashdata('success_msg', 'Dialogue Added Successfully');
            }
            redirect('dialogue/add');
        }
    }
}
