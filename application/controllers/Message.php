<?php

class Message extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;
        
        /*if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }*/
        
        $this->load->model('MessageModel');
    }

    public function selectMessageType()
    {
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        //$data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('message/select_message_type', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function showAllTopics()
    {
        $data['allTopics'] = $this->MessageModel->allTopics();

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        //$data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('message/all_topics', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function setMessage($topicId)
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);

        if (!$clean) {
            $data['topic'] = $this->MessageModel->info('message_topics', ['id'=>$topicId]);
        
            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            //$data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('message/set', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            $dataToInsert = [
                'topic' => $clean['topicId'],
                'body'  => $post['body'],
                'type'  => 1, //email type
                'schedule_date' => date('Y-m-d', strtotime($clean['dateToShow'])),
                'created_by' => $this->loggedUserId,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->MessageModel->insert('messages', $dataToInsert);
            $this->session->set_flashdata('success_msg', 'Message setted successfully');
            redirect('message/topics');
        }
    }
}
