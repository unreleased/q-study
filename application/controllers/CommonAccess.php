<?php
//test
class CommonAccess extends CI_Controller
{
    public $loggedUserId,$loggedUserType;
    public function __construct()
    {
        parent::__construct();
        
        $this->loggedUserId = $this->session->userdata('user_id');
        $this->loggedUserType = $this->session->userdata('userType');

        $this->load->model('Parent_model');
        $this->load->model('tutor_model');
        $this->load->model('Student_model');
        $this->load->model('ModuleModel');
        $this->load->model('Tutor_model');
        $this->load->model('QuestionModel');
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $this->load->library('upload');
    }


    /**
     * view student progress by his/her tutor
     * based on search params
     *
     * @return void
     */
    public function viewStudentProgress()
    {
        $data = $this->commonPart();

        if ($this->loggedUserType==2 || $this->loggedUserType==6) {
            $data['isStudent'] = $this->loggedUserId;
            $data['studentName'] = $this->Student_model->studentName($this->loggedUserId);
            $data['studentClass'] = $this->Student_model->studentClass($this->loggedUserId);
        }
        
        $conditions = array(
            'sct_id'=> $this->loggedUserId,
            );

        $studentIds = $this->Student_model->allStudents($conditions);
        $data['students'] = $this->renderStudents($studentIds);
        $data['moduleTypes'] = $this->renderModuletypes($this->ModuleModel->allModuleType());

        $data['maincontent'] = $this->load->view('students/student_progress', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * wrap students info with option
     *
     * @param  array $studentIds student ids of tutor
     * @return string             wrapped string
     */
    public function renderStudents($studentIds)
    {

        $options='';
        if (count($studentIds)) {
            foreach ($studentIds as $studentId) {
                $student = $this->Student_model->userInfo($studentId);
                $student = $student[0];
                $options .= '<option value="'.$studentId.'">'.$student['name'].'</option>';
            }
        }
        return $options;
    }

    /**
     * wrap module types with option tag
     *
     * @param  array $moduleTypes all module type of a tutor attached with
     * @return string              wrapped string
     */
    public function renderModuletypes($moduleTypes)
    {
        $options = '';
        foreach ($moduleTypes as $moduleType) {
            $options .= '<option value="'.$moduleType['id'].'">'.$moduleType['module_type'].'</option>';
        }
        return $options;
    }

    public function renderStProgress($items)
    {
        $row = '';
        foreach ($items as $item) {
            $row .= '<tr>';
            $row .= '<td>'.$this->ModuleModel->moduleName($item['module']).'</td>';
            $row .= '<td>'.$this->ModuleModel->moduleTypeName($item['moduletype']).'</td>';
            $row .= '<td>'.date('Y-m-d', $item['answerDate']).'</td>';
            $row .= '<td>'.$item['answerTime'].'</td>';
            $row .= '<td>'.$item['timeTaken'].'</td>';
            $row .= '<td>'.$item['originalMark'].'</td>';
            $row .= '<td>'.$item['studentMark'].'</td>';
            $row .= '<td>'.$item['percentage'].'</td>';
            $row .= '</tr>';
        }
        return $row;
    }

    /**
     * till now seems common on all functions
     *
     * @return array essential data/view
     */
    public function commonPart()
    {
        $user_id = $this->loggedUserId;

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['user_info'] = $this->tutor_model->userInfo($user_id);
        $data['all_module'] = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_grade'] = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type'] = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course'] = $this->tutor_model->getAllInfo('tbl_course');

        return $data;
    }

    public function studentByClass()
    {
        $post = $this->input->post();

        $class = isset($post['stClass']) ? $post['stClass'] : 0;
        $studentsByClass = array_column($this->Student_model->studentByClass($class), 'student_id');
        $loggedUserStudents =  $this->Student_model->allStudents(['sct_id'=>$this->loggedUserId]);
        
        $students = array_intersect($studentsByClass, $loggedUserStudents);
        $options = '';
        foreach ($students as $student) {
            $studentName = $this->Student_model->studentName($student);
            $options .= '<option value="'.$student.'">'. $studentName .'</option>';
        }
        echo $options;
    }

    public function StProgTableData()
    {
        $post = $this->input->post();
        if (isset($post['studentId'])) {
            $conditions['student_id'] = $post['studentId'];
        }if (isset($post['moduleTypeId'])) {
            $conditions['moduletype'] = $post['moduleTypeId'];
        }
        $allProgress=$this->Student_model->studentProgress($conditions);
        $data['st_progress'] = $this->renderStProgress($allProgress);
        echo $data['st_progress'];
    }

    /**
     * Redirect a suspended user to a custom page
     *
     * @return void
     */
    public function suspendUserRedirection()
    {
        $this->load->view('user_suspended');
    }


    public function searchTutor()
    {
        $post = $this->input->get();
        $clean = $this->security->xss_clean($post);

        $data['country_list'] = $this->Student_model->getAllInfo(' tbl_country');
        $data['subject_list'] = $this->tutor_model->uniqueColVals('tbl_subject', 'subject_name');
        $data['user_info'] = $this->tutor_model->userInfo($this->loggedUserId);
        $data['city_list'] = $this->tutor_model->uniqueColVals('additional_tutor_info', 'city');
        $data['state_list'] = $this->tutor_model->uniqueColVals('additional_tutor_info', 'state');
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['searchItems'] = [];
        if (!$post) {
            $data['searchItems'] = [];
        } else {
            $conditions = array_filter($clean);
            $conditions['user_type'] = 3;
            if (isset($conditions['country_id'])) {
                $conditions['country_id'] =  (int) $clean['country_id'];
            }
            
            $tutors = $this->Tutor_model->tutorInfo($conditions);
            $data['searchItems'] = $tutors;
        }
        $data['maincontent'] = $this->load->view('tutors/tutor_search', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * Show tutor info
     *
     * @param integer $userId tutor account id
     *
     * @return void
     */
    public function showTutorProfile($userId)
    {

        $conditions = [
        'tbl_useraccount.id'=>$userId,
        'tbl_useraccount.user_type'=>3,
        ];
        $tutor = $this->tutor_model->tutorInfo($conditions);

        if (!isset($tutor[0])) {
            show_404();
        } else {
            $data['tutor_info'] = $tutor[0];
            $country = $this->tutor_model->getRow('tbl_country', 'id', $data['tutor_info']['country_id']);
            $data['tutor_info']['country'] = $country['countryName'];
            
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('tutors/tutor_profile', $data, true);
            $this->load->view('master_dashboard', $data);
        }
    }

    /**
     * Frontend view of search word
     *
     * @return void
     */
    public function searchDictionaryWord()
    {
        $data['allWords'] = $this->QuestionModel->allDictionaryWord();
        $data['pageType'] = "q-dictionary";
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('tutors/question/search_dictionary_word', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * For auto complete suggestion
     *
     * @return string json encoded word items
     */
    public function ajaxSearchDicWord()
    {
        $keyword = $this->input->get('query');
        $data['suggestions'] = $this->QuestionModel->searchWord($keyword);

        echo json_encode($data);
    }

    /**
     * Initial page view of dictionary item
     *
     * @param string $word word to show
     *
     * @return void
     */
    public function viewDictionaryWord()
    {
        $word = $this->input->post('word');
        $word = $this->getWordInfo($word, 1, 0);
        if (!$word) {
            show_404();
        }
        $data['word_info'] = json_decode($word['word_info'][0]['questionName']);
        $data['word'] = $word['word_info'][0]['answer'];
        $data['creator_info'] = $word['word_creator_info'][0];
        $data['total_items'] = count($this->QuestionModel->search('tbl_question', ['answer'=>$data['word'], 'dictionary_item'=>1]));
        $wordCount = $this->QuestionModel->search(
            'tbl_question', //table
            [ // conditions
            'answer'=>$data['word'],
            'dictionary_item'=>1
            ]
        );
        $wordCount = count($wordCount);

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['pageType'] = 'q-dictionary';
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['pagination'] = $this->pagination($wordCount);
        $question_box = 'preview/dictionary_word';
        $data['maincontent'] = $this->load->view($question_box, $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * Get word and word creator info
     *
     * @param string  $word   word to get info
     * @param integer $limit  limit data(should always 1 as 1 item per page)
     * @param integer $offset skip item(1,2,3,...)
     *
     * @return array           word info and creator info array
     */
    public function getWordInfo($word, $limit, $offset)
    {
        $data['word_info'] = $this->QuestionModel->dicItemsByWord($word, $limit, $offset);

        if (!count($data['word_info'])) {
            return 0;
        }
        $wordCreatorId = $data['word_info'][0]['user_id'];
        $conditions = ['id'=>$wordCreatorId];
        $data['word_creator_info'] = $this->Tutor_model->tutorInfo($conditions);
        
        return $data;
    }

    /**
     * To view previous next dictionary word item
     *
     * @return string json ecoded string
     */
    public function wordInfoByAjaxCall($offset = 0)
    {
        $post = $this->input->post();
        $wordReq = $post['word'];
        $limit = 1;

        $word = $this->getWordInfo($wordReq, $limit, $offset);
        
        if (!$word) {
            echo 'Word Not Found';
            return 0;
        }
        $data['word_info'] = json_decode($word['word_info'][0]['questionName']);
        $data['word'] = $word['word_info'][0]['answer'];
        $data['creator_info'] = $word['word_creator_info'][0];
        
        if (isset($data['creator_info']['image']) && file_exists('assets/uploads/'.$data['creator_info']['image'])) {
            $data['creator_info']['image'] = base_url()."assets/uploads/".$data['creator_info']['image'];
        } else {
            $data['creator_info']['image'] = base_url()."assets/images/default_user.jpg";
        }
        echo json_encode($data);
    }

    /**
     * View a faq by all user
     *
     * @param  integer $id faq id to view
     * @return void
     */
    public function viewFaq($id)
    {
        $this->load->model('FaqModel');
        $data['faq'] = $this->FaqModel->info(['id'=>$id]);
        $data['allFaqs'] = $this->FaqModel->allFaqs();
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $data['leftnav'] = $this->load->view('faqs/all_faq_left_nav', $data, true);
        
        $data['maincontent'] = $this->load->view('faqs/view_faq', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * Responsible for  pagination link
     *
     * @param  integer $totItems total item count
     * @return string
     */
    public function pagination($totItems = 0)
    {
        $this->load->library('pagination');
        $config = array();
        $config["base_url"] = "CommonAccess/wordInfoByAjaxCall";
        $config["total_rows"] = $totItems;//$total_row;
        $config["per_page"] = 1;
        $config['use_page_numbers'] = true;
        $config['num_links'] = $totItems;//$total_row;
        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['prev_link'] = 'Previous';
        $config['next_link'] = false;//'Next..';
        $config['attributes'] = array('class' => 'myclass page-link');

        //$config['first_link'] = 'First';
        $config['full_tag_open'] = '<li class="page-item">';
        $config['full_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    /**
     * Front page items like about us, pricing view
     *
     * @param  string $item item name ex:pricing
     * @return void
     */
    public function viewLandPageItem($item)
    {
        $this->load->model('FaqModel');
        $item = $this->FaqModel->info(['item_type'=>$item]);
        if (count($item)) {
            $data['body'] = strlen($item['body'])?$item['body'] : '';
            $data['title'] = strlen($item['title'])?$item['title'] : '';
        } else {
            $data['body'] = '';
            $data['title'] = 'Not Found';
        }

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['leftnav'] = $this->load->view('faqs/frontPageItems/addOtherItemsLeftNav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('faqs/frontPageItems/viewItems', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * User can send message to q-study.
     *
     * @return void
     */
    public function contactUs()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        if (!$post) {
            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('faqs/contact_us', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            $dataToInsert = [
            'user_name' => $clean['userName'],
            'user_email' => $clean['userEmail'],
            'message_body' => $clean['userMessage'],
            'sent_at' =>date('Y-m-d H:i:s'),
            'updated_at' =>date('Y-m-d H:i:s'),
            ];
            
            $this->Admin_model->insertInfo('user_message', $dataToInsert);
            $this->session->set_flashdata('success_msg', 'Message Sent Successfully');
            redirect('/');
        }
    }

    public function notifyMessage()
    {
        $this->load->model('MessageModel');
        $conditions = [
            'schedule_date'=>date('Y-m-d')
        ];
       
        $messageToSend = $this->MessageModel->allMessages($conditions);

        foreach ($messageToSend as $message) {
            $creator =  $message['created_by'];
            $studentsOfTutor = $this->Tutor_model->getInfo('tbl_enrollment', 'sct_id', $creator);
        }
    }

    public function test()
    {
        $this->load->view('test2');
    }

    /*public function test2()
    {

        $post = $this->input->get();
        $offset = isset($post['start']) ? $post['start']  : 0;
        $limit = isset($post['length']) ? $post['length'] : 5;

        $allWord = $this->QuestionModel->groupedWordItems($limit, $offset);

        $data['wordChunk'] = [];
        //$data=0;
        foreach ($allWord as $word) {
            $data['wordChunk'][$word['creator_id']][] = $word;
        }
        $data['data'] =[];
        foreach ($data['wordChunk'] as $userChunk) {
            $cnt=0;
            foreach ($userChunk as $word) {
                $checked = $word['word_approved'] ? "checked":"";
                $word['sl'] = ++$cnt;
                $word['ques_created_at'] = date('d M Y', strtotime($word['ques_created_at']));
                $word['view'] ='<a href="q-dictionary/approve/'.$word['word_id'].'">View</a>';
                $word['select'] = '<input class="approvalCheck" wordId="'.$word['word_id'].'" type="checkbox" name="" '.$checked.'>';
                $word['delete'] = '<a href="#" wordId="'.$word['word_id'].'" class="wordDel"> <i class="fa fa-times"></i> </a>';
                $data['data'][] = $word;
            }
        }
        //$data['draw'] =1;
        $data['recordsTotal'] =$this->QuestionModel->countDictWord();
        $data['recordsFiltered'] =$data['recordsTotal'];
        echo json_encode($data);
    }*/
}
