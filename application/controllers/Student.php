<?php

class Student extends CI_Controller
{
    //test with sourcetree error fix
    public $loggedUserId, $userType;
    public function __construct()
    {
        parent::__construct();

        $user_id            = $this->session->userdata('user_id');
        $user_type          = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;
        $this->userType     = $user_type;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }

        $this->load->model('Student_model');
        $this->load->model('tutor_model');
        $this->load->model('Preview_model');
        $this->load->model('ModuleModel');
        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');
        $this->load->library('upload');
    } //end __construct()

    public function index()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/students_dashboard', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end index()

    public function student_setting()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_setting', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end student_setting()

    public function student_details()
    {
        $data['user_info']      = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['studentRefLink'] = $this->Student_model->getStudentRefLink($this->session->userdata('user_id'));

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_details', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end student_details()

    public function update_student_details()
    {
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[6]|min_length[5]');
        $this->form_validation->set_rules('passconf', 'passconf', 'trim|required|matches[password]');
        if ($this->form_validation->run() == false) {
            echo 0;
        } else {
            $password = md5($this->input->post('password'));
            $data     = ['user_pawd' => $password];
            $this->Student_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }
    } //end update_student_details()

    public function my_enrollment()
    {
        $data['user_info']              = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['get_involved_teacher']   = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 3);
        $data['get_involved_school']    = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 4);
        $data['get_involved_corporate'] = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 5);

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/my_enrollment_list', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end my_enrollment()

    public function save_ref_link()
    {
        $data_link = $this->input->post('link');
        if (!empty($data_link)) {
            $userType = $this->input->post('userType');
            $j        = 0;
            foreach ($data_link as $single_link) {
                if ($single_link) {
                    $get_link_validate = $this->Student_model->getLinkInfo('tbl_useraccount', 'SCT_link', 'user_type', $single_link, $userType);
                    if (!$get_link_validate) {
                        $j++;
                    }
                }
            }

            if ($j > 0) {
                echo 2;
            } else {
                $this->Student_model->delete_enrollment($userType, $this->session->userdata('user_id'));
                foreach ($data_link as $single_link) {
                    $get_link_status = $this->Student_model->getLinkInfo('tbl_useraccount', 'SCT_link', 'user_type', $single_link, $userType);

                    if ($get_link_status) {
                        $enrollment_info = $this->Student_model->getLinkInfo('tbl_enrollment', 'sct_id', 'st_id', $get_link_status[0]['id'], $this->session->userdata('user_id'));
                        // echo '<pre>';print_r($get_link_status);die;
                        if (!$enrollment_info) {
                            $link['sct_id']   = $get_link_status[0]['id'];
                            $link['sct_type'] = $get_link_status[0]['user_type'];
                            $link['st_id']    = $this->session->userdata('user_id');
                            $this->Student_model->insertInfo('tbl_enrollment', $link);
                        }
                    }
                }

                echo 1;
            } //end if
        } else {
            echo 0;
        } //end if
    } //end save_ref_link()

    public function get_ref_link()
    {
        $user_type       = $this->input->post('user_type');
        $st_id           = $this->session->userdata('user_id');
        $enrollment_info = $this->Student_model->get_sct_enrollment_info($st_id, $user_type);
        echo json_encode($enrollment_info);
    } //end get_ref_link()

    public function student_upload_photo()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/upload', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end student_upload_photo()

    private function upload_user_photo_options()
    {
        $config                  = [];
        $config['upload_path']   = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        // $config['max_width'] = 1080;
        // $config['max_height'] = 640;
        // $config['min_width']  = 150;
        // $config['min_height'] = 150;
        $config['overwrite'] = false;
        return $config;
    } //end upload_user_photo_options()

    public function sure_student_photo_upload()
    {
        $this->upload->initialize($this->upload_user_photo_options());
        if (!$this->upload->do_upload('file')) {
            echo 0;
        } else {
            $imageName            = $this->upload->data();
            $user_profile_picture = $imageName['file_name'];
            $data                 = ['image' => $user_profile_picture];
            $rs['res']            = $this->Student_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }
    } //end sure_student_photo_upload()

    public function view_course()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/view_course', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end view_course()

    public function q_study_course()
    {
        $data['user_info']  = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['tutor_type'] = 7;

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/q_study_course', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end q_study_course()

    /**
     * All module by tutor type and module type
     * function hit from q_study_course page
     *
     * @param  integer $tutorType   tutor type=7 or 3
     * @param  integer $module_type module type
     * @return void
     */
    public function all_module_by_type($tutorType, $module_type)
    {
        $user_country         = $this->session->userdata('country_id');
        $data['user_info']    = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['subject_info'] = $this->Student_model->subjectInfo($tutorType);
        $data['tutorType']    = $tutorType;
        $data['moduleType']   = $module_type;
        $data['headerlink']   = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']       = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink']   = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['studentSubjects'] = $this->Student_model->studentSubjects($this->loggedUserId);
        if ($tutorType == 7) {
            //q-study
            $data['tutorInfo']  = $this->Student_model->getInfo('tbl_useraccount', 'user_type', 7);
            $data['tutorImage'] = isset($data['tutorInfo']) ? $data['tutorInfo'][0]['image'] : '';

            $allChapters         = $this->Student_model->chaptersOfSubject(array_column($data['studentSubjects'], 'subject_id'));
            $data['maincontent'] = $this->load->view('students/tutorial/all_tutorial_list', $data, true);
        }
        if ($tutorType == 3) {
            //tutor
            $data['maincontent'] = $this->load->view('students/tutorial/all_tutorial_list', $data, true);
        }

        $this->load->view('master_dashboard', $data);
    } //end all_module_by_type()

    /**
     * All chapters of a subject(ajax hit)
     *
     * @param  integer $subjectId subject id
     * @return string             rendered chapter items
     */
    public function renderedChapters($subjectId)
    {
        $chapters = $this->Student_model->chaptersOfSubject($subjectId);
        $row      = '<option value="">Select Chapter</option>';
        foreach ($chapters as $chapter) {
            $row .= '<option value="' . $chapter['id'] . '">' . $chapter['chapterName'] . '</option>';
        }
        echo $row;
    }

    /**
     * All module added by Q-study for student(ajax hit).
     *
     * First we picked students enrolled course and match the course with subject recorded.
     * We can choose subject and chapter to filter module search
     *
     * @return string render module items with table tag
     */
    public function studentsModuleByQStudy()
    {
        $data['student_error_ans'] = $this->Student_model->getInfo('tbl_st_error_ans', 'st_id', $this->session->userdata('user_id'));

        $post         = $this->input->post();
        $subjectId    = $post['subjectId'];
        $chapterId    = $post['chapterId'];
        $moduleType   = $post['moduleType'];
        $tutorType    = $post['tutorType'];
        $studentGrade = $this->Student_model->studentClass($this->loggedUserId);

        $conditions = array(
            'subject'              => $subjectId,
            'chapter'              => $chapterId,
            'moduleType'           => $moduleType,
            'tbl_module.user_type' => $tutorType,
            'studentGrade'         => $studentGrade,
        );

        $allModule = $this->ModuleModel->allModule(array_filter($conditions));

        $moduleIdWithWrongAns = array_unique(array_column($data['student_error_ans'], 'module_id'));
        //common module which he/she belongs and answered wrong
        $moduleIdWithWrongAns = array_intersect(array_column($allModule, 'id'), $moduleIdWithWrongAns);
        $moduleRepeatToday    = [];
        $todayDate            = date('Y-m-d');

        //module for repetition
        foreach ($allModule as $module) {
            //if module not exits in wrong ans given list then skip
            if (!in_array($module['id'], $moduleIdWithWrongAns)) {
                continue;
            }

            $temp     = json_decode($module['repetition_days']);
            $sl_dates = count($temp) ? $temp : [];
            foreach ($sl_dates as $sl_date) {
                $repeatDate = explode('_', $sl_date);
                $repeatDate = date('Y-m-d', strtotime($repeatDate[1]));
                if ($repeatDate == $todayDate) {
                    $moduleRepeatToday[] = $module;
                    break;
                }
            }
        }

        $row = '';
        //previously answered wrong, Now these module will repeat
        foreach ($moduleRepeatToday as $wrongAnsGiven) {
            $video_link = json_decode($wrongAnsGiven['video_link']);
            $link       = 'get_tutor_tutorial_module/' . $wrongAnsGiven['id'] . '/1';
            if ($video_link) {
                $link = 'video_link/' . $wrongAnsGiven['id'] . '/' . $wrongAnsGiven['moduleType'];
            }

            $row .= '<tr>';
            $row .= '<td><a onclick="get_permission(' . $wrongAnsGiven['id'] . ')">' . 'Repetition for : [' . $wrongAnsGiven['moduleName'] . ']</a></td>';
            $row .= '<td>' . $wrongAnsGiven['creatorName'] . '</td>';
            $row .= '<td>' . $wrongAnsGiven['trackerName'] . '</td>';
            $row .= '<td>' . $wrongAnsGiven['individualName'] . '</td>';
            $row .= '<td>' . $wrongAnsGiven['subject_name'] . '</td>';
            $row .= '<td>' . $wrongAnsGiven['chapterName'] . '</td>';
            $row .= '</tr>';
        }

        $row .= '<input type="hidden" id="first_module_id" value="' . $allModule[0]['id'] . '">';
        if ($data['student_error_ans'] && $moduleType == 2) {
            foreach ($data['student_error_ans'] as $error_ans) {
                $row .= '<tr>
                    <td colspan="6">
                        <a href="get_tutor_tutorial_module/' . $error_ans['module_id'] . '/' . $error_ans['question_order_id'] . '" style="font-size: 13px;">
                            Repetition for [Q-' . $error_ans['question_order_id'] . ']
                        </a>
                    </td>
                </tr>';
            }
        }

        foreach ($allModule as $module) {
            $video_link = json_decode($module['video_link']);
            $link       = 'get_tutor_tutorial_module/' . $module['id'] . '/1';
            if ($video_link) {
                $link = 'video_link/' . $module['id'] . '/' . $module['moduleType'];
            }

            $row .= '<tr>';
            $row .= '<td><a onclick="get_permission(' . $module['id'] . ')">' . $module['moduleName'] . '</a></td>';
            $row .= '<td>' . $module['creatorName'] . '</td>';
            $row .= '<td>' . $module['trackerName'] . '</td>';
            $row .= '<td>' . $module['individualName'] . '</td>';
            $row .= '<td>' . $module['subject_name'] . '</td>';
            $row .= '<td>' . $module['chapterName'] . '</td>';
            $row .= '</tr>';
        }
        echo strlen($row) ? $row : 'no module found';
    }

    public function tutorial($tutorType)
    {
        $user_country         = $this->session->userdata('country_id');
        $data['user_info']    = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['subject_info'] = $this->Student_model->subjectInfo($tutorType);

        if ($tutorType == 7) {
            $data['all_module'] = $this->Student_model->all_module_by_type($user_country, $tutorType);
        }

        if ($tutorType == 3) {
            $sct_info           = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), $this->session->userdata('userType'));
            $data['all_module'] = $this->Student_model->module_by_type($user_country, $sct_info[0]['sct_id']);
        }

        // echo '<pre>';print_r($data['subject_info']);die;
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/tutorial/all_tutorial_list', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end tutorial()

    public function tutor_course()
    {
        $data['user_info']  = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['tutor_type'] = 3;
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/q_study_course', $data, true);
        $this->load->view('master_dashboard', $data);
    } //end tutor_course()

    // added by sobuj
    public function get_tutor_tutorial_module($modle_id, $question_order_id)
    {
        // $this->session->unset_userdata('data');die;
        $gurd_array = $this->session->userdata('data');

        $this->check_browser_back_next_previlige($gurd_array, $question_order_id);

        $data['user_info']       = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['question_info_s'] = $this->tutor_model->getModuleQuestion($modle_id, $question_order_id, null);
        $data['total_question']  = $this->tutor_model->getModuleQuestion($modle_id, null, 1);
        $data['page_title']      = '.:: Q-Study :: Tutor yourself...';

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        // echo '<pre>';print_r($data['question_info_s']);die;
        if ($data['question_info_s'][0]['question_type'] == 1) {
            $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_general', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 2) {
            $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_true_false', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 3) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent']             = $this->load->view('stude_module_tutor_tutorial/ans_vocabulary', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 4) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent']             = $this->load->view('stude_module_tutor_tutorial/answ_multiple_choice', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 5) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent']             = $this->load->view('stude_module_tutor_tutorial/ans_multiple_response', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 6) {
            $quesInfo = $this->tutor_model->getModuleQuestion($modle_id, $question_order_id, null);

            $data['question_info_s']    = $quesInfo;
            $questionType               = $quesInfo[0]['questionType'];
            $quesInfo                   = json_decode($quesInfo[0]['questionName']);
            $data['question_info_skip'] = json_decode($data['question_info_s'][0]['questionName']);

            $data['numOfRows']    = isset($quesInfo->numOfRows) ? $quesInfo->numOfRows : 0;
            $data['numOfCols']    = isset($quesInfo->numOfCols) ? $quesInfo->numOfCols : 0;
            $data['questionBody'] = isset($quesInfo->question_body) ? $quesInfo->question_body : '';

            $data['questionId']  = $data['question_info_s'][0]['question_id'];
            $data['question_id'] = $data['question_info_s'][0]['question_id'];

            // echo '<pre>';print_r($data['question_info_s']);die;
            $quesAnsItem     = $quesInfo->skp_quiz_box;
            $items           = $this->indexQuesAns($quesAnsItem);
            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols']);

            $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_skip', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 7) {
            $data['question_info_left_right'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent']              = $this->load->view('stude_module_tutor_tutorial/ans_matching', $data, true);
        } //end if

        $this->load->view('master_dashboard', $data);
    } //end get_tutor_tutorial_module()

    private function check_browser_back_next_previlige($gurd_array, $question_order_id)
    {
        if (is_array($gurd_array)) {
            if (array_key_exists($question_order_id, $gurd_array)) {
                $has_module_type = $gurd_array[$question_order_id]['module_type'];
                if ($has_module_type) {
                    if ($has_module_type == 1) {
                        return;
                    } else {
                        $hasData = $gurd_array[$question_order_id]['question_id'];
                        if ($hasData) {
                            $redirect_order_id = ($question_order_id + 1);
                            redirect('get_tutor_tutorial_module/' . $gurd_array[$question_order_id]['module_id'] . '/' . $redirect_order_id);
                        } else {
                            return;
                        }
                    }
                }
            } //end if
        } else {
            return;
        } //end if
    } //end check_browser_back_next_previlige()

    /**
     * before passing items to renderSkpQuizPrevTable() index it first with this func
     *
     * @param  array $items json object array
     * @return array        array with proper indexing
     */
    public function indexQuesAns($items)
    {
        // print_r($items);die;
        $arr = [];
        foreach ($items as $item) {
            $temp            = json_decode($item);
            $cr              = explode('_', $temp->cr);
            $col             = $cr[0];
            $row             = $cr[1];
            $arr[$col][$row] = [
                'type' => $temp->type,
                'val'  => $temp->val,
            ];
        }

        return $arr;
    } //end indexQuesAns()

    /**
     * render the indexed item to table data for preview
     *
     * @param  array   $items   ques ans as indexed item
     * @param  integer $rows    num of row in table
     * @param  integer $cols    num of cols in table
     * @param  integer $showAns optional, set 1 will show the answers too
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 0)
    {

        $row = '';
        for ($i = 1; $i <= $rows; $i++) {
            $row .= '<tr>';
            for ($j = 1; $j <= $cols; $j++) {
                if ($items[$i][$j]['type'] == 'q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="' . $items[$i][$j]['val'] . '" name="skip_counting[]" class="form-control input-box  rsskpinpt' . $i . '_' . $j . '" readonly style="min-width:50px; max-width:50px"></td>';
                } else {
                    $ansObj = [
                        'cr'   => $i . '_' . $j,
                        'val'  => $items[$i][$j]['val'],
                        'type' => 'a',
                    ];
                    $ansObj = json_encode($ansObj);
                    $val    = ($showAns == 1) ? ' value="' . $items[$i][$j]['val'] . '"' : '';

                    $row .= '<td><input autocomplete="off" type="text" ' . $val . ' data_q_type="0" data_num_colofrow="' . $i . '_' . $j . '" value="" name="skip_counting[]" class="form-control input-box ans_input  rsskpinpt' . $i . '_' . $j . '"  style="min-width:50px; max-width:50px">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    $row .= '</td>';
                }
            }

            $row .= '</tr>';
        } //end for

        return $row;
    } //end renderSkpQuizPrevTable()

    private function take_decesion_1($question_id, $module_id, $question_order_id, $text, $text_1, $answer_info = null)
    {
        // echo 'take_decesion_1';die;
        $ans_array = $this->session->userdata('data');
        if (!is_array($ans_array)) {
            $ans_array = [];
        }

        if ($text == $text_1) {
            if ($_POST['next_question'] == 0) {
                $this->save_student_answer($module_id, $_POST['current_order']);
            } else {
                $question_info_ai = $this->tutor_model->getModuleQuestion($module_id, $question_order_id, null);
                $link1            = base_url();
                $link2            = $link1 . 'get_tutor_tutorial_module/' . $module_id . '/' . $question_order_id;
                $ind_ans          = [
                    'question_order_id' => $question_info_ai[0]['question_order'],
                    'module_type'       => $question_info_ai[0]['moduleType'],
                    'module_id'         => $question_info_ai[0]['module_id'],
                    'question_id'       => $question_info_ai[0]['question_id'],
                    'link'              => $link2,
                ];
                $ans_array[$question_order_id] = $ind_ans;
                $this->session->set_userdata('data', $ans_array);
                if ($answer_info != null) {
                    echo $answer_info;
                } else {
                    echo 2;
                }
            } //end if
        } else {
            if ($answer_info != null) {
                echo $answer_info;
            } else {
                echo 3;
            }
        } //end if
    } //end take_decesion_1()

    private function take_decesion_2($question_id, $module_id, $question_order_id, $text, $text_1)
    {
        $ans_array = $this->session->userdata('data');
        if (!is_array($ans_array)) {
            $ans_array = [];
        }

        if ($text != $text_1) {
            $data['st_id'] = $this->session->userdata('user_id');

            $data['question_id'] = $question_id;

            $data['module_id'] = $module_id;

            $this->db->insert('tbl_st_error_ans', $data);
        }

        if ($_POST['next_question'] == 0) {
            $this->save_student_answer($module_id, $_POST['current_order']);
        } else {
            $question_info_ai = $this->tutor_model->getModuleQuestion($module_id, $question_order_id, null);
            $link1            = base_url();
            $link2            = $link1 . 'get_tutor_tutorial_module/' . $module_id . '/' . $question_order_id;
            $ind_ans          = [
                'question_order_id' => $question_info_ai[0]['question_order'],
                'module_type'       => $question_info_ai[0]['moduleType'],
                'module_id'         => $question_info_ai[0]['module_id'],
                'question_id'       => $question_info_ai[0]['question_id'],
                'link'              => $link2,
            ];
            $ans_array[$question_order_id] = $ind_ans;
            $this->session->set_userdata('data', $ans_array);
            echo 5;
        }
    } //end take_decesion_2()

    public function st_answer_matching()
    {
        // echo '<pre>';print_r($_POST['module_type']);die;
        $question_id       = $_POST['id'];
        $module_id         = $_POST['check_module_id'];
        $question_order_id = ($_POST['check_order_id'] - 1);
        $text              = $_POST['user_answer'];
        $find              = [
            '&nbsp;',
            '\n',
            '\t',
            '\r',
        ];
        $repleace = [
            '',
            '',
            '',
            '',
        ];
        $text = strip_tags($text);
        $text = str_replace($find, $repleace, $text);
        $text = trim($text);

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);

        $text_1 = $answer_info[0]['answer'];
        $find   = [
            '&nbsp;',
            '\n',
            '\t',
            '\r',
        ];
        $repleace = [
            '',
            '',
            '',
            '',
        ];
        $text_1 = strip_tags($text_1);
        $text_1 = str_replace($find, $repleace, $text_1);
        $text_1 = trim($text_1);

        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_id, $module_id, $question_order_id, $text, $text_1);
        } else {
            $this->take_decesion_2($question_id, $module_id, $question_order_id, $text, $text_1);
        }
    } //end st_answer_matching()

    public function st_answer_matching_vocabolary()
    {
        $this->form_validation->set_rules('answer', 'answer', 'required');

        if ($this->form_validation->run() == false) {
            echo 1;
        } else {
            $text        = strtolower($this->input->post('answer'));
            $question_id = $this->input->post('question_id');
            $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);

            $module_id         = $_POST['module_id'];
            $question_order_id = ($_POST['next_question'] - 1);

            $text_1 = strtolower($answer_info[0]['answer']);

            if ($_POST['module_type'] == 1) {
                $this->take_decesion_1($question_id, $module_id, $question_order_id, $text, $text_1);
            } else {
                $this->take_decesion_2($question_id, $module_id, $question_order_id, $text, $text_1);
            }
        } //end if
    } //end st_answer_matching_vocabolary()

    public function st_answer_matching_true_false()
    {
        $this->form_validation->set_rules('answer', 'answer', 'required');
        if ($this->form_validation->run() == false) {
            echo 1;
        } else {
            $text        = $this->input->post('answer');
            $question_id = $this->input->post('question_id');

            $module_id         = $_POST['module_id'];
            $question_order_id = ($_POST['next_question'] - 1);

            $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
            $text_1      = $answer_info[0]['answer'];

            if ($_POST['module_type'] == 1) {
                $this->take_decesion_1($question_id, $module_id, $question_order_id, $text, $text_1);
            } else {
                $this->take_decesion_2($question_id, $module_id, $question_order_id, $text, $text_1);
            }
        } //end if
    } //end st_answer_matching_true_false()

    public function st_answer_matching_multiple_choice()
    {
        $question_id = $_POST['id'];
        $text_1      = $_POST['answer_reply'];
        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $text        = $answer_info[0]['answer'];

        $module_id         = $_POST['module_id'];
        $question_order_id = ($_POST['next_question'] - 1);

        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_id, $module_id, $question_order_id, $text, $text_1);
        } else {
            $this->take_decesion_2($question_id, $module_id, $question_order_id, $text, $text_1);
        }
    } //end st_answer_matching_multiple_choice()

    public function st_answer_matching_multiple_response()
    {
        $question_id = $_POST['id'];
        $text_1      = $_POST['answer_reply'];

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        // $text = $answer_info[0]['answer'];
        $text         = json_decode($answer_info[0]['answer']);
        $result_count = count(array_intersect($text_1, $text));

        $module_id         = $_POST['module_id'];
        $question_order_id = ($_POST['next_question'] - 1);

        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_id, $module_id, $question_order_id, count($text_1), $result_count);
        } else {
            $this->take_decesion_2($question_id, $module_id, $question_order_id, count($text), $result_count);
        }
    } //end st_answer_matching_multiple_response()

    public function st_answer_multiple_matching()
    {
        $total = $_POST['total_ans'];

        $question_id = $_POST['id'];
        $st_ans      = [];
        for ($i = 1; $i <= $total; $i++) {
            $ans_id   = 'answer_' . $i;
            $st_ans[] = $_POST[$ans_id];
        }

        $answer                   = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);

        $answer_info['student_ans'] = $st_ans;

        $module_id         = $_POST['module_id'];
        $question_order_id = ($_POST['next_question'] - 1);

        $text   = 0;
        $text_1 = 0;
        for ($k = 0; $k < sizeof($answer_info['student_ans']); $k++) {
            if ($answer_info['student_ans'][$k] != $answer_info['tutor_ans'][$k]) {
                $text++;
            }
        }

        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_id, $module_id, $question_order_id, $text, $text_1, json_encode($answer_info));
        } else {
            $this->take_decesion_2($question_id, $module_id, $question_order_id, $text, $text_1);
        }
    } //end st_answer_multiple_matching()

    public function st_answer_skip()
    {
        // echo '<pre>';print_r($_POST);die;
        $module_id         = $_POST['module_id'];
        $question_order_id = ($_POST['next_question'] - 1);

        $post       = $this->input->post();
        $questionId = $_POST['question_id'];
        // $this->input->post('question_id');
        $givenAns = $this->indexQuesAns($post['given_ans']);

        $temp = $this->tutor_model->getInfo('tbl_question', 'id', $questionId);
        // echo '<pre>';print_r($temp);die;
        $savedAns = $this->indexQuesAns(json_decode($temp[0]['answer']));

        $temp2     = json_decode($temp[0]['questionName']);
        $numOfRows = $temp2->numOfRows;
        $numOfCols = $temp2->numOfCols;
        // echo $numOfRows .' ' . $numOfCols;
        $wrongAnsIndices = [];

        $text   = 0;
        $text_1 = 0;
        for ($row = 1; $row <= $numOfRows; $row++) {
            for ($col = 1; $col <= $numOfCols; $col++) {
                if (isset($savedAns[$row][$col])) {
                    $wrongAnsIndices[] = ($savedAns[$row][$col] != $givenAns[$row][$col]) ? $row . '_' . $col : null;
                }
            }
        }

        $wrongAnsIndices = array_filter($wrongAnsIndices);
        if (count($wrongAnsIndices)) {
            // For False Condition
            $text_1 = 1;
        }

        if ($_POST['module_type'] == 1) {
            // echo $text_1;die;
            $this->take_decesion_1($questionId, $module_id, $question_order_id, $text, $text_1);
        } else {
            $this->take_decesion_2($questionId, $module_id, $question_order_id, $text, $text_1);
        }
    } //end st_answer_skip()

    private function save_student_answer($module_id, $question_order_id)
    {
        $ans_array = $this->session->userdata('data');
        if (!is_array($ans_array)) {
            $ans_array = [];
        }
        $question_info_ai = $this->tutor_model->getModuleQuestion($module_id, $question_order_id, null);

        $ind_ans = [
            'question_order_id' => $question_info_ai[0]['question_order'],
            'module_type'       => $question_info_ai[0]['moduleType'],
            'module_id'         => $question_info_ai[0]['module_id'],
            'question_id'       => $question_info_ai[0]['question_id'],
        ];

        $ans_array[$question_order_id] = $ind_ans;
        $this->session->set_userdata('data', $ans_array);
        $total_ans         = $this->session->userdata('data', $ans_array);
        $data['st_ans']    = json_encode($total_ans);
        $data['st_id']     = $this->session->userdata('user_id');
        $data['module_id'] = $module_id;
        $this->db->insert('tbl_student_answer', $data);
        $this->session->unset_userdata('data', $ans_array);
        echo 6;
    } //end save_student_answer()
} //end class
