<?php

defined('BASEPATH') or exit('No direct script access allowed');

class WhiteBoard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        /*$user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        if ($user_id != null && $user_type != null) {
            redirect('dashboard');
        }*/
    }
    public function test()
    {
        $this->load->view('test');
    }
    public $tpl_file = 'std_whiteboard_answer';//'incl_whiteboard'; //template file
    public $page_title = ''; //page title
    public $menu = ''; //menu of the page
    public $onload = ''; // onload event of the page
    public $loginmsg = ''; //message shows who is log in
    public $loginhomemsg = ''; //message shows on home page
    public $classname = ''; //name of the class
    public $incform = 'std_whiteboard_answer';//''; //name of the include form
    public $main_incform = 'std_whiteboard_answer';//'incl_my_task'; //name of the main include form
    public $title = 'Q-Study - Student Area'; //title of the page
    public $pagebutton = '';
    public $accorMenu = '';
    public $statusbar = '';
    public $subtitle = 'Student Area';
    public $formname = 'f_my_exam';
    public $accord = '';
    public $model = 'quest_ans_student';
    public $keyfield = '';
    public $models = array('user_account', 'quiz_category', 'quiz_typem', 'equations', 'equations_details', 'whiteboard', 'quiz_answer_choice',
        'quiz_multiple_choice', 'quiz_answer', 'quiz_matching', 'quiz_assign_score', 'vocabulary', 'question_answer', 'quiz_module_files',
        'sms_settings', 'sms_list', 'quiz_module', 'dialogue', 'color_box');
    public $models_load = '';
    public $defaultView = 'showExamBoardToolsNew()';//'showExamBoardTools()';
    public $searchForm = '';
    public $backaction = '';
    public $newdataUrl = '';
    public $edittitle = '';
    public $editView = '';
    public $insertView = '';
    public $showVersion = false; //show the version
    public $authenticated = true; //if controller require authentication
    public $nav_bar = ''; //navigation menu
    public $accordion_menu = ''; //accordion menu
    public $user_photo = ''; //
    public $slider = ''; //slider function
    public $requireImage = false; //form is multipart/form-data
    public $chckUrl = '';
    public $validation = ''; //form validation method
    public $edituri = ''; //uri for edit
    public $inserturi = ''; //uri for insert
    public $errorView = ''; //view on error occur
    public $modaldata = ''; //view data as modal window
    public $img = ''; //image for the title
    public $lookupType = 0; //type of lookup
    public $displayLookup = ''; //for displaying lookup
    public $default = ''; //display grid or data view
    public $requireAddonLine = ''; //if page require inline add
    public $AddonLines = array(); //lines for inline add
    public $AddonLinesType = array(); //line type for inline add
    public $AddonLinesClass = array(); //line type for inline add
    public $save_method = ''; //special save rather then model's save method
    public $showview = ''; //for showing only for other user
    public $showincform = ''; //name of the include form for showview
    public $backactionOther = ''; //back url for other then normal controller
    public $visibleCoursePane = ''; //is course pane visible
    public $backurl = ''; //back page url
    public $content_class = 'content_parent'; //class for the content div
    public $title_class = 'page_title_overall'; //class for the title div
    public $css_script = ''; //script for css class of the page
    public $showcustomize = false;
    public $pagebutton_on_model = false;
    public $redirect_on_uri = false; //redirect to backaction through uri
    public $redirect_uri_part = ''; //redirect to backaction based on uri data
    public $one_record = false; //only for one record
    public $show_like_edit = ''; //show and edit view are same
    public $signin_pages = false; //page for the sign in
    public $footer_class = 'footer'; //class for the footer
    public $showTitleAsText = false; //show the title as per title property
    public $titleAsMethod = 'show_main_title()'; //show the title as per method
    public $inserttitle = ''; //show insert mode title
    public $accordmenu_in_model = false; //get the accordmenu from model
    public $pageTitleAsMethod = true; //set page title by method of model
    public $pageTitleFromMethod = 'show_page_title()'; //get page title from method of model

    function My_exam()
    {
        parent::MY_Controller();
    }

    
    function searchTasks()
    {
        $subject = $this->input->post('subject');
        $chapter = $this->input->post('chapter');
        $host = $this->input->post('host');
        $task = $this->input->post('task');
        if (!$subject) {
            $search = $this->quest_ans_student->search_all_task_evestudy($task, $host);
        } else {
            $search = $this->quest_ans_student->search_all_tasks_evestudy($subject, $chapter, $task, $host);
        }
        $array = array('result' => 't', 'response' => $search['html'], 'stmt' => $search['stmt']);
        echo json_encode($array);
    }
    
    function showNextQuestion()
    {
        $tutor = $this->input->post('tutor');
        $task = $this->input->post('task');
        $module = $this->input->post('module');
        $question = $this->input->post('question');
        $prevquestion = $this->input->post('prevquestion');
        $answer = $this->input->post('answer');
        $soundplayed = $this->input->post('soundplayed');
        $category = $this->input->post('category');
        $workout = $this->input->post('workout');
        $workout_draw = $this->input->post('workout_draw');
        
        setSessionVar('task', $task);
        setSessionVar('module', $module);
        setSessionVar('question', $question);
        setSessionVar('prevquestion', $prevquestion);
        //$this->quest_ans_student->showExamBoardTools($question);
        setSessionVar('question_answer', 't');
        $all = getSessionVar('alltasks');
        if (!$all) {
            $alink = '/my-exam/' . $tutor . '/' . $task . '/' . $module . '/' . $question . '/' . $prevquestion;
        } else {
            $alink = '/all-tasks/' . $tutor . '/' . $task . '/' . $module . '/' . $question . '/' . $prevquestion;
        }

        //set student answer
        $this->quest_ans_student->module_id = $module;
        $this->quest_ans_student->rs_host_id = $tutor;
        $this->quest_ans_student->question_id = $prevquestion;
        $this->quest_ans_student->answer = $answer;
        $this->quest_ans_student->soundplayed = $soundplayed;
        $this->quest_ans_student->quiz_cate = $category;
        $this->quest_ans_student->workout = $workout;
        $this->quest_ans_student->workout_draw = $workout_draw;
        if ($category != '9') {
            //now save the data
            $save = $this->quest_ans_student->save();

            if ($save['return']) {
                $array = array('result' => 't', 'redirect_url' => $alink, 'error' => '',
                    'answer' => $save['right'], 'solution' => $save['solution']);
            } else {
                $array = array('result' => 'f', 'redirect_url' => '', 'error' => $this->quest_ans_student->error_msg,
                    'answer' => $save['right'], 'solution' => $save['solution']);
            }
        } else {
            $scores = $this->quest_ans_student->getScoreValueFromId();
            $i = 0;
            foreach ($scores as $value) {
                //for ($i = 0; $i <= count($scores); $i++) {
                //$this->quest_ans_student->answer_orig_marks[$i] = $scores[$i];
                $saveassignment = $this->quest_ans_student->saveAssignment($i);
                if ($saveassignment['return']) {
                    $array = array('result' => 't', 'redirect_url' => $alink, 'error' => '',
                        'answer' => $saveassignment['right'], 'solution' => $saveassignment['solution']);
                } else {
                    $array = array('result' => 'f', 'redirect_url' => '', 'error' => $this->quest_ans_student->error_msg,
                        'answer' => $saveassignment['right'], 'solution' => $saveassignment['solution']);
                }
                $i++;
            }
        }
        echo json_encode($array);
    }

    function rsneednt_answer_next_url()
    {
        
        $tutor = $this->input->post('tutor');
        $task = $this->input->post('task');
        $module = $this->input->post('module');
        $question = $this->input->post('question');
        $prevquestion = $this->input->post('prevquestion');
        $category = $this->input->post('category');
//        setSessionVar('task', $task);
//        setSessionVar('module', $module);
//        setSessionVar('question', $question);
//        setSessionVar('prevquestion', $prevquestion);
        //$this->quest_ans_student->showExamBoardTools($question);
       // setSessionVar('question_answer', 't');
//        $skip = $this->input->post('skip');
//        setSessionVar('skip', 't');
        
                setSessionVar('question_answer', 't');
        $all = getSessionVar('alltasks');
        if (!$all) {
            $alink = '/my-exam/' . $tutor . '/' . $task . '/' . $module . '/' . $question . '/' . $prevquestion;
        } else {
            $alink = '/all-tasks/' . $tutor . '/' . $task . '/' . $module . '/' . $question . '/' . $prevquestion;
        }
        
        
        
        if ($category==9) {
            $stmt = $this->db->query("select ANSWER FROM QUESTION_ANSWER where ID='".$prevquestion."'");
            $r = $stmt->row();
            $answers = $r->ANSWER;
            $rslt = array();
            if ($answers!='') {
                $rslt = array('results'=>'f','redirect_url'=>'');
            } else {
                $rslt = array('results'=>'t','redirect_url'=>$alink);
            }
        } else {
            $rslt = array('results'=>'f','redirect_url'=>'');
        }
        echo json_encode($rslt);
        exit;
    }
    
    
    
    function showSelectedQuestion()
    {
        $host = $this->input->post('host');
        $task = $this->input->post('task');
        $module = $this->input->post('module');
        $question = $this->input->post('question');
        $prevquestion = $this->input->post('prevquestion');
        $skip = $this->input->post('skip');
        setSessionVar('skip', 't');
        //$show_question = $this->quest_ans_student->showExamBoardTools($question);
        $all = getSessionVar('alltasks');
        if (!$all) {
            $alink = '/my-exam/' . $host . '/' . $task . '/' . $module . '/' . $question . '/' . $prevquestion;
        } else {
            $alink = '/all-tasks/' . $host . '/' . $task . '/' . $module . '/' . $question . '/' . $prevquestion;
        }
        $array = array('result' => 't', 'redirect_url' => $alink, 'error' => '');
        echo json_encode($array);
    }

    function getQuestionCategory()
    {
        $question = $this->input->post('question');
        $cate = $this->quest_ans_student->getQuestionQuizCategory($question);
        if ($cate) {
            $array = array('result' => 't', 'response' => $cate, 'error' => '');
        } else {
            $array = array('result' => 'f', 'response' => '', 'error' => '');
        }
        echo json_encode($array);
    }

    function getModuleImages()
    {
        $module = $this->input->post('module');
        $img = $this->quiz_module_files->getModuleImages($module);
        if ($img) {
            $array = array('result' => 't', 'response' => $img, 'error' => '');
        } else {
            $array = array('result' => 'f', 'response' => '', 'error' => '');
        }
        echo json_encode($array);
    }

    function showCompleteQuestion()
    {
        $tutor = $this->input->post('tutor');
        $task = $this->input->post('task');
        $module = $this->input->post('module');
        $question = $this->input->post('question');
        $prevquestion = $this->input->post('prevquestion');
        $answer = $this->input->post('answer');
        $soundplayed = $this->input->post('soundplayed');
        $category = $this->input->post('category');
        $workout = $this->input->post('workout');
        $workout_draw = $this->input->post('workout_draw');
        $end_time = $this->input->post('end_time');
        setSessionVar('end_time', $end_time);

        setSessionVar('tutor', $tutor);
        setSessionVar('task', $task);
        setSessionVar('module', $module);
        setSessionVar('question', $question);
        setSessionVar('prevquestion', $prevquestion);
        //$this->quest_ans_student->showExamBoardTools($question);
        setSessionVar('question_answer', 't');
        $alink = '/answer-complete/' . $tutor . '/' . $task . '/' . $module;

        //set student answer
        $this->quest_ans_student->rs_host_id = $tutor;
        $this->quest_ans_student->module_id = $module;
        $this->quest_ans_student->question_id = $question;
        $this->quest_ans_student->answer = $answer;
        $this->quest_ans_student->soundplayed = $soundplayed;
        $this->quest_ans_student->quiz_cate = $category;
        $this->quest_ans_student->workout = $workout;
        $this->quest_ans_student->workout_draw = $workout_draw;
        //check whether sms settings has data
        $sms_settings = $this->sms_settings->hasSettings();
        $mobile_not_equal = $this->user_account->IsSenderMobileNotEqualTo();
        if ($sms_settings) {//sms settings has data
            if ($mobile_not_equal) {//sender and to mobile not same
                if ($category != '9') {
                    //now save the data
                    $save = $this->quest_ans_student->save();

                    if ($save['return']) {
                        $array = array('result' => 't', 'redirect_url' => $alink, 'error' => '',
                            'answer' => $save['right'], 'solution' => $save['solution']);
                    } else {
                        $array = array('result' => 'f', 'redirect_url' => '', 'error' => $this->quest_ans_student->error_msg,
                            'answer' => $save['right'], 'solution' => $save['solution']);
                    }
                } else {
                    $scores = $this->quest_ans_student->getScoreValueFromId();
                    $i = 0;
                    foreach ($scores as $value) {
                        //for ($i = 0; $i <= count($scores); $i++) {
                        //$this->quest_ans_student->answer_orig_marks[$i] = $scores[$i];
                        $saveassignment = $this->quest_ans_student->saveAssignment($i);
                        if ($saveassignment['return']) {
                            $array = array('result' => 't', 'redirect_url' => $alink, 'error' => '',
                                'answer' => $saveassignment['right'], 'solution' => $saveassignment['solution']);
                        } else {
                            $array = array('result' => 'f', 'redirect_url' => '', 'error' => $this->quest_ans_student->error_msg,
                                'answer' => $saveassignment['right'], 'solution' => $saveassignment['solution']);
                        }
                        $i++;
                    }
                }
            } else {//sender and to mobile equal or some mandatory data is missing
                $array = array('result' => 'f', 'redirect_url' => '', 'error' => $this->user_account->error_msg,
                    'answer' => '', 'solution' => '');
            }
        } else {//sms settings does not have any data
            $array = array('result' => 'f', 'redirect_url' => '', 'error' => $this->sms_settings->error_msg,
                'answer' => '', 'solution' => '');
        }
        echo json_encode($array);
    }
    function isWeekend_for_repeat($start_date, $end_date, $expected_days)
    {
        $start_timestamp = strtotime($start_date);
        $end_timestamp   = strtotime($end_date);
        $dates = array();
        while ($start_timestamp <= $end_timestamp) {
            if (in_array(date('l', $start_timestamp), $expected_days)) {
                $dates[] = date('Y-m-d', $start_timestamp);
            }
            $start_timestamp = strtotime('+1 day', $start_timestamp);
        }
        return $dates;
    }
    function ErrorRevision()
    {
        $host = $this->input->post('host');
        $task = $this->input->post('task');
        $module = $this->input->post('module');
        $question = $this->input->post('question');
        $revision = $this->input->post('revision');
        $answer = $this->input->post('answer');
        $soundplayed = $this->input->post('soundplayed');
        $category = $this->input->post('category');
        $workout = $this->input->post('workout');
        $workout_draw = $this->input->post('workout_draw');
    /*  $cur_date = date('Y-m-d');
        $startDate = date('Y-m-d',strtotime('+1 day', strtotime($cur_date)));
        //$EndDate = date('Y-m-d',strtotime('+1 year', strtotime($cur_date)));
        $EndDate = date('Y').'-12-31';
        $nxtDay = date('l',strtotime('+1 day', strtotime($cur_date)));
        $nxtDay2 = date('l',strtotime('+2 day', strtotime($cur_date)));
        $expected_days= array($nxtDay,$nxtDay2);

        $repeated_dates = $this->isWeekend_for_repeat($startDate, $EndDate, $expected_days);
        $repeated_dateList = implode(',',$repeated_dates); */
        $user_id = getSessionVar('userid');
        /*
        if($revision==4){
             $this->db->query("insert into QUESTION_REPEATED (HOST_ID,USER_ID,MODULE_ID,QUESTION_ID,STATUS,ANSWER_DEFAULT_DATE,REPEATED_DATES) values (".$host.",".$user_id.",".$module.",".$question.",0,'".$cur_date."','".$repeated_dateList."')");

        }
        */
        setSessionVar('task', $task);
        setSessionVar('module', $module);
        setSessionVar('question', $question);
        //setSessionVar('prevquestion', $prevquestion);
        //$this->quest_ans_student->showExamBoardTools($question);
        setSessionVar('question_answer', 't');
        $alink = '/answer-complete/' . $host . '/' . $task . '/' . $module;

        //set student answer
        $this->quest_ans_student->module_id = $module;
        $this->quest_ans_student->rs_host_id = $host;
        $this->quest_ans_student->question_id = $question;
        $this->quest_ans_student->answer = $answer;
        $this->quest_ans_student->soundplayed = $soundplayed;
        $this->quest_ans_student->quiz_cate = $category;
        $this->quest_ans_student->error_rev = $revision;
        $this->quest_ans_student->workout = $workout;
        $this->quest_ans_student->workout_draw = $workout_draw;
        if ($category != '9') {
            //now save the data
            $save = $this->quest_ans_student->saveError();

            if ($save['return']) {
                $array = array('result' => 't', 'redirect_url' => $alink, 'error' => '',
                    'answer' => $save['right'], 'solution' => $save['solution']);
            } else {
                $array = array('result' => 'f', 'redirect_url' => '', 'error' => $this->quest_ans_student->error_msg,
                    'answer' => $save['right'], 'solution' => $save['solution']);
            }
        } else {
            $scores = $this->quest_ans_student->getScoreValueFromId();
            $i = 0;
            foreach ($scores as $value) {
                //for ($i = 0; $i <= count($scores); $i++) {
                //$this->quest_ans_student->answer_orig_marks[$i] = $scores[$i];
                $saveassignment = $this->quest_ans_student->saveAssignmentError($i);
                if ($saveassignment['return']) {
                    $array = array('result' => 't', 'redirect_url' => $alink, 'error' => '',
                        'answer' => $saveassignment['right'], 'solution' => $saveassignment['solution']);
                } else {
                    $array = array('result' => 'f', 'redirect_url' => '', 'error' => $this->quest_ans_student->error_msg,
                        'answer' => $saveassignment['right'], 'solution' => $saveassignment['solution']);
                }
                $i++;
            }
        }
        echo json_encode($array);
    }

    function setStartTime()
    {
        $hour = $this->input->post('hours');
        $min = $this->input->post('mins');
        $sec = $this->input->post('secs');
        $time = $hour . ':' . $min . ':' . $sec;
        if ($time) {
            $stime = getSessionVar('start_time');
            if (!$stime) {
                setSessionVar('start_time', $time);
            }
            $array = array('result' => 't', 'error' => '', 'time' => $time);
        } else {
            $array = array('result' => 'f', 'error' => '', 'time' => '');
        }
        echo json_encode($array);
    }

    function draw_whiteboard()
    {
        
        $draw = $this->quest_ans_student->getDrawWhiteboard();
        if ($draw) {
            $array = array('result' => 't', 'error' => '', 'response' => $draw);
        } else {
            $array = array('result' => 't', 'error' => $draw, 'response' => '');
        }
        
        echo json_encode($array);
    }
    
    function savetoimg()
    {
        $img = $this->input->post('image');
        $id = $this->input->post('id');
        $user = getSessionVar('userid');
        list($type, $img) = explode(';', $img);
        list(, $img)      = explode(',', $img);
        $data = base64_decode($img);

        $afile = 'files/image/' . $user . '_' . $id . '.png';
        file_put_contents('./' . $afile, $data);
        $array = array('result' => 't', 'error' => '', 'response' => $this->config->item('base_url') . $afile);
        echo json_encode($array);
    }

    function getImageName()
    {
        $img = $this->input->post('image');
        $id = $this->input->post('id');
        $user = getSessionVar('userid');
        list($type, $img) = explode(';', $img);
        list(, $img)      = explode(',', $img);
        $data = base64_decode($img);

        $afile = 'files/image/' . $user . '_' . $id . '.png';
        file_put_contents('./' . $afile, $data);
        $array = array('result' => 't', 'error' => '', 'response' => $this->config->item('base_url') . $afile);
        echo json_encode($array);
    }
    
    function get_qdesc()
    {
        $Qid = $this->input->post('qid');
        $stmt = "SELECT QUESTION_DESC FROM QUESTION_ANSWER WHERE ID='".$Qid."'";
        
        $dbh = ibase_connect($this->config->item('DB_DATABASE'), $this->config->item('DB_USER'), $this->config->item('DB_PASS'));
        // Get result
        $result = ibase_query($dbh, $stmt);
        // Loop through results
        while ($row = ibase_fetch_object($result, IBASE_TEXT)) {
            $rsqdesc = $row->QUESTION_DESC;
        }
        ibase_free_result($result);
        ibase_close($dbh);
        
        if ($rsqdesc) {
            $rsqdesc = $rsqdesc;
        } else {
            $rsqdesc = '';
        }
        echo $rsqdesc;
        exit;
    }
    
    function showsubQuestion_item()
    {
        $qid = $this->input->post('qid');
        $mids = $this->input->post('mids');
                //load module for getting modules_name
        $load_module = $this->quiz_module->load($mids);
        if ($load_module) {
            $modules_name = 'Module Name: ' . $this->quiz_module->modules_name;
        } else {
            $modules_name = '';
        }
        $stmt = "SELECT * FROM QUIZ_ASSIGN_SCORE WHERE QUIZ_ANSWER_ID='".$qid."'";
        
        $r = $this->db->query($stmt);
        //new
        $html = '';
            $html .= '<div class="scorepanels">';
            $html .= '<table class="table rsscroreboard">';
            $html .= '<tr>';
            $html .= '<td class="table_header tpheaders" colspan="5" style="text-align:right">Score &nbsp;<span class="rshide_des rsclse_scores"><img class="forward_enabled" src="/images/forward_enabled.png" alt="no-images"/></span></td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="table_header mrsnames" colspan="5">' . @$modules_name . '</td>';
            $html .= '</tr>';
            $html .= '<tr class="rsnewheadings">';
            $html .= '<td class="table_header" style="width:25px"></td>';
            $html .= '<td class="table_header" style="width:30px">SL</td>';
            $html .= '<td class="table_header">Description</td>';
            $html .= '<td class="table_header">Mark</td>';
            $html .= '<td class="table_header">Obtained</td>';
            $html .= '</tr>';
           
            $i = 0;
            $total = 0;
        foreach ($r->result() as $qw) {
            $frac_check = $this->quest_ans_student->convert_decimal_to_fraction($qw->ASSIGN_SCORE);
            if (strpos($frac_check, '/') !== false) {
                $rslts = explode('/', $frac_check);
                $scnd_main = $rslts[0];
                $scnd_top = $rslts[1];
                $scnd_bottom = $rslts[2];
                $qscore_is = '<table class="rsscondryTable std_score">';
                $qscore_is .='<tbody>';
                $qscore_is .='<tr>';
                $qscore_is .='<td rowspan="2" class="scandry_mainval std_score_main">';
                $qscore_is .=$scnd_main;
                $qscore_is .='</td>';
                $qscore_is .='<td style="border-bottom:solid 1px" class="scndry_top ">';
                $qscore_is .=$scnd_top;
                $qscore_is .='</td>';
                $qscore_is .='</tr>';
                $qscore_is .='<tr>';
                $qscore_is .='<td class="scndry_bottom">';
                $qscore_is .= $scnd_bottom;
                $qscore_is .='</td>';
                $qscore_is .='</tr>';
                $qscore_is .='</tbody>';
                $qscore_is .='</table>';
            } else {
                $qscore_is = $qw->ASSIGN_SCORE;
            }
                    $html .='<tr class="valuesheadings">';
            
                    $html .='<td class="table_row"></td>';
                    $html .='<td class="table_row">'.$qw->ASSIGN_SLNO.'</td>';
            if ($qw->ASSIGN_DESCRIPTION!='') {
                $html .='<td class="table_row" style="text-align:center"><img class="scroring_note" data_scrore_desc="'.$qw->ASSIGN_DESCRIPTION.'" src="/images/notepad1.png" alt="no-image"/></td>';
            } else {
                $html .='<td class="table_row"></td>';
            }
                    $html .= '<td class="table_row">'.$qscore_is.'</td>';
                    $html .= '<td class="table_row"></td>';
                    $html .='</tr>';
                
                    $total +=$qw->ASSIGN_SCORE;
            
                    $i++;
        }
            $html .= '<tr>';
            $html .= '<td class="table_row tmarktext" colspan="3" style="text-align:center">Total marks:</td><td class="table_row tmarkvalues">' . $total . '</td><td class="table_row"></td>';
            $html .= '</tr>';
            $html .= '</table>';
            
            $html .= '</div>';
        echo $html;
        exit;
    }
}
