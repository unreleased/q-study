<?php
/**
* render properties as table data
* @param  array $properties all properties
* @return string $row            rendered string
*/
public function renderProperties($properties)
{
    $row = '';
    $cnt = 1;
    foreach ($properties as $prop) {
        $row .= '<tr>';
        $row .= '<td>'. $cnt++ . '</td>';
        $row .= '<td>'. $prop['property_status'] . '</td>';
        $row .= '<td>'.'$'.number_format($prop['price']) . '</td>';
        $row .= '<td>'. $prop['city']. ',' .$prop['street']. '</td>';
        $row .= '<td>'. $prop['year_built'] . '</td>';
        $row .= '<td>'.base_url().$prop['short_url'] . '</td>';
        $row .= '<td>';

        $row .='<a href="'.base_url().$prop['long_url'].'"><i style="padding-right:5px" data-toggle="tooltip" title="view" data-placement="top" class="fa fa-eye"></i></a>';

        $row .='<a href="edit_listing/'.$prop['id'].'"><i style="padding-right:5px" data-toggle="tooltip" title="edit" class="fa fa-pencil"></i></a>';

        if (!$prop['suspension_status']) {
            $row .='<a  href="suspend_listing/'.$prop['id'].'"><i style="padding-right:5px" data-toggle="tooltip" title="suspend" class="fa fa-pause-circle-o"></i></a>';
        } else {
            $row .='<a  style="color:red" href="unsuspend_listing/'.$prop['id'].'"><i style="padding-right:5px" data-toggle="tooltip" title="unsuspend" class="fa fa-play-circle-o"></i></a>';
        }

        $row .='<a href="delete_listing/'.$prop['id'].'"><i style="padding-right:5px" data-toggle="tooltip" title="delete" class="fa fa-trash"></i></a>';
        $row .= '</td>';
    }
    return $row;
}
