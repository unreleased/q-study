<?php


class School extends CI_Controller{
    
    public function index() {
        $data['header'] = $this->load->view('common/header', '', true);
        $data['header_sign_up'] = $this->load->view('common/header_sign_up', '', true);
        $data['footer'] = $this->load->view('common/footer', '', true);
        
        $this->load->view('school/school_dashboard', $data);
    }
}
