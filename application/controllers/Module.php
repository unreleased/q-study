<?php
/**
 * Module controller class
 */
class Module extends CI_Controller
{

    public $loggedUserId, $loggedUserType;


    public function __construct()
    {
        parent::__construct();

        $user_id              = $this->session->userdata('user_id');
        $user_type            = $this->session->userdata('userType');
        $this->loggedUserId   = $user_id;
        $this->loggedUserType = $user_type;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }

        $this->load->model('Parent_model');
        $this->load->model('tutor_model');
        $this->load->model('Student_model');
        $this->load->model('ModuleModel');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('Preview_model');
        $this->load->model('QuestionModel');
        $this->load->helper('CommonMethods');
    }//end __construct()


    public function view_course()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['maincontent'] = $this->load->view('module/view_course', $data, true);
        $this->load->view('master_dashboard', $data);
    }//end view_course()


    public function all_module()
    {

        $user_id = $this->session->userdata('user_id');
        $data['user_info']          = $this->tutor_model->userInfo($user_id);
        $data['all_module']         = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        

        $data['all_grade']          = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type']    = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course']         = $this->tutor_model->getAllInfo('tbl_course');
        $data['allRenderedModType'] = $this->renderAllModuleType();
        $data['all_country']        = $this->renderAllCountry();

        $studentIds          = $this->tutor_model->allStudents(['sct_id' => $user_id]);
        $data['allStudents'] = $this->renderStudentIds($studentIds);

        $data['maincontent'] = $this->load->view('module/all_module', $data, true);
        $this->load->view('master_dashboard', $data);
    }//end all_module()


    /**
     * Add module (view part)
     *
     * @return void
     */
    public function add_module()
    {
        $user_id                = $this->session->userdata('user_id');
        $data['loggedUserType'] = $this->loggedUserType;
        $data['user_info']      = $this->tutor_model->userInfo($user_id);

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        


        $data['all_module']        = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_module_type']   = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course']        = $this->tutor_model->getAllInfo('tbl_course');
        $data['all_country']       = $this->renderAllCountry();
        $data['all_subjects']      = $this->renderAllSubject();
        $data['all_chapters']      = $this->renderAllChapter();
        $data['all_module_type']   = $this->renderAllModuleType();
        $data['all_question_type'] = $this->tutor_model->getAllInfo('tbl_questiontype');
        foreach ($data['all_question_type'] as $row) {
            $question_list[$row['id']] = $this->tutor_model->getUserQuestion('tbl_question', $row['id'], $user_id);
        }

        $data['all_question'] = $question_list;
        $studentIds           = $this->tutor_model->allStudents(['sct_id' => $user_id]);
        $data['allStudents']  = $this->renderStudentIds($studentIds);
        $data['maincontent']  = $this->load->view('module/add_module', $data, true);
        $this->load->view('master_dashboard', $data);
    }//end add_module()

    /**
     * Tutor can set module repetition days(works while module edit)
     *
     * @param integer $moduleId module id
     *
     * @return void
     */
    public function setRepetitionDays($moduleId)
    {
        $post=$this->input->post();
        
        $module = $this->ModuleModel->moduleInfo($moduleId);
        if (!sizeof($module)) {
            $this->session->set_flashdata('error_msg', 'Module not exists.');
            redirect('all-module');
        }

        $user_id                = $this->session->userdata('user_id');
        $data['loggedUserType'] = $this->loggedUserType;
        $data['user_info']      = $this->tutor_model->userInfo($user_id);
        $data['module_info']    = $module;
 
        if (!$post) {
            $sl_date = json_decode($module['repetition_days']);
            $data['selectedSl'] = [];
            $sl_date = count($sl_date) ? $sl_date: [];
            foreach ($sl_date as $item) {
                $temp = explode('_', $item);
                $data['selectedSl'][] = $temp[0];
            }
            
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['header']     = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
            $data['maincontent']  = $this->load->view('module/set_module_repetition_days', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            $dataToUpdate[] = [
                'id'              =>$moduleId,
                'repetition_days' => json_encode($post['sl_date'])
            ];

            $this->ModuleModel->update('tbl_module', $dataToUpdate, 'id');
            $this->session->set_flashdata('success_msg', 'Repetition days added successfully');
            redirect('module/repetition/'.$moduleId);
        }
    }

    public function getStudentByGradeCountry()
    {
        $student_grade = $this->input->post('studentGrade');
        $country_id = $this->input->post('country_id');
        $user_id = $this->session->userdata('user_id');
        
        $students = $this->ModuleModel->getStudentByGradeCountry($student_grade, $country_id, $user_id);
        foreach ($students as $row) {
            echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
        }
    }


    /**
     * Responsible for saving module data.
     *
     * @return void
     */
    public function saveModuleQuestion()
    {

        $post              = $this->input->post();
        
        $video_link = str_replace('</p>', '', $_POST['video_link']);
        $video_array = array_filter(explode('<p>', $video_link));

        $clean             = $this->security->xss_clean($post);
        $optionalTime      = explode(':', $clean['optTime']);
        $optionalHour      = isset($optionalTime[0]) ? $optionalTime[0]*60*60 : 0; //second
        $optionalMinute    = isset($optionalTime[1]) ? $optionalTime[1]*60    : 0; //second
        
        $moduleTableData   = [];
        $moduleTableData[] = [
        'moduleName'        => $clean['moduleName'],
        'trackerName'       => $clean['trackerName'],
        'individualName'    => $clean['individualName'],
        'isSMS'             => isset($clean['isSMS']) ? $clean['isSMS'] : 0,
        'isAllStudent'      => isset($clean['isAllStudent']) ? $clean['isAllStudent'] : 0,
        'individualStudent' => isset($clean['individualStudent']) ? json_encode($clean['individualStudent']) : '',
        'video_link'        => json_encode($video_array),
        'subject'           => $clean['subject'],
        'chapter'           => $clean['chapter'],
        'country'           => $clean['country'],
        'studentGrade'      => $clean['studentGrade'],
        'moduleType'        => $clean['moduleType'],
        'user_id'           => $this->loggedUserId,
        'user_type'         => $this->loggedUserType,
        'exam_date'         => isset($clean['dateCreated']) ? strtotime($clean['dateCreated']) : 0,
        'exam_start'        => isset($clean['startTime']) ? ($clean['startTime']) : 0,
        'exam_end'          => isset($clean['endTime']) ? ($clean['endTime']) : 0,
        'optionalTime'      => $optionalHour+$optionalMinute,
        ];
        // Save module info first
        $moduleId = $this->ModuleModel->insert('tbl_module', $moduleTableData);

        // If ques order set record those to tbl_modulequestion table
        $arr   = [];
        $items = isset($clean['qId_ordr']) ? array_filter($clean['qId_ordr']) : [];
        if (count($items)) {
            foreach ($items as $qId_ordr) {
                $temp  = explode('_', $qId_ordr);
                $question_info = $this->ModuleModel->getInfo('tbl_question', 'id', $temp[0]);
                $arr[] = [
                'question_id'    => $temp[0],
                'question_type'  => $question_info[0]['questionType'],
                'module_id'      => $moduleId,
                'question_order' => $temp[1],
                'created'        => time(),
                ];
            }

            $this->ModuleModel->insert('tbl_modulequestion', $arr);
        }

        
        if ($moduleId) {
            echo $moduleId;
            // Module recorded.
        } else {
            echo 'false';
            // Module record failed.
        }
    }//end saveModuleQuestion()


    /**
     * This method will duplicate  a module with additional info given
     *
     * @return void
     */
    public function moduleDuplicate()
    {
        $post   = $this->input->post();
        $newMod = $this->security->xss_clean($post);

        $origModId  = $newMod['origModId'];
        $origMod    = $this->ModuleModel->moduleInfo($origModId);
        $newModName = isset($newMod['moduleName']) ? $newMod['moduleName'] : '';
        //if country name or student grade changed only then same module name permissible
        if ($newModName == $origMod['moduleName'] && $origMod['country'] == $newMod['country'] && $origMod['studentGrade'] == $newMod['studentGrade']) {
            echo 'false';
            die;
        } else {
            $moduleTableData   = [];
            $moduleTableData[] = [
            'moduleName'        => $newMod['moduleName'],
            'trackerName'       => $origMod['trackerName'],
            'individualName'    => $origMod['individualName'],
            'isSMS'             => isset($newMod['isSMS']) ? $newMod['isSMS'] : 0,
            'isAllStudent'      => isset($newMod['isAllStudent']) ? $newMod['isAllStudent'] : 0,
            'individualStudent' => isset($newMod['individualStudent']) ? json_encode($newMod['individualStudent']) : $origMod['individualStudent'],
            'subject'           => $origMod['subject'],
            'chapter'           => $origMod['chapter'],
            'country'           => $newMod['country'],
            'studentGrade'      => $newMod['studentGrade'],
            'moduleType'        => $newMod['moduleType'],
            'user_id'           => $this->loggedUserId,
            'user_type'         => $this->loggedUserType,
            'exam_date'         => isset($newMod['dateCreated']) ? strtotime($newMod['dateCreated']) : time(),
            'exam_start'        => $origMod['exam_start'],
            'exam_end'          => $origMod['exam_end'],
            'optionalTime'      => $origMod['optionalTime'],
            ];
            // Save module info first
            $newModuleId = $this->ModuleModel->insert('tbl_module', $moduleTableData);
            $origModQues = $this->ModuleModel->moduleQuestion($origModId);
            $arr         = [];
            if (count($origModQues)) {
                foreach ($origModQues as $ques) {
                    $arr[] = [
                    'question_id'    => $ques['question_id'],
                    'module_id'      => $newModuleId,
                    'question_order' => $ques['question_order'],
                    'created'        => time(),
                    ];
                }

                $this->ModuleModel->insert('tbl_modulequestion', $arr);
            }

            echo 'true';
        }//end if
    }//end moduleDuplicate()

    
    public function editModule($moduleId)
    {
        $user_id                = $this->session->userdata('user_id');
        $data['loggedUserType'] = $this->loggedUserType;
        $data['user_info']      = $this->tutor_model->userInfo($user_id);

        $module = $this->ModuleModel->moduleInfo($moduleId);
        if (!sizeof($module)) {
            $this->session->set_flashdata('error_msg', 'Module not exists.');
            redirect('all-module');
        }

        $moduleQuestion = $this->ModuleModel->moduleQuestion($moduleId);
        $quesOrdrMap    = [];
        foreach ($moduleQuestion as $temp) {
            $quesOrdrMap[$temp['question_id']] = $temp['question_order'];
        }

        $data['qoMap']      = $quesOrdrMap;
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $user_id            = $this->session->userdata('user_id');

        $data['module_info']       = $module;
        $data['all_country']       = $this->renderAllCountry($module['country']);
        $data['all_subjects']      = $this->renderAllSubject($module['subject']);
        $data['all_chapters']      = $this->renderAllChapter($module['chapter']);
        $data['all_module_type']   = $this->renderAllModuleType($module['moduleType']);
        $data['all_question_type'] = $this->tutor_model->getAllInfo('tbl_questiontype');
        
        $optionalHour              = $module['optionalTime']>3600 ? sprintf('%02d', $module['optionalTime']/3600) : "00";
        $optionalMinute            = sprintf('%02d', ($module['optionalTime']/60) - ($optionalHour*60));
        $data['optionalTime']      = (string)$optionalHour.':'.$optionalMinute;

        foreach ($data['all_question_type'] as $row) {
            $question_list[$row['id']] = $this->tutor_model->getUserQuestion('tbl_question', $row['id'], $user_id);
        }

        $indivStdIds          = $module['individualStudent'];
        $data['all_question'] = $question_list;
        $studentIds           = $this->tutor_model->allStudents(['sct_id' => $user_id]);
        $data['allStudents']  = $this->renderStudentIds($studentIds, $indivStdIds);

        $data['maincontent'] = $this->load->view('module/edit_module', $data, true);
        $this->load->view('master_dashboard', $data);
    }//end editModule()


    public function updateRequestedModule()
    {
        $post              = $this->input->post();

        $optionalTime      = explode(':', isset($post['optTime'])?$post['optTime']:"0:0");
        
        $optionalHour      = isset($optionalTime[0]) ? $optionalTime[0]*60*60 : 0; //second
        $optionalMinute    = isset($optionalTime[1]) ? $optionalTime[1]*60    : 0; //second
        $video_link = str_replace('</p>', '', $_POST['video_link']);
        $video_array = array_filter(explode('<p>', $video_link));

        $clean             = $this->security->xss_clean($post);
        $moduleToUpdate    = $clean['moduleId'];
        $moduleTableData   = [];
        $moduleTableData[] = [
        'id'                => $moduleToUpdate,
        'moduleName'        => $clean['moduleName'],
        'trackerName'       => $clean['trackerName'],
        'individualName'    => $clean['individualName'],
        'isSMS'             => isset($clean['isSMS']) ? $clean['isSMS'] : 0,
        'video_link'        => json_encode($video_array),
        'isAllStudent'      => isset($clean['isAllStudent']) ? $clean['isAllStudent'] : 0,
        'individualStudent' => isset($clean['individualStudent']) ? json_encode($clean['individualStudent']) : '',
        'subject'           => $clean['subject'],
        'chapter'           => $clean['chapter'],
        'country'           => $clean['country'],
        'studentGrade'      => $clean['studentGrade'],
        'moduleType'        => $clean['moduleType'],
        'user_id'           => $this->loggedUserId,
        'user_type'         => $this->loggedUserType,
        'exam_date'         => isset($clean['dateCreated']) ? strtotime($clean['dateCreated']) : time(),
        'exam_start'        => isset($clean['startTime']) ? ($clean['startTime']) : 0,
        'exam_end'          => isset($clean['endTime']) ? ($clean['endTime']) : 0,
        'optionalTime'      => $optionalHour+$optionalMinute,

        ];
        // Update module info first
        $this->ModuleModel->update('tbl_module', $moduleTableData, 'id');
        //echo $this->db->last_query();
        // If ques order set, delete recorded module_question first,
        // then insert requested data to tbl_modulequestion table
        $arr   = [];
        $items = isset($clean['qId_ordr']) ? array_filter($clean['qId_ordr']) : [];
        if (count($items)) {
            $this->ModuleModel->deleteModuleQuestion($moduleToUpdate);
            foreach ($items as $qId_ordr) {
                $temp  = explode('_', $qId_ordr);
                $question_info = $this->ModuleModel->getInfo('tbl_question', 'id', $temp[0]);
                $arr[] = [
                'question_id'    => $temp[0],
                'question_type'  => $question_info[0]['questionType'],
                'module_id'      => $moduleToUpdate,
                'question_order' => $temp[1],
                'created'        => time(),
                ];
            }

            $this->ModuleModel->insert('tbl_modulequestion', $arr);
        }

        echo 'true';
    }//end updateRequestedModule()


    /**
     * Module reorder view part
     *
     * @return void
     */
    public function reorderModule()
    {
        $user_id                = $this->session->userdata('user_id');
        $data['loggedUserType'] = $this->loggedUserType;
        $data['user_info']      = $this->tutor_model->userInfo($user_id);

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id                    = $this->session->userdata('user_id');
        $data['user_info']          = $this->tutor_model->userInfo($user_id);
        $data['all_module']         = $this->ModuleModel->allModule();
        $data['all_grade']          = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type']    = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course']         = $this->tutor_model->getAllInfo('tbl_course');
        $data['allRenderedModType'] = $this->renderAllModuleType();
        $data['all_country']        = $this->renderAllCountry();
        $data['row']                = $this->renderReorderPageModule($data['all_module']);
        $studentIds                 = $this->tutor_model->allStudents(['sct_id' => $user_id]);
        $data['allStudents']        = $this->renderStudentIds($studentIds);

        $data['maincontent'] = $this->load->view('module/reorder_module', $data, true);
        $this->load->view('master_dashboard', $data);
    }//end reorderModule()


    /**
     * Module order save(save on ajax call)
     *
     * @return string
     */
    public function saveModuleOrdering()
    {
        $post  = $this->input->post();
        $clean = $this->security->xss_clean($post);

        $arr   = [];
        $items = isset($clean['modId_ordr']) ? array_filter($clean['modId_ordr']) : [];

        if (count($items)) {
            foreach ($items as $modId_ordr) {
                $temp  = explode('_', $modId_ordr);
                $arr[] = [
                'id'         => $temp[0],
                'ordering'   => $temp[1],
                'updated_at' => date('Y-m-d H:i:s'),
                ];
            }

            $this->ModuleModel->update('tbl_module', $arr, 'id');
            echo 'true';
        }
    }//end saveModuleOrdering()


    /**
     * Wrap all students name with option tag.
     *
     * @return string Students.
     */
    public function renderStudentIds($studentIds, $selectedIds = '')
    {
        $sel    = [];
        $stdIds = [];
        if (strlen($selectedIds) > 1) {
            $stdIds = json_decode($selectedIds);
        }

        $option  = '';
        $option .= '<option value="">--Student--</option>';
        foreach ($studentIds as $studentId) {
            $stInfo  = $this->Student_model->getInfo('tbl_useraccount', 'id', $studentId);
            $sel     = in_array($studentId, $stdIds) ? 'selected' : '';
            $option .= '<option value="'.$studentId.'" '.$sel.'>'.$stInfo[0]['name'].'</option>';
        }

        return $option;
    }//end renderStudentIds()


    /**
     * Wrap all Countries recorded in DB with option tag.
     *
     * @return string Countries.
     */
    public function renderAllCountry($selectedId = -1)
    {
        $option    = '';
        $option   .= '<option value="">--Country--</option>';
        $countries = $this->tutor_model->getAllInfo('tbl_country');
        foreach ($countries as $country) {
            $sel     = ($country['id'] == $selectedId) ? 'selected' : '';
            $option .= '<option value="'.$country['id'].'" '.$sel.'>'.$country['countryName'].'</option>';
        }

        return $option;
    }//end renderAllCountry()


    /**
     * Wrap all Subjects with option tag.
     *
     * @return string Users created subjects.
     */
    public function renderAllSubject($selectedId = -1)
    {
        $option   = '';
        $option  .= '<option value="">--Subject--</option>';
        $subjects = $this->tutor_model->getInfo('tbl_subject', 'created_by', $this->loggedUserId);
        foreach ($subjects as $subject) {
            $sel     = ($subject['subject_id'] == $selectedId) ? 'selected' : '';
            $option .= '<option value="'.$subject['subject_id'].'" '.$sel.'>'.$subject['subject_name'].'</option>';
        }

        return $option;
    }//end renderAllSubject()


    /**
     * Wrap all chapters with option tag.
     *
     * @return string Users created chapters.
     */
    public function renderAllChapter($selectedId = -1)
    {
        $option   = '';
        $option  .= '<option value="">--Chapter--</option>';
        $chapters = $this->tutor_model->getInfo('tbl_chapter', 'created_by', $this->loggedUserId);

        foreach ($chapters as $chapter) {
            $sel     = ($chapter['id'] == $selectedId) ? 'selected' : '';
            $option .= '<option value="'.$chapter['id'].'" '.$sel.'>'.$chapter['chapterName'].'</option>';
        }

        return $option;
    }//end renderAllChapter()


    /**
     * Wrap all Module types with option tag.
     *
     * @return string All module types recorded in database.
     */
    public function renderAllModuleType($selectedId = -1)
    {
        $option      = '';
        $option     .= '<option value="">--Moduletype--</option>';
        $moduleTypes = $this->ModuleModel->allModuleType();

        foreach ($moduleTypes as $moduleType) {
            $sel     = ($moduleType['id'] == $selectedId) ? 'selected' : '';
            $option .= '<option value="'.$moduleType['id'].'" '.$sel.'>'.$moduleType['module_type'].'</option>';
        }

        return $option;
    }//end renderAllModuleType()


    /**
     * Render all module for reorder page
     *
     * @param  array $modules all modules to reorder default empty array
     * @return string          processed table items
     */
    public function renderReorderPageModule($modules = [])
    {
        $row = '';
        $maxOrder = 0;
        foreach ($modules as $module) {
            $moduleOrder = ($module['ordering'])?$module['ordering']:"";
            $maxOrder = max($maxOrder, $moduleOrder);
            $checked = ($module['ordering'])?"checked":"";

            $row .= '<tr id="'.$module['id'].'">';
            $row .= '<td>'.date('d-M-Y', $module['exam_date']).'</td>';
            $row .= '<td id="modName">'.$module['moduleName'].'</td>';
            $row .= '<td>'.$module['moduleType'].'</td>';
            $row .= '<td>'.$module['subject_name'].'</td>';
            $row .= '<td><input type="checkbox" id="moduleChecked" '.$checked.'><input type="number" min="1" style="max-width: 54px;margin-left: 65px; border: 1px solid #4995b5;" autocomplete="off" class="moduleOrder" disabled="" value="'.$moduleOrder.'" id="modOrdr">';
            $row .= '<input type="hidden" id="modId_ordr" name="modId_ordr[]" value="">';
            $row .= '<input type="hidden" id="modId"  value="'.$module['id'].'">';
            $row .= '</td>';
            $row .= '<tr>';
        }
        $row .= '<input type="hidden" id="maxOrder" value="'.$maxOrder.'">';
        return $row;
    }//end renderReorderPageModule()

    public function moduleSearchFromReorderPage()
    {
        $post = $this->input->post();
        $post = array_filter($post);
        $post[' user_id'] = $this->loggedUserId;
        $modules = $this->ModuleModel->allModule($post);
        $html = $this->renderReorderPageModule($modules);
        echo count($modules)?$html:'No module found';
    }

    public function module_preview($modle_id, $question_order_id)
    {
        $data['user_info']       = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['question_info_s'] = $this->tutor_model->getModuleQuestion($modle_id, $question_order_id, null);
        
        $data['total_question'] = $this->tutor_model->getModuleQuestion($modle_id, null, 1);
        $data['page_title']     = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink']     = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']         = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink']     = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['quesOrder'] = $question_order_id;
        if (isset($data['question_info_s'][0]['questionType'])) {
            $quesInfo = json_decode($data['question_info_s'][0]['questionName']);
            
            if ($data['question_info_s'][0]['questionType'] == 1) {
                $data['maincontent'] = $this->load->view('module/preview/preview_general', $data, true);
            } elseif ($data['question_info_s'][0]['questionType'] == 2) {
                $data['maincontent'] = $this->load->view('module/preview/preview_true_false', $data, true);
            } elseif ($data['question_info_s'][0]['questionType'] == 3) {
                $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
                $data['maincontent']             = $this->load->view('module/preview/preview_vocabulary', $data, true);
            } elseif ($data['question_info_s'][0]['questionType'] == 4) {
                $data['question_info_vcabulary'] = $quesInfo;
                $data['maincontent']             = $this->load->view('module/preview/preview_multiple_choice', $data, true);
            } elseif ($data['question_info_s'][0]['question_type'] == 5) {
                $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
                $data['maincontent'] = $this->load->view('module/preview/preview_multiple_response', $data, true);
            } elseif ($data['question_info_s'][0]['questionType'] == 6) {
                // skip quiz
                $data['numOfRows']    = isset($quesInfo->numOfRows) ? $quesInfo->numOfRows : 0;
                $data['numOfCols']    = isset($quesInfo->numOfCols) ? $quesInfo->numOfCols : 0;
                $data['questionBody'] = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
                $data['questionId']   = $data['question_info_s'][0]['question_id'];
                $quesAnsItem          = $quesInfo->skp_quiz_box;
                $items                = indexQuesAns($quesAnsItem);

                $data['skp_box']     = renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols']);
                $data['maincontent'] = $this->load->view('module/preview/skip_quiz', $data, true);
            } elseif ($data['question_info_s'][0]['question_type'] == 7) {
                //
                $data['question_info_left_right'] = json_decode($data['question_info_s'][0]['questionName']);
                $data['maincontent'] = $this->load->view('module/preview/preview_matching', $data, true);
            } elseif ($data['question_info_s'][0]['questionType'] == 8) {
                // assignment
                $data['questionBody']    = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
                $items                   = $quesInfo->assignment_tasks;
                $data['totalItems']      = count($items);
                $data['assignment_list'] = renderAssignmentTasks($items);
                $data['maincontent']     = $this->load->view('module/preview/assignment', $data, true);
            }//end if
        } else {
            $data['maincontent']     = $this->load->view('module/preview/moduleWithoutQues', $data, true);
        } // no question to preview
        

        $this->load->view('master_dashboard', $data);
    }//end module_preview()


    public function deleteModule()
    {
        $post  = $this->input->post();
        $clean = $this->security->xss_clean($post);

        $moduleId = $clean['moduleId'];
        $this->ModuleModel->delete($moduleId);

        echo 'true';
    }//end deleteModule()


    /**
     * Render all searched question(check box checked result)
     * This function could be optimized more
     *
     * @param array $quesList all question by search params
     *
     * @return string           rendered string
     */
    public function quesSearch()
    {
        $post = array_filter($this->input->post());
        $post['user_id'] = $this->loggedUserId;
        $type = 'add';
        if (isset($post['reqType']) && $post['reqType']=='edit') {
            $moduleId      = $post['moduleId'] ?$post['moduleId'] : 0;
            $moduleQues = $this->ModuleModel->moduleQuestion($moduleId);
            $qOrdr = [];
            foreach ($moduleQues as $ques) {
                $qOrdr[$ques['question_id']] = $ques['question_order'];
            }
            $moduleQuesIds = array_keys($qOrdr);
            //as post used as search params
            unset($post['reqType'], $post['moduleId']);
            $type='edit';
        }
        $quesList = $this->QuestionModel->search('tbl_question', $post);
        
        //echo $this->renderSearchedQuestion($quesList);

        $serachedQuesIds = array_column($quesList, 'id');

        $allQuestionType = $this->tutor_model->getAllInfo('tbl_questiontype');
        $questionGroup = [];
        foreach ($allQuestionType as $questionType) {
            $questionGroup[$questionType['id']] = $this->tutor_model->getUserQuestion('tbl_question', $questionType['id'], $this->loggedUserId);
        }
        
        $row = '';
        $maxOrder = 0;
        foreach ($allQuestionType as $key) {
            $row .= '<div class="col-md-3"><table class="table table-bordered tbl_ques" id="module_setting2"><thead><tr>';
            $row .='<th style="">'.$key['questionType'].'<p style="float:right;">Re-Order</p></th></tr></thead><tbody>';
            $i=0;
            foreach ($questionGroup[$key['id']] as $question) {
                if (!in_array($question['id'], $serachedQuesIds)) {
                    continue;
                }
                
                $checked = '';
                $quesOrder='';
                $qId_ordr = '';
                if ($type=='edit' && in_array($question['id'], $moduleQuesIds)) {
                    $checked = 'checked';
                    $quesOrder = $qOrdr[$question['id']];
                    $qId_ordr = $question['id'].'_'.$quesOrder;
                    $maxOrder = max($maxOrder, $quesOrder);
                }

                $row .= '<tr><td><div class="form-check"><label class="form-check-label"><label class="form-check-label" for="defaultCheck21">';

                $row .= '<input class="form-check-input1" type="checkbox" value="'.$question['id'].'"  name="moduleQuestion[]" id="quesChecked" '.$checked.'>';
                $row .='<span>'.' Q'.$i.' </span>';

                $row .= '<input type="hidden" class="questionId" value="'.$question['id'].'">';
                $row .= '<input type="hidden" class="questionType" value="'.$key['questionType'].'">';
                $row .= '<i class="fa fa-info-circle quesInfoIcon" data-toggle="modal" data-target=".question-preview-modal" class="fa fa-info-circle" style="color:orange;"></i>';

                $row .= '<a style="display:inline !important" href="question_edit/'.$key['id'].'/'.$question['id'].'"><i class="fa fa-pencil"></i></a>';
                $row .= '<input type="number" min="1" style="max-width: 54px;margin-left: 65px;" autocomplete="off" class="questionOrder" disabled="disabled" value="'.$quesOrder.'" id="qOrdr"><input type="hidden" id="qId_ordr" name="qId_ordr[]" value="'.$qId_ordr.'">';
                $row .= '<input type="hidden" id="qId"  value="'.$question['id'].'">';

                $row .= '</label></div></td></tr>';
                $i++;
            }
            $row .='</tbody></table></div>';
        }
        $data['row'] = $row;
        $data['maxOrder'] = $maxOrder;
        echo json_encode($data);
        //echo  $row;
    }

    /**
     * Get all student of a course(ajax hit)
     *
     * Match course with subject picked,
     * get all student of that course,
     * render students,
     * return
     *
     * @return [type] [description]
     */
    public function getStudentByCourse()
    {
        $post = $this->input->post();

        $subjectId = $post['subjectId'];
        $subject = $this->ModuleModel->search('tbl_subject', ['subject_id'=>$subjectId]);
        if (count($subject)) {
            $course = $this->ModuleModel->search('tbl_course', ['courseName'=>$subject[0]['subject_name']]);
            $courseId = $course[0]['id'];
            $temp = $this->ModuleModel->search('tbl_registered_course', ['course_id'=>$courseId]);
            $studentIds = array_unique(array_column($temp, 'user_id'));
            $students = $this->renderStudentIds($studentIds);
        } else {
            $students = '';
        }

        echo $students;
    }

    /**
     * From module(add/edit) section we can view the question info.
     * Clicking the info icon a modal will open up with question info.
     *
     * @return void
     */
    public function quesInfoForModal()
    {
        $post = $this->input->post();
        $questionId = $post['questionId'];
        $data['quesInfo'] = $this->QuestionModel->info($questionId);
        $data['additionalInfo'] = json_decode($data['quesInfo']['questionName']);
        $quesType = $data['quesInfo']['questionType'];
        $previewBody = '';
        
        if ($quesType==1) {
            $previewBody=$this->load->view('module/modal_preview/general', $data, true);
        } elseif ($quesType==2) {
            $previewBody=$this->load->view('module/modal_preview/true_false', $data, true);
        } elseif ($quesType==3) {
            $previewBody=$this->load->view('module/modal_preview/vocabulary', $data, true);
        } elseif ($quesType==4) {
            $previewBody=$this->load->view('module/modal_preview/multiple_choice', $data, true);
        } elseif ($quesType==5) {
            $previewBody=$this->load->view('module/modal_preview/multiple_response', $data, true);
        } elseif ($quesType==6) {
            $data['numOfRows']    = isset($data['additionalInfo']->numOfRows) ? $data['additionalInfo']->numOfRows : 0;
            $data['numOfCols']    = isset($data['additionalInfo']->numOfCols) ? $data['additionalInfo']->numOfCols : 0;
            $quesAnsItem         = $data['additionalInfo']->skp_quiz_box;
            $items = $this->indexQuesAns($quesAnsItem);

            $data['skipQuizBox'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols'], 1);
            /*print_r($data['skipQuizBox']);
            die;*/
            $previewBody=$this->load->view('module/modal_preview/skip_quiz', $data, true);
        } elseif ($quesType==7) {
            $data['left_side'] = $data['additionalInfo']->left_side;
            $data['right_side'] = $data['additionalInfo']->right_side;
            $data['siz']=count($data['additionalInfo']->left_side); //all side should've same num of elements.
            $data['answer'] = json_decode($data['quesInfo']['answer']);
            $data['colorA'] = [];
            $data['colorB'] = [];
            for ($i=0; $i <$data['siz']; $i++) {
                $randColor = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                $data['colorA'][$i] =$randColor;
                $data['colorB'][(int)$data['answer'][$i]-1] = $randColor;
            }
            $previewBody=$this->load->view('module/modal_preview/matching', $data, true);
        }
        
        echo $previewBody;
    }

    /**
     * before passing items to renderSkpQuizPrevTable() index it first with this func
     *
     * @param  array $items json object array
     * @return array        array with proper indexing
     */
    public function indexQuesAns($items)
    {
        $arr = [];
        foreach ($items as $item) {
            $temp = json_decode($item);
            $cr = explode('_', $temp->cr);
            $col = $cr[0];
            $row = $cr[1];
            $arr[$col][$row] = array(
                'type' => $temp->type,
                'val' => $temp->val
                );
        }
        return $arr;
    }

    /**
     * render the indexed item to table data for preview
     *
     * @param  array   $items   ques ans as indexed item
     * @param  int     $rows    num of row in table
     * @param  int     $cols    num of cols in table
     * @param  integer $showAns optional, set 1 will show the answers too
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 0)
    {

        $row = '';
        for ($i=1; $i<=$rows; $i++) {
            $row .='<tr>';
            for ($j=1; $j<=$cols; $j++) {
                if ($items[$i][$j]['type']=='q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="'.$items[$i][$j]['val'].'" name="skip_counting[]" class="form-control input-box  rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px; background-color: rgb(255, 183, 197);"></td>';
                } else {
                    $ansObj = array(
                        'cr'=>$i.'_'.$j,
                        'val'=> $items[$i][$j]['val'],
                        'type'=> 'a',
                        );
                    $ansObj = json_encode($ansObj);
                    $val = ($showAns==1)?' value="'.$items[$i][$j]['val'].'"' : '';
                    
                    $row .= '<td><input autocomplete="off" type="text" '.$val.' data_q_type="0" data_num_colofrow="'.$i.'_'.$j.'" value="" name="skip_counting[]" class="form-control input-box ans_input  rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px;background-color: rgb(186, 255, 186); ">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    $row .='</td>';
                }
            }
            $row .= '</tr>';
        }
        
        return $row;
    }
    /**
     * Save the drawing image
     *
     * @return string image path
     */
    public function get_draw_image()
    {
        $this->load->library('image_lib');
        $img = $_POST['imageData'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $path = 'assets/uploads/preview_draw_images/';
        $draw_file_name = 'draw'.uniqid();
        $file = $path . $draw_file_name . '.png';
        file_put_contents($file, $data);
        
        $config['image_library'] = 'gd2';
        $config['source_image'] = $file;
        $config['maintain_ratio'] = true;
        $config['width'] = 400;
        $config['height'] = 250;

        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        
        echo base_url().$file;
    }

    public function test()
    {
        $this->load->view('test');
    }
}//end class
