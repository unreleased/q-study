<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        if ($user_id != null && $user_type != null) {
            redirect('dashboard');
        }
    }

    public function index()
    {
        $this->load->model('FaqModel');
        $data['allFaqs'] = $this->FaqModel->allFaqs();


        $data['header'] = $this->load->view('common/header', $data, true);
        $data['menu'] = $this->load->view('menu', $data, true);
        $data['footer_link'] = $this->load->view('common/footer_link', $data, true);
        $data['footer'] = $this->load->view('common/footer', $data, true);
        $this->load->view('index', $data);
    }
}
